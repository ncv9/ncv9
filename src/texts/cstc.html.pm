#lang pollen

◊define-meta[title]{Conlang Syntax Test Cases}
◊define-meta[date]{2021-06-22 – ?}

◊msgbox-outdated{9c}

A work-in-progress translation of the ◊link["https://cofl.github.io/conlang/resources/mirror/conlang-syntax-test-cases.html"]{Conlang Syntax Test Cases}, used to help develop grammar.

◊section/n[1]{The sun shines.}

◊gloss/x{
  ◊glfree{◊nc{eleþ šileħe.}}
  ◊gla{el-eþ šile-ħe.}
  ◊glb{sun-%nom.%sg shine-%3sg}
  ◊glfree{The sun shines.}
}

The word for ◊trans{sun} is in the singular here because the sentence mentions a particular sun (namely, ◊em{the} sun).

◊section/n[2]{The sun is shining.}

◊gloss/x{
  ◊glfree{◊nc{eleþ šileħe.}}
  ◊gla{el-eþ šile-ħe.}
  ◊glb{sun-%nom.%sg shine-%3sg}
  ◊glfree{The sun is shining.}
}

Unfortunately, this sentence is ambiguous with Sentence #1.

◊section/n[3]{The sun shone.}

◊gloss/x{
  ◊glfree{◊nc{eleþ šileħeþ.}}
  ◊gla{el-eþ šile-ħe-þ.}
  ◊glb{sun-%nom.%sg shine-%3sg-%past}
  ◊glfree{The sun shone.}
}

Here, we use the perfective inflection, as well as the past suffix. The initial consonant of the verb is not eclipsable, so eclipsis has no effect.

As of cþBF4, the subject affixes are divided by direct and inverse aspects instead of by imperfective and perfective aspects.

◊section/n[4]{The sun will shine.}

◊gloss/x{
  ◊glfree{◊nc{eleþ os šileat mîrm·aru.}}
  ◊gla{el-eþ os šile-at mîrm·ar-u.}
  ◊glb{sun-%nom.%sg %inf.%dat shine-%inf predict-3%gc}
  ◊glfree{It is predicted that the sun will shine.}
}

Ŋarâþ Crîþ lacks a future tense, but the intended meaning can be expressed using the verb ◊nc{mîrm·arit}.

◊section/n[5]{The sun has been shining.}

◊gloss/x{
  ◊glfree{◊nc{eleþ šileħelta.}}
  ◊gla{el-eþ šile-ħel-ta.}
  ◊glb{sun-%nom.%sg shine-%3sg.%inv-%past}
  ◊glfree{The sun was shining.}
}

Ŋarâþ Crîþ does not have a perfect aspect; we simply use the past imperfective form here.

◊section/n[6]{The sun is shining again.}

◊gloss/x{
  ◊glfree{◊nc{eleþ šileat ħare.}}
  ◊gla{el-eþ šile-at ħar-e.}
  ◊glb{sun-%nom.%sg shine-%inf do_again-%3sg}
  ◊glfree{The sun is shining again.}
}

Note that the meaning of “again” is expressed using an auxiliary verb instead of with an adverb as in English.

◊section/n[7]{The sun will shine tomorrow.}

◊gloss/x{
  ◊glfree{◊nc{enven sodas eleþ šileħe.}}
  ◊gla{env-en sod-as el-eþ šile-ħe.}
  ◊glb{day-%gen.%sg next-%loc.%sg sun-%nom.%sg shine-%3sg}
  ◊glfree{The sun will shine tomorrow.}
}

The expression ◊nc{enven sado}, literally meaning ◊trans{the next thing out of days}, is used to mean ◊trans{tomorrow}.

◊section/n[8]{The sun shines brightly.}

◊gloss/x{
  ◊glfree{◊nc{eleþ lirnaþ šileħe.}}
  ◊gla{el-eþ lirn-aþ šile-ħe.}
  ◊glb{sun-%nom.%sg bright-%ser shine-%3sg}
  ◊glfree{The sun shines brightly.}
}

Instead of using an adverb, we use a serial verb construction.

◊section/n[9]{The bright sun shines.}

◊gloss/x{
  ◊glfree{◊nc{lirna eleþ šileħe.}}
  ◊gla{lirn-a el-eþ šile-ħe.}
  ◊glb{bright-%rel.%nom,%nom.%sg sun-%nom.%sg shine-%3sg}
  ◊glfree{The bright sun shines.}
}

Ŋarâþ Crîþ lacks adjectives; instead, it uses verb participles.

◊section/n[10]{The sun is rising now.}

◊gloss/x{
  ◊glfree{◊nc{ondelt elens drênu.}}
  ◊gla{ond-elt el-ens drên-u.}
  ◊glb{now-%loc.%sg sun-%acc.%sg wake_up-3%gc}
  ◊glfree{The sun is rising now.}
}

A third-person generic subject can be used to make an impersonal subject.

◊section/n[11]{All the people shouted.}

◊gloss/x{
  ◊glfree{◊nc{šinen navo ŋiłoþ.}}
  ◊gla{šin-en nav-o ŋił-o-þ.}
  ◊glb{all-%gen.%sg human-%nom.%pl shout-3%pl-%past}
  ◊glfree{All the people shouted.}
}

◊l1{šino} is used in the genitive singular as a universal determiner with a plural head for a partitive construction.

◊section/n[12]{Some of the people shouted.}

◊gloss/x{
  ◊glfree{◊nc{nemen navo ŋiłoþ.}}
  ◊gla{nem-en nav-o ŋił-o-þ.}
  ◊glb{any-%gen.%sg human-%nom.%pl shout-3%pl-%past}
  ◊glfree{Some of the people shouted.}
}

This is almost identical to ◊link["#11"]{11}.

◊section/n[13]{Many of the people shouted twice.}

◊gloss/x{
  ◊glfree{◊nc{navin &denfo nefarel ŋiłaþ.}}
  ◊gla{nav-in &denf-o nefar-el ŋił-a-þ.}
  ◊glb{human-%gen.%pl majority-%nom.%sg two.%dat.%cel-times shout-3%sg-%past}
  ◊glfree{A majority of the people shouted twice.}
}

◊l1{nefarel} means ◊trans{twice}, derived from ◊l1{nefas} the dative celestial form of ◊trans{2}, plus the suffix ◊l1{-el}.

◊section/n[14]{Happy people often shout.}

◊gloss/x{
  ◊glfree{◊nc{faras tesaroc navaf ŋiłu.}}
  ◊gla{far-as tesar-oc nav-af ŋił-u.}
  ◊glb{often-%loc.%sg happy-%rel.%nom,%nom.%hum person-%nom.%gc shout-3%gc}
  ◊glfree{People who are happy often shout.}
}

This is a general statement; hence the generic number. This sentence refers to people who are currently happy, as opposed to those who are generally happy (that would use ◊l1{tesarit vjełat}).

◊section/n[15]{The kitten jumped up.}

◊gloss/x{
  ◊glfree{◊nc{lârinčedin dtečaþ.}}
  ◊gla{lârinč-edin d\teč-a-þ.}
  ◊glb{kitten-%nom.%sv %pfv\jump-3%sg-%past}
  ◊glfree{The kitten jumped.}
}

The perfective aspect is used (◊trans{jumped up} instead of ◊trans{was jumping up}).

Note that ◊l1{lârinčin} is a collective noun, so we have to use a marked form (the ◊term{singulative}) to refer to one kitten.

◊section/n[16]{The kitten jumped onto the table.}

◊gloss/x{
  ◊glfree{◊nc{lârinčedin tačiþ nelas gesilvełaþ.}}
  ◊gla{lârinč-edin tač-iþ nel-as g\es-il-veła-þ.}
  ◊glb{kitten-%nom.%sv jump-%ser table-%dat.%sg %pfv\%inch-on_top_of-exist.3%sg-%past}
  ◊glfree{The kitten jumped and arrived at the top of the table.}
}

We use a serial verb construction, translating literally to ◊trans{the kitten jumped and became on top of the table}. The sentence below is also correct but does not necessarily imply that the kitten ended up on top of the table.

◊gloss/x{
  ◊glfree{◊nc{lârinčedin nelan ilar dtečaþ.}}
  ◊gla{lârinč-edin nel-an il-ar d\teč-a-þ.}
  ◊glb{kitten-%nom.%sv table-%acc.%sg on_top_of-%all %pfv\jump-3%sg-%past}
  ◊glfree{The kitten jumped toward the top of the table.}
}

◊section/n[17]{My little kitten walked away.}

◊gloss/x{
  ◊glfree{◊nc{nôra lârinčedin’pe ejas noteþ.}}
  ◊gla{nôr-a lârinč-edin=’pe e-jas not-e-þ.}
  ◊glb{small-%rel.%nom,%nom.%cel kitten-%nom.%sv 1%sg-away_from walk-3%sg-%past}
  ◊glfree{My little kitten walked away from me.}
}

We see our first use of the possessive clitics.

◊section/n[18]{It’s raining.}

◊gloss/x{
  ◊glfree{◊nc{mêvan têmu.}}
  ◊gla{mêv-an têm-u.}
  ◊glb{rain-%acc.%sg precipitate-3%gc}
  ◊glfree{It’s raining.}
}

Straightforward and almost the same as in ŊCv7, save for the generic number.

◊section/n[19]{The rain came down.}

◊gloss/x{
  ◊glfree{◊nc{mêvan’ul têmirþ fêsênajas gcehaþ.}}
  ◊gla{mêv-an=’ul têm-irþ fê-sêna-jas g\ceh-a-þ.}
  ◊glb{rain-%acc.%sg=%mcsubj precipitate-%ser 3%gc-above-%abl %pfv\come-3%sg-%past}
  ◊glfree{The rain fell and came from above.}
}

This is a serial verb construction, but ◊l1{mêva} is the object of the converbal clause and the subject of the main clause. In order to avoid confusion, we use the clitic ◊l1{=’ul} to clarify that ◊l1{mêva} is the subject of the main clause. This can also be stated more concisely as one of the following:

◊gloss/x{
  ◊glfree{◊nc{mêva fêsênajas gcehaþ.}}
  ◊gla{mêv-a fê-sêna-jas g\ceh-a-þ.}
  ◊glb{rain-%nom.%sg 3%gc-above-%abl %pfv\come-3%sg-%past}
  ◊glfree{The rain came from above.}
}

◊gloss/x{
  ◊glfree{◊nc{mêvan fêsênajas dtêmaþ.}}
  ◊gla{mêv-an fê-sêna-jas d\têm-a-þ.}
  ◊glb{rain-%acc.%sg 3%gc-above-%abl %pfv\precipitate-3%sg-%past}
  ◊glfree{It rained from above.}
}

◊section/n[20]{The kitten is playing in the rain.}

◊gloss/x{
  ◊glfree{◊nc{lârinčedin mêvas þonal šitra.}}
  ◊gla{lârinč-edin mêv-as þon-al šitr-a.}
  ◊glb{kitten-%nom.%sv rain-%dat.%sg among-%adv play-3%sg}
  ◊glfree{The kitten is playing in the midst of the rain.}
}

Note that it would be incorrect to use ◊l1{es} instead of ◊l1{þon} to translate ◊trans{in} in this sentence. In addition, ◊l1{þon} has an adnominal bias.

When translating this sentence, I was curious about how Arka translated ◊trans{in the rain}. Asaka Lulu (known as ◊arka{davfapel unilent} in Arka) helpfully answered that ◊link["http://mindsc.ape.jp/klel/yui.cgi?rein=pot+esk&ziko=sev&axn=axnyui&ism=ismyui"]{◊i{◊arka{pot esk}} is attested in the ◊cite{Ardia}.}

◊section/n[21]{The rain has stopped.}

◊gloss/x{
  ◊glfree{◊nc{mêvan gcart·êmal.}}
  ◊gla{mêv-an g\car-t·êm-al.}
  ◊glb{rain-%acc.%sg %pfv\%cess-precipitate-3%sg.%inv}
  ◊glfree{It has stopped raining.}
}

Among other things, the present perfective can be used to refer to an immediate past event.

◊section/n[22]{Soon the rain will stop.}

◊gloss/x{
  ◊glfree{◊nc{falþon’po fêmîr mêvan gcart·êmal.}}
  ◊gla{falþ-on=’po fê-mîr mêv-an g\car-t·êm-al.}
  ◊glb{short_time-%acc.%sg=%sep 3%gc-after rain-%acc.%sg %pfv\%cess-precipitate-3%sg.%inv}
  ◊glfree{Soon the rain will stop.}
}

◊l1{falþon’po fêmîr} can be translated to ◊trans{after now by a short span of time}: the third-person generic prefix on the relational ◊l1{mîr} conventionally refers to the current time, and the clitic ◊l1{=’po} on an accusative noun phrase describes the amount of separation between the two events.

◊section/n[23]{I hope the rain stops soon.}

◊gloss/x{
  ◊glfree{◊nc{mêvan falþon’po fêmîr os c·artêmit afče.}}
  ◊gla{mêv-an falþ-on=’po fê-mîr os c·ar-têm-it afč-e.}
  ◊glb{rain-%acc.%sg short_time-%acc.%sg=%sep 3%gc-after %inf.%dat (%inf.%dat)\%cess-precipitate-%inf hope-1%sg}
  ◊glfree{I hope the rain stops soon.}
}

Another nominalized verb phrase occurs here. The word order is changed here because the rain can be seen as the topic.

◊section/n[24]{Once wild animals lived here.}

◊gloss/x{
  ◊glfree{◊nc{tectontelt derendaf valosta.}}
  ◊gla{tectont-elt der-endaf val-os-ta.}
  ◊glb{past-%loc.%sg animal-%nom.%gc reside-3%gc.%inv-%past}
  ◊glfree{Once wild animals lived here.}
}

Yes, you could say ◊l1{fêtectoen antrilcos} instead of ◊l1{tectontelt}, but that’s a ◊em{bit} too wordy, isn’t it?

◊section/n[25]{Slowly she looked around.}

◊gloss/x{
  ◊glfree{◊nc{cþires vogennaneþ.}}
  ◊gla{cþir-es vo<genna>n-e-þ.}
  ◊glb{environs-%dat.%sg look_at<%ddt.%rec>-3%sg-%past}
  ◊glfree{She looked around slowly.}
}

We see our first use of a calculus affix.

◊section/n[26]{Go away!}

◊gloss/x{
  ◊glfree{◊nc{le esecljes þal!}}
  ◊gla{le es-eclj-es þal!}
  ◊glb{%imp %inch-far-2%sg %assert}
  ◊glfree{Go far away (from me)!}
}

If we wanted to, we could use the ◊l0{-pe} object affix on the verb, but the context makes it clear anyway.

◊section/n[27]{Let's go!}

◊gloss/x{
  ◊glfree{◊nc{le nelsên!}}
  ◊gla{le nels-ên!}
  ◊glb{%imp go-1%pl.%incl}
  ◊glfree{Let's go!}
}

The destination is unspecified, so it can be omitted.

◊section/n[28]{You should go.}

◊gloss/x{
  ◊glfree{◊nc{os nelsit triłe.}}
  ◊gla{os nels-it trił-e.}
  ◊glb{%inf.%dat go-%inf recommend-1%sg}
  ◊glfree{I recommend going.}
}

If the direct object is not clear from context, the following is more appropriate:

◊gloss/x{
  ◊glfree{◊nc{os nelsit triłeve.}}
  ◊gla{os nels-it trił-e-ve.}
  ◊glb{%inf.%dat go-%inf recommend-1%sg-2%sg}
  ◊glfree{I recommend to you to go.}
}

◊section/n[29]{I will be happy to go.}

◊gloss/x{
  ◊glfree{◊nc{nelsit dotesarape viþca.}}
  ◊gla{nels-it do-tesar-a-pe viþca.}
  ◊glb{go-%inf %caus-happy-3%sg-1%sg %cond}
  ◊glfree{Going would make me happy.}
}

In this case, ◊l1{nelsit} is the nominative argument, not a target of an auxiliary verb, because ◊l1{dotesarit} is transitive, not auxiliary.

◊section/n[30]{He will arrive soon.}

◊gloss/x{
  ◊glfree{◊nc{falþon’po fêmîr cehal.}}
  ◊gla{falþ-on=’po fê-mîr ceh-al.}
  ◊glb{short_time-%acc.%sg=%dist 3%gc-after arrive-3%sg.%inv}
  ◊glfree{He arrives soon.}
}

The ‘present’ tense is actually nonpast – it can include the future.

◊section/n[31]{The baby’s ball has rolled away.}

◊gloss/x{
  ◊glfree{◊nc{genenčens rislałiðin erjas rislal.}}
  ◊gla{g\enenč-ens rislał-iðin er-jas risl-al.}
  ◊glb{(%gen.%sv)\newborn-%gen.%sv spherical_object-%nom.%sv 3%sg.%cel-away_from roll-3%sg.%inv}
  ◊glfree{The baby’s ball has rolled away.}
}

◊l1{enenčin} is a celestial noun. That it has a human referent in this case is irrelevant.

◊section/n[32]{The two boys are working together.}

◊gloss/x{
  ◊glfree{◊nc{fernac nefac ciþnanriþ.}}
  ◊gla{fern-ac nef-ac ciþn-an-riþ.}
  ◊glb{child-%nom.%du two.%hum collaborate-3%du-%recip}
  ◊glfree{The two children are working together.}
}

Ŋarâþ Crîþ lacks a single word for ◊trans{boy} or ◊trans{girl}. Indicating one sex is possible, but it would sound unusual to native speakers in this case.

◊section/n[33]{The mist will probably clear away.}

◊gloss/x{
  ◊glfree{◊nc{relten feŋit šonat lerjal.}}
  ◊gla{relt-en feŋ-it šon-at lerj-al.}
  ◊glb{mist-%nom.%co fade-%inf do_completely-%inf probable-3%sg.%inv}
  ◊glfree{The mist will probably clear away.}
}

An example of an auxiliary chain with three verbs.

◊section/n[34]{Lovely flowers are growing everywhere.}

◊gloss/x{
  ◊glfree{◊nc{šjonos vescþil tovra nalfo.}}
  ◊gla{šjon-os vescþ-il tovr-a nalf-o.}
  ◊glb{all-%loc.%sg attractive-%rel.%nom,%nom.%cel flower-%nom.%co grow-3%pl}
  ◊glfree{Lovely flowers are growing everywhere.}
}

Here, ◊l1{šino} is being used to mean ◊trans{everywhere}.

◊l1{tovra} is also a collective noun. For that reason, the unmarked form is used for many flowers.

◊section/n[35]{We should eat more slowly.}

◊gloss/x{
  ◊glfree{◊nc{on mêgennančat triłepjô.}}
  ◊gla{on mê<genna>nč-at trił-e-pjô.}
  ◊glb{%inf.%acc eat<%ddt.%rec>-%inf recommend-1%sg-1%du.%incl}
  ◊glfree{I suggest to us to eat more slowly.}
}

This is also valid:

◊gloss/x{
  ◊glfree{◊nc{le mêgennančân.}}
  ◊gla{le mê<genna>nč-ân.}
  ◊glb{%imp eat<%ddt.%rec>-1%pl.%incl}
  ◊glfree{Let’s eat more slowly.}
}

◊section/n[36]{You have come too soon.}

◊gloss/x{
  ◊glfree{◊nc{nderðarespe dôm niðes cehares.}}
  ◊gla{n\derð-ares-pe dôm nið-es g\ceh-ares.}
  ◊glb{%pfv\disturb-2%sg.%inv-1%sg to_the_extent_that early_time-%loc.%sg %pfv\arrive-2%sg.%inv}
  ◊glfree{You have come so early that you have bothered me.}
}

The most suitable translation for this sentence depends on the context – namely, the reason why the speaker didn’t want the listener to come so early.

◊section/n[37]{You must write more neatly.}

◊gloss/x{
  ◊glfree{◊nc{a cþerit elnat łanes.}}
  ◊gla{a cþer-it eln-at łan-es.}
  ◊glb{%inf.%loc write-%inf neat-%inf must-2%sg}
  ◊glfree{When you write, you must do it neatly.}
}

Another way for Ŋarâþ Crîþ v9 to avoid having a lexical adverb category, though by now, I'm not sure where the line is.

◊section/n[38]{Directly opposite stands a wonderful palace.}

◊gloss/x{
  ◊glfree{◊nc{pâ asfêtfelîf refla cjarfon veła.}}
  ◊gla{pâ as-fê-tfel-îf refl-a cjarf-on veła.}
  ◊glb{directly %adv-3%gc-across-1%pl.%incl surprise-%rel.%nom,%nom.%sg palace-%nom.%sg exist.3%sg}
  ◊glfree{Directly opposite from us exists a wonderful palace.}
}

Here, we encounter our first trivalent relational. We also see the use of the relational modifier ◊l1{pâ}.

◊section/n[39]{Henry’s dog is lost.}

◊gloss/x{
  ◊glfree{◊nc{#aveþel cermjôr nele.}}
  ◊gla{#aveþ-el cermjôr-∅ nel-e.}
  ◊glb{%name-%gen.%sg dog-%nom.%sg be_lost-3%sg}
  ◊glfree{#aveþon’s dog is lost.}
}

Yes, this means that we’re replacing names.

◊section/n[40]{My cat is black.}

◊gloss/x{
  ◊glfree{◊nc{lâres’pe crîna čilveła.}}
  ◊gla{lâr-es=’pe crîn-a čil-veła.}
  ◊glb{cat-%dat.%sg=%poss.1 black-%nom.%sg on-exist.3%sg}
  ◊glfree{My cat is black. (On my cat there is black.)}
}

Only a nominal form for the color black exists; to use it predicatively, we need the help of the relational ◊l1{čil}.

◊section/n[41]{The little girl’s doll is broken.}

◊gloss/x{
  ◊glfree{◊nc{nôrjor fernen criþnavan claču.}}
  ◊gla{nôr-jor fern-en criþnav-an clač-u.}
  ◊glb{small-%rel.%nom,%gen.%hum child-%gen.%sg doll-%acc.%sg break-3%gc}
  ◊glfree{The little child’s doll is broken.}
}

Here, ◊trans{broken} means ◊trans{broken into pieces} instead of merely ◊trans{nonfunctioning}.

◊section/n[42]{I usually sleep soundly.}

◊gloss/x{
  ◊glfree{◊nc{a rilþit faras seta.}}
  ◊gla{a rilþ-it far-as set-a.}
  ◊glb{%inf.%loc rest-%inf often-%loc.%sg quiet-1%sg}
  ◊glfree{When I sleep, I am often quiet.}
}

Compare to Sentence #37.

◊section/n[43]{The children ran after Jack.}

◊gloss/x{
  ◊glfree{◊nc{ferno mitriþ #saþon nelroþ.}}
  ◊gla{fern-o mitr-iþ #saþ-on nelr-o-þ.}
  ◊glb{child-%nom.%pl run-%ser %name-%acc.%sg chase-3%pl-%past}
  ◊glfree{The children ran and chased #saþo.}
}

Another serial verb construction.

◊section/n[44]{I can play after school.}

◊gloss/x{
  ◊glfree{◊nc{os sarałen ninłit mîr šitrit penta.}}
  ◊gla{os sarał-en ninł-it mîr šitr-it pent-a.}
  ◊glb{%inf.%dat school-%acc.%sg attend-%inf after play-%inf able_to-1%sg}
  ◊glfree{I can play after attending school.}
}

The action of attending school takes a span of time instead of happening at a single point. ◊l1{mîr}, then, implies that the event described by the object must be finished before the one described by the subject starts.

◊section/n[45]{We went to the village for a visit.}

◊gloss/x{
  ◊glfree{◊nc{tfaþos gcarðaþta.}}
  ◊gla{tfaþ-os g\carð-acþ-ta.}
  ◊glb{village-%dat.%sg %pfv\tour-1%pl.%excl-%past}
  ◊glfree{We visited the village.}
}

Translating this sentence was challenging because the English word ◊i{visit} has several meanings.

Note that the cluster ◊l0{-cþt-} simplifies to ◊l1{-þt-}.

◊section/n[46]{We arrived at the river.}

◊gloss/x{
  ◊glfree{◊nc{elten gcehacþis.}}
  ◊gla{elt-en g\ceh-acþis.}
  ◊glb{river-%acc.%sg %pfv\arrive-1%pl.%excl.%inv}
  ◊glfree{We arrived at the river.}
}

Nothing too weird here.

◊section/n[47]{I have been waiting for you.}

◊gloss/x{
  ◊glfree{◊nc{mârelveþ.}}
  ◊gla{mâr-el-ve-þ.}
  ◊glb{wait-1%sg.%inv-2%sg-%past}
  ◊glfree{I was waiting for you.}
}

Squeeze it all into one word!

◊section/n[48]{The campers sat around the fire.}

◊gloss/x{
  ◊glfree{◊nc{cfirnicadir senals cþaral searnoþ.}}
  ◊gla{cfirnica-d-ir sen-als cþar-al searn-o-þ.}
  ◊glb{camp-%agent.%anim-%nom.%pl fire-%dat.%sg around-%adv sit-3%pl-%past}
  ◊glfree{The campers sat around the fire.}
}

We’re starting to see verb-to-noun derivations now.

◊section/n[49]{A little girl with a kitten sat near me.}

◊gloss/x{
  ◊glfree{◊nc{nôrac lêdenčilcen ferna pecþîšaþ searnaþ.}}
  ◊gla{nôr-ac lêdenč-ilcen fern-a pe-cþîš-aþ searn-a-þ.}
  ◊glb{small-%rel.%nom,%nom.%hum kitten-%inst.%sv child-%nom.%sg 1%sg-near-%ser sit-3%sg-%past}
  ◊glfree{A small child with a kitten went near me and sat down.}
}

Note that ◊l1{cþîšat} ◊trans{near} is a verb and not a relational. Incidentally, it is transitive, even though its antonym ◊l1{ecljat} is semitransitive.

◊section/n[50]{The child waited at the door for her father.}

◊gloss/x{
  ◊glfree{◊nc{ferna cþerilt tfosos mâraþ.}}
  ◊gla{fern-a cþer-ilt tfos-os mâr-a-þ.}
  ◊glb{child-%nom.%sg door-%loc.%sg cross_parent-%dat.%sg wait-3%sg-%past}
  ◊glfree{The child waited at the door for her father.}
}

From the English sentence, we know that the child is female, so ◊l1{tfoso} is the appropriate term for her father.

◊section/n[51]{Yesterday the oldest girl in the village lost her kitten.}

◊gloss/x{
  ◊glfree{◊nc{enven dores tfaþen îþ *sedapes čêrcas lârinčedin gcarvełaþ.}}
  ◊gla{env-en dor-es tfaþ-en îþ *sedap-es čêrc-as lârinč-edin g\car-veła-þ.}
  ◊glb{day-%gen.%sg previous-%loc.%sg village-%gen.%sg %sup female-%rel.%nom,%loc.%hum youth-%loc.%sg kitten-%nom.%sv %pfv\%cess-exist.3%sg-%past}
  ◊glfree{Yesterday the oldest girl in the village lost her kitten.}
}

Note that we have to use ◊l1{*sedapat} here: the oldest ◊em{girl} in the village is not necessarily the same person as the oldest ◊em{young person} in the village! Nevertheless, a native speaker would probably describe that person as, say, the second-oldest child in the village.

On a sidenote, this sentence marks the first ◊i{nef} in the CSTC translations.

◊section/n[52]{Were you born in this village?}

◊gloss/x{
  ◊glfree{◊nc{ša lê tfoþos gðenuveþ?}}
  ◊gla{ša lê tfoþ-os gðen-u-ve-þ?}
  ◊glb{%int %this.%cel village-%loc.%sg give_birth-3%gc-2%sg-%past}
  ◊glfree{Were you born in this village?}
}

Usually, ◊l1{lê} triggers an eclipsis, but eclipsis does not apply to words beginning in a stop–fricative cluster.

◊section/n[53]{Can your brother dance well?}

◊gloss/x{
  ◊glfree{◊nc{ša armo’ve torait pente?}}
  ◊gla{ša arm-o=’ve tora-it pent-e?}
  ◊glb{%int parallel_sibling-%nom.%sg=%poss.2 dance-%inf able_to-3%sg}
  ◊glfree{Can your sibling of the same gender dance well?}
}

Depending on whom you’re speaking to, ◊l1{ša melsas’ve torait pente?} might be more appropriate.

◊section/n[54]{Did the man leave?}

◊gloss/x{
  ◊glfree{◊nc{ša tê nava gendraþ?}}
  ◊gla{ša tê nav-a g\endr-a-þ?}
  ◊glb{%int %that.%hum person-%nom.%sg %pfv\leave-3%sg-%past}
  ◊glfree{Did that person leave?}
}

Nothing we haven’t yet seen.

◊section/n[55]{Is your sister coming for you?}

◊gloss/x{
  ◊glfree{◊nc{ša armo’ve oroc ceha?}}
  ◊gla{ša arm-o=’ve o-roc ceh-a?}
  ◊glb{%int parallel_sibling-%nom.%sg=%poss.2 2%sg-on_behalf_of arrive-3%sg}
  ◊glfree{Is your sibling of the same gender coming for you?}
}

Again, you might want to say ◊l1{melsas’ve} instead, depending on the listener.

◊section/n[56]{Can you come tomorrow?}

◊gloss/x{
  ◊glfree{◊nc{ša enven sodos cehit geves?}}
  ◊gla{ša env-en sod-os ceh-it gev-es?}
  ◊glb{%int day-%gen.%sg next-%loc.%sg arrive-%inf %request-2%sg}
  ◊glfree{Are you willing to come tomorrow?}
}

The auxiliary verb ◊l1{gevat} is used to make requests.

◊section/n[57]{Have the neighbors gone away for the winter?}

◊gloss/x{
  ◊glfree{◊nc{ša cþire arcaþ nîs elgres arico?}}
  ◊gla{ša cþir-e arc-aþ nîs elgres aric-o?}
  ◊glb{%int neighbor-%nom.%pl winter-%dat.%sg during here.%dat.%sg be_away.3%pl}
  ◊glfree{Are the neighbors away from here during the winter?}
}

The auxiliary verb ◊l1{gevat} is used to make requests.

◊section/n[58]{Does the robin sing in the rain?}

◊gloss/x{
  ◊glfree{◊nc{ša ceaþt·anof mêvas þonal sifłu?}}
  ◊gla{ša ceaþt·an-of mêv-as þon-al sifł-u?}
  ◊glb{%int robin-%nom.%gc rain-%dat.%sg among-%adv vocalize-3%gc}
  ◊glfree{Does the robin sing in the rain?}
}

◊l1{sifłit} is used for the vocalizations of a bird.

◊section/n[59]{Are you going with us to the concert?}

◊gloss/x{
  ◊glfree{◊nc{ša pelcar arnent·rarerþas nelsares?}}
  ◊gla{ša pelcar arnent·rar-erþas nels-ares?}
  ◊glb{%int %pr.1%pl.%inst concert-%dat.%sg go_to-2%sg.%inv}
  ◊glfree{Are you going with us to the concert?}
}

◊l1{nelsit} can be used to mean ◊trans{attend an event} as well as the literal ◊trans{move toward a destination}.

◊section/n[60]{Have you ever travelled in the jungle?}

◊gloss/x{
  ◊glfree{◊nc{ša moþcrîlþas cfileca os nelsit ŋačes?}}
  ◊gla{ša moþcrî-lþas cfil-eca os nels-it ŋač-es?}
  ◊glb{%int dense_forest-%loc.%sg journey-%inst.%sg %inf.%dat go_to-%inf cross-2%sg}
  ◊glfree{Have you ever traveled in the jungle?}
}

The experiential aspect is expressed with the verb ◊l1{ŋačat} (the event is its dative argument). ◊trans{To travel} is expressed with ◊l1{cfileca nelsit}, or literally ◊trans{go with a travel}.

◊section/n[61]{We sailed down the river for several miles.}

◊gloss/x{
  ◊glfree{◊nc{elteþ nîs prômeca nafsan penmino gcelpraþta.}}
  ◊gla{elt-eþ nîs prôm-eca nafs-an pen-mino g\celpr-acþ-ta.}
  ◊glb{river-%dat.%sg through boat-%inst.%sg nafsa-%acc.%sg what-16⁰ %pfv\move_by_distance-1%pl-%past}
  ◊glfree{We sailed down the river for several miles.}
}

◊l1{celprit} is used to express movement by a given distance. ◊l1{penmino} is an indefinite exact numeral (since its significand is ◊l0{pen-}) with an exponent of ◊${16^0}.

◊section/n[62]{Everybody knows about hunting.}

◊gloss/x{
  ◊glfree{◊nc{šinof os d·endit idesa neman racru.}}
  ◊gla{šin-of os d·end-it i-desa nem-an racr-u.}
  ◊glb{all-%nom.%gc %inf.%dat hunt-%inf %adn-below some-%acc.%sg know-3%gc}
  ◊glfree{Everybody knows (something) about hunting.}
}

In addition to its use to mean ◊trans{below}, ◊l1{desa} is used to mean ◊trans{related to} or ◊trans{regarding}.

◊section/n[63]{On a Sunny morning after the solstice we started for the mountains.}

◊gloss/x{
  ◊glfree{◊nc{malenvas imîr lirna cintos cercêli gesnelsaþta.}}
  ◊gla{malenv-as i-mîr lirn-a cint-os cercêl-ir g\es-nels-acþ-ta.}
  ◊glb{solstice-%dat.%sg %adn-after bright-%rel.%nom,%nom.%sg morning-%loc.%sg mountain-%dat.%co %pfv\%inch-go_to-1%pl-%past}
  ◊glfree{On a sunny morning after the solstice we started for the mountains.}
}

◊l1{malenva} can refer to an equinox or solstice.

◊section/n[64]{Tom laughed at the monkey’s tricks.}

◊gloss/x{
  ◊glfree{◊nc{#môran siħen ansin ndošinroþ.}}
  ◊gla{#môr-an siħ-en ans-in n\do-šinr-o-þ.}
  ◊glb{%name-%acc.%sg monkey-%gen.%sg stunt-%nom.%pl %pfv\%caus-laugh-3%pl-%past}
  ◊glfree{The monkey’s tricks made #môra laugh.}
}

◊trans{Trick} in this sentence could have multiple interpretations. I chosse to interpret it as a ◊trans{stunt}.

◊section/n[65]{An old man with a walking stick stood beside the fence.}

◊gloss/x{
  ◊glfree{◊nc{cjaŋeprijanaros šidrêr dengas fan jorniłalta.}}
  ◊gla{cja~ŋeprijanar~os šidr-êr deng-as fan jornił-al-ta.}
  ◊glb{%inst~walking_stick~%inst.%sg elder-%nom.%sg fence-%dat.%sg next_to stand-3%sg.%inv-%past}
  ◊glfree{An old person with a walking stick stood beside the fence.}
}

◊l1{ŋeprinoros} comes from ◊l1{ŋeprit} ◊trans{prop up, support} and ◊l1{inoros} ◊trans{stick, rod}, with the double vowel merged into one. (There is no double vowel to elide in the L stem, ◊l0{ŋeprijanar-}.)

◊section/n[66]{The squirrel’s nest was hidden by drooping boughs.}

◊gloss/x{
  ◊glfree{◊nc{malens onan disron erinoros daħrorista.}}
  ◊gla{mal-ens on-an disr-on erinor-os daħr-oris-ta.}
  ◊glb{squirrel-%gen.%sv nest-%acc.%sg droop-%rel.%nom,%nom.%ter branch-%nom.%co conceal-3%pl.%inv-%past}
  ◊glfree{The squirrel’s nest was hidden by drooping boughs.}
}

Ŋarâþ Crîþ has no passive construction because the topic and focus can be implied by varying the word order.

◊section/n[67]{The little seeds waited patiently under the snow for the warm spring sun.}

◊gloss/x{
  ◊glfree{◊nc{nôra seren tfeldarnirþ ginas desa doaŋaras lerþil eleþes mâroþ.}}
  ◊gla{nôr-a ser-en tfeldarn-irþ gin-as desa do-aŋar-as lerþ-il eleþ-es mâr-o-þ.}
  ◊glb{small-%rel.%nom,%nom.%cel seed-%nom.%co endure-%ser snow-%dat.%sg under %caus-warm-%rel.%nom,%dat.%cel spring-%gen.%sg sun-%dat.%sg wait-3%pl-%past}
  ◊glfree{The little seeds waited patiently under the snow for the warm spring sun.}
}

Note that ◊l1{aŋaras lerþil eleþes} would give an unintended meaning here – the sun itself is a lot more than warm! Rather, the sun is causing other things to become warm.

◊section/n[68]{Many little girls with wreaths of flowers on their heads danced around the bonfire.}

◊gloss/x{
  ◊glfree{◊nc{tovrasarter cidroc nôrac mel ferno nasenals cþarnîs dtoraoþ.}}
  ◊gla{tovrasart-er cidr-oc nôr-ac mel fern-o nasen-als cþarnîs d\tora-o-þ.}
  ◊glb{wreath_with_flowers-%acc.%pl wear-%rel.%nom,%nom.%cel small-%rel.%nom,%nom.%cel many child bonfire-%dat.%sg around %pfv\dance-3%pl-%past}
  ◊glfree{Many little children with wreaths of flowers on their heads danced around the bonfire.}
}

◊l1{cþar} is the wrong relational to use here, as it indicates the state of surrounding something, not moving around it. ◊l1{cþarar} does not have the intended meaning, either; it means ‘moving toward something as to surround it’. ◊l1{cþarnîs} is the correct relational to use in this case.

◊section/n[69]{The cover of the basket fell to the floor.}

◊gloss/x{
  ◊glfree{◊nc{nirþeven nagrol cajoþ gcjašaþ.}}
  ◊gla{nirþev-en nagr-ol caj-oþ g\cjaš-a-þ.}
  ◊glb{basket-%gen.%sg cover-%nom.%sg floor-%dat.%di %pfv\fall-3%sg-%past}
  ◊glfree{The cover of the basket fell to the floor.}
}

Haha funny number! ◊l1{cajos} is a mass noun, but this isn’t a concern for this sentence.

◊section/n[70]{The first boy in the line stopped at the entrance.}

◊gloss/x{
  ◊glfree{◊nc{tlontos elacþ čêrca areleþułeþ gcarnoteþ.}}
  ◊gla{tlont-os elacþ čêrc-a areleþuł-eþ g\car-not-e-þ.}
  ◊glb{queue-%loc.%sg first.%nom.%hum youth-%nom.%sg entrance-%loc.%sg %pfv\%term-move-3%sg-%past}
  ◊glfree{The first teenager in the line stopped at the entrance.}
}

Replaced ‘boy’ with something Ŋarâþ Crîþ has a word for. ◊l1{areleþiłes} is the location derivation of the verb ◊l1{areleþit}, which is what you get when you place the relational ◊l1{es} along with the allative modifier ◊l1{ar} on a scaffolding verb. Looking at it this way, it means ◊trans{place at which one starts being inside something}.

◊section/n[71]{On the top of the hill in a little hut lived a wise old woman.}

◊gloss/x{
  ◊glfree{◊nc{dercel sênalelt cerþilten darna šidrêr valelta.}}
  ◊gla{derc-el sêna-l-elt cerþ-ilten darn-a šidr-êr val-el-ta.}
  ◊glb{hill-%gen.%sg above-%nlz-%loc.%di hut-%loc.%sv wise-%rel.%nom,%nom.%sg elder-%nom.%sg reside-3%sg.%inv-%past}
  ◊glfree{On the top of the hill in a little hut lived a wise old person.}
}

Note that we have to use the nominalized form of ◊l1{sêna} instead of the relational directly (which would mean ◊tr{above}, not ◊tr{on the top of}).

◊section/n[72]{During our residence in the country we often walked in the pastures.}

◊gloss/x{
  ◊glfree{◊nc{trecai os v·alit’pe nîs faras miłersoþ notecþista.}}
  ◊gla{trec-ai os v·al-it=’pe nîs far-as miłers-oþ not-ecþis-ta.}
  ◊glb{field-%loc.%pl %inf.%dat reside-%inf=%poss.1 through often-%loc.%di pasture-%loc.%pl walk-1%pl.%excl.%inv-%past}
  ◊glfree{During our residence in the country we often walked in the pastures.}
}

The noun ◊l1{treca} ◊trans{plain} is often used in the plural to refer to the countryside.

◊section/n[73]{When will your guests from the city arrive?}

◊gloss/x{
  ◊glfree{◊nc{ša artfaþos ijas arceoþ pełevas gcehoris?}}
  ◊gla{ša artfaþ-os i-jas arce-oþ pełev-as g\ceh-oris?}
  ◊glb{%int city-%dat.%sg %adn-from guest-%nom.%pl when-%loc.%di %pfv\arrive-3%pl.%inv}
  ◊glfree{When will your guests from the city arrive?}
}

Here, the noun phrase ◊l1{artfaþos ijas arceoþ} ◊trans{guests from the city} acts as the topic and thus occurs before the interrogative word ◊l1{pełevas}.

◊section/n[74]{Near the mouth of the river, its course turns sharply towards the East.}

◊gloss/x{
  ◊glfree{◊nc{eltin šimens cþîšari fjonos alaþas âŋa cþîfsocon siveła.}}
  ◊gla{elt-in šim-ens cþîš-ari fjon-os al-aþas âŋa cþîfsoc-on si-veła.}
  ◊glb{river-%gen.%sg end-%acc.%sg near-%rel.%nom,%loc.%cel place-%loc.%sg east-%dat.%sg bending acute_angle-%nom.%sg %appl.%loc-exist.3%sg}
  ◊glfree{Near the mouth of the river, its course turns sharply towards the East.}
}

We need the applicative prefix ◊l0{si-} here to promote the locative adjunct to a dative argument because adverbial relationals can only describe either the action itself or the result on one of the nominative, accusative, or dative arguments (i.e. on the shifted subject).

Note that the sentence below, which tries to avoid the applicative prefix, has a slightly different meaning:

◊gloss/x{
  ◊glfree{◊nc{šimens cþîšari fjonos alaþas âŋaen ilteþ cþîfsocon veła.}}
  ◊gla{šim-ens cþîš-ari fjon-os al-aþas âŋa-en ilt-eþ cþîfsoc-on veła.}
  ◊glb{end-%acc.%sg near-%rel.%nom,%loc.%cel place-%loc.%sg east-%dat.%sg bending-%adn river-%loc.%sg acute_angle-%nom.%sg exist.3%sg}
  ◊glfree{There is a sharp turn near its mouth in the river that turns toward the east (but the sharp turn is not necessarily identical to the turn toward the east).}
}

◊section/n[75]{Between the two lofty mountains lay a fertile valley.}

◊gloss/x{
  ◊glfree{◊nc{ragasos c·ercêli nerła nefos refsesa cþirel vełaþ.}}
  ◊gla{rag-asos c·ercêl-i nerła nefos refs-esa cþir-el veła-þ.}
  ◊glb{tall-%rel.%nom,%dat.%ter mountain-%dat.%coll between two.%dat.%ter fertile-%rel.%nom,%nom.%sg valley-%nom.%sv exist-3%sg-%past}
  ◊glfree{Between the two lofty mountains lay a fertile valley.}
}

We use the bare form of ◊l1{eþit} with the relational phrase with ◊l1{nerła} modifying the verb. This construction is preferred over using ◊l1{nerła} predicatively in this case because the relational phrase ◊l1{ragasos c·ercêli nerła} forms a sort of topic. That is, the latter option would delay the information of ‘betweenness’ to the end.

◊section/n[76]{Among the wheat grew tall red poppies.}

◊gloss/x{
  ◊glfree{◊nc{ŋjâles þon ragime cenþa *naša nalfoþ.}}
  ◊gla{ŋjâl-es þon rag-ime cenþ-a *naš-a nalf-o-þ.}
  ◊glb{what-%dat.%co among tall-and red-%rel.%nom,%nom.%cel (flower)-%nom.%co grow-3%pl-%past}
  ◊glfree{Among the wheat grew tall red *poppies.}
}

◊l1{*naša} refers to a species of red flower in Njôro.

◊section/n[77]{The strong roots of the oak trees were torn from the ground.}

◊gloss/x{
  ◊glfree{◊nc{vesran mjerlen ranan cajoþ gcjâþuþ.}}
  ◊gla{vesr-an mjerl-en ran-an caj-oþ g\cjâþ-u-þ.}
  ◊glb{strong-%rel.%nom,%acc.%cel oak-%gen.%co root-%acc.%co ground-%dat.%di %pfv\tear-3%gc-%past}
  ◊glfree{The strong roots of the oak trees were torn from the ground.}
}

Mostly straightforward, but notice that ◊l1{vesran} is separated from its head ◊l1{ranan}.

◊section/n[78]{The sun looked down through the branches upon the children at play.}

◊gloss/x{
  ◊glfree{◊nc{elin łirla erinoroþ nîs šitrjor ferner cehalta.}}
  ◊gla{el-in łirl-a erinor-oþ nîs šitr-jor fern-er ceh-al-ta.}
  ◊glb{sun-%gen.%sg light-%nom.%co branch-%dat.%co through play-%rel.%nom,%acc.%hum child-%acc.%pl reach-3%sg.%inv-%past}
  ◊glfree{The light of the sun reached through the branches to the children playing.}
}

It would be weird to say that the sun was looking in Ŋarâþ Crîþ, so the sentence is rephrased to be more idiomatic.

◊section/n[79]{The west wind blew across my face like a friendly caress.}

◊gloss/x{
  ◊glfree{◊nc{seritas ijas avona ceven šinon setest galit evralta.}}
  ◊gla{serit-as i-jas avon-a cev-en šin-on set-est gal-it evr-al-ta.}
  ◊glb{west-%dat.%di %adn-from wind-%nom.di face-%gen.%sg all-%acc.%sg calm-%rel.%nom,%sembl.%hum friend-%sembl.%sg blow-3%sg-%past}
  ◊glfree{The wind from the west blew on the entirety my face like a gentle friend.}
}

The ◊trans{like a friendly caress} part was adapted to fit with Ŋarâþ Crîþ, but nothing else spectacular.

◊section/n[80]{The spool of thread rolled across the floor.}

◊gloss/x{
  ◊glfree{◊nc{fomenoros cajoþ nîs risleþ.}}
  ◊gla{fomenor-os caj-oþ nîs risl-e-þ.}
  ◊glb{spool-%nom.%sg ground-%dat.%di through roll-3%sg-%past}
  ◊glfree{The spool of thread rolled across the floor.}
}

This sentence shows a tendency toward satellite framing.

◊section/n[81]{A box of growing plants stood in the Window.}

◊gloss/x{
  ◊glfree{◊nc{nalfiłi vanen evin colmaþ vełalta.}}
  ◊gla{nalf-iłi van-en ev-in colm-aþ vełal-ta.}
  ◊glb{grow-%rel.%nom,%gen.%cel plant-%gen.%co box-%nom.%sg window-%loc.%sg exist.3%sg.%inv-%past}
  ◊glfree{A box of growing plants stood in the Window.}
}

We use ◊l1{eþit} instead of ◊l1{jorniłit} because the subject is not animate.

