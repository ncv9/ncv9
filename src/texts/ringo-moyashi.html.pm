#lang pollen

◊define-meta[title]{Apple sentences (Moyashi)}
◊define-meta[date]{2021-09-10 – 2021-09-15}

◊msgbox-outdated{9c}

A translation of ◊link["https://conlinguistics.wikia.org/ja/wiki/%E3%82%8A%E3%82%93%E3%81%94%E6%96%87#.E3.83.AA.E3.83.B3.E3.82.B4.E3.82.92.E9.A3.9F.E3.81.B9.E8.B6.B3.E3.82.8A.E3.81.AA.E3.81.8457.E6.96.87"]{◊ja{「リンゴを食べ足りない57文」}} (◊cite{“57 ‘not enough apples to eat’ sentences”}) by Moyashi, used to help develop grammar.

◊section/n[1]{◊ja{こんにちは。}}

◊gloss/x{
  ◊glfree{◊nc{nadenva.}}
  ◊gla{nadenva.}
  ◊glb{hello}
  ◊glfree{Hello.}
}

◊gloss/x{
  ◊glfree{◊nc{cenpe.}}
  ◊gla{cenpe.}
  ◊glb{hello}
  ◊glfree{Hello.}
}

In Necarasso Cryssesa, the standard greeting was ◊v6{nadenva}, from ◊v6{nade} ◊trans{good} and ◊v6{enva} ◊trans{day}.

In Ŋarâþ Crîþ v7, the standard greeting was ◊v7{cem’pe}, the emphatic first-person pronoun, as in ◊trans{I am here}.

Both of these greetings have cognates in ŊCv9.

◊section/n[2]{◊ja{私はXXX（あなたの名前を入れて下さい）です。}}

◊gloss/x{
  ◊glfree{◊nc{cenþ’pe +merlan #flirora.}}
  ◊gla{cenþ=’pe +merlan #flirora.}
  ◊glb{self.%nom.%sg=%poss.1 %name}
  ◊glfree{I am +merlan #flirora.}
}

Not all independent clauses need verbs. In this case, there are two nominative noun phrases, implying that their referents are equal to each other.

Note that ◊l1{cenþ’pe} is pronounced as ◊l2{kenθe} because of complex coda simplification.

◊section/n[3]{◊ja{これは何ですか？}}

◊gloss/x{
  ◊glfree{◊nc{ša ela penen?}}
  ◊gla{ša el-a penen?}
  ◊glb{%int this_thing-%nom.%sg what.%acc.%sg}
  ◊glfree{What is this?}
}

A clause with no verb and a nominative and accusative argument states that the referent of the nominative argument belongs to the class of objects described by the accusative argument. Note that both nominative+nominative and nominative+accusative verbless clauses can be translated using ◊trans{be} in English.

◊section/n[4]{◊ja{これはリンゴです。}}

◊gloss/x{
  ◊glfree{◊nc{ela nemiren.}}
  ◊gla{el-a nem-iren.}
  ◊glb{this_thing-%nom.%sg apple-%acc.%sg}
  ◊glfree{This is an apple.}
}

The same deal as Example #3.

◊section/n[5]{◊ja{これは大きなリンゴです。}}

◊gloss/x{
  ◊glfree{◊nc{ela mervan nemiren.}}
  ◊gla{el-a merv-an nem-iren.}
  ◊glb{this_thing-%nom.%sg large-%rel.%nom,%acc.%cel apple-%acc.%sg}
  ◊glfree{This is a big apple.}
}

Now with a verb participle.

◊section/n[6]{◊ja{これはあなたのリンゴですか？}}

◊gloss/x{
  ◊glfree{◊nc{ša ela nemiren’ve?}}
  ◊gla{ša el-a nem-iren=’ve?}
  ◊glb{%int this_thing-%nom.%sg apple-%acc.%sg=%poss.2}
  ◊glfree{Is this your apple?}
}

Possessive clitics are not distinguished by number.

◊section/n[7]{◊ja{これは私のリンゴです。}}

◊gloss/x{
  ◊glfree{◊nc{ela nemiren’pe.}}
  ◊gla{el-a nem-iren=’pe.}
  ◊glb{this_thing-%nom.%sg apple-%acc.%sg=%poss.1}
  ◊glfree{This is my apple.}
}

The same deal as the last four sentences.

◊section/n[8]{◊ja{（私から近いところを指して）このリンゴは小さい。}}

◊gloss/x{
  ◊glfree{◊nc{lê nemir nôra.}}
  ◊gla{lê nem-ir nôr-a.}
  ◊glb{this.%cel apple-%nom.%sg small-3%sg}
  ◊glfree{(pointing to a place near me) This apple is small.}
}

We finally have a (finite) verb.

◊section/n[9]{◊ja{（あなたから近いところを指して）そのリンゴは大きい。}}

◊gloss/x{
  ◊glfree{◊nc{tê nemir nôra.}}
  ◊gla{tê nem-ir nôr-a.}
  ◊glb{that.%cel apple-%nom.%sg small-3%sg}
  ◊glfree{(pointing to a place near you) That apple is small.}
}

Japanese has a three-way demonstrative distinction, but Ŋarâþ Crîþ has only a two-way distinction.

◊section/n[10]{◊ja{（私からもあなたからも遠いところを指して）あのリンゴはとても大きい。}}

◊gloss/x{
  ◊glfree{◊nc{tê nemir nôra.}}
  ◊gla{tê nem-ir nôr-a.}
  ◊glb{that.%cel apple-%nom.%sg small-3%sg}
  ◊glfree{(pointing to a place far from you or me) That apple is small.}
}

This translation is identical to Example #9 for this reason.

◊section/n[11]{◊ja{リンゴは果実である。}}

◊gloss/x{
  ◊glfree{◊nc{nemis mjenafen.}}
  ◊gla{nem-is mjen-afen.}
  ◊glb{apple-%nom.%gc fruit-%acc.%gc}
  ◊glfree{An apple is a fruit.}
}

◊l1{mjena} can refer to fruits as well as nuts.

◊section/n[12]{◊ja{とあるリンゴは大きい。}}

◊gloss/x{
  ◊glfree{◊nc{nemen nemir merva.}}
  ◊gla{nem-en nem-ir merv-a.}
  ◊glb{some-%gen.%sg apple-%nom.%sg large-3%sg}
  ◊glfree{A certain apple is big.}
}

The determiner ◊l1{nemen} (the genitive singular form of ◊l1{nema}) is used here to indicate a known but unnamed object.

◊section/n[13]{◊ja{一般的にリンゴは甘い。}}

◊gloss/x{
  ◊glfree{◊nc{nemis cełiru.}}
  ◊gla{nem-is cełir-u.}
  ◊glb{apple-%nom.%gc taste_sweet-3%gc}
  ◊glfree{(In general,) apples are sweet.}
}

A textbook example of the generic number.

◊section/n[14]{◊ja{風が吹く。}}

◊gloss/x{
  ◊glfree{◊nc{avonan evru.}}
  ◊gla{avon-an evr-u.}
  ◊glb{air-%acc.%di blow-3%gc}
  ◊glfree{The wind blows.}
}

◊l1{avona} can mean either ◊trans{air} or ◊trans{wind}, although the latter has a specific term, ◊l1{velšavona}.

◊section/n[15]{◊ja{リンゴが落ちる。}}

◊gloss/x{
  ◊glfree{◊nc{nemir gcjašal.}}
  ◊gla{nem-ir g\cjaš-al.}
  ◊glb{apple-%nom.%sg %pfv\fall-3%sg.%inv}
  ◊glfree{The apple falls.}
}

We use the perfective tense here, as opposed to the imperfective as in Example #14.

◊section/n[16]{◊ja{リンゴが私の前にある。}}

◊gloss/x{
  ◊glfree{◊nc{nemir etorvełape.}}
  ◊gla{nem-ir etor-veła-pe.}
  ◊glb{apple-%nom.%sg in_front_of-exist.3%sg-1%sg}
  ◊glfree{The apple is in front of me.}
}

The relational ◊l1{etor} ◊trans{in front of} is used in a finite form.

◊section/n[17]{◊ja{私はリンゴを掴む。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren nagral.}}
  ◊gla{nemiren nagral.}
  ◊glb{apple-%acc.%sg catch-1%sg.%inv}
  ◊glfree{I catch the apple.}
}

We assume that the apple was moving (in the air) before it was caught.

◊section/n[18]{◊ja{私はあなたにリンゴを渡す。}}

◊gloss/x{
  ◊glfree{◊nc{nemils morteþoc rjas gcemalve.}}
  ◊gla{nem-ils mort-eþoc rjas g\cem-al-ve.}
  ◊glb{apple-%dat.%sg hand-%dat.%du between.%asymm %pfv-give-1%sg.%inv-2%sg}
  ◊glfree{I hand you an apple.}
}

◊l1{morteþoc rjas cemat} translates to ◊trans{give between hands} or, more idiomatically, ◊trans{hand over}. ◊l1{cemat} is what was called a ◊term{twisted verb} in the Ŋarâþ Crîþ v7 grammar: the recipient takes the accusative case and the theme takes the dative case, instead of the other way around as expected.

◊section/n[19]{◊ja{私が渡すリンゴをあなたは食べる。}}

◊gloss/x{
  ◊glfree{◊nc{pecirmeþan nemiren mênčeres.}}
  ◊gla{pe-cirm-eþan nem-iren mênč-eres.}
  ◊glb{1%sg-give-%rel.%dat,%acc.%cel apple-%acc.%sg eat-2%sg.%inv}
  ◊glfree{You eat the apple that I passed you.}
}

Since ◊l1{pecirmeþan}, by having a dative rcase, has a head that is the dative argument of ◊l1{cemat}, the object prefix can refer to either the nominative or accusative argument. In this case, the context makes it clear which is the case, but in case it did not, the following would also be acceptable:

◊gloss/x{
  ◊glfree{◊nc{cenþ’pe cirmeþan nemiren mênčeres.}}
  ◊gla{cenþ=’pe cirm-eþan nem-iren mênč-eres.}
  ◊glb{self.%nom.%sg=%poss.1 give-%rel.%dat,%acc.%cel apple-%acc.%sg eat-2%sg.%inv}
  ◊glfree{You eat the apple that I passed you.}
}

Note that ◊l1{cemat} has impersonator stems to avoid ambiguity with certain forms of ◊l1{cenþ}.

◊section/n[20]{◊ja{（あなたは含まない）我々は（習慣として）リンゴを食べる。}}

◊gloss/x{
  ◊glfree{◊nc{nemins mênčat nespecþ.}}
  ◊gla{nem-ins mênč-at nesp-ecþ.}
  ◊glb{apple-%acc.%gc eat-%inf %hab-1%pl.%excl}
  ◊glfree{We (not including you) eat apples (habitually).}
}

Ŋarâþ Crîþ v7 had a set of first-person pronouns with clusivity distinctions, but the rest of the language did not distinguish clusivity. Version 9, on the other hand, adds a clusivity distinction in the verb subject and object affixes, as well as on relational object affixes. The cognates to the v7 first-person dual and plural affixes are now exclusive, with the inclusive counterparts being derived by inverting the tone of the vowel.

◊section/n[21]{◊ja{あなたがたは（習慣として）リンゴを食べる。}}

◊gloss/x{
  ◊glfree{◊nc{nemins mênčat nesper.}}
  ◊gla{nem-ins mênč-at nesp-er.}
  ◊glb{apple-%acc.%gc eat-%inf %hab-2%pl}
  ◊glfree{You (plural) eat apples (habitually).}
}

The same deal as in Example #20.

◊section/n[22]{◊ja{（あなたを含む）我々は（習慣として）リンゴを食べる。}}

◊gloss/x{
  ◊glfree{◊nc{nemins mênčat nespêcþ.}}
  ◊gla{nem-ins mênč-at nesp-êcþ.}
  ◊glb{apple-%acc.%gc eat-%inf %hab-1%pl.%incl}
  ◊glfree{We (including you) eat apples (habitually).}
}

And now for the inclusive.

◊section/n[23]{◊ja{彼は（かつての習慣として）リンゴを食べていた。}}

◊gloss/x{
  ◊glfree{◊nc{nemins mênčat nospelta.}}
  ◊gla{nem-ins mênč-at nosp-el-ta.}
  ◊glb{apple-%acc.%gc eat-%inf %hab-3%sg.%inv-%past}
  ◊glfree{He used to eat apples.}
}

Note that ◊l1{nespat} undergoes a vowel affection in the past tense.

◊section/n[24]{◊ja{彼女はリンゴを（習慣的に）食べるだろう。}}

◊gloss/x{
  ◊glfree{◊nc{nemins mênčat nespat þare.}}
  ◊gla{nem-ins mênč-at nosp-at þar-e.}
  ◊glb{apple-%acc.%gc eat-%inf %hab-%inf %epspos-3%sg}
  ◊glfree{Perhaps she eats apples (habitually).}
}

There are several possible English translations, so I picked one arbitrarily.

◊section/n[25]{◊ja{あなたはリンゴを食べ始める。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren gesmênčeres.}}
  ◊gla{nem-iren g\es-mênč-eres.}
  ◊glb{apple-%acc.%sg %pfv\%inch-eat-2%sg.%inv}
  ◊glfree{You start to eat an apple.}
}

The inchoative or inceptive aspect is marked with the prefix ◊l1{es-}. Either the perfective or the imperfective aspect could be used here.

◊section/n[26]{◊ja{あなたはリンゴを食べている。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênčes.}}
  ◊gla{nem-iren mênč-es.}
  ◊glb{apple-%acc.%sg eat-2%sg}
  ◊glfree{You are eating an apple.}
}

If we wanted to distinguish the progressive aspect from other senses of the imperfective, we can use the following:

◊gloss/x{
  ◊glfree{◊nc{nemiren os m·ênčat elves.}}
  ◊gla{nem-iren os m·ênč-at el-ves.}
  ◊glb{apple-%acc.%sg %inf.%dat eat-%inf inside-exist.2%sg}
  ◊glfree{You are eating an apple.}
}

◊section/n[27]{◊ja{あなたはリンゴを食べ終える。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren germ·ênčeres.}}
  ◊gla{nem-iren g\er-m·ênč-eres.}
  ◊glb{apple-%acc.%sg %pfv\%term-eat-2%sg.%inv}
  ◊glfree{You finish eating the apple.}
}

◊l1{ermênčat} means ◊trans{finish eating}. In contrast, ◊l1{carmênčat} means ◊trans{stop eating}.

◊section/n[28]{◊ja{彼はリンゴを食べ始めるだろう。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren on esmênčat mîrm·are.}}
  ◊gla{nem-iren on es-mênč-at mîrm·ar-e.}
  ◊glb{apple-%acc.%sg %inf.%acc %inch-eat-%inf predict-1%sg}
  ◊glfree{(I predict that) He will start eating an apple.}
}

Ŋarâþ Crîþ often uses the present tense for future events, but it is possible to make it clear that the action is predicted to occur in the future.

◊section/n[29]{◊ja{彼はリンゴを食べ始めた。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren gesmênčeþ.}}
  ◊gla{nem-iren g\es-mênč-e-þ.}
  ◊glb{apple-%acc.%sg %pfv-%inch-eat-3%sg-%past}
  ◊glfree{He started eating an apple.}
}

Now for the perfective.

◊section/n[30]{◊ja{彼はリンゴを食べている。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênče.}}
  ◊gla{nem-iren mênč-e.}
  ◊glb{apple-%acc.%sg eat-3%sg}
  ◊glfree{He is eating an apple.}
}

Same deal as Example #26.

◊section/n[31]{◊ja{彼はリンゴを食べ終えるだろう。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren on erm·ênčat mîrm·are.}}
  ◊gla{nem-iren on er-m·ênč-at mîrm·ar-e.}
  ◊glb{apple-%acc.%sg %inf.%acc %term-eat-%inf predict-1%sg}
  ◊glfree{(I predict that) He will finish eating the apple.}
}

Same deal as Example #27.

◊section/n[32]{◊ja{彼はリンゴを食べ終えていた。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren erm·ênčelta.}}
  ◊gla{nem-iren er-m·ênč-el-ta.}
  ◊glb{apple-%acc.%sg %term-%eat-3%sg.%inv-%past}
  ◊glfree{He was finishing eating the apple.}
}

We use the imperfective aspect here.

◊section/n[33]{◊ja{このリンゴは小さかった。}}

◊gloss/x{
  ◊glfree{◊nc{lê nemir nôralta.}}
  ◊gla{lê nem-ir nôr-al-ta.}
  ◊glb{this.%cel apple-%nom.%sg small-3%sg.%inv-%past}
  ◊glfree{This apple was small.}
}

Also the imperfective aspect.

◊section/n[34]{◊ja{このリンゴは小さくない。}}

◊gloss/x{
  ◊glfree{◊nc{lê nemir mervit išira.}}
  ◊gla{lê nem-ir merv-it išir-a.}
  ◊glb{this.%cel apple-%nom.%sg large-%inf at_least_slightly-3%sg}
  ◊glfree{This apple is not small.}
}

◊l1{iširit} can be translated as ◊trans{not un-}.

◊section/n[35]{◊ja{このリンゴはもっと大きくなるだろう。}}

◊gloss/x{
  ◊glfree{◊nc{lê nemils on memitrarvit mîrm·are.}}
  ◊gla{lê nem-ils on me<mitra>rv-it mîrm·ar-e.}
  ◊glb{this.%cel apple-%dat.%sg %inf.%acc large<%ddt>-%inf predict-1%sg}
  ◊glfree{I think this apple will become larger.}
}

The calculus affix ◊l0{-mitra-}, indicating the derivative with respect to time, turns ◊l1{mervit} ◊trans{large} into ◊l1{memitravit} ◊trans{grow larger}.

◊section/n[36]{◊ja{「彼はリンゴを食べていた。」とあなたは言う。}}

◊gloss/x{
  ◊glfree{◊nc{«nemiren mênčelta» ne marares.}}
  ◊gla{«nem-iren mênč-el-ta» ne mar-ares.}
  ◊glb{apple-%acc.%sg eat-3%sg.%inv-%past %quot.%acc say-2%sg.%inv}
  ◊glfree{“He was eating an apple,” you say.}
}

Note that a final period inside a quotation embedded within another sentence is omitted.

◊section/n[37]{◊ja{彼はリンゴを食べていたとあなたは言った。}}

◊gloss/x{
  ◊glfree{◊nc{«nemiren mênčelta» reþ marasta.}}
  ◊gla{«nem-iren mênč-el-ta» reþ mar-as-ta.}
  ◊glb{apple-%acc.%sg eat-3%sg.%inv-%past %quot.%acc.%ind say-2%sg-%past}
  ◊glfree{You said that he had been eating an apple.}
}

We now have an indirect quotation. We need to change the particle, but the paraphrased sentence is still surrounded by quotation marks.

◊section/n[38]{◊ja{彼はリンゴを食べているとあなたは言った。}}

◊gloss/x{
  ◊glfree{◊nc{«nemiren mênče» reþ marasta.}}
  ◊gla{«nem-iren mênč-e» reþ mar-as-ta.}
  ◊glb{apple-%acc.%sg eat-3%sg %quot.%acc.%ind say-2%sg-%past}
  ◊glfree{You said that he was eating an apple (at the time you said that).}
}

The temporal deixis within an indirect quotation is not the same as that outside it. Instead, tenses inside the indirect quotation are relative to the time when its content is realized (that is, said, thought, &c.).

◊section/n[39]{◊ja{彼はリンゴを食べるだろうとあなたは言うだろう。}}

◊gloss/x{
  ◊glfree{◊nc{«nemiren mênčat vere» reþ marit veres.}}
  ◊gla{«nem-iren mênč-at ver-e» reþ mar-it ver-es.}
  ◊glb{apple-%acc.%sg eat-%inf intend_to-3%sg %quot.%acc.%ind say-%inf intend_to-2%sg}
  ◊glfree{You will say that he will eat an apple (some time after you say that).}
}

Technically speaking, of course, you can’t read other peoples’ minds and find out their intentions.

◊section/n[40]{◊ja{彼はリンゴを食べるだろうとあなたは言った。}}

◊gloss/x{
  ◊glfree{◊nc{«nemiren mênčat vere» reþ marasta.}}
  ◊gla{«nem-iren mênč-at ver-e» reþ mar-as-ta.}
  ◊glb{apple-%acc.%sg eat-%inf intend_to-3%sg %quot.%acc.%ind say-2%sg-%past}
  ◊glfree{You said that he would eat an apple.}
}

This is slightly more straightforward than the last sentence.

◊section/n[41]{◊ja{私はリンゴを食べられる可能性がある。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênčat pentat þara.}}
  ◊gla{nem-iren mênč-at pent-at þar-a.}
  ◊glb{apple-%acc.%sg eat-%inf able_to-%inf %epspos-1%sg}
  ◊glfree{It is possible that I could eat an apple.}
}

I interpreted the ◊ja{◊q{可能性がある}} part as an epistemic possibility and the ◊ja{◊q{食べられる}} part as a situational possibility.

◊section/n[42]{◊ja{もしリンゴが食べられるならば、嬉しい。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren o mênčat pentat ganhalpe viþca.}}
  ◊gla{nem-iren o mênč-at pent-at g\anh-al-pe viþca.}
  ◊glb{apple-%acc.%sg %inf.%nom eat-%inf able_to-%inf %pfv\be_enjoyable-3%sg.%inv-1%sg %cond}
  ◊glfree{If I could eat an apple, I’d be happy.}
}

Here, ◊l1{nemiren o mênčat pentat} acts as the stimulus or cause for the verb ◊l1{anhit}. The conditional mood is marked by the tailp ◊l1{viþca}.

◊section/n[43]{◊ja{私がリンゴを食べられないのは必然である。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênčat rjotat marða.}}
  ◊gla{nem-iren mênč-at rjot-at marð-a.}
  ◊glb{apple-%acc.%sg eat-%inf cannot-%inf must-1%sg}
  ◊glfree{It is inevitable that I am not able to eat an apple.}
}

The verb chain in this sentence contains two modal auxiliaries of the same flavor: ◊l1{rjotat} and ◊l1{marðat}, which are both situational.

◊section/n[44]{◊ja{リンゴが食べられたら良いのになぁ。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren os m·ênčat afče.}}
  ◊gla{nem-iren os m·ênč-at afč-e.}
  ◊glb{apple-%acc.%sg %inf.%dat eat-%inf wish_for-1%sg}
  ◊glfree{I wish I could eat an apple.}
}

I’m not that good with dealing with Japanese tailps. Had to resort to DeepL to know what this sentence meant.

◊section/n[45]{◊ja{リンゴを食べなければならない。}}

◊gloss/x{
  ◊glfree{◊nc{nemins mênčat łanu.}}
  ◊gla{nem-ins mênč-at łan-u.}
  ◊glb{apple-%acc.%gc eat-%inf must-3%gc}
  ◊glfree{It is necessary to eat apples.}
}

The translation implies that the sentence describes a general obligation rather than an order to a particular person.

◊section/n[46]{◊ja{リンゴを食べたほうが良い。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren on mênčat triłeve.}}
  ◊gla{nem-iren on mênč-at trił-e-ve.}
  ◊glb{apple-%acc.%sg %inf.%acc eat-%inf recommend-1%sg-2%sg}
  ◊glfree{You’d better eat an apple.}
}

Instead of an auxiliary verb meaning ◊trans{should}, Ŋarâþ Crîþ use the verb ◊l1{triłit} ◊trans{recommend, advise, suggest}, which makes the advisor explicit.

◊section/n[47]{◊ja{リンゴを食べるか食べないか選択できる。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren os m·ênčat visêrčige dolilcit saraħas.}}
  ◊gla{nem-iren os m·ênč-at visêrč-ige do-lilc-it sara-ħas.}
  ◊glb{apple-%acc.%sg %inf.%dat eat-%inf choose-or %caus-invert-%inf may-2%sg}
  ◊glfree{You can choose whether to eat the apple or not.}
}

◊l1{visêrčit} means ◊tr{◊S chooses ◊I over ◊|O|}, indicating a preference for one choice over the other. In order to describe a choice without indicating which choice is preferred, one can use ◊l1{visêrčige dolilcit} (literally ◊trans{choose or invert} (i.e. make the reverse choice)). If the second choice is absent, it is implied to be the absence of the first choice.

◊section/n[48]{◊ja{リンゴを食べない方が良い。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren on mênčat garit triłeve.}}
  ◊gla{nem-iren on mênč-at garit trił-e-ve.}
  ◊glb{apple-%acc.%sg %inf.%acc eat-%inf refrain_from-%inf recommend-1%sg-2%sg}
  ◊glfree{You shouldn’t eat the apple.}
}

The same deal as Example #46, except with an extra negative verb.

◊section/n[49]{◊ja{リンゴを食べてはならない。}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênčat garit łanes.}}
  ◊gla{nem-iren mênč-at gar-it łan-es.}
  ◊glb{apple-%acc.%sg eat-%inf refrain_from-%inf must-2%sg}
  ◊glfree{You must not eat the apple.}
}

The same deal as Example #45, except with an extra negative verb.

◊section/n[50]{◊ja{私は（能力的に）リンゴを食べられない。}}

◊gloss/x{
  ◊glfree{◊nc{nemins mênčat geða.}}
  ◊gla{nem-ins mênč-at geð-a.}
  ◊glb{apple-%acc.%gc eat-%inf cannot-1%sg}
  ◊glfree{I am not capable of eating apples.}
}

Situational and dynamic possibility are conflated in the affirmative polarity (◊l1{pentat}) but distinguished in the negative (◊l1{rjotat} vs. ◊l1{geðat}).

◊section/n[51]{◊ja{この大きくてかつ甘いリンゴを私は食べる。}}

◊gloss/x{
  ◊glfree{◊nc{mervirþ cełirinþ lê nemiren mênčal.}}
  ◊gla{merv-irþ cełir-inþ lê nem-iren mênč-al.}
  ◊glb{large-%ser sweet-%rel.%nom,%acc.%cel this.%cel apple-%acc.%sg eat-1%sg.%inv}
  ◊glfree{I eat this big and sweet apple.}
}

It is common to move ◊l1{lê} immediately before the noun in the noun phrase it modifies if the noun phrase contains a prosodically large modifier.

◊section/n[52]{◊ja{私はこのリンゴまたはそのリンゴまたは、その両方を食べる。}}

◊gloss/x{
  ◊glfree{◊nc{lê nemei enten’ce šinon mênčal.}}
  ◊gla{lê nem-ei ent-en=’ce šin-on mênč-al.}
  ◊glb{this.%cel apple-%gen.%sg that_one.%cel-%gen.%sg=and all-%acc.%sg eat-1%sg.%inv}
  ◊glfree{I eat both this apple and that apple.}
}

Since ◊l1{šino} is used to mean ◊trans{the whole} or ◊trans{entirety}, it is used in the singular number.

◊section/n[53]{◊ja{彼は、大きいリンゴまたは甘いリンゴのどちらか一方のみを食べる。}}

◊gloss/x{
  ◊glfree{◊nc{mervan nemiren cełirinþ salin’re mênčel.}}
  ◊gla{merv-an nem-iren cełir-inþ sal-in=’re mênč-el.}
  ◊glb{large-%rel.%nom,%acc.%cel apple-%acc.%sg sweet-%rel.%nom,%acc.%cel thing-%acc.%sg=xor eat-3%sg.%inv}
  ◊glfree{He eats either the big apple or the sweet apple, but not both.}
}

Ŋarâþ Crîþ distinguishes between the inclusive and exclusive ors in coordination.

◊section/n[54]{◊ja{あなたがそのリンゴ食べるならば、またその時に限り、私はこのリンゴを食べる。}}

◊gloss/x{
  ◊glfree{◊nc{lê nemiren os m·ênčat’pe tecto os entan ħelit’ve mârit vera.}}
  ◊gla{lê nem-iren os m·ênč-at=’pe tecto os ent-an ħel-it=’ve mâr-it ver-a.}
  ◊glb{this.%cel apple-%acc %inf.%dat eat-%inf=%poss.1 before %inf.%dat that_thing.%cel-%acc.%sg do_this-%inf=%poss.2 wait-%inf intend_to-1%sg}
  ◊glfree{I will wait until you eat that apple before I eat this one.}
  ◊glfree[#:prefix "Or: "]{Only when you eat that apple will I eat this one.}
}

First, the entire sentence is rephrased to work around Ŋarâþ Crîþ’s lack of a morpheme meaning ◊trans{only}. (Perhaps one will come eventually. This, however, is not the time.) In addition, if the nominative argument of a nominalized verb phrase is pronominal, then it appears as a possessive clitic on the infinitive.

◊section/n[55]{◊ja{彼があのリンゴを食べるかどうかに関わらず、私はこのリンゴを食べる。}}

◊gloss/x{
  ◊glfree{◊nc{tê nemiren en mênčat lendis lef elan mênčal.}}
  ◊gla{tê nem-iren en mênč-at lend-is lef el-an mênč-al.}
  ◊glb{that.%cel apple-%acc.%sg %inf.%gen eat-%inf truth_value-%dat.%sg orthogonal this_thing.%cel-%acc.%sg eat-1%sg.%inv}
  ◊glfree{Whether he eats that apple or not, I will eat this one.}
}

We use the noun ◊l1{len} ◊trans{truth value} in order to avoid presupposing that one of the apples was eaten and having the sentence be interpreted as ◊trans{ignoring the fact that he ate that apple, I will eat this one}.

◊section/n[56]{◊ja{残念なことに、ここにリンゴはない。}}

◊gloss/x{
  ◊glfree{◊nc{ħespe; eši nemir ceła.}}
  ◊gla{ħespe; eši nem-ir ceła.}
  ◊glb{unfortunately here.%loc.%sg apple-%nom.%sg not_exist.3%sg}
  ◊glfree{Unfortunately, there is no apple here.}
}

There’s an interjection but nothing too hard here.

◊section/n[57]{◊ja{さようなら。}}

◊gloss/x{
  ◊glfree{◊nc{tormec.}}
  ◊gla{tormec.}
  ◊glb{goodbye}
  ◊glfree{Goodbye.}
}

Wasn’t this list fun to try out?
