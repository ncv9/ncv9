#lang pollen

◊define-meta[title]{Leipzig Valency Classes Wordlist}
◊define-meta[date]{2022-01-02 – ?}

A “translation” of the ◊link["https://www.arthaey.com/conlang/valency-verbs.html"]{Leipzig Valency Classes Wordlist} (see that page for more info).

◊table/x[
  #:options (table-options
    #:caption "blahblah"
    #:colgroup '(c d c c c c d)
    #:long? #t
  ) #:id "lvcw"
]{
  Label & Role frame & Verb & ◊sc{nom} & ◊sc{acc} & ◊sc{dat} & others
  ◊sc{rain} & (it) rains & ◊nc{têmit} & (C) & ◊nc{mêvan} & (L) &
  ◊sc{be dry} & S is dry & ◊nc{elcat} & S & ∅ & ∅ &
  ◊sc{burn} & S burns & ◊nc{šorcrit} & S & ∅ & ∅ &
  ◊sc{sink} & S sinks & ◊nc{nemrat} & S & ∅ & ∅ &
  ◊sc{roll} & A rolls & ◊nc{rislat} & A & ∅ & (L) &
  ◊sc{be a hunter} & S is a hunter & ∅ & S & ◊nc{dendisiren} & ∅ &
  ◊sc{be hungry} & E is hungry & ◊nc{renþat} & E & ◊nc{têŋon} & ∅ &
  ◊sc{be sad} & E is sad & ◊nc{tračit} & E & ∅ & ∅ &
  ◊sc{die} & S dies & ◊nc{dranlit} & S & ∅ & ∅ &
  ◊sc{feel cold} & S is cold & ◊nc{adercit} & S & ∅ & ∅ &
  ◊sc{feel pain} & E feels pain in M & ◊nc{eþit} & ◊nc{apasa} & ∅ & ∅ & ◊sc{loc} = M; ◊sc{loc/gen} = E
  ◊sc{scream} & S screams & ◊nc{ageat} & S & ∅ & ∅ &
  ◊sc{laugh} & S laughs & ◊nc{šinrit} & S & ∅ & ∅ &
  ◊sc{play} & S plays & ◊nc{šitrit} & S & ∅ & ∅ &
  ◊sc{live} & S lives somewhere (L) & ◊nc{valit} & S & ∅ & L &
  ◊sc{leave} & A left L & ◊nc{endrit} & A & ∅ & L &
  ◊sc{go} & S goes somewhere (L) & ◊nc{nelsit} & S & ∅ & L &
  ◊sc{sing} & S sings & ◊nc{narvit} & S & ∅ & (P?) &
  ◊sc{jump} & A jumps & ◊nc{tačit} & A & (L) & (L) &
  ◊sc{sit down} & S sits down (somewhere (L)) & ◊nc{(es)searnit} & S & L & ∅ &
  ◊sc{sit} & S sits somewhere (L) & ◊nc{searnit} & S & L & ∅ &
  ◊sc{run} & A runs & ◊nc{mitrit} & A & ∅ & ∅ &
  ◊sc{climb} & A climbs (up (L)) & ◊nc{sênnotat} & A & L & ∅ &
  ◊sc{cough} & S coughs & ◊nc{gehorat} & S & ∅ & ∅ &
  ◊sc{blink} & S blinks & ◊nc{cšiłit} & S & ∅ & ∅ &
  ◊sc{shave} & A shaves (his beard / hair) & ◊nc{plecþat} & A & ◊nc{cemen} & ∅ &
  ◊sc{dress} & A dresses P & ◊nc{dopelčit} & A & P & (I) &
}
