#lang pollen

◊define-meta[title]{404: page not found}
◊define-meta[hide-title? #t]
◊define-meta[hide-trail? #t]

◊h1[#:class "error-page-title"]{◊cenvos{194}}

0x194 = 404: the page◊span[#:id "url-fillin"] could not be found.

◊link["javascript:history.back()"]{Return to the previous page} or ◊link["/"]{go to the main page.}

◊script{
document.getElementById("url-fillin").innerText = " " + document.location.pathname;
}
