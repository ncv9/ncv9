#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Project Shiva is now backburner’d!}
◊define-meta[date]{2023-03-30}

In ◊link["https://discord.gg/CxHF6HpATT"]{Saturn’s Sojourn}, I mentioned how I wanted to resolve ◊l0{-lVl-} sequences in Ŋarâþ Crîþ, namely by adding a new terminal coda ◊l0{¬d} when one occurs word-finally. A few messages later, Saturnine asked about my ◊link["https://ncv9.flirora.xyz/grammar/about/principles.html"]{goals with the language} and was perplexed at why I would go through “nine iterations and another overhaul”.

I answered “v9e’s system was an improvement over v9c, but it still has some hacks”.

Saturn then asked “Improvement in what sense? The principles you’ve laid out for the language are rather arbitrary ones which most naturalistic conlangs more or less have. The question is really what's the goal.”

I elaborated about how noun declensions were more ‘unified’ in v9e: I cited the number of vowel tables in v9c, but I’d also include the reorganization of the fifteen or so paradigms in v9c into six categories. The participle system is also less table-heavy in v9e than in v9c. I feel, however, that Ŋarâþ Crîþ’s morphology could be made even more elegant than it is now while remaining interesting.

I also mentioned that I’ve been working on Ŋarâþ Crîþ for almost ten years, prompting Saturn to ask why I would work on a conlang for so long. (I took a hiatus from 2016 to 2018 to work on other conlangs, many of which had wild ideas that were awkward to use.)

◊blockquote[#f ◊@{— Saturnine}]{
    Genuinely curious. I seem to be missing where it is you’re trying to go with all this.
    Is it just for its own sake?
    Or do you have something you'd like to use this conlang for/alongside?
    What is the ◊em{goal} of the project.
}

Having Ŋarâþ Crîþ is a goal in itself, but I mentioned that I had written ◊link["https://www.youtube.com/watch?v=k4ndOV1yfcw"]{a song in Ŋarâþ Crîþ} and wanted to write more.

Saturn then asked why, given that goal, I would continually overhaul it, to which I responded, “Because the day that Ŋarâþ Crîþ stops changing is the day it dies.”

Saturn noted that my motivations for conlanging was different from his own: he makes conlangs to be used to write “dialogue and in-world literature”, while I engage in “conlanging for the sake of it”.

After explaining the Ŋarâþ Crîþ translation of the motto, I mentioned that the current inflection system actually is passable, and that I would have to write more Ŋarâþ Crîþ to evaluate its deficiencies more effectively. This is another thing that Saturn found foreign: the concepts of looking at “flaws in the system to evaluate” in a conlang.

I also mentioned two annoyances I’d experienced while writing in Ŋarâþ Crîþ: the coexistence of rich agreement with head-final word order and the low syllable count of Ŋarâþ Crîþ compared to, say, Japanese.

There were other aspects I forgot to mention, such as the distribution of nominal stems across cases. Namely, the four most commonly used cases often get only one stem between them, while the L and S stems are used for cases that are used rarely.

Nevertheless, I think most of my work with Project Shiva so far has been too theoretical, trying to fix the inflection system without using Ŋarâþ Crîþ enough to know its supposed flaws. For this reason, I am temporarily suspending work on Project Shiva.

This is not to say that Project Shiva has been futile. For one thing, it might be picked up once more, when the ‘defects’ of Ŋarâþ Crîþ inflection are better known. But even if it is not, it has led to the flavored hyphen notation as well as prompting investigation into ◊link["/iit/index.html"]{inverse inflection theory}. Even if Project Shiva does not survive, study into IIT can on its own. We might see a day when f9i doesn’t create an SQLite database of over 600,000 inflected forms in order to enable searching entries from inflected words.

But right now, I want to focus on using Ŋarâþ Crîþ, as well as reflecting on what the goals of Ŋarâþ Crîþ are.
