#lang pollen

◊(require pollen/pagetree)

◊define-meta[pdf-type article]
◊define-meta[title]{Project Vivian}
◊define-meta[date]{2022-06-26 – ?}
◊define-meta[book-author]{+merlan #flirora}

Writing songs in Ŋarâþ Crîþ!

◊section[#:id "rationale"]{Rationale}

◊items{
    ◊item{Encourage developing the language further}
    ◊item{Spread the message of our lord and savior Ŋarâþ Crîþ}
    ◊item{Use my SynthV Studio Pro license for something better than gathering dust}
}

◊section[#:id "status"]{Status}

◊table/x[#:options (table-options #:caption ◊@{
    Status of Project Vivian songs.
} #:colgroup '(fig d c))]{
  # & Title or incipit & Progress
  0x00 & ◊link["00.html"]{◊nc{naðasels nelčon tfełor}} & Done
  0x01 & ◊link["01.html"]{◊nc{asnelerþ}} & Done
  0x02 & ◊link["02.html"]{◊nc{naldenôr}} & Done
}

◊subsection[#:id "status-special"]{Special songs}

◊table/x[#:options (table-options #:caption ◊@{
    Status of special songs.
} #:colgroup '(fig d c))]{
  # & Title or incipit & Progress
  MSTMS2 & ◊link["mstms2.html"]{◊nc{crînas tfel darłit geðates cemi’ve}} & Done
}

◊section[#:id "terms"]{Terms for covering Project Vivian songs}

Covering songs under Project Vivian is permitted, provided that you do the following:

◊enum{
  ◊item{Credit the original song.}
  ◊item{Cover the song in the original or another language. Covers using relexes, ◊tt{sed}, or ◊tt{/dev/urandom} are not permitted.}
  ◊item{If you cover a song in a different language, then mention the language in question.}
  ◊item{If you are covering a song in a conlang that is not well known, then provide an interlinear gloss and a link to the documentation for the conlang.}
}
