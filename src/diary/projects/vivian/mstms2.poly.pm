#lang pollen

◊define-meta[title]{◊@{MSTMS2: ◊cite[((lang "art-x-ncs-v9"))]{crînas tfel darłit geðates cemi’ve}}}
◊define-meta[date]{2022-06-10 – 2022-10-01}

◊dl[#:class "infobox"]{
    ◊; 闇の彼方へ手を伸ばせないあなたへ
    ◊dt{Title}◊dd{◊cite[#:lang (ŋc-code 9)]{crînas tfel darłit geðates cemi’ve} (◊cite{To You Who Can’t Be Reached Across the Darkness})}◊;
    ◊dt{Lyrics}◊dd{+merlan #flirora}◊;
    ◊dt{Music}◊dd{+merlan #flirora}◊;
    ◊dt{Vocals}◊dd{Mai (Synthesizer V Studio)}◊;
    ◊dt{Video}◊dd{+merlan #flirora}◊;
    ◊dt{Link}◊dd{◊link["http://www.nicovideo.jp/watch/so42907163"]{NND} / ◊link["https://youtu.be/OK2i9Qq-YGw"]{YouTube}}◊;
}

◊section{Lyrics}

◊poem[#:lang (ŋc-code 9)]{
sofnas gðenic’pe šinen neveł on mos eþit martanis fose
fetjas afčirþ gaseneljeveþa doorelcinta.
ai šimitradrilt nîs cilreseþen nemin esalels varmjoneve
fel senla meneþa so’moc šino cilþa gašiden vloþ.

relsa; gðelrit rjotale. ša peni mjorcoc’pe pjahelit þaren se?
vjor vlageł aleħe sefgicþ marevi nemo von?
ea cema natro es cirtoþ varon’moc escolpilt oginame
asfiłaþa lê gcoðas cenþ’pe cerecit senla.

vânot meðen poðas minaeþ drênecin.
niðes serfil gcehal se; ercan respen aleaþ siħlal.
orosin þriŋa cehaþa łani tfel mečit
pentat anðameca drełes nelčan cevon
sênrarþ gašjodenca’ve torae.

varcþe nesmâevin afčien cþeris;
nemen neðas tačirþ cjamaldos’pe darłit rendale.
łelþot šileħes meva megaþ grisþatan ciłas jasen
areles ragan łirla nentan’moc mirlirne se.

cajoþ desa senþels gralsit penta so
narva vemitrasrime calan vfimitrałel viþca.
le sârit pečaspe; fêsêna mivar penhenrotoven
el osmos narvit penta þal.

le sârit pečaspe; le endrit garaspe;
ercas šesos vlinas silve
:cantel šimei sados elt gisdicte vilfin
moħon’pe avanros fose.

inoran dołerlas gcincesteve seljen
secþro tecsat ħarat renda
:tê gelin ariga garivon inoren
aiþon’pe ndoasfal so’moc.
}

◊subsection{Gloss}

◊gloss/x{
  ◊glfree{◊nc{sofnas gðenic’pe šinen neveł on mos eþit martanis fose fetjas afčirþ gaseneljeveþa doorelcinta.}}
  ◊gla{sofn-as gðen-ic=’pe šin-en nev-eł on mos-∅ eþ-it mar-t-anis fose fetj-as afč-irþ g\asenelj-eveþa do-or-el-cin-ta.}
  ◊glb{youth-%loc.%di parent-%nom.%du=%poss.1 all-%gen.%sg person-%loc.%gc %inf.%acc psyche-%nom.%sg exist-%inf say-%pst-3%du.%inv because this_idea-%dat.%sg hope-%ser (%inf\)question-%inf.%abess %caus-believe-1%sg.%inv-%refl-%pst}
  ◊glfree{Because in my childhood my parents told me that everyone had a soul, I hoped that was true and made myself believe it without question.}
}

◊gloss/x{
  ◊glfree{◊nc{ai šimitradrilt nîs cilreseþen nemin esalels varmjoneve fel senla meneþa so’moc šino cilþa gašiden vloþ.}}
  ◊gla{ai ši<mitra>dr-ilt nîs cilres-eþen nem-in es-al-els varmjon-eve fel senl-a men-eþa so=’moc šin-o cilþ-a gašid-en vloþ-∅.}
  ◊glb{but old<%ddt>-%inf.%dat while meet-%rel.%dat,%gen.%cel some-%gen.%pl inside-%nomn-%dat.%di examine-%inf.%loc %tgellp try-1%sg see-%rel.%dat,%nom.%cel if=also all-%nom.%sg drip-%rel.%nom,%nom.%cel shadow-%gen.%co puddle-%nom.%sg}
  ◊glfree{But when I looked into some of the people I met as I grew up, I saw only a dripping puddle of shadows no matter how hard I tried.}
}

◊gloss/x{
  ◊glfree{◊nc{relsa; gðelrit rjotale. ša peni mjorcoc’pe pjahelit þaren se?}}
  ◊gla{rels-a; gðelr-it rjot-a-le. ša pen-i mjorc-oc=’pe pjahel-it þar-en se?}
  ◊glb{inexplicable-3%sg explain-%inf fail_to-1%sg-3%sg.%cel %int what-%dat.%sg eye-%nom.%du=%poss.1 broken-%inf might-3%du %mir}
  ◊glfree{Strange; I can’t explain it. Could something be wrong with my eyes?}
}

◊gloss/x{
  ◊glfree{◊nc{vjor vlageł aleħe sefgicþ marevi nemo von?}}
  ◊gla{vjor vlag-eł ale-ħe sefg-icþ marev-i nem-o von?}
  ◊glb{or decay-%rel.%nom,%sembl.%cel breathe-%rel.%nom,%nom.%cel log-%sembl.%gc hollow-%nom,%nom.%pl some-%nom.%pl exist.3%pl}
  ◊glfree{Or are there some who breathe yet are hollow like a rotting log?}
}

◊gloss/x{
  ◊glfree{◊nc{ea cema natro es cirtoþ varon’moc escolpilt oginame asfiłaþa lê gcoðas cenþ’pe cerecit senla.}}
  ◊gla{ea cem-a natr-o es cirt-oþ var-on=’moc es-colp-ilt ogin-ame asfił-aþa lê gcoð-as cenþ=’pe cerec-it senl-a.}
  ◊glb{and %pr.%refl.%gen.%sg skin-%dat.%sg inside flesh-%dat.%di life-%nom.%di=also %inch-crumble-%inf.%dat detest-and cause_to_recoil-%rel.%acc,%nom.%cel this.%cel feeling-%acc.%pl %pr.%refl.%nom.%sg=%poss.1 forget-%inf try-1%sg}
  ◊glfree{Dreading the life force’s falling apart in the flesh under my skin and recoiling from it, I try to forget these feelings.}
}

◊gloss/x{
  ◊glfree{◊nc{vânot meðen poðas minaeþ drênecin.}}
  ◊gla{vân-ot með-en poð-as mina-eþ drên-e-cin.}
  ◊glb{usual-%sembl.%di night-%gen.%sg other-%loc.%sg alone-%ser wake-1%sg-%refl}
  ◊glfree{On another night as usual, I keep myself awake alone.}
}

◊gloss/x{
  ◊glfree{◊nc{niðes serfil gcehal se; ercan respen aleaþ siħlal.}}
  ◊gla{nið-es serf-il g\ceh-al se; erc-an resp-en ale-aþ siħl-al.}
  ◊glb{early-%loc.%di autumn-%nom.%di %pfv\arrive-3%sg.%inv %mir cold-%rel.%nom,%acc.%sg fog-%acc.%di breathe-%ser sigh-1%sg.%inv}
  ◊glfree{Autumn has already come; I breathe in the cold fog and sigh.}
}

◊gloss/x{
  ◊glfree{◊nc{orosin þriŋa cehaþa łani tfel mečit pentat anðameca drełes nelčan cevon sênrarþ gašjodenca’ve torae.}}
  ◊gla{oros-in þriŋ-a ceh-aþa łan-i tfel meč-it pent-at anð-ameca dreł-es nelč-an cev-on sênr-arþ gašjod-enca=’ve tora-e.}
  ◊glb{gibbous_moon-%gen.%sg glow-%nom.%sg find-%rel.%acc,%nom.%cel cloud-%dat.%co across reach-%inf able_to-%inf not_yet-but tear-%nom.%co stain-%rel.%acc,%acc.%cel face-%acc.%sg raise-%ser shadow-%inst.%sv=%poss.2 dance-1%sg}
  ◊glfree{Although I can’t yet reach you through the clouds lit by the gibbous moon’s glow, I’ll raise my face stained with tears and dance with your shadow.}
}

◊gloss/x{
  ◊glfree{◊nc{varcþe nesmâevin afčien cþeris;}}
  ◊gla{varcþ-e nesmâev-in afči-en cþer-is;}
  ◊glb{deep-%rel.%nom,%nom.%cel night_sky-%gen.%di hope-%gen.%di door-%nom.%sg}
  ◊glfree{The deep night sky is the door to hope;}
}

◊gloss/x{
  ◊glfree{◊nc{nemen neðas tačirþ cjamaldos’pe darłit rendale.}}
  ◊gla{nem-en neð-as tač-irþ cja-mald-os=’pe darł-it rend-a-le.}
  ◊glb{some-%gen.%sg day-%loc.%sg jump-%ser %inst-hand-%inst.%sg=%poss.1 reach-%inf want_to-1%sg-3%sg.%cel}
  ◊glfree{I want to jump and reach it with my hand some day.}
}

◊gloss/x{
  ◊glfree{◊nc{łelþot šileħes meva megaþ grisþatan ciłas jasen areles ragen łirla nentan’moc mirlirne se.}}
  ◊gla{łelþ-ot šile-ħes mev-a meg-aþ grisþ-atan cił-as jas-en arel-es rag-an łirl-a nent-an=’moc mir-lirn-e se.}
  ◊glb{firefly-%loc.%co shine-%rel.%nom,%dat.%cel sea-%nom.%sg distort-%ser reflect-%nom.%acc,%acc.%cel speck-%dat.%co from-%adn faint-%rel.%nom,%nom.%sg high-%rel.%nom,%cel.%sg light-%nom.%co full_moon-%acc.%sg=also %cmp-shine-3%sg %mir}
  ◊glfree{The dim light from the specks shining like fireflies shine brighter than even the full moon high up, distorted and reflected by the sea.}
}

◊gloss/x{
  ◊glfree{◊nc{cajoþ desa senþels gralsit penta so narva vemitrasrime calan vfimitrałel viþca.}}
  ◊gla{caj-oþ desa senþ-els grals-it pent-a so narv-a ve<mitra>sr-ime cal-an v\fi<mitra>ł-el viþca.}
  ◊glb{ground-%dat.%di below grudge-%dat.%di abandon-%inf able_to-1%sg if voice-%nom.%sg strong<%ddt>-and body-%nom.%sg %pfv\light<%ddt>-3%sg.%inv would}
  ◊glfree{If I could leave behind my grudges underground, my voice would become stronger and my body lighter.}
}

◊gloss/x{
  ◊glfree{◊nc{le sârit pečaspe; fêsêna mivar penhenrotoven el osmos narvit penta þal.}}
  ◊gla{le sâr-it peč-as-pe; fêsêna mivar pen-henroto-ven el osm-os narv-it pent-a þal.}
  ◊glb{%imp release-%inf avoid-2%sg-1%sg 3%gc-above word-%acc.%pl what-16²-%ctr.written_work this.%ter dream-%loc.%sg sing-%inf able_to-1%sg %assert}
  ◊glfree{Don’t let go of me; I can sing hundreds of words more in this dream.}
}

◊gloss/x{
  ◊glfree{◊nc{le sârit pečaspe; le endrit garaspe;}}
  ◊gla{le sâr-it peč-as-pe; le endr-it gar-as-pe;}
  ◊glb{%imp release-%inf avoid-2%sg-1%sg %imp leave-%inf refrain_from-2%sg-1%sg}
  ◊glfree{Don’t let go; don’t leave me;}
}

◊gloss/x{
  ◊glfree{◊nc{ercas šesos vlinas silve :cantel šimei sados elt gisdicte vilfin moħon’pe avanros fose.}}
  ◊gla{erc-as šes-os vlin-as silv-e :cant-el šim-ei sad-os elt gisd-icte vil-fin moħ-on=’pe avanr-os fose.}
  ◊glb{cold-%rel.%nom,%dat.%sg always-%loc.%di dawn-%dat.%sg fear-1%sg morning-%gen.%sg end-%gen.%sg next-%dat.%sg no_later_than dew-%sembl.%sv one.%short-%ctr.drop psyche-%acc.%sg=1%sg evaporate-3%gc.%pfv because}
  ◊glfree{I always fear the cold dawn, because my soul will evaporate like a drop of dew by the end of the next morning.}
}

◊gloss/x{
  ◊glfree{◊nc{inoran dołerlas gcincesteve seljen secþro tecsat ħarat renda :tê gelin ariga garivon inoren aiþon’pe ndoasfal so’moc.}}
  ◊gla{inor-an do-łerl-as g\cin-cest-eve selj-en secþr-o tecs-at ħar-at rend-a :tê g\el-in arig-a gariv-on inor-en aj-þon=’pe n\do-asf-al so=’moc.}
  ◊glb{branch-%acc.%co %caus-shake-%rel.%nom,%dat.%cel %pfv\%refl-equal-%inf.%loc afternoon-%gen.%sg breeze-%dat.%di touch-%inf again-%inf want_to-1%sg that.%cel sun-%gen.%sg warmth-%nom.%di fragile-%rel.%nom,%acc.%pl emptiness-%gen.%sg bone-%acc.%pl=%poss.1 %pfv\%caus-melt-3%sg.%inv if=also}
  ◊glfree{I want to feel the afternoon breeze shaking the branches once again while I am myself, even if the sun’s warmth melts my frail, hollow bones.}
}

◊section{Resource credits}

◊details{
    ◊summary{Music}
    ◊dl[#:class "infobox"]{
        ◊dt{DAW}◊dd{◊link["https://ardour.org/"]{Ardour} 7.5.0}◊;
        ◊dt{Instrument plugins}◊dd{◊link["https://surge-synthesizer.github.io/index.html"]{Surge XT}; DrumSynth (◊link["https://github.com/distrho/distrho-ports"]{DISTRHO Ports} version)}◊;
        ◊dt{Other plugins}◊dd{◊link["https://lsp-plug.in/index.php"]{LSP} plugins}◊;
    }
}
