#lang pollen

◊define-meta[title]{◊@{0x01: ◊cite[((lang "art-x-ncs-v9"))]{asnelerþ}}}
◊define-meta[date]{2023-03-23 – 2024-06-30}

◊dl[#:class "infobox"]{
    ◊dt{Title}◊dd{◊cite[#:lang (ŋc-code 9)]{asnelerþ} (◊cite{Extrication})}◊;
    ◊dt{Lyrics}◊dd{+merlan #flirora}◊;
    ◊dt{Music}◊dd{+merlan #flirora}◊;
    ◊dt{Vocals}◊dd{Mai (Synthesizer V Studio)}◊;
    ◊dt{Video}◊dd{+merlan #flirora}◊;
    ◊dt{Link}◊dd{◊link["https://youtu.be/l94sIhUMvO4"]{YouTube}}◊;
}

◊section{Lyrics}

◊poem[#:lang (ŋc-code 9)]{
le drênascin; srivîrei łirtês;
natro’ve gênon finrit vfolcal.
renva caðjano eþimeca
ša peni roc crînas aserłas?

criþjâlirþ odoþon goroco
mesdot tełava reþ dooraþac
trasmos ocþarîne relcrilt es’moc
cenþ’ve gventeras se.

arðei eltes coðałes srefne;
telmaðei relcoc gašidas fesde;
refas elêþ cjaloþ cþirhinþe
enro esar cjerantin mori.

sênraþepa grisfials grîvelca
vanâjen tênþon ndotosel.
endîr atrasnir vôr aspel saden cenra.

eneða cajoþ gasnelsal;
cfetrivistoþ nerin lłenviris.
calpenel crôneca evon
cincinrarþ mel ðêser mînčelve.

le drênascin; cimetrei łirtês;
carie; varon cjašit ħare.
cerels têŋi’ce cfilsameca
ša peni roc senþerin meftes?

gvonros aračeþa crînan fesmirþ
ondelt cenþ’ve jorniłit łanes.
visrec’âcþ velšit racfit anðes so
cinras menrieve.

arðei eltes coðałes srefne;
telmaðei relcoc gašidas fesde;
cþelfa asnîrleþans sreŋet
emedrocþ cfiþar narmens’ve roħi.

sênraþepa tfilres jas grîvelca
sêrennels vêsros gcemalve.
endîr atrasnir vôr aspel saden cenra.

elgren endîr’ce cþarmel’pe šimeþ;
senlelþa’ve ðêf vargru.
pelþit vedenecameca
rindelc’âcþ cenþ’ve sênnotat łanes
:gjavercþan avena enleas ragoþ ar.
gciheve eþei mârdan enseran ŋgrenfos.

eneða cajoþ gasnelsal;
nenso teltos ar ganagriris.
geðroga vatoren resvel
cindoesliþ vifþens’moc ŋgeldra.

le drênascin; telmaðei łirtês;
łilicþ ineþen ciren mâra.
refas mos’ve nasenil cerþel
inolocþ arelit denecþal.

ai asenen tesnerþ ndroneve
arigas narecirþ esšorcrime
annêrtoþ creł tê *saðas cintlerfliþ
čirnarþ gatrada se.
}

◊subsection{Gloss}

◊gloss/x{
  ◊glfree{◊nc{le drênascin; srivîrei łirtês;}}
  ◊gla{le drên-as-cin; srivîr-ei łirt-ês;}
  ◊glb{%imp wake-2%sg-%refl resentment-%gen.%sg child-%dat.%sg}
  ◊glfree{Wake up, child of resentment;}
}

◊gloss/x{
  ◊glfree{◊nc{natro’ve gênon finrit vfolcal.}}
  ◊gla{natr-o=’ve gên-on finr-it v\folc-al.}
  ◊glb{face-%dat.%sg=%poss.2 dirt-%acc.%di wipe-%inf %pfv\intend_to-1%sg.%inv}
  ◊glfree{I will wipe off the dirt from your face.}
}

◊gloss/x{
  ◊glfree{◊nc{renva caðjano eþimeca ša peni roc crînas aserłas?}}
  ◊gla{renv-a caðjan-o eþ-imeca ša pen-i roc crîn-as aserł-as?}
  ◊glb{bright-%rel.%nom,%nom.%sg world-%nom.%sg exist-but %int what-%dat.%sg for darkness-%dat.%di wallow-2%sg}
  ◊glfree{Although a bright world exists, why do you wallow in the dark?}
}

◊gloss/x{
  ◊glfree{◊nc{criþjâlirþ «odoþon goroco mesdot tełava» reþ dooraþac trasmos ocþarîne relcrilt es’moc cenþ’ve gventeras se.}}
  ◊gla{criþjâl-irþ «o-do-þon goroc-o mesd-ot tełav-a» reþ do-or-aþac trasm-os o-cþarîne relcr-ilt es=’moc cenþ=’ve gventer-as se.}
  ◊glb{mislead-%ser 2%sg-%rev-among mold-%nom.%di honey-%sembl.%di smell_sweet-3%sg %quot.%acc.%ind %caus-think-%rel.%acc,%nom.%hum moth-%nom.%co 2%sg-around.%adv fly-%inf.%dat inside=also self.%nom.%sg=%poss.2 idle-2%sg %mir}
  ◊glfree{Tricked into thinking that the mold around you smells as sweet as honey, you lie there while moths fly around you.}
}

◊gloss/x{
  ◊glfree{◊nc{arðei eltes coðałes srefne;}}
  ◊gla{arð-ei elt-es coð-ałes srefn-e;}
  ◊glb{vigor-%gen.%sg river-%nom.%sg emotion-%dat.%gc tangle-3%sg}
  ◊glfree{The stream of vigor intertwines with feelings;}
}

◊gloss/x{
  ◊glfree{◊nc{telmaðei relcoc gašidas fesde;}}
  ◊gla{telmað-ei relc-oc gašid-as fesl-te;}
  ◊glb{tenacity-%gen.%di wing-%nom.%du shadow-%dat.%co lead_away_from-3%du}
  ◊glfree{The wings of resoluteless lead away from shadows;}
}

◊gloss/x{
  ◊glfree{◊nc{refas elêþ cjaloþ cþirhinþe enro esar cjerantin mori.}}
  ◊gla{ref-as el-êþ cjal-oþ cþirh-inþe enr-o es-ar cjerant-in mor-i.}
  ◊glb{true-%rel.%nom,%nom.%sg sun-%nom.%sg scale-%dat.%co illuminate-%rel.%gen_dat,%nom.%cel hole-%dat inside-toward butterfly-%nom.%co descend-3%pl}
  ◊glfree{Butterflies, their scales lit by the true sun, descend into the cavern.}
}

◊gloss/x{
  ◊glfree{◊nc{sênraþepa grisfials grîvelca vanâjen tênþon ndotosel.}}
  ◊gla{sênr-aþepa grisfi-als grîv-elca vanâj-en tesn-þon n\do-tos-el.}
  ◊glb{raise-%rel.%acc,%inst.%cel horizon-%dat.%sg ceremonial_knife-%inst.%sg pessimism-%gen.%di chain-%acc.%pl %pfv\%caus-dissolve-1%sg.%inv}
  ◊glfree{With this raised knife, I’ll disolve the chains of gloom into the horizon;}
}

◊gloss/x{
  ◊glfree{◊nc{endîr atrasnir vôr aspel saden cenra.}}
  ◊gla{end-îr atrasn-ir vôr asp-el sad-en cenr-a.}
  ◊glb{now-%nom.%di color-%dat.%pl full_of lifetime-%gen.%sg next-%gen.%sg start-%nom.%sg}
  ◊glfree{Now your next life, full of color, starts.}
}

◊gloss/x{
  ◊glfree{◊nc{eneða cajoþ gasnelsal;}}
  ◊gla{eneð-a caj-oþ g\asnels-al;}
  ◊glb{ash-%nom.%co ground-%dat.%sg %pfv\return-3%sg.%inv}
  ◊glfree{Ash returns to the ground;}
}

◊gloss/x{
  ◊glfree{◊nc{cfetrivistoþ nerin lłenviris.}}
  ◊gla{cfetrivist-oþ ner-in l\łenv-iris.}
  ◊glb{bedrock-%dat.%co curse-%nom.%pl %pfv\permeate-3%pl.%inv}
  ◊glfree{Curses seep through the bedrock.}
}

◊gloss/x{
  ◊glfree{◊nc{calpenel crônica evon cincinraþ mel ðêser mînčelve.}}
  ◊gla{calpen-el crôn-ica ev-on cin-cinr-aþ mel ðêser mînč-el-ve.}
  ◊glb{mystical_ice-%gen.%co tooth-%inst.%pl mouth-%nom.%sg %refl-open-%ser many occurrence-%loc.%pl eat-3%sg.%inv-2%sg}
  ◊glfree{A mouth, lined by teeth of ice, will open and swallow you many times.}
}

◊gloss/x{
  ◊glfree{◊nc{le drênascin; cimetrei łirtês;}}
  ◊gla{le drên-as-cin; cimetr-ei łirt-ês;}
  ◊glb{%imp wake-2%sg-%refl uneasiness-%gen.%di child-%dat.%sg}
  ◊glfree{Wake up, child of unease;}
}

◊gloss/x{
  ◊glfree{◊nc{carie; varon cjašit ħare.}}
  ◊gla{carie; var-on cjaš-it ħar-e.}
  ◊glb{thank_goodness life-%nom.%di fall-%inf again-3%sg.%pfv}
  ◊glfree{Thank goodness you’re alive again.}
}

◊gloss/x{
  ◊glfree{◊nc{cerels têŋi’ce cfilsameca ša peni roc senþerin meftes?}}
  ◊gla{cer-els têŋ-i=’ce cfils-ameca ša pen-i roc senþ-erin meft-es?}
  ◊glb{house-%dat.%sg food-%dat.%co=and have_available-but %int what-%dat.%sg for grudge-%acc.%di retain-2%sg}
  ◊glfree{Although you have food and shelter, why do you hold onto a grudge?}
}

◊gloss/x{
  ◊glfree{◊nc{gvonros aračeþan crînan fesmirþ ondelt cenþ’ve jorniłit łanes.}}
  ◊gla{gvonr-os arač-eþan crîn-an fesm-irþ ond-elt cenþ=’ve jornił-it łan-es.}
  ◊glb{troubled_past-%loc.%sg sleep-%rel.%dat,%acc.%cel darkness-%acc.%di turn_toward-%ser now-%loc.%di self.%nom.%sg=%poss.2 stand_up-%inf must-2%sg}
  ◊glfree{Now you must stand up toward the darkness where you slept in those troubled times.}
}

◊gloss/x{
  ◊glfree{◊nc{visrec’âcþ velšit racfit anðes so cinras menrieve.}}
  ◊gla{visr-eca=’^cþ velš-it racf-it anð-es so cinr-as menr-ie-ve.}
  ◊glb{strength-%inst.%di=%poss.%rel move-%inf know_how_to-%inf not_yet-2%sg if start-%loc.%sg guide-1%sg.%pfv-2%sg}
  ◊glfree{If you don’t yet know how to move with your own strength, then I’ll teach you at the start.}
}

◊gloss/x{
  ◊glfree{◊nc{arðei eltes coðałes srefne;}}
  ◊gla{arð-ei elt-es coð-ałes srefn-e;}
  ◊glb{vigor-%gen.%sg river-%nom.%sg emotion-%dat.%gc tangle-3%sg}
  ◊glfree{The stream of vigor intertwines with feelings;}
}

◊gloss/x{
  ◊glfree{◊nc{telmaðei relcoc gašidas fesde;}}
  ◊gla{telmað-ei relc-oc gašid-as fesl-te;}
  ◊glb{tenacity-%gen.%di wing-%nom.%du shadow-%dat.%co lead_away_from-3%du}
  ◊glfree{The wings of resoluteless lead away from shadows;}
}

◊gloss/x{
  ◊glfree{◊nc{cþelfa asnîrleþans sreŋet emedrocþ cfiþar narmens’ve roħi.}}
  ◊gla{cþelf-a asnîrleþ-ans sreŋ-et emedr-ocþ cfiþ-ar narm-ens=’ve roħ-i.}
  ◊glb{glimmer-%rel.%nom,%nom.%cel recover-%rel.%dat,%dat.%cel work_into_a_thin_sheet-%rel.%acc,%sembl.%cel silver-%gen.%gc leaf-%nom.%co dignity-%dat.%di=%poss.2 guard-3%pl}
  ◊glfree{The leaves glimmering like silver foil guard your recovered dignity.}
}

◊gloss/x[#:id "line1"]{
  ◊glfree{◊nc{sênraþepa tfilres jas grîvelca sêrennels vêsros gcemalve.}}
  ◊gla{sênr-aþepa tfilr-es jas grîv-elca sêrn-nels vêsr-os g\cem-al-ve.}
  ◊glb{raise-%rel.%acc,%inst.%cel heart-%dat.%sg from ceremonial_knife-%inst.%sg stand-%supine strength-%dat.%di %pfv\give-1%sg.%inv-2%sg}
  ◊glfree{With this raised knife, I’ll give you strength to stand up from my heart.}
}

(The PV shows the synonymous ◊l1{jorniłnels} instead of ◊l1{sêrennels} in ◊xref/p{line1} due to a production error.)

◊gloss/x{
  ◊glfree{◊nc{endîr atrasnir vôr aspel saden cenra.}}
  ◊gla{end-îr atrasn-ir vôr asp-el sad-en cenr-a.}
  ◊glb{now-%nom.%di color-%dat.%pl full_of lifetime-%gen.%sg next-%gen.%sg start-%nom.%sg}
  ◊glfree{Now your next life, full of color, starts.}
}

◊gloss/x{
  ◊glfree{◊nc{elgren endîr’ce cþarmel’pe šimeþ;}}
  ◊gla{elgr-en end-îr=’ce cþarm-el=’pe šim-eþ;}
  ◊glb{here-%nom.%sg now-%nom.%sg=and magic-%gen.%co=%poss.1 end-%nom.%sg}
  ◊glfree{Here and now is the end of my magic.}
}

◊gloss/x{
  ◊glfree{◊nc{senlelþa’ve ðêf vargru.}}
  ◊gla{senl-elþa=’ve ðêf vargr-u.}
  ◊glb{effort-%abess.%co=%poss.2 occurrence-%loc.%gc stand_still-3%gc}
  ◊glfree{Without your effort, nothing will change.}
}

◊gloss/x{
  ◊glfree{◊nc{pelþit vedenecameca rindelc’âcþ cenþ’ve sênnotat łanes}}
  ◊gla{pelþ-it ve-denec-ameca rind-elca=’^cþ cenþ=’ve sênnot-at łan-es}
  ◊glb{advise-%inf 2%sg-continue-but desire-%inst.%di=%poss.%refl self.%nom.%sg=%poss.2 ascend-%inf must-2%sg}
  ◊glfree{I will continue to give you advice, but you must ascend on your own desire}
}

◊gloss/x{
  ◊glfree{◊nc{:gjavercþan avena enleas ragoþ ar.}}
  ◊gla{:gjavercþ-an aven-a enle-as rag-oþ ar.}
  ◊glb{damaged_by_strong_winds-%rel.%nom,%acc.%sg wasteland-%acc.%di far-%rel.%nom,%dat.%sg peak-%dat.%sg toward}
  ◊glfree{Toward the peak beyond the wind-battered wasteland.}
}

◊gloss/x{
  ◊glfree{◊nc{gciheve eþei mârdan enseran ŋgrenfos.}}
  ◊gla{gcih-eve eþ-ei mârdan enser-an ŋ\grenf-os.}
  ◊glb{reach-%inf.%loc existence-%gen.%di last.%acc.%cel brick-%acc.%cel %pfv\place-3%gc.%inv}
  ◊glfree{And when you arrive there, the last brick of your existence will be laid.}
}

◊gloss/x{
  ◊glfree{◊nc{eneða cajoþ gasnelsal;}}
  ◊gla{eneð-a caj-oþ g\asnels-al;}
  ◊glb{ash-%nom.%co ground-%dat.%sg %pfv\return-3%sg.%inv}
  ◊glfree{Ash returns to the ground;}
}

◊gloss/x{
  ◊glfree{◊nc{nenso teltos ar ganagriris.}}
  ◊gla{nens-o telt-os ar g\anagr-iris.}
  ◊glb{star-%nom.%pl cliff-%dat.%sg toward %pfv\hide-3%pl.%inv}
  ◊glfree{The stars hide behind the cliff.}
}

◊gloss/x{
  ◊glfree{◊nc{geðroga vatoren resvel cindoesliþ vifþens’moc ŋgeldra.}}
  ◊gla{geðrog-a vator-en resv-el cin-do-esl-iþ viv-þens=’moc ŋ\geldr-a.}
  ◊glb{foul_smelling-%rel.%nom,%nom.%sg cinnabar-%gen.%co fog-%nom.%di %refl-%caus-spread-%ser fly-%acc.%gc=also %pfv\suffocate-3%sg}
  ◊glfree{The foul-smelling vermillion fog spreads and chokes even the flies.}
}

◊gloss/x{
  ◊glfree{◊nc{le drênascin; telmaðei łirtês;}}
  ◊gla{le drên-as-cin; telmað-ei łirt-ês;}
  ◊glb{%imp wake-2%sg-%refl tenacity-%gen.%di child-%dat.%sg}
  ◊glfree{Wake up, child of resoluteness;}
}

◊gloss/x{
  ◊glfree{◊nc{łilicþ ineþen ciren mâra.}}
  ◊gla{łil-icþ ineþ-en cir-en mâr-a.}
  ◊glb{cloud-%sembl.%gen white-%gen.%sg castle-%nom.%sg wait-3%sg}
  ◊glfree{A castle as white as the clouds awaits you.}
}

◊gloss/x{
  ◊glfree{◊nc{refas mos’ve nasenil cerþel inolocþ arelit denecþal.}}
  ◊gla{ref-as mos-∅=’ve nasen-il cerþ-el inol-ocþ arel-it denecþ-al.}
  ◊glb{true-%rel.%nom,%nom.%sg mind-%nom.%sg=%poss.2 manmade_fire-%gen.%sg remnant-%gen.%sg stick-%sembl.%gc glow-%inf continue-3%sg.%pst.%impfv}
  ◊glfree{Your true soul kept glowing like the logs left after a campfire.}
}

◊gloss/x{
  ◊glfree{◊nc{ai asenen tesnerþ ndroneve arigas narecirþ esšorcrime}}
  ◊gla{ai asen-en tesn-erþ ndron-eve arig-as narec-irþ es-šorcr-ime}
  ◊glb{but anger-%gen.%di lightning-%acc.%st wake-%inf.%loc warmth-%dat.%di evoke-%ser %inch-burn-and}
  ◊glfree{But when a bolt of anger’s lightning struck, it started burning, evoking a sense of warmth,}
}

◊gloss/x{
  ◊glfree{◊nc{annêrtoþ creł tê *saðas cintlerfliþ čirnarþ gatrada se.}}
  ◊gla{annêrþ-toþ creł tê *sað-as cin-tlerfl-iþ čirn-arþ g\atrag-ta se.}
  ◊glb{evil_spirit-%dat.%sg against that.%cel mud-%dat.%di %refl-uncover-%ser brave-%ser %pfv\defeat-3%sg.%pst.%pfv %mir}
  ◊glfree{And it revealed itself from that mud against the evil spirit and bravely defeated it.}
}

◊section{Resource credits}

◊details{
    ◊summary{Music}
    ◊dl[#:class "infobox"]{
        ◊dt{DAW}◊dd{◊link["https://ardour.org/"]{Ardour} 8.6.0}◊;
        ◊dt{Instrument plugins}◊dd{◊link["https://surge-synthesizer.github.io/index.html"]{Surge XT}; ◊link["https://geonkick.org/"]{Geonkick}}◊;
        ◊dt{Other plugins}◊dd{MDA Overdrive (◊link["https://gitlab.com/drobilla/mda-lv2"]{LV2 port}); ◊link["https://lsp-plug.in/index.php"]{LSP} plugins}◊;
    }
}
