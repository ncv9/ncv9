#lang pollen

◊(require pollen/pagetree)

◊define-meta[pdf-type article]
◊define-meta[title]{Project Orikuchi}
◊define-meta[date]{2022-12-18 – ?}
◊define-meta[book-author]{+merlan #flirora}

◊blockquote[
    "https://en.wikipedia.org/wiki/Shinobu_Orikuchi"
    ◊@{— ◊cite/inbook{Shinobu Orikuchi} in ◊link["https://en.wikipedia.org/wiki/Shinobu_Orikuchi"]{◊cite{Wikipedia}}}]{
  ◊p{◊b{Shinobu Orikuchi} (◊ja{折口 信夫}, ◊ja-Latn{Orikuchi Shinobu}, 11 February 1887–3 September 1953), also known as ◊b{Chōkū Shaku} (◊ja{釋 迢空}, ◊ja-Latn{Shaku Chōkū}), was a Japanese ethnologist, linguist, folklorist, novelist, and poet.}
}

Adding transitive auxiliary verbs to Ŋarâþ Crîþ.

◊section{List of candidate verbs}

◊items{
    ◊item{◊b{◊nc{mîrm·arit, mîrm·ore, mîrm·arelta, mîrm·ore, mîrm·oreta, mîrm·araþor, mîrm·aseve}} ◊i{vdv:I₁·e·or/γ}: ◊S predicts that ◊O will hold for ◊|I|; ◊S expects, predicts, foresees ◊|I|. (Not sure about this.)}
    ◊item{◊b{◊nc{triłit, triłe, triłelta, triłas, triłelas, dtrełeve}} ◊i{vdv:II₄·as}: ◊S recommends ◊I to do ◊|O|.}
    ◊item{◊b{◊nc{lensat, lensa, lensalta, lense, lenseši, lensaþos, linseve}} ◊i{vdv:I₁·e·os/α}: ◊S helps ◊I do ◊|O|.}
    ◊item{◊b{◊nc{oreþmat, oreþma, oreþmalta, oreþma, oreþmena, oreþmaþos, goriþmeve}} ◊i{vdv:I₁·a·os/β}: ◊S commands ◊O to do ◊|I|.}
    ◊item{◊b{◊nc{alirnat, alirna, alirnalt, alirnon, alirnelon, galerseve}} ◊i{vdr:II₄·on}: ◊S promises to ◊I that ◊O is true.}
    ◊item{◊b{◊nc{melirnat, melirna, melernalt, melirniþ, melirnena, melernavas, melorneve}} ◊i{vdr:I₃x/β}: 2. ◊S entrusts ◊|I| with carrying out the task ◊|O|.}
}

◊section{Design}

Act on the case frame as ◊l0{do-} does or what?
