#lang pollen

◊(require pollen/pagetree)

◊define-meta[pdf-type article]
◊define-meta[title]{Project Nibiru}
◊define-meta[date]{2022-06-19 – 2022-06-25}
◊define-meta[book-author]{+merlan #flirora}

Reforming the Ŋarâþ Crîþ verb and relational inflection system.

◊section[#:id "goals"]{Goals}

◊items{
    ◊item{use the Project Elaine framework for verb conjugation and make use of stem fusion}
    ◊item{allow derivation from verbs to nouns (i.e. provide all necessary stems)}
    ◊item{support at least the same forms as the old system}
    ◊item{be aesthetically pleasing}
}

Relationals are included in the scope of this project because of the existence of finite and nominalized forms.

◊section[#:id "subjunctive"]{To subjunctive or not to subjunctive?}

Things that could possibly use a subjunctive mood:

◊items{
    ◊item{future tense}
    ◊item{negative polarity – not happening.}
    ◊item{optative mood, probably with a particle}
    ◊item{with nominalized VPs acting as adjuncts (e.g. locative and semblative cases), distinguish between a realized action and an intended but unrealized action}
}

In general, add marking for subjunctive in contexts where it isn’t already shown.

◊section[#:id "long-short"]{Short and long verbs}

Proposal: separate verbs into short (1 full syllable in stem) and long (2+ full syllables in stem) verbs.

The third-person singular present imperfective stays the same for short verbs but is the stem fused with null for long verbs.

This, however, produces ambiguity with verbs such as ◊l0{crešarat}, whose 3◊sc{sg} would be ◊l0{crešar}, which is also the 2◊sc{pl} of ◊l0{crešit}. This verb could still be classified as short because it is a compound of ◊l0{crešit} and ◊l0{sarat}, the latter of which is short. Also, verbs whose stems have only 1 syllable in the stem could be classified as long anyway if the final bridge is sufficiently complex (e.g. ◊l0{neftrit}).

Perhaps the condition should be whether fusion with null gives at least 2 syllables? Alternatively, perhaps long verbs should be rare even among 2+-syllable stems.

◊section[#:id "overview"]{Proposal overview}

Stems: I, N (nonpast), P (past), R (nominative-rcase relative), Q (other-rcase relative), L (locative)

Theme: ◊nc{◊ṫ[0]} (◊l0{a} or ◊l0{i}), with transformed forms ◊nc{◊ṫ[0]◊ẋ{◊ẋfr3}}, ◊nc{◊ṫ[0]◊ẋ{◊ẋv2}}, ◊nc{◊ṫ[0]◊ẋ{◊ẋv3}}

◊subsection[#:id "finite"]{Finite forms}

◊table/x[#:options (table-options #:caption ◊@{Conjugation of verbs according to the subject (direct aspect).})]{
  Person \ Number & Singular & Dual & Plural & Generic
      1st excl. & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}n} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}cþ} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}f}
      1st incl. & & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3 · ◊ẋht}n} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2 · ◊ẋht}cþ} &
      2nd & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}s} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}ns}* & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}r} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}f}
      3rd & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}n} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv3}} & ◊nc{-u}
}

◊table/x[#:options (table-options #:caption ◊@{Conjugation of verbs according to the subject (inverse aspect).})]{
  Person \ Number & Singular & Dual & Plural & Generic
      1st excl. & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}l} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}nis} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}cþis} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}f}
      1st incl. & & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3 · ◊ẋht}nis} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2 · ◊ẋht}cis} &
      2nd & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}res} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}nsis} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}ris} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}f}
      3rd & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}l} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}nis} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv3}ris} & ◊nc{-os}
}

Use the N stem for the present tense and the P stem for the past tense. If the aspect is perfective, then eclipse the form. For the past tense, suffix ◊l0{-þ} for the direct 1◊sc{sg}, 3◊sc{sg}, and 3◊sc{pl} forms; otherwise, suffix ◊l0{-ta} after the object affix or use the ◊nc{◊ṡ{P}◊ẋ{t}} stem (TODO: how to choose?).

The second-person dual direct suffix is ◊l0{-◊ṫ[0]◊ẋ{◊ẋv2}ns} if there are no subsequent suffixes but ◊l0{-◊ṫ[0]◊ẋ{◊ẋv2}n} if there are.

Object affixes remain unchanged.

◊table/x[#:options (table-options #:caption "Object affixes.")]{
  Person \ Number & Singular & Dual & Plural & Generic
  1st excl. & ◊nc{pe} & ◊nc{pjo} & ◊nc{po} & ◊nc{ten}
  1st incl. & & ◊nc{pjô} & ◊nc{pô} &
  2nd & ◊nc{ve} & ◊nc{vi} & ◊nc{vo} & ◊nc{ves}
  3rd celestial & ◊nc{le}
  3rd terrestrial & ◊nc{lu}
  3rd human & ◊nc{les} & ◊nc{lis} & ◊nc{los}
  3rd epicene & & ◊nc{li} & ◊nc{lo} & ◊nc{las}
  Reflexive & ◊colspan[4]{◊nc{cin}}
  Reciprocal & ◊colspan[4]{◊nc{riþ}}
}

add a distinct ‘equative’ form? (used to assert that one action is tantamount to another)

◊subsection[#:id "participle"]{Participle forms}

Notes:

◊items{
    ◊item{this is the bulkiest section}
    ◊item{genus 0 could use some simplification; can we also simplify 2?}
    ◊item{want to keep distinction between type I and type II genera}
}

◊subsection[#:id "converb"]{Converb forms}

infinitive form is ◊l0{◊ṡ{I}◊ṫ[0]t}

converb forms would probably use the I stem, but a different stem is a possibility for some of them

◊subsection[#:id "nominalized"]{Nominalized forms}

introduce distinct subjunctive forms for locative and semblative cases?

adnominal forms for last 4 cases not used that often; do we really want to keep them?

dative nominalized forms are used often, especially as objects of relationals, so probably want to shorten them to an inflected form

◊subsection[#:id "derivs"]{Derivations}

S stem of derived nouns derivable from N stem by modifying the final bridge

L stem exists separately and is definitely distinct from N (perhaps use it for locative, instrumental, & abessive nominalized forms?)

for calculus affixes, infix them in every stem

◊subsection[#:id "irregular"]{Irregular verbs}

might want more irregular verbs beyond ◊l0{eþit} and ◊l0{telit}

for the existing irregular verbs, 2◊sc{du} ends with a simple coda, but will this hold for future ones?
