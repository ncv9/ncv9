#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Orthography and phonology (Elaine)}
◊define-meta[short-title]{Orthography and phonology}

The phonology and orthography of Ŋarâþ Crîþ can be divided into eight layers in two modes (◊term{writing} and ◊term{speaking}):

◊items{
  ◊item{◊term{Layer 0} is the underlying morphographemic representation. Content in this layer exists structurally instead of linearly. In this grammar, text in this layer is written in double square brackets: ◊l0{tanc-a}.}
  ◊item{◊term{Layer 1} is the graphemic representation. This representation is subsequently exported to the spoken and written modes. Text in this layer is written with angle brackets: ◊l1{tanca}.}
  ◊item{◊term{Layer 2w} is the surface glyphic representation. This represents the sequence of Cenvos glyphs that is written, observing required ligatures and final forms. Text in this layer is written with double angle brackets: ◊l2w{tanca}; for a more interesting example, ◊l1{mencoc} becomes ◊l2w{◊lig{me}nco◊lig{c$}}.}
  ◊item{◊term{Layer 2w*} is an intermediate layer between 2w and 3w, in which discretionary ligatures are introduced to 2w text. For instance, ◊l2w{#flirora} can be realized as ◊l2w*{#fli◊lig{ro}◊lig{ra}}.}
  ◊item{◊term{Layer 3w} is the topological representation, showing optional ligatures as well as stroke order variations. Text in this layer is written with double angle brackets: ◊l3w{◊sym{t1;1}◊sym{a1;3}◊sym{n1;1}◊sym{c1;1}◊sym{a1;1}}. More interestingly, ◊l2w{◊lig{me}nco◊lig{c$}} could become ◊l3w{◊sym{me1;1}◊sym{n1;1}◊sym{c1;1}◊lig{◊sym{o1;1}◊sym{c$1;1}}}.}
  ◊item{◊term{Layer 4w} is the presentational representation, adding to 3w variations in the strokes themselves and how strokes within a glyph are joined. Text in this layer is written with double angle brackets: ◊l4w{◊sym{t1;1}◊sym{a1;3}◊sym{n1;1}◊sym{c1;1}◊sym{a1;1}}.}
  ◊item{◊term{Layer 2s} is the phonemic representation. We use slashes for this, as usual: ◊l2{tanka}.}
  ◊item{◊term{Layer 3s} is the phonetic representation, or what is pronounced. We use square brackets for this, as usual: ◊l3{tʰa⁴ɲcʰa²}.}
}

The conversions from 0 to 1, 1 to 2w, and 2s to 3s are functional: each valid input corresponds to exactly one output. The conversion from 1 to 2s is almost so, except when a ◊l1{&} is present. In the opposite direction, the conversions from 4w to 3w, from 3w to 2w*, and from 2w* to 2w are functional. Furthermore, for any conversion, it can be determined whether a given input can be converted into a given output without external information.

In addition, the conversion between 1 and 2w is bijective: valid layer-1 and layer-2w representations can be paired with each other.

◊section[#:id "layer-12"]{Layers 1 and 2w: Cenvos and its romanization}

Rather than starting at layer 0, we start at layers 1 and 2w.

◊term{Cenvos}, the native script of Ŋarâþ Crîþ, is written from right to left. This script can be analyzed on two levels: ◊term{graphemes}, which constitute the abstract level and ◊term{glyphs}, which are the characters being written. For instance, Cenvos has one grapheme romanized as ◊l1{c} that corresponds to two different glyphs: the non-final form ◊cenvos{c~} (denoted as ◊l2w{c}) and the final form ◊cenvos{c} (◊l2w{◊lig{c$}}). As another example, the sequence ◊cenvos{me} (◊l1{me} = ◊l2w{◊lig{me}}) consists of one glyph but two graphemes.

In this grammar, we primarily use the romanization, whose symbols largely map one-to-one with Cenvos graphemes. Cenvos has four kinds of graphemes:

◊items{
  ◊item{◊term{True letters} are graphemes that represent sounds.}
  ◊item{◊term{Markers}, while considered letters, do not represent sounds. Instead, they indicate that the words affected are treated specially. They occur on the level of a word and do not actively participate in morphology.}
  ◊item{◊term{Punctuation} includes the clause-end punctuation ◊l1{.}, ◊l1{;}, ◊l1{?}, and ◊l1{!}; the clitic boundary mark ◊l1{’}; the lenition mark ◊l1{·}; the grouping brackets ◊l1{{}}; and the quotation marks ◊l1{«»}.}
  ◊item{◊term{Digits} can be used to write short numerals.}
}

Of course, there is also the space.

◊(define (row c n r)
  `(@! ,(cenvos c) "&" ,(nc n) "&" ,(nc r)))

◊table/x[
  #:options (table-options
    #:caption "The graphemes of Ŋarâþ Crîþ. (The columns are read from left to right.)"
    #:colgroup '(c c rb c c rb c c c)
    #:first-col-header? #f
  )
]{
  Cen & Name & Rom & Cen & Name & Rom & Cen & Name & Rom
  ◊pseudoheader{True letters}
  ◊(row "c~" "ca" "c") & ◊(row "m" "ma" "m") & ◊(row "h" "ar" "h")
  ◊(row "e" "e" "e") & ◊(row "a" "a" "a") & ◊(row "ħ" "ħo" "ħ")
  ◊(row "n" "na" "n") & ◊(row "f" "fa" "f") & ◊(row "ê" "ên" "ê")
  ◊(row "ŋ~" "ŋa" "ŋ") & ◊(row "g" "ga" "g") & ◊(row "ô" "ôn" "ô")
  ◊(row "v" "va" "v") & ◊(row "p" "pa" "p") & ◊(row "â" "ân" "â")
  ◊(row "o" "o" "o") & ◊(row "t" "ta" "t") & ◊(row "u" "uħo" "u")
  ◊(row "s" "sa" "s") & ◊(row "č" "ča" "č") & ◊(row "w" "cełaŋa" "w")
  ◊(row "þ" "þa" "þ") & ◊(row "î" "în" "î") & ◊(row "x" "avarte" "x")
  ◊(row "š" "ša" "š") & ◊(row "j" "ja" "j") & ◊(row "y" "priþnos" "y")
  ◊(row "r" "ra" "r") & ◊(row "i" "i" "i") & ◊(row "z" "telrigjon" "z")
  ◊(row "l" "la" "l") & ◊(row "d" "da" "d")
  ◊(row "ł" "ła" "ł") & ◊(row "ð" "ða" "ð")
  ◊pseudoheader{Final forms and ligatures (layer 2w)}
  ◊(row "c" "" (lig "c$")) & ◊(row "me" "" (lig "me")) & ◊(row "ww" "" (lig "ww"))
  ◊(row "ŋ" "" (lig "ŋ$")) & ◊(row "mm" "" (lig "mm")) & ◊(row "xx" "" (lig "xx"))
  ◊(row "ee" "" (lig "ee")) & ◊(row "jâ" "" (lig "jâ")) & ◊(row "yy" "" (lig "yy"))
  ◊(row "em" "" (lig "em")) & ◊(row "âj" "" (lig "âj")) & ◊(row "zz" "" (lig "zz"))
  ◊pseudoheader{Markers}
  ◊(row "#" "carþ" "#") & ◊(row "×" "njor" "+*") & ◊(row "*" "nef" "*")
  ◊(row "+" "tor" "+") & ◊(row "@" "es" "@") & ◊(row "&" "sen" "&")
  ◊pseudoheader{Punctuation}
  ◊(row "." "gen" ".") & ◊(row "'" "ŋos" "’") & ◊(row "«" "fos" "«")
  ◊(row ";" "tja" ";") & ◊(row "·" "łil" "·") & ◊(row "»" "þos" "»")
  ◊(row "?" "šac" "?") & ◊(row "{" "rin" "{") & ◊(row "/" "jedva" "/")
  ◊(row "!" "cjar" "!") & ◊(row "}" "cin" "}") & ◊(row "-" "mivaf·ome" "-")
}

The letters ◊l1{w}, ◊l1{x}, ◊l1{y}, and ◊l1{z} are ◊term{USR letters}. These are used in foreign languages written in Cenvos to represent phonemes that are not approximated by the phonology of Ŋarâþ Crîþ. Each foreign orthography is free to assign them as it pleases.

Cenvos has two graphemes that change form at the end of the word: ◊l1{c} and ◊l1{ŋ}, as well as several ligatures. We do not distinguish these forms in the romanization.

The marker ◊l1{*} is used for foreign words, such as loanwords and foreign names. ◊l1{#} is used to prefix given names. ◊l1{+} is used to prefix surnames passed by native conventions (i.e. from parent to child within the same gender); ◊l1{+*} marks a surname passed using non-native conventions. Place names are prefixed with ◊l1{@}. ◊l1{#}, ◊l1{+}, ◊l1{+*}, and ◊l1{@} can all be used with ◊l1{*}, in which case ◊l1{*} occurs first. Note that ◊l1{+*} is a single letter of its own and not a ligature.

At the start of a word, ◊l1{&} indicates reduplication of an unspecified prefix of the rest of the word. For instance, ◊l1{&cên} can be pronounced as if it were ◊l1{cêcên} or ◊l1{cêncên}. (◊l1{&} occurs after all other markers in this case.) This usage is not productive in standard Ŋarâþ Crîþ, but it appears in a few words, as well as in some idiosyncratic cases. At the middle or the end of a word, or alone, it indicates ellipsis of part or all of the word, most often to abbreviate or censor a word. Lastly, ◊l1{&{}} is used similarly to the ellipsis in Western punctuation.

Markers can be applied to multi-word strings by surrounding the string with the delimiters ◊l1{{}}. In legal language, ◊l1{{}} are also used around phrases to resolve ambiguities.

The sentence punctuation ◊l1{.}, ◊l1{?}, and ◊l1{!} are used as expected. ◊l1{;} is used to separate two independent clause phrases within the same sentence. The quotation marks, ◊l1{«»}, are used around quotations, direct or indirect. A ◊l1{.} at the end of a quotation embedded within another sentence is omitted.

◊l1{’} is used to separate clitics from the rest of the word to which they are attached. ◊l1{·} indicates lenition; it could be described as a “letter modifier”. It is also used as a decimal point: officially, it is used after the most significant digit of an inexact numeral when written with digits, but it also used unofficially to write non-integers.

◊l1{/}, as its derivation from ◊l1{i} suggests, is used to separate the number of ◊i{mjari} from the number of ◊i{edva} when writing currency amounts.

The morpheme boundary marker, ◊l1{-}, is sometimes used metalinguistically to mark a morpheme boundary, but it is not strictly a part of layer 1.

Spaces are placed in the following places:

◊items{
  ◊item{between orthographic words, but not between a clitic and the word to which it is attached}
  ◊item{after (but not before) ◊l1{.}, ◊l1{;}, ◊l1{?}, and ◊l1{!}}
  ◊item{before ◊l1{«} and after ◊l1{»} (but not on the other sides)}
  ◊item{around ◊l1{&{}}}
}

[TODO: cover mentions of letters within the language, corresponding to v7 p17 “When letters or markers are referred to, … but the effects on other glyphs are not standardized”]

Digits are interchangeable with short-form numerals, but not with long-form numerals. They are also written right-to-left in Cenvos, with the most significant digit first: ◊cenvos{2A3} is 0x2A3 = 675.

◊table/x[
  #:options (table-options
    #:caption "The digits of Ŋarâþ Crîþ. (The columns are read from left to right.)"
    #:first-col-header? #f
  )
]{
  Cen & # & Cen & # & Cen & # & Cen & #
  ◊cenvos{0} & ◊nc{0} & ◊cenvos{1} & ◊nc{1} & ◊cenvos{2} & ◊nc{2} & ◊cenvos{3} & ◊nc{3}
  ◊cenvos{4} & ◊nc{4} & ◊cenvos{5} & ◊nc{5} & ◊cenvos{6} & ◊nc{6} & ◊cenvos{7} & ◊nc{7}
  ◊cenvos{8} & ◊nc{8} & ◊cenvos{9} & ◊nc{9} & ◊cenvos{A} & ◊nc{A} & ◊cenvos{B} & ◊nc{B}
  ◊cenvos{C} & ◊nc{C} & ◊cenvos{D} & ◊nc{D} & ◊cenvos{E} & ◊nc{E} & ◊cenvos{F} & ◊nc{F}
}



◊subsection[#:id "letternum"]{Letter numbering}

Sometimes, an integer must be assigned to each letter. In this case, the assignment shown in the table below is used. Note that numbers are not assigned fully sequentially. Furthermore, this function is valid only for layer 1 graphemes.

◊(define (row2 c n)
  `(@! ,(nc c) "&"
    (cls "figure" ,(my-number->string n 16)) "&"
    (cls "figure" ,(my-number->string n))))

◊table/x[
  #:options (table-options
    #:caption "Letter numbering in Ŋarâþ Crîþ. (The columns are read from left to right.)"
    #:colgroup '(c c rb c c rb c c c)
    #:first-col-header? #f
  )
]{
  Letter & Hex & Dec & Letter & Hex & Dec & Letter & Hex & Dec
  ◊pseudoheader{True letters}
  ◊(row2 "c" 0) & ◊(row2 "m" 32) & ◊(row2 "h" 17)
  ◊(row2 "e" 1) & ◊(row2 "a" 9) & ◊(row2 "ħ" 18)
  ◊(row2 "n" 2) & ◊(row2 "f" 10) & ◊(row2 "ê" 257)
  ◊(row2 "ŋ" 43) &◊(row2 "g" 11)  & ◊(row2 "ô" 260)
  ◊(row2 "v" 3) & ◊(row2 "p" 12) & ◊(row2 "â" 265)
  ◊(row2 "o" 4) & ◊(row2 "t" 13) & ◊(row2 "u" 19)
  ◊(row2 "s" 5) & ◊(row2 "č" 222) & ◊(row2 "w" -1)
  ◊(row2 "þ" 85) &◊(row2 "î" 14)  & ◊(row2 "x" -2)
  ◊(row2 "š" 94) & ◊(row2 "j" 110) & ◊(row2 "y" -3)
  ◊(row2 "r" 6) & ◊(row2 "i" 15) & ◊(row2 "z" -4)
  ◊(row2 "l" 7) & ◊(row2 "d" 16)
  ◊(row2 "ł" 119) & ◊(row2 "ð" 341)
  ◊pseudoheader{Markers}
  ◊(row2 "#" 20) & ◊(row2 "+*" 22) & ◊(row2 "*" 25)
  ◊(row2 "+" 21) & ◊(row2 "@" 23) & ◊(row2 "&" 26)
}

The ◊term{letter sum} of a word is the sum of all of its letters. This value is used in some of the noun declension paradigms.

It is theorized that letter numbers were assigned in the following manner:

◊items{
  ◊item{The basic true letters inherited from ◊term{Necarasso Cryssesa} (i.e. those corresponding to ◊l1{c e n v o s r l m a f g p t î i d h}) received sequential numbers from zero. The number of ◊l1{m} was changed due to superstitions against the number eight.}
  ◊item{◊l1{ŋ þ š ł č ð} received numbers based on what letter pairs (or triplets in the case of ◊l1{ð}) they were based on.}
  ◊item{◊l1{ê}, ◊l1{ô}, and ◊l1{â} were numbered as 256 + base glyph number.}
  ◊item{The other letters and the markers received sequential numbers after ◊l1{h}, skipping 0x18.}
}

◊subsection[#:id "collation"]{Collation}

The true letters and the markers are collated in their respective order, except for ◊l1{&}, which is ignored. Lenited letters are treated as their respective base letters, except when two words differ only by the presence or absence of a lenition mark, in which case the lenited variant is collated after the base letter: ◊l1{saga} < ◊l1{sag·a} < ◊l1{sada} < ◊l1{saħa}. Numerals are collated after all letters.

In a directory of personal names, entries are collated on surnames, with given names considered only when surnames are identical. Headings in such a list include the prefix up to an including the first true letter: ◊l1{+merlan #flirora} would be found under ◊l1{+m}.

Ordered items can be labeled using numerals (starting from 0) or letters. In the latter case, only the letters ◊l1{c e n v o s r l m a f g p t î i d h} are used.

◊subsection[#:id "numquotes"]{Numquotes}

A digit immediately preceding text surrounded by quotation or grouping marks constitutes a ◊term{numquote}. The digit is usually not pronounced in this case. Numquotes are mainly used for secondary purposes that lack any dedicated punctuation.

◊table/x[#:options (table-options
    #:caption "Numquotes in Ŋarâþ Crîþ."
    #:first-col-header? #f
    #:colgroup '(c d)
  )]{
  Numquote & Meaning
  ◊nc{B{}} & Contains parenthetical information: provides supplementary information. The sentence should still be grammatical without the parenthetical content.
  ◊nc{1{}} & Lists an alias of a referent mentioned by name.
  ◊nc{2{}} & Surrounds a key-value list. Used as such: ◊l1{2{3{&{}} 4{&{}} 3{&{}} 4{&{}}}}
  ◊nc{3{}} & Used for listing a key inside ◊l1{2{}}.
  ◊nc{4{}} & Used for listing a value inside ◊l1{2{}}. When not directly inside a ◊l1{2{}} numquote, marks a list: elements are delimited by spaces, and ◊l1{{}} can be used to insert multi-word elements.
  ◊nc{9{}} & Used to contain abbreviated quantities in the traditional currency system.
  ◊nc{*9{}} & Used to contain abbreviated quantities in a currency system other than the traditional one.
}

◊section[#:id "layer0"]{Layer 0}

The phonotactics of Ŋarâþ Crîþ can be expressed in terms of a state machine with five states: ◊${s} (◊term{syllabic}), ◊${g} (◊term{glide}), ◊${o} (◊term{onset}), ◊${n} (◊term{nuclear}), and ◊${\omega} (◊term{terminal}). Each transition defined in the state machine has a set of accepted payloads.

◊make-figure["elaine-fsm.svg" #:alt "The finite state machine describing the phonotactics of Ŋarâþ Crîþ."]{The finite state machine describing the phonotactics of Ŋarâþ Crîþ.}

A word, or rather the phonotactically relevant part thereof, starts in the syllabic state and ends in the terminal state.

◊items{
    ◊item{The ◊term{syllabic state}, ◊${s}, is reached between syllables. In this state, an initial can be accepted to transition to the glide state.}
    ◊item{The ◊term{glide state}, ◊${g}, is reached immediately after an initial. This state can accept a medial to transition to the onset state.}
    ◊item{From the ◊term{onset state}, ◊${o}, a vowel (also called a nucleus) leads to the nuclear state.}
    ◊item{From the ◊term{nuclear state}, ◊${n}, a simple coda can be accepted to transition back to the syllabic state. Alternatively, a simple or complex coda may be accepted to transition to the terminal state.}
    ◊item{The ◊term{terminal state} is the end state for a word and marks the end of the final syllable. There are no transitions from this state.}
}

The payloads associated with a transition are strings of ◊term{manifested grapheme phrases}. A manifested grapheme phrase is either a true letter not followed by a lenition marker (◊term{plain letter}), any of ◊l0{p t d č c g m f v ð} followed by a lenition mark (◊term{lenited letter}), or, word-initially, one of the digraphs ◊l0{mp vp dt nd gc ŋg vf ðþ lł} (◊term{eclipsed letter}). All other graphemes are ignored for the purposes of phonotactics.

A manifested grapheme phrase has a ◊term{base letter}. The base letter of a plain letter is itself. The base letter of a lenited letter is the letter without the lenition mark. The base letter of an eclipsed letter is the second letter of the digraph.

A vowel is any of ◊l0{e o a î i ê ô â u}. ◊l0{j} is a semivowel. All other manifested grapheme phrases are consonants.

An ◊term{effective plosive} is a manifested grapheme phrase whose base letter is any of ◊l0{p t d c g}. An ◊term{effective fricative} is a manifested grapheme phrase whose base letter is any of ◊l0{f v þ ð s š h ħ}.

A ◊term{hatted vowel} is one of ◊l0{î ê ô â}. All other vowels are ◊term{unhatted vowels}.

An ◊term{initial} is the beginning of a syllable and consists of one of the following:

◊items{
  ◊item{nothing at all}
  ◊item{a single consonant}
  ◊item{an effective plosive or fricative plus ◊l0{r} or ◊l0{l}}
  ◊item{any of ◊l0{cf cþ cs cš gv gð tf dv}; that is, a plosive plus a fricative of the same voicing, such that the plosive has a more retracted place of articulation than the fricative}
}

The set of valid initials is denoted by the capital Greek letter iota, ◊${\Iota}.

A ◊term{medial} may either be empty or ◊l0{j}. The set of medials is denoted by the capital Greek letter mu, ◊${\Mu}.

The set of vowels is denoted by the capital Greek letter nu, ◊${\Nu}.

A coda is either a ◊term{simple coda} or a ◊term{complex coda}. A simple coda is one of ◊l0{s r n þ rþ l t c f cþ} or nothing at all. A complex coda is one of ◊l0{st lt nt ns ls nþ lþ m}, with ◊l0{-m} used only in a handful of function words. The set of all simple codas is denoted by the capital Greek letter kappa, ◊${\Kappa}, and the set of all simple or complex codas is denoted by the capital Greek letter omega, ◊${\Omega}.

An ◊term{onset} is an initial plus a medial. A ◊term{bridge} is the coda of one syllable plus the onset of the following syllable.

◊subsection[#:id "layer0-pv"]{Validation}

Valid morphemes have addditional criteria that they must satisfy:

◊items{
    ◊item{All bridges must be valid.}
    ◊item{◊l0{j} cannot precede ◊l0{i}, ◊l0{î}, or ◊l0{u}.}
    ◊item{◊l0{h} cannot occur word-initially.}
    ◊item{Conversely, eclipsed letters may only occur word-initially.}
}

A bridge is ◊term{canonical} if it follows the maximal-onset principle; that is, if the onset has the maximal number of consonants for the given sequence of manifested grapheme phrases. For instance, ◊l0{-n-t-} and ◊l0{-r-þl-} are canonical, but ◊l0{-c-þ-} and ◊l0{-rþ-l-} are not (as they can be regrouped as ◊l0{-∅-cþ-} and to ◊l0{-r-þl-}).

A bridge is ◊term{valid} if it can arise as the result of ◊term{repairing} a canonical bridge. Bridge repair is intended to change a bridge that is awkward to pronounce into one that is less so. It has the following properties:

◊enum{
  ◊item{Lenition in the onset does not affect whether bridge repair preserves the bridge.}
  ◊item{The presence of ◊l0{j} in the onset has no influence on bridge repair.}
  ◊item{All bridges with a coda that is null, ◊l0{-r}, or ◊l0{-l} are unaffected by bridge repair.}
  ◊item{If a bridge with a complex initial ◊var{I} is not changed by bridge repair, then the bridge with an initial containing only the first consonant of ◊var{I} is also unchanged.}
}

Importantly, bridge repair is not idempotent: ◊l0{-sð-} is repaired to ◊l0{-ss-}, but ◊l0{-ss-} is repaired to ◊l0{-þ-}. In addition, bridge repair might yield the pseudo-coda ◊l0{-ŋ}, which changes the preceding medial and vowel.

The following subsections describe the rules for bridge repair. A bridge that is modified by one rule might be further changed by later rules.

◊subsubsection[#:id "bridge-rules-coalesce"]{Coalescence of ◊l0{-tš-}}

The bridge ◊l0{-tš-} is changed to ◊l0{-č-}.

◊subsubsection[#:id "bridge-rules-fortition"]{Fortition of ◊l0{h-} and ◊l0{ħ-}}

The onset ◊l0{h-} is fortited to ◊l0{c-} after ◊l0{-s}, ◊l0{-þ}, ◊l0{-rþ}, ◊l0{-t}, ◊l0{-c}, ◊l0{-f}, or ◊l0{-cþ}. ◊l0{hr-} and ◊l0{hl-} are fortited analogously.

The onset ◊l0{ħ-} is fortited to ◊l0{g-} after ◊l0{-t}, ◊l0{-c}, and ◊l0{-f}. ◊l0{ħr-} and ◊l0{ħl-} are fortited analogously.

◊subsubsection[#:id "bridge-rules-metathesis"]{Metathesis of ◊l0{t} before ◊l0{c} or ◊l0{g}}

◊l0{-tc-} and ◊l0{-tg-} are metathesized to ◊l0{-ct-} and ◊l0{-cd-}, respectively. Likewise, ◊l0{-tcr-}, ◊l0{-tcl-}, ◊l0{-tgr-}, and ◊l0{-tgl-} are metathesized to ◊l0{-ctr-}, ◊l0{-ctl-}, ◊l0{-cdr-}, and ◊l0{-cdl-}.

Similar bridges with lenited onsets, such as ◊l0{-tc·-} and ◊l0{-tg·r-} are treated analogously, with the resulting onset remaining lenited.

◊l0{t} is deleted before ◊l0{cf}, ◊l0{cþ}, ◊l0{cš}, ◊l0{cš}, ◊l0{gv}, and ◊l0{gð}, devoicing the last two of these.

◊subsubsection[#:id "bridge-rules-assimilation-n"]{Nasal assimilation}

For these rules, ◊l0{m·} is counted as a nasal, even though it is pronounced as a fricative.

◊l0{-t} before a nasal onset assimilates to ◊l0{-n}.

◊l0{-c} before a nasal onset assimilates to the ◊xref["elaine_phonology.html" "bridge-rules-ng" "Sub-subsection"]{pseudo-coda ◊l0{-ŋ}}. As a special case, ◊l0{-cŋ-} is repaired to ◊l0{-ŋ-} instead.

◊subsubsection[#:id "bridge-rules-denasalization"]{Denasalization of ◊l0{ŋ-}}

After ◊l0{-s}, ◊l0{-þ}, ◊l0{-rþ}, ◊l0{-f}, and ◊l0{cþ}, ◊l0{ŋ-} is denasalized to ◊l0{g-}.

◊subsubsection[#:id "bridge-rules-devoicing"]{Devoicing of ◊l0{v-} and ◊l0{ð-}}

After ◊l0{-þ}, ◊l0{-rþ}, ◊l0{-t}, ◊l0{-c}, ◊l0{-f}, and ◊l0{-cþ}, ◊l0{v-} devoices to ◊l0{f-} and ◊l0{ð-} devoices to ◊l0{þ-}. Additionally, ◊l0{ð-} is devoiced after ◊l0{-s}.

This process occurs analogously for the onsets ◊l0{vr-}, ◊l0{vl-}, ◊l0{ðr-}, and ◊l0{ðl-}, except that ◊l0{-þ} is deleted before ◊l0{vr-} and ◊l0{vl-} instead. Additionally, ◊l0{-rþCR-} onsets (with R = ◊l0{r} or ◊l0{l} and C = ◊l0{v} or ◊l0{ð}) are corrected to ◊l0{-RC-}.

As usual, similar rules apply to lenited onsets: ◊l0{v·} devoices to ◊l0{f·}, and ◊l0{ð·} is replaced with a copy of the preceding consonant.

◊subsubsection[#:id "bridge-rules-assimilation-ths"]{Assimilation of ◊l0{s} after ◊l0{þ}}

After a ◊l0{þ}, ◊l0{s} is replaced with ◊l0{þ}. Additionally, ◊l0{ss} is coalesced into ◊l0{þ}, unless it is not followed by a consonant and the latter ◊l0{s} arose from a ◊l0{ð} in the previous step.

◊subsubsection[#:id "bridge-rules-degemination"]{Degemination before another consonant}

◊l0{þ}, ◊l0{t}, ◊l0{c}, and ◊l0{f} are degeminated before another consonant in the onset; for instance, ◊l0{-ffr-} is corrected to ◊l0{-fr-}, and ◊l0{-ccs-} is corrected to ◊l0{-cs-}. ◊l0{td} is degeminated to ◊l0{d}, and ◊l0{cg} is degeminated to ◊l0{g}.

This rule also applies when the second instance of the degeminated consonant is lenited, in which case the first copy of the consonant is elided: ◊l0{-tt·l-} → ◊l0{-t·l-}.

◊subsubsection[#:id "bridge-rules-shorten4"]{Partial coda elision of bridges with ◊l0{-rþ} and ◊l0{-cþ} codas}

If the coda is ◊l0{-rþ}, then it becomes ◊l0{-r} before a fricative followed by ◊l0{r} or ◊l0{l}, or before the onsets ◊l0{cf-}, ◊l0{cþ-}, ◊l0{cs-}, ◊l0{cš-}, ◊l0{tf-}, and ◊l0{dv-}. Before any other two-letter onset, it becomes ◊l0{-þ}.

If the coda is ◊l0{-cþ}, then it is maintained before the onsets ◊l0{þ-}, ◊l0{š-}, ◊l0{m·-}, ◊l0{t-}, ◊l0{ħ-}, ◊l0{m·-}, or ◊l0{t·-}. Before ◊l0{cf-}, ◊l0{cþ-}, ◊l0{cs-}, ◊l0{cš-}, or ◊l0{tf-}, or before any of the onsets consisting of ◊l0{þ}, ◊l0{š}, or ◊l0{ħ} followed by ◊l0{r} or ◊l0{l}, the onset loses its first consonant, and ◊l0{cs-} additionally becomes ◊l0{þ-}. In all other cases, the coda becomes ◊l0{-þ}.

◊subsubsection[#:id "bridge-rules-ng"]{The pseudo-coda ◊l0{-ŋ}}

Nasal assimilation might produce the pseudo-coda ◊l0{-ŋ} instead of an actual (simple) coda. In this case, the preceding vowel becomes ◊l0{o} for ◊l0{a o u}, ◊l0{jo} for ◊l0{e i}, ◊l0{ô} for ◊l0{â ô}, and ◊l0{jô} for ◊l0{ê î}, with any glides merging with the preceding glide. The pseudo-coda itself becomes ◊l0{-r}.

◊subsection[#:id "concat"]{Concatenation}

Concatenating two morphemes invokes repair processes to maintain validity invariants. In addition, there are environments that may naturally (if rarely) occur within a morpheme but are repaired away when created by appending morphemes.

Deduplication, which occurs on concatenation, affects fricatives in the onset position that precede a non-hatted vowel followed by a homophonous manifested grapheme phrase:

◊items{
    ◊item{The onset ◊l0{f} or ◊l0{tf} followed by a non-hatted vowel then ◊l0{f} or ◊l0{p·} is replaced with ◊l0{t}.}
    ◊item{The onset ◊l0{þ} or ◊l0{cþ} followed by a non-hatted vowel then ◊l0{þ} or ◊l0{t·} is replaced with ◊l0{t}. In addition, a preceding ◊l0{þ} or ◊l0{cþ} coda is replaced with ◊l0{s}, and a preceding ◊l0{rþ} coda is replaced with ◊l0{r}.}
    ◊item{◊l0{h} followed by a non-hatted vowel then ◊l0{h} or ◊l0{c·} is replaced with ◊l0{p}.}
    ◊item{◊l0{v} followed by a non-hatted vowel then ◊l0{v} or ◊l0{m·} is replaced with ◊l0{n}.}
    ◊item{◊l0{ð} followed by a non-hatted vowel then ◊l0{ð} or ◊l0{d·} is replaced with ◊l0{ŋ}.}
    ◊item{◊l0{ħ} followed by a non-hatted vowel then ◊l0{ħ} or ◊l0{g·} is replaced with ◊l0{g}.}
}

Overall, concatenation invokes the following processes in order:

◊enum{
    ◊item{Any new instances of ◊l0{j} before ◊l0{i}, ◊l0{î}, or ◊l0{u} are elided.}
    ◊item{Deduplication rules are applied.}
    ◊item{Newly formed bridges are canonicalized and repaired.}
}

Note that deduplication happens before any canonicalization; for instance, appending the syllables ◊l0{reþ} and ◊l0{eþ} together gives ◊l0{reþeþ}, not ◊l0{reteþ} (although appending the stem ◊l0{reþ} to the suffix ◊l0{eþ} ◊em{does} give ◊l0{reteþ}).

◊subsection[#:id "stem-fusion"]{Stem fusion}

In Ŋarâþ Crîþ, a ◊term{stem} consists of one or more syllables followed by an onset. In addition, the final onset of a stem must not contain a lenited consonant.

◊term{Stem fusion} describes a set of related processes on a stem. Stem fusion with a null consonant turns a stem into a word (with a terminal end). Stem fusion with a non-null consonant combines a stem with one of ◊l0{t}, ◊l0{n}, or ◊l0{þ} into another stem.

To describe stem fusion, we use the following notation:

◊items{
  ◊item{◊${\mathcal{S}_{xy}} is the set of all morphemes with start type ◊${x} and end type ◊${y}, with ◊${x} and ◊${y} being one of ◊${s} (syllabic), ◊${g} (glide), ◊${o} (onset), ◊${n} (nuclear), or ◊${\omega} (terminal).
    ◊items{
      ◊item{◊${\mathcal{S}_{xy}^{n}} is the subset of ◊${\mathcal{S}_{xy}} whose elements undergo ◊${n} cycles from ◊${x} back to ◊${x}.
        ◊items{
          ◊item{Ex: ◊${\mathcal{S}_{xx}^{0}} contains only the empty string for all boundary types ◊${x}.}
          ◊item{Ex: ◊${\mathcal{S}_{sg}^{0}} is the set of all initials.}
          ◊item{Ex: ◊${\mathcal{S}_{no}^{1}} includes ◊l0{stafc} but not ◊l0{þþj} or ◊l0{tatag}.}
        }
      }
    }
  }
  ◊item{Given ◊${\alpha \in \mathcal{S}_{xy}} and ◊${\beta \in \mathcal{S}_{yz}}, ◊${\alpha\cat\beta \in \mathcal{S}_{xz}} is the result of appending ◊${\alpha} and ◊${\beta}, performing repair processes as necessary.
    ◊items{
      ◊item{Ex: if ◊${\alpha = \cv{feva} \in \mathcal{S}_{ss}} and ◊${\beta = \cv{ve} \in \mathcal{S}_{ss}}, then ◊${\alpha\cat\beta = \cv{fenave} \in \mathcal{S}_{ss}}.}
      ◊item{This operation is also defined for ◊${\alpha \in \mathcal{S}_{xo}} and ◊${\beta \in \mathcal{S}_{gz}}, in which case the glides at the end of ◊${\alpha} and the start of ◊${\beta} are merged.}
      ◊item{◊${\alpha\beta} is the result of appending ◊${\alpha} and ◊${\beta} ◊em{without} performing any repair processes.}
    }
  }
  ◊item{◊${\Iota = \mathcal{S}_{sg}^{0}} is the set of all initials.}
  ◊item{◊${\Mu = \mathcal{S}_{go}^{0} = \{\epsilon_{\Mu}, \cv{j}\}} is the set of all glides.}
  ◊item{◊${\Nu = \mathcal{S}_{on}^{0}} is the set of all vowels.}
  ◊item{◊${\Kappa = \mathcal{S}_{ns}^{0}} is the set of all simple codas.}
  ◊item{◊${\Omega = \mathcal{S}_{n\omega}^{0}} is the set of all codas, simple or complex.}
  ◊item{◊${\Gamma = \mathcal{S}_{ng}^{0}} is the set of all coda–onset pairs. The glide is not included because stems ending in ◊l0{j} are treated specially in stem fusion.}
  ◊item{◊${\Pi} is the set of effective plosives and fricatives – that is, the set of consonants that can form an initial when followed by ◊l0{l} or ◊l0{r}.}
  ◊item{◊${\Tau \subset \mathcal{S}_{so} \setminus \mathcal{S}_{so}^0} is the set of valid stems. A stem must contain at least one syllable, and its final onset must not contain a lenited consonant.}
}

Given a stem ◊${\tau \in \Tau}, ◊${\tau^{\epsilon} \in \mathcal{S}_{s\omega}} is the result of fusing ◊${\tau} with a null consonant, and ◊${\tau^{\theta} \in \mathcal{S}_{so}} is the result of fusing ◊${\tau} with a non-null consonant ◊${\theta \in \{\cv{n}, \cv{t}, \cv{þ}\}}.

The following variables are used: ◊${\Sigma_{xy} \in \mathcal{S}_{xy}}, ◊${\gamma \in \Gamma}, ◊${\iota \in \Iota}, ◊${\nu \in \Nu}, ◊${\kappa \in \Kappa}, ◊${\omega \in \Omega}, ◊${\theta \in \{\cv{n}, \cv{t}, \cv{þ}\}}.

Earlier rules take precedence over later ones. In addition, we use a shorthand for rules that yield a common sequence of syllables regardless of the fusion consonant: a rule such as ◊${\tau \leadsto \sigma}, where ◊${\tau \in \Tau} and ◊${\sigma \in \mathcal{S}_{ss}}, is interpreted as the rules ◊${\tau^\epsilon = \sigma_{s\omega}} and ◊${\tau^\theta = \sigma \cat \theta}.

Another shorthand used in this document is ◊${\tau \curvearrowright \tau'}, which implies ◊${\tau^\epsilon = (\tau')^\epsilon} and ◊${\tau^\theta = (\tau')^\theta}.

◊b{TODO:} The rules for stem fusion are not finalized and therefore are temporarily omitted.

◊subsubsection[#:id "fusion-properties"]{Properties of stem fusion}

Fusion with ◊l0{t} is invariant (i.e. yields the same stem as the original) only when the final onset of the stem is ◊l0{t-}.

Fusion with ◊l0{n} is invariant only when the final bridge of the stem is ◊l0{-nn-}.

Fusion with ◊l0{þ} is invariant only when the final onset of the stem is ◊l0{þ-} or ◊l0{cþ-}.

◊section[#:id "layer-2"]{Layer 2s}

◊b{TODO:} deal with complex codas before a clitic boundary

Traditionally, only manifested grapheme phrases are considered to be significant in the conversion from layer 1 to layer 2s. However, other graphemes such as punctuation can affect prosody.

◊table/x[
  #:options (table-options
    #:caption "Layer 1 to layer 2s conversions."
    #:colgroup '(c rb c c)
    #:first-col-header? #f
  )
]{
  MGPs & IPA & MGPs & IPA
  ◊nc{c} & k & ◊nc{p} & p
  ◊nc{e} & e & ◊nc{t} & t
  ◊nc{n nd} & n & ◊nc{č} & t͡ʂ
  ◊nc{ŋ ŋg} & ŋ & ◊nc{î} & ì
  ◊nc{v m· vp} & v & ◊nc{j} & j
  ◊nc{o} & o & ◊nc{i} & i
  ◊nc{s} & s & ◊nc{d dt} & d
  ◊nc{þ t·} & θ & ◊nc{ð d· ðþ} & ð
  ◊nc{š č·} & ʂ & ◊nc{h c·} & x
  ◊nc{r} & ɹ & ◊nc{ħ g·} & ʕ
  ◊nc{l lł} & l & ◊nc{ê} & è
  ◊nc{ł} & ɬ & ◊nc{ô} & ò
  ◊nc{m mp} & m & ◊nc{â} & à
  ◊nc{a} & a & ◊nc{u} & u̜
  ◊nc{f p·} & f & ◊nc{f· v· ð·} & ∅
  ◊nc{g gc} & ɡ
}

Layer 2 has a two-way tone contrast between vowels: the high tone (H) is the default, being contrasted with the low tone (L). For historical reasons, the presence or absence of a low tone on a vowel is called [±creaky].

◊section[#:id "layer-3"]{Layer 3s}

The conversion from layer 2s to layer 3s is comparatively more complex.

First, the following changes are made:

◊items/dense{
  ◊item{kθ → x͡θ}
  ◊item{ʕ → ħ / V[+creaky] _}
  ◊item{n → m / _ C[+labial] }
  ◊item{n → ɱ / _ C[+labiodental]}
  ◊item{n → n̪ / _ C[+dental] }
  ◊item{n → ɳ / _ C[+retroflex]}
  ◊item{n C₁[+velar] → ɲ C₁[+palatal]}
  ◊item{n → ŋ / _ C[+lateral] V[+front]}
  ◊item{sʂ → ʂː}
  ◊item{C₁={ɹ, ɬ} → w / C₁V _}
  ◊item{l → ɾ / V[+back] _ V}
  ◊item{θ → θ̠ / s_, _s, _ʂ}
  ◊item{ʂj → ʃ}
  ◊item{ʂ → ʃ / _ i}
  ◊item{t͡ʂj → t͡ʃ}
  ◊item{t͡ʂ → t͡ʃ / _ i}
  ◊item{C₁[+voiced] → C₁[−voiced, −aspirated] / C₂[−voiced]}
}

Plosives in a coda are unreleased. All unvoiced plosives and affricates outside of a coda are aspirated.

While Ŋarâþ Crîþ has two tone levels phonemically, their realizations in the phonetic level is more complex. It is common to describe phonetic tone using seven levels, from 0 (the lowest) to 6 (the highest). Each syllable has one or more tones.

In order to describe tone, we must introduce the concept of ◊term{“stress”}, which is placed according to the following rules:

◊items{
  ◊item{Syllables with a high tone have a priority over syllables with a low tone – that is, a syllable with a low tone will be selected only if the word in question has only low-tone syllables.}
  ◊item{If the coda of the final syllable is either empty, or it consits of only ◊l3{s} or ◊l3{n}, then the syllables are chosen in the order ◊b{2nd-to-last → 3rd-to-last → last → 4th-to-last → … → first}.}
  ◊item{If the coda of the final syllable is a complex coda, then the syllables are chosen in the order ◊b{last → 3rd-to-last → 2nd-to-last → 4th-to-last → … → first}.}
  ◊item{If the coda is anything else, then the syllables are chosen from end to start: ◊b{last → 2nd-to-last → 3rd-to-last → … → first}.}
  ◊item{Monosyllabic function words generally lack any stressed syllable.}
}

We also introduce the concept of a ◊term{tone accounting unit} (TAU), which is the level at which tones are realized. That is, the tone of a syllable depends only on the contents of the TAU in which it lies. Instances of content words occupy different TAUs from each other, but some function words occupy the same TAU as the preceding or following word (in particular, such words have no stressed syllable and are confined to a relatively fixed position):

◊items{
  ◊item{Head particles, nominalized verb particles, and monosyllabic determiners occupy the same TAU as the following word.}
  ◊item{◊l1{so}, monosyllabic relationals ... occupy the same TAU as the preceding word.}
}

(Stress is accounted by orthographic word, not by TAU.)

First, two adjacent vowels are fused into a diphthong if the vowels are not identical, the first vowel is stressed, the second vowel is ◊l3{i} or ◊l3{u̜}, and the syllable to which the second vowel belongs can be interpreted as having an empty coda. For purposes of tonekeeping, a diphthong is considered to be composed of two different syllables.

In general, unstressed H and L syllables have tone levels 4 and 2, respectively; stressed H and L syllables have tone levels 5 and 1. However, an open H or L syllable before a stressed syllable gets level 3 or 1, respectively, instead. Diphthongs get different values: 65 for HH, 53 for HL, 13 for LH, and 21 for LL.

If two adjacent copies of an identical vowel have the same tone level at this stage, then the one closer to the stressed syllable rises by one tone level and the one farther from it falls by one level.

A tone level of ◊var{n} is then changed into a tone contour in the following situations, unless doing so would result in an out-of-bounds tone level:

◊items{
  ◊item{◊var{n} to (◊var{n} : ◊var{n} + 1): when the coda is ◊l3{st} or ◊l3{x͡θ}}
  ◊item{◊var{n} to (◊var{n} : ◊var{n} − 1): when the coda is ◊l3{rθ} or ◊l3{ns}}
  ◊item{◊var{n} to (◊var{n} + 1 : ◊var{n}): when the nucleus is preceded by two or more voiceless consonants}
}

In addition, other syllables change their tone levels:

◊items{
  ◊item{Raise the tone level by 1 (if it is not already 6) if the coda is a voiceless fricative, or if the coda is ◊l3{x͡θ}.}
  ◊item{Lower the tone level by 1 if the coda is ◊l3{ɹ}.}
  ◊item{Lower the tone level by 1 if the coda is a nasal followed by a voiced obstruent or nasal.}
}

Finally, if all tones have a level of 4 or higher, then the lowest tone (breaking ties by preferring later tones) is lowered to 3, and all other tones in the same syllable are lowered by the same amount. All level-3 tones are then lowered to level 2.

◊subsection[#:id "isochrony"]{Isochrony}

The isochrony of Ŋarâþ Crîþ falls somewhere between syllable and mora timing, where:

◊items{
  ◊item{The body of a syllable is always 1 unit long.}
  ◊item{The coda of a syllable is between 0 and 1 unit long, with the hierarchy ◊l2{t, k < n < l, ɹ < f, s, θ, ɹθ, kθ < st, lt, ns, ls, nθ}.}
  ◊item{Codas are shortened after two consecutive vowels: for instance, the ◊l1{l} in ◊l1{moriel} is pronounced for less time than that in ◊l1{mjarel}.}
}

◊section[#:id "mutations"]{Mutations}

Ŋarâþ Crîþ has two kinds of initial mutations: ◊term{lenition} and ◊term{eclipsis}. Neither kind of mutation has any effect on plosive-fricative onsets or any of ◊l0{r l n ŋ ħ}.

Lenition tends to turn plosives into fricatives and is indicated with a middle dot ◊l0{·} after the consonant affected. In particular, it affects ◊l0{p t d č c g m f v ð}. (See ◊xref["phonology.html" "layer-2" "Section" #:type 'inline]{Layer 2} for pronunciation details.) Partial lenition does not affect any of ◊l0{f v ð}; that is, it does not lenite consonants that would become silent. Unless otherwise qualified, lenition refers to total lenition, which affects ◊l0{f v ð}.

In a word containing ◊l0{&}, both instances of the reduplicated prefix are lenited. For example, ◊l1{&d·enfo} can be pronounced as ◊l3{ðeðenfo} but not as *◊l3{ðedenfo}.

Lenition occurs in the following environments:

◊items{
  ◊item{On the stem in abessive forms of nouns in paradigms 7, 8, 9, 10, and 13}
  ◊item{On a noun modified by ◊l1{šinen} or ◊l1{nemen} when used as determiners, if that noun is not a form of ◊l1{ðên}}
  ◊item{Partially, on a noun modified by ◊l1{ruf} not immediately following it}
  ◊item{Partially, on a noun modified by ◊l1{mê} immediately preceding it}
  ◊item{On a terrestrial noun modified by a participle-form verb belonging to a Type I genus}
  ◊item{To a dative-case nominalized verb phrase as explained in ◊xref["verbs.html" "nominalized" "Section" #:type 'inline]{Nominalized forms}}
  ◊item{Partially, on a verb when receiving the comparative prefixes ◊l0{mir-} or ◊l0{ła-}}
  ◊item{On a classifier attached to the numeral ◊l1{ces} or any numeral ending in ◊l1{ħas} or ◊l1{sreþas}}
  ◊item{On the second item of a compound noun, if it is neither terrestrial nor a form of ◊l1{vês}}
  ◊item{On a verb with the cessative prefix ◊l0{car-} or the terminative prefix ◊l0{er-}}
}

Eclipsis tends to add voice to voiceless consonants and change voiced stops into nasals. It is indicated by prefixing a consonant: ◊l0{t d c g f þ ł} become ◊l0{dt nd gc ŋg vf ðþ lł}, respectively. ◊l0{p} becomes ◊l0{vp} before any of ◊l0{i e u î ê} and ◊l0{mp} elsewhere. If a word starts with a vowel, then it is eclipsed by prefixing ◊l0{g}.

In a word containing ◊l0{&}, only the first instance of the reduplicated prefix is eclipsed. For example, ◊l1{n&denfin} can be pronounced as ◊l3{nedenfin} but not as *◊l3{nenenfin}.

Eclipsis occurs in the following environments:

◊items{
  ◊item{On the genitive dual, plural, and singulative forms of nouns}
  ◊item{On a noun modified by ◊l1{lê} or ◊l1{tê} immediately preceding it}
  ◊item{On a noun modified by ◊l1{dân}}
  ◊item{On a finite form of a verb or relational with perfective aspect}
  ◊item{To a locative, instrumental, or abessive-case nominalized verb phrase that is not an object of a modifying relational, as explained in ◊xref["verbs.html" "nominalized" "Section" #:type 'inline]{Nominalized forms}}
  ◊item{On a short numeral modified by ◊l1{ceþe}}
}

Lenition can happen on any syllabic onset of a word, but eclipsis is limited to word-initial positions.

In this documentation, lenition is sometimes marked with an empty circle ◊|sei|, and eclipsis with an filled circle ◊|uru|. Partial lenition is marked with an empty triangle ◊|sei2|.

◊section[#:id "loanwords"]{Loanwords}

Almost all loanwords in Ŋarâþ Crîþ are nouns.

Generally, when borrowing from languages that use the Cenvos script or a script related to it, and whose orthographies in the script in question do not deviate too far from Ŋarâþ Crîþ usage, Ŋarâþ Crîþ prefers to borrow the word graphemically than phonemically.

◊section[#:id "typography"]{The typography of Ŋarâþ Crîþ}

◊b{TODO:} This section has not changed and is therefore omitted.
