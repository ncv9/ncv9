#lang pollen

◊(require pollen/pagetree)

◊define-meta[pdf-type article]
◊define-meta[title]{Project Caladrius}
◊define-meta[date]{2022-01-30 – 2022-06-14}
◊define-meta[book-author]{+merlan #flirora}

◊blockquote[
    "http://bestiary.ca/beasts/beast143.htm"
    ◊@{— ◊cite/inbook{Caladrius} in ◊link["http://bestiary.ca/beasts/beast143.htm"]{◊cite{The Medieval Bestiary}}}]{
  ◊p{A bird which can tell if a sick man will die, and can cure disease.}
}

Reforming the Ŋarâþ Crîþ noun declension system.

◊section[#:id "current-state"]{The current state of Ŋarâþ Crîþ noun declension}

◊table/x[#:options (table-options #:caption "Properties of declension paradigms.") #:id "paradigm-summary"]{
  Paradigm & Gender & Principal parts & Lemma ending & Gen./Loc. & Inst./Abess.
  0 & C & NLS & ◊nc{-0} & I & Suffix
  1 & C & NLS & ◊nc{-0s} & I & Suffix
  2 & C & NLS & ◊nc{-0þ} & I & Suffix
  3 & C & NLS & ◊nc{-0r} & II & Suffix
  4 & C & NGLS & ◊nc{-i[ns]} & II* & Suffix
  5 & C & NLS & None & IV & Suffix
  6 & C & NLS & ◊nc{-0l} & II & Suffix
  7 & T & NLS & ◊nc{-os} & III & Circumfix
  8 & T & NLS & ◊nc{-or} & III & Circumfix
  9 & T & NLS & ◊nc{-on} & III & Circumfix
  10 & T & NAGLS & ◊nc{-el} & III & Circumfix
  11 & any & NLS & None & IV & Suffix
  12 & any & NS & None & V & Suffix
  13 & C & NLS & ◊nc{-Vn} & I* & Circumfix
  14, 14r & C & NLS & ◊nc{-V(r)þ} & I* & Suffix
}

Here, the genitive/locative type is one of the following:

◊items{
  ◊item{◊b{I:} genitive ◊l0{-2n -2cþ -3n -2ns -3f}, locative ◊l0{-0s -esac -1s -2ns -0f}}
  ◊item{◊b{I*:} genitive ◊l0{-il -0c(il) -0vi -VrVn -?}, locative same as I}
  ◊item{◊b{II:} genitive ◊l0{-4i -4ci -4vi -4ħin -0st}, locative ◊l0{-Vlt -Vlt0c -Vlt -Vnþ -Vf}}
  ◊item{◊b{II*:} genitive ◊l0{-en -enco -eþ -ens -efin}, locative same as II}
  ◊item{◊b{III:} genitive ◊l0{-el -acel -iel -ełan -e}, locative ◊l0{-os -osac -or -oren -ef}}
  ◊item{◊b{IV:} genitive ◊l0{-a -ac -o -en -ef}, locative same as I}
  ◊item{◊b{V:} genitive ◊l0{-es -ec -eris -tel -e}, locative ◊l0{-a -ac -o -en -af}}
}

◊subsection[#:id "current-state-aliasing"]{Aliasing}

◊items{
  ◊item{5, 11, and 12 alias with each other and with any other paradigm other than 0, although the latter is not common because 5, 11, and 12 mostly house nouns with monosyllabic lemmas}
  ◊item{2 and 14r alias with each other}
}

◊subsection[#:id "current-state-statistics"]{Statistics}

Using the following code:

◊pre{◊;
for p in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14r; do
  echo -n "$p: ";
  grep -E '# [^:]+: noun (celestial|terrestrial|human) (singular|collective|mass) '"$p"' ' dictionary/ncv9.kaltoklel | wc -l;
done◊;
}

we get the following statistics for the number of nouns in each paradigm:

◊table/x[#:options (table-options #:caption "Statistics for nominal declension paradigms." #:colgroup '(c fig) #:placement 'forcehere) #:id "paradigm-statistics"]{
  Paradigm & #
  0 & 164
  1 & 16
  2 & 11
  3 & 26
  4 & 23
  5 & 16
  6 & 8
  7 & 21
  8 & 8
  9 & 14
  10 & 7
  11 & 17
  12 & 4
  13 & 19
  14 & 6
  14r & 2
}

Note that the overwhelming majority of nouns are in paradigm 0. If we don’t change the lemmas of some nouns to end with a consonant, then we could divide the space of vowel-final nouns more finely to compensate.

◊subsection[#:id "current-state-stress"]{Interactions with stress}

The current noun declension paradigms are agnostic to stress. For example, ◊l1{sâna} ◊trans{bear}, which has ultimate stress, is declined similarly to ◊l1{arla} ◊trans{needle}, which has penultimate stress.

◊subsection[#:id "current-state-thematic-derivatives"]{Derivatives of thematic vowels}

◊table/x[#:options (table-options #:caption "Current derivatives of thematic vowels. 3* = 3₂′, 3₄, 3₆." #:placement 'forcehere) #:id "thematic-derivatives-current"]{
  0 & 1 & 2 & 3₀ & 4 & 3₂ & 3* & 1₃′=2 & 0₆′ & 4₆
  a & o & e & i & a & i & i & e & e & a
  o & e & e & i & a & i & i & & o & e
  e & i & i & i & e & o & e & i
  i & i & i & & e & o & e & i & e & e
}

◊section[#:id "issues"]{Outstanding issues with current draft}

◊items{
  ◊item{it doesn’t exist yet}
}

◊section[#:id "syncretism"]{Homophony}

Homophony between different cases is ◊strong{absolutely unacceptable}, even between different numbers. Ŋarâþ Crîþ depends on strong distinctions between the core cases. After all, the difference between the accusative and the dative is the one between protecting your friend from your enemies and protecting your enemies from your friend.

Homophony for forms of the same case but different numbers is by far less bad.

◊section[#:id "declension-model"]{Declension model}

◊(define (ncv . text)
  `(cls "varcell"
    (,@(apply nc text)
     ,(hatnote "varcell-note" "Variable entry" "Δ"))))

◊i{Familiarity with the material in ◊link["elaine.html"]{Project Elaine} is expected.}

There are three types of components when it comes to declensions: ◊term{constants}, ◊term{stems}, and ◊term{themes}

◊items{
  ◊item{◊term{Constants} stay the same across a paradigm.}
  ◊item{A ◊term{stem} is a variable that consists of one or more syllables followed by an onset that does not contain a lenited consonant.}
  ◊item{A ◊term{theme} is a short variable, usually one letter long, that is constrained to be one of a predetermined number of options. Themes include both ◊term{thematic vowels} and ◊term{thematic consonants}.}
}

Most paradigms specify an ◊term{N}, an ◊term{L}, and an ◊term{S stem}. The N stem is used for the first four cases, the L for the next three, and S for the semblative case. Some paradigms specify additional stems.

The main thematic vowel of nouns, which is found in the nominative singular, is denoted by ◊nc{◊ṫ[0]} (the Greek letter theta, for ◊i{◊mark{th}eme}). Some nouns have a locative thematic vowel, which is denoted by ◊nc{◊ṫ[1]}, for ◊i{◊mark{l}ocative}.

The thematic consonant (or consonant cluster), if any, is denoted by ◊nc{◊ṫ[2]}, for ◊lang["grc"]{◊i{◊mark{σ}ῠ́μφωνος}}.

Each major category is given an archetype paradigm for the most common or representative shape of the lemma.

◊section[#:id "decl1p"]{The first declension (penultimate)}

Guidelines:

◊items{
  ◊item{◊nc{◊ṫ[0]} can be ◊l0{a}, ◊l0{e}, or ◊l0{o}.}
  ◊item{◊nc{◊ṫ[1]} can be either ◊l0{a} or ◊l0{e}, but is more frequently ◊l0{a}.}
  ◊item{◊nc{◊ṡ{L}} must be different from ◊nc{◊ṡ{N}}. The most common difference is to change the final vowel of ◊nc{◊ṡ{N}}.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
  ◊item{◊nc{◊ṡ{N}} cannot end in ◊l0{-nn} if ◊nc{◊ṫ[0]} is ◊l0{e}; otherwise, ◊nc{◊ṡ{N}◊ẋ{n}} is the same as ◊nc{◊ṡ{N}}.}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for first-declension -V nouns.
  ◊sup{1} ◊l0{i} in the suffix becomes ◊l0{e} after the onsets ◊l0{t-}, ◊l0{d-}, ◊l0{s-}, ◊l0{þ-}, ◊l0{ð-}, ◊l0{tf-}, or ◊l0{dv-}, as well as any onsets that end with ◊l0{l}.
} #:placement 'forcehere) #:id "declensions-1-v"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊ncv{◊ṡ{N}◊ṫ[0]} & ◊nc{◊ṡ{N}◊ṫ[0]c} & ◊ncv{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋpl}} & ◊ncv{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}l} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}f}
  Accusative & ◊ncv{◊ṡ{N}◊ṫ[0]n} & ◊nc{◊ṡ{N}ôr} & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊ncv{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}efen}
  Dative & ◊ncv{◊ṡ{N}◊ṫ[0]s} & ◊nc{◊ṡ{N}◊ẋ{t}◊ṫ[0]s} & ◊ncv{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}i} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}þ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}fes}
  Genitive & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}n} & ◊nc{◊ṡ{N}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋgen}n} & ◊nc{◊ṡ{N}in} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}n} & ◊nc{◊ṡ{N}◊ẋ{n}ef}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]s} & ◊nc{◊ṡ{L}◊ṫ[1]c} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋpl}s} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋgen}ns} & ◊nc{◊ṡ{L}ef}
  Instrumental & ◊nc{◊ṡ{L}eca} & ◊nc{◊ṡ{L}ecca} & ◊nc{◊ṡ{L}ica}◊sup{1} & ◊nc{◊ṡ{L}inca}◊sup{1} & ◊nc{◊ṡ{L}efca}
  Abessive & ◊nc{◊ṡ{L}eþa} & ◊nc{◊ṡ{L}ecþa} & ◊nc{◊ṡ{L}iþa}◊sup{1} & ◊nc{◊ṡ{L}inþa}◊sup{1} & ◊nc{◊ṡ{L}efþa}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for first-declension nouns.} #:placement 'forcehere) #:id "declensions-1-variable"]{
  Form & ◊l0{-◊ṫ[0]} & ◊l0{-◊ṫ[0]s} & ◊l0{-◊ṫ[0]◊ẋ{◊ẋht}þ}
  Allowed ◊nc{◊ṫ[0]} & ◊nc{a}, ◊nc{e}, ◊nc{o} & ◊nc{a}, ◊nc{e} & ◊nc{a}, ◊nc{e}
  Nominative direct & ◊nc{◊ṡ{N}◊ṫ[0]} & ◊nc{◊ṡ{N}◊ṫ[0]s} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}þ}
  Nominative plural & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋpl}} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋpl · ◊ẋht}þ}
  Nominative singulative & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}l} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen · ◊ẋht}þ}
  Accusative direct & ◊nc{◊ṡ{N}◊ṫ[0]n} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}ns} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}ns}
  Accusative singulative & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}◊ẋ{n}je} & ◊nc{◊ṡ{N}◊ẋ{n}je}
  Dative direct & ◊nc{◊ṡ{N}◊ṫ[0]s} & ◊nc{◊ṡ{N}o} & ◊nc{◊ṡ{N}◊ṫ[0]s}
  Dative plural & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}i} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}ri} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}si}
}

◊section[#:id "decl2p"]{The second declension (penultimate)}

Guidelines:

◊items{
  ◊item{◊nc{◊ṫ[1]} can be either ◊l0{e} or ◊l0{i}.}
  ◊item{Has a separate ◊nc{◊ṡ{G}} stem, which can be same as or different from ◊nc{◊ṡ{N}}.}
  ◊item{◊nc{◊ṡ{L}} can potentially be the same as ◊nc{◊ṡ{N}} but is usually different from it. The difference is usually more substantial than a change in the final vowel of the stem.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
}

◊subsection[#:id "decl2p-ic"]{◊l0{-in} and ◊l0{-is} nouns}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for ◊l0{-in} and ◊l0{-is} nouns.
} #:placement 'forcehere) #:id "declensions-2p-ic"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊ncv{◊ṡ{N}◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}jor} & ◊ncv{◊ṡ{N}a◊ṫ[2]} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgenp}n} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}s}
  Accusative & ◊ncv{◊ṡ{N}◊ẋ{n}e} & ◊nc{◊ṡ{N}◊ẋ{n}ec} & ◊nc{◊ṡ{N}eri} & ◊nc{◊ṡ{N}eħin} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}ns}
  Dative & ◊nc{◊ṡ{N}◊ẋ{t}ês} & ◊nc{◊ṡ{N}ecþ} & ◊nc{◊ṡ{N}erþ} & ◊nc{◊ṡ{N}erin} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}þ}
  Genitive & ◊ncv{◊ṡ{G}en} & ◊ncv{◊ṡ{G}jôr} & ◊ncv{◊ṡ{G}eþ} & ◊ncv{◊ṡ{G}◊ẋ{n}es} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}st}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]lt} & ◊nc{◊ṡ{L}◊ṫ[1]lt◊ṫ[0]c} & ◊nc{◊ṡ{G}◊ṫ[1]◊ẋ{e×i}lt} & ◊nc{◊ṡ{G}◊ṫ[1]lten} & ◊nc{◊ṡ{L}◊ṫ[1]f}
  Instrumental & ◊nc{◊ṡ{L}◊ṫ[1]lca} & ◊nc{◊ṡ{L}◊ṫ[1]lhac} & ◊nc{◊ṡ{L}◊ṫ[1]lco} & ◊nc{◊ṡ{L}◊ṫ[1]lcen} & ◊nc{◊ṡ{L}◊ṫ[1]lcaf}
  Abessive & ◊nc{◊ṡ{L}◊ṫ[1]lþa} & ◊nc{◊ṡ{L}◊ṫ[1]lþac} & ◊nc{◊ṡ{L}◊ṫ[1]lþo} & ◊nc{◊ṡ{L}◊ṫ[1]lþen} & ◊nc{◊ṡ{L}◊ṫ[1]lþaf}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for second-declension penultimate nouns.} #:placement 'forcehere) #:id "declensions-2p-variable"]{
  Form & ◊l0{-in}, ◊l0{-is} & ◊l0{-◊ṫ[0]◊ẋ{◊ẋht}r}
  Allowed ◊nc{◊ṫ[0]} & ◊nc{i} & ◊nc{e}, ◊nc{i}
  Nominative direct & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}◊ṫ[2]}
  Nominative plural & ◊nc{◊ṡ{N}a◊ṫ[2]} & ◊nc{◊ṡ{N}î◊ẋ{◊ẋht}◊ṫ[2]}
  Accusative direct & ◊nc{◊ṡ{N}◊ẋ{n}e} & ◊nc{◊ṡ{N}◊ẋ{n}el}
  Genitive direct & ◊nc{◊ṡ{G}en} & ◊nc{◊ṡ{G}il}
  Genitive dual & ◊nc{◊ṡ{G}jôr} & ◊nc{◊ṡ{G}◊ẋ{t}il}
  Genitive plural & ◊nc{◊ṡ{G}eþ} & ◊nc{◊ṡ{G}evi}
  Genitive singulative & ◊nc{◊ṡ{G}◊ẋ{n}es} & ◊nc{◊ṡ{G}◊ẋ{n}il}
}

◊section[#:id "decl2u"]{The second declension (ultimate)}

Guidelines:

◊items{
  ◊item{As in IIp, ◊nc{◊ṫ[1]} can be either ◊l0{e} or ◊l0{i}.}
  ◊item{Does not have a separate ◊nc{◊ṡ{G}} stem.}
  ◊item{◊nc{◊ṡ{L}} can potentially be the same as ◊nc{◊ṡ{N}} but is usually different from it. The difference is usually more substantial than a change in the final vowel of the stem.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for vowel + ◊l0{r} nouns.
} #:placement 'forcehere) #:id "declensions-2u-vr"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}◊ṫ[0]c} & ◊ncv{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgenp}n} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}s}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]rin} & ◊nc{◊ṡ{N}j◊ṫ[0]◊ẋ{◊ẋfr2 · ◊ẋht}r} & ◊nc{◊ṡ{N}eri} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]rþ} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}ns}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]ls} & ◊nc{◊ṡ{N}◊ẋ{t}el} & ◊nc{◊ṡ{N}ari} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]ls} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}þ}
  Genitive & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}i} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}ci} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}vi} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}ħin} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}st}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]lt} & ◊nc{◊ṡ{L}◊ṫ[1]lt◊ṫ[0]c} & ◊nc{◊ṡ{N}◊ṫ[1]◊ẋ{e×i}lt} & ◊nc{◊ṡ{N}◊ṫ[1]lten} & ◊nc{◊ṡ{L}◊ṫ[1]f}
  Instrumental & ◊nc{◊ṡ{L}◊ṫ[1]lca} & ◊nc{◊ṡ{L}◊ṫ[1]lhac} & ◊nc{◊ṡ{L}◊ṫ[1]lco} & ◊nc{◊ṡ{L}◊ṫ[1]lcen} & ◊nc{◊ṡ{L}◊ṫ[1]lcaf}
  Abessive & ◊nc{◊ṡ{L}◊ṫ[1]lþa} & ◊nc{◊ṡ{L}◊ṫ[1]lþac} & ◊nc{◊ṡ{L}◊ṫ[1]lþo} & ◊nc{◊ṡ{L}◊ṫ[1]lþen} & ◊nc{◊ṡ{L}◊ṫ[1]lþaf}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for second-declension ultimate nouns.} #:placement 'forcehere) #:id "declensions-2u-variable"]{
  Form & ◊l0{-◊ṫ[0]r} & ◊l0{-◊ṫ[0]l} & ◊l0{-◊ṫ[0]þ} & ◊l0{-◊ṫ[0]rþ}
  Allowed ◊nc{◊ṫ[0]} & ◊nc{a}, ◊nc{e}, ◊nc{i} & ◊nc{a}, ◊nc{i}, ◊nc{o} & ◊nc{a}, ◊nc{e} & ◊nc{a}, ◊nc{e}
  Nominative plural & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}o◊ṫ[2]} & ◊nc{◊ṡ{N}o◊ṫ[2]}
}

◊section[#:id "decl3"]{The third declension}

Guidelines:

◊items{
  ◊item{There is no thematic vowel.}
  ◊item{◊nc{◊ṡ{L}} must be different from ◊nc{◊ṡ{N}}. The most common difference is to change the final vowel of ◊nc{◊ṡ{N}}.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}). In any case, it must be distinct from ◊nc{◊ṡ{L}}.}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for third-declension -os nouns.
} #:placement 'forcehere) #:id "declensions-3-os"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊ncv{◊ṡ{N}os} & ◊nc{◊ṡ{N}oc} & ◊ncv{◊ṡ{N}or} & ◊ncv{◊ṡ{N}oren} & ◊nc{◊ṡ{N}u}
  Accusative & ◊ncv{◊ṡ{N}on} & ◊ncv{◊ṡ{N}◊ẋ{t}on} & ◊ncv{◊ṡ{N}◊ẋ{þ}on} & ◊nc{◊ṡ{N}elt} & ◊nc{◊ṡ{N}an}
  Dative & ◊nc{◊ṡ{N}oþ} & ◊nc{◊ṡ{N}◊ẋ{t}oþ} & ◊nc{◊ṡ{N}asor} & ◊nc{◊ṡ{N}◊ẋ{n}es} & ◊nc{◊ṡ{N}as}
  Genitive & ◊nc{◊ṡ{N}el} & ◊nc{◊ṡ{N}◊ẋ{t}el} & ◊nc{◊ṡ{N}jel} & ◊nc{◊ṡ{N}◊ẋ{n}el} & ◊nc{◊ṡ{N}◊ẋ{n}e}
  Locative & ◊nc{◊ṡ{L}os} & ◊nc{◊ṡ{L}ocþ} & ◊nc{◊ṡ{L}or} & ◊nc{◊ṡ{L}oren} & ◊nc{◊ṡ{L}ef}
  Instrumental & ◊ncv{cja◊ṡ{L}os} & ◊ncv{cja◊ṡ{L}ocþ} & ◊nc{cja◊ṡ{L}or} & ◊nc{cja◊ṡ{L}olt} & ◊nc{cja◊ṡ{L}ef}
  Abessive & ◊ncv{þja○◊ṡ{L}os} & ◊ncv{þja○◊ṡ{L}ocþ} & ◊nc{þja○◊ṡ{L}or} & ◊nc{þja○◊ṡ{L}olt} & ◊nc{þja○◊ṡ{L}ef}
  Semblative & ◊nc{◊ṡ{S}ot} & ◊nc{◊ṡ{S}octos} & ◊nc{◊ṡ{S}et} & ◊nc{◊ṡ{S}ełi} & ◊nc{◊ṡ{S}ocþ}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for third-declension nouns.} #:placement 'forcehere) #:id "declensions-3-variable"]{
  Form & ◊l0{-os} & ◊l0{-on} & ◊l0{-or}
  Nominative default & ◊nc{◊ṡ{N}os} & ◊nc{◊ṡ{N}on} & ◊nc{◊ṡ{N}or}
  Nominative plural & ◊nc{◊ṡ{N}or} & ◊nc{◊ṡ{N}or} & ◊nc{◊ṡ{N}osôr}
  Nominative singulative & ◊nc{◊ṡ{N}oren} & ◊nc{◊ṡ{N}oren} & ◊nc{◊ṡ{N}ons}
  Accusative default & ◊nc{◊ṡ{N}on} & ◊nc{◊ṡ{N}anon} & ◊nc{◊ṡ{N}on}
  Accusative dual & ◊nc{◊ṡ{N}◊ẋ{t}on} & ◊nc{◊ṡ{N}anor} & ◊nc{◊ṡ{N}◊ẋ{t}on}
  Accusative plural & ◊nc{◊ṡ{N}◊ẋ{þ}on} & ◊nc{◊ṡ{N}anor} & ◊nc{◊ṡ{N}◊ẋ{þ}on}
  Instrumental default & ◊nc{cja◊ṡ{L}os} & ◊nc{cja◊ṡ{L}on} & ◊nc{cja◊ṡ{L}or}
  Instrumental dual & ◊nc{cja◊ṡ{L}ocþ} & ◊nc{cja◊ṡ{L}oc} & ◊nc{cja◊ṡ{L}oc}
  Abessive default & ◊nc{þja○◊ṡ{L}os} & ◊nc{þja○◊ṡ{L}on} & ◊nc{þja○◊ṡ{L}or}
  Abessive dual & ◊nc{þja○◊ṡ{L}ocþ} & ◊nc{þja○◊ṡ{L}oc} & ◊nc{þja○◊ṡ{L}oc}
}

◊l0{-el} nouns have additional A and G stems and thus have their own declension:

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for third-declension -el nouns.
} #:placement 'forcehere) #:id "declensions-3-el"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}el} & ◊nc{◊ṡ{N}oc} & ◊nc{◊ṡ{N}or} & ◊nc{◊ṡ{N}ons} & ◊nc{◊ṡ{A}ul}
  Accusative & ◊nc{◊ṡ{A}en} & ◊nc{◊ṡ{A}◊ẋ{t}en} & ◊nc{◊ṡ{A}on} & ◊nc{◊ṡ{N}elt} & ◊nc{◊ṡ{A}an}
  Dative & ◊nc{◊ṡ{A}oþ} & ◊nc{◊ṡ{N}◊ẋ{t}oþ} & ◊nc{◊ṡ{A}asor} & ◊nc{◊ṡ{N}◊ẋ{n}es} & ◊nc{◊ṡ{A}as}
  Genitive & ◊nc{◊ṡ{G}el} & ◊nc{◊ṡ{G}◊ẋ{t}el} & ◊nc{◊ṡ{G}ol} & ◊nc{◊ṡ{G}◊ẋ{n}el} & ◊nc{◊ṡ{A}◊ẋ{n}e}
  Locative & ◊nc{◊ṡ{L}os} & ◊nc{◊ṡ{L}ocþ} & ◊nc{◊ṡ{L}or} & ◊nc{◊ṡ{L}oren} & ◊nc{◊ṡ{L}ef}
  Instrumental & ◊nc{cja◊ṡ{L}el} & ◊nc{cja◊ṡ{L}els} & ◊nc{cja◊ṡ{L}or} & ◊nc{cja◊ṡ{L}olt} & ◊nc{cja◊ṡ{L}ef}
  Abessive & ◊nc{þja○◊ṡ{L}el} & ◊nc{þja○◊ṡ{L}els} & ◊nc{þja○◊ṡ{L}or} & ◊nc{þja○◊ṡ{L}olt} & ◊nc{þja○◊ṡ{L}ef}
  Semblative & ◊nc{◊ṡ{S}ot} & ◊nc{◊ṡ{S}octos} & ◊nc{◊ṡ{S}et} & ◊nc{◊ṡ{S}ełi} & ◊nc{◊ṡ{S}ocþ}
}

In this case, the N and G stems must be distinct.

◊section[#:id "decl4"]{The fourth declension}

Guidelines:

◊items{
  ◊item{◊nc{◊ṫ[0]} is ◊l0{o} for terrestrial nouns and ◊l0{a} for celestial nouns. Naturally, it can be either for human nouns.}
  ◊item{◊nc{◊ṫ[1]} can be either ◊l0{a} or ◊l0{e}.}
  ◊item{Some vowels in the paradigm depend on the letter sum of a certain form modulo some integer:
    ◊items{
      ◊item{◊nc{◊ḋ[0]} = ◊l0{e a i a i e} « ◊sc{nom.di} if ◊nc{◊ṫ[0]} = ◊l0{a}; otherwise ◊l0{o}}
      ◊item{◊nc{◊ḋ[1]} = ◊l0{i i a i e i} « ◊sc{nom.di} if ◊nc{◊ṫ[0]} = ◊l0{a}; otherwise ◊l0{e}}
    }
  }
  ◊item{◊nc{◊ṡ{L}} ◊strong{must be different} from ◊nc{◊ṡ{N}}. This is a departure from paradigms 5 and 11 on the old system, as the final coda of the nominative direct is no longer mutated in certain forms. The easiest way to fix these two stems being the same is to change the final bridge of ◊nc{◊ṡ{N}}.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for fourth-declension nouns.
  ◊sup{1} ◊nc{◊ṡ{N}es} if ◊nc{◊ṡ{N}} ends in ◊l0{j}.
  ◊sup{2} ◊l0{i} in the suffix becomes ◊l0{e} after the onsets ◊l0{t-}, ◊l0{d-}, ◊l0{s-}, ◊l0{þ-}, ◊l0{ð-}, ◊l0{tf-}, or ◊l0{dv-}, as well as any onsets that end with ◊l0{l}.
} #:placement 'forcehere) #:id "declensions-4"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ẋ{ε}} & ◊nc{◊ṡ{N}ec} & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ḋ[1]n} & ◊nc{◊ṡ{N}af}
  Accusative & ◊nc{◊ṡ{N}◊ḋ[0]n} & ◊nc{◊ṡ{N}◊ẋ{t}◊ḋ[0]n} & ◊nc{◊ṡ{N}as} & ◊nc{◊ṡ{N}◊ḋ[0]nþ} & ◊nc{◊ṡ{N}afen}
  Dative & ◊nc{◊ṡ{N}i}◊sup{1} & ◊nc{◊ṡ{N}ic} & ◊nc{◊ṡ{N}ir} & ◊nc{◊ṡ{N}ên} & ◊nc{◊ṡ{N}afes}
  Genitive & ◊nc{◊ṡ{N}a} & ◊nc{◊ṡ{N}ac} & ◊nc{◊ṡ{N}o} & ◊nc{◊ṡ{N}ân} & ◊nc{◊ṡ{N}◊ẋ{n}ef}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]s} & ◊nc{◊ṡ{L}◊ṫ[1]c} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋpl}s} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋgen}ns} & ◊nc{◊ṡ{L}ef}
  Instrumental & ◊nc{◊ṡ{L}eca} & ◊nc{◊ṡ{L}ecca} & ◊nc{◊ṡ{L}ica}◊sup{2} & ◊nc{◊ṡ{L}inca}◊sup{2} & ◊nc{◊ṡ{L}efca}
  Abessive & ◊nc{◊ṡ{L}eþa} & ◊nc{◊ṡ{L}ecþa} & ◊nc{◊ṡ{L}iþa}◊sup{2} & ◊nc{◊ṡ{L}inþa}◊sup{2} & ◊nc{◊ṡ{L}efþa}
  Semblative & ◊colspan*[5 'merged]{Same as I (III) for ◊nc{◊ṫ[0]} = ◊nc{a} (◊nc{o})}
}

◊section[#:id "decl5"]{The fifth declension}

Guidelines:

◊items{
  ◊item{This paradigm has only N and S stems, with no L stem.}
  ◊item{◊nc{◊ṫ[0]} is any ◊em{nucleus}.}
  ◊item{◊nc{◊ṫ[2]} is a (possibly empty) simple coda other than ◊l0{-c} or ◊l0{-t}, with a transformation ◊nc{◊ẋ{◊ẋ*mod}}.}
  ◊item{Some variables in the paradigm depend on the letter sum of a certain form modulo some integer:
    ◊items{
      ◊item{◊nc{◊ḋ[0]} = ◊l0{e a i ô u o î ê â} « ◊sc{nom.di}}
      ◊item{◊nc{◊ḋ[1]} = ◊l0{o ô ô o} « ◊sc{nom.gc}}
      ◊item{◊nc{◊ḋ[2]} = ◊l0{i i e e} « ◊sc{nom.du}}
      ◊item{◊nc{◊ḋ[3]} = ◊l0{g d v} « ◊sc{acc.di}, increment until ◊nc{◊ḋ[3]} ≠ the last onset of ◊nc{◊ṡ{N}}}
      ◊item{◊nc{◊ḋ[4]} = ◊l0{e a i u a e o i jâ jê jô jê jâ o} « ◊sc{acc.pl}, increment until ◊nc{◊ḋ[4]} ≠ ◊nc{◊ṫ[0]}}
    }
  }
  ◊item{◊nc{◊ṡ{N}} is not necessraily a stem, as it is not required to have at least one full syllable.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for fifth-declension nouns.
  ◊sup{1} ◊nc{◊ṡ{N}◊ḋ[1]t} if ◊nc{◊ṡ{N}} does not contain at least one full syllable
} #:placement 'forcehere) #:id "declensions-5"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}◊ẋ{t}◊ḋ[1]c}◊sup{1} & ◊nc{◊ṡ{N}◊ḋ[0]◊ṫ[2]} & ◊nc{(◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod})◊ẋ{t}e} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}u}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}◊ḋ[2]n} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}jor} & ◊nc{◊ṡ{N}◊ḋ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}◊ḋ[2]◊ẋ{e×i}n} & ◊nc{(◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod})◊ẋ{t}en} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}an}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}er} & ◊nc{◊ṡ{N}◊ṫ[0]◊ḋ[3]a◊ṫ[2]} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}ir} & ◊nc{(◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod})◊ẋ{t}es} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}as}
  Genitive & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}es} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}ec} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}eris} & ◊nc{(◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod})◊ẋ{t}el} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}e}
  Locative & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}a} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}ac} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}o} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}en} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}af}
  Instrumental & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eca} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}ehac} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}ego} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}egen} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}ecaf}
  Abessive & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eþa} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eþac} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eðo} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eðen} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eþaf}
  Semblative & ◊nc{◊ṡ{S}ot} & ◊nc{◊ṡ{S}octos} & ◊nc{◊ṡ{S}et} & ◊nc{◊ṡ{S}ełi} & ◊nc{◊ṡ{S}ocþ}
}

◊table/x[#:options (table-options #:caption ◊@{The ◊nc{◊ẋ{◊ẋ*mod}} transformation for the fifth declension.} #:placement 'forcehere #:first-col-header? #f) #:id "declensions-5-zhe"]{
  ◊nc{◊ṫ[2]} & ◊nc{◊ṫ[2]◊ẋ{◊ẋ*mod}}
  ∅ & ◊nc{ħ}
  ◊nc{s} & ◊nc{r}
  ◊nc{r} & ◊nc{r}
  ◊nc{n} & ◊nc{nþ}
  ◊nc{þ} & ◊nc{s}
  ◊nc{rþ} & ◊nc{rs}
  ◊nc{cþ} & ◊nc{cs}
  ◊nc{l} & ◊nc{ł}
  ◊nc{f} & ◊nc{m}
}

◊section[#:id "decl6"]{The sixth declension}

Guidelines:

◊items{
  ◊item{◊nc{◊ṫ[0]} can be ◊l0{e} or ◊l0{i}.}
  ◊item{◊nc{◊ṡ{N}} cannot end with ◊l0{-nn-}.}
  ◊item{◊nc{◊ṡ{L}} must be different from ◊nc{◊ṡ{N}}.}
  ◊item{This paradigm additionally has I and I′ stems, which are usually similar to the L stem.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for sixth-declension nouns.
  ◊sup{1} ◊nc{◊ṡ{N}ien}, ◊nc{◊ṡ{N}◊ẋ{n}ien} for ◊l0{-in} nouns
} #:placement 'forcehere) #:id "declensions-6"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]n} & ◊nc{◊ṡ{N}jor} & ◊nc{◊ṡ{N}in}◊sup{1} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}l} & ◊nc{◊ṡ{N}u}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]n◊ṫ[0]◊ẋ{ei/ae}} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ṫ[0]n◊ṫ[0]◊ẋ{ei/ae}n} & ◊nc{◊ṡ{N}an}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]ns} & ◊nc{◊ṡ{N}◊ṫ[0]ŋa} & ◊nc{◊ṡ{N}eri} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]s} & ◊nc{◊ṡ{N}as}
  Genitive & ◊nc{◊ṡ{N}il} & ◊nc{◊ṡ{N}◊ẋ{t}il} & ◊nc{◊ṡ{N}evi} & ◊nc{◊ṡ{N}◊ẋ{n}in}◊sup{1} & ◊nc{◊ṡ{N}◊ẋ{n}e}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]s} & ◊nc{◊ṡ{L}◊ṫ[1]c} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋpl}s} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋgen}ns} & ◊nc{◊ṡ{L}ef}
  Instrumental & ◊nc{cja◊ṡ{I}es} & ◊nc{cja◊ṡ{I}ecþ} & ◊nc{cja◊ṡ{I′}o} & ◊nc{cja◊ṡ{I′}ans} & ◊nc{cja◊ṡ{I}ef}
  Abessive & ◊nc{þja○◊ṡ{I}es} & ◊nc{þja○◊ṡ{I}ecþ} & ◊nc{þja○◊ṡ{I′}o} & ◊nc{þja○◊ṡ{I′}ans} & ◊nc{þja○◊ṡ{I}ef}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
}

◊section[#:id "external-links"]{Links}

◊items{
  ◊item{◊link["caladrius_old.html"]{Oudated material for Project Caladrius} (before the completion of the initial version of Project Elaine).}
}
