#lang pollen

◊(require pollen/pagetree)

◊; For some reason, expressions don’t evaluate in a define-meta, so we
◊; can’t just use ◊cite[#:lang "zxx"]{n-v-km-l-s-zioa}.
◊define-meta[title]{◊@{Musings on ◊cite[((lang "zxx"))]{n-v-km-l-s-zioa}}}
◊define-meta[date]{2022-01-15}

I recently stumbled on a song titled ◊link["https://youtu.be/ac3e7BzAieI"]{◊cite[#:lang "zxx"]{n-v-km-l-s-zioa}} by ◊ja{テンタクル忍者} featuring KAFU (note to self: that name doesn’t stand for ◊i{Konlangs Are Fucking Unbased}). According to the ◊link["https://web.archive.org/web/20211101131804/https://tentacle-ninjya.goat.me/FSH5DIcu1a"]{liner notes (archived)}, the lyrics are just keyboard spam, so I would’ve disliked the video and moved on had it not been for the catchy music and visuals.

◊section{A first dive into the lyrics}

Most of the words in the lyrics are one or two letters long, particularly in the first two stanzas. The longest word is ◊i{◊lang["zxx"]{voepfxo}}, at seven letters.

Conspicuous are the words ◊i{◊lang["zxx"]{i}} and ◊i{◊lang["zxx"]{j}}, which each begin one of the stanzas in the song on a separate line. They both appear elsewhere inline in the lyrics. If I had to assign a meaning to them, then I’d guess that they both mean something like ◊trans{and} or ◊trans{furthermore}, with no distinction like in ŊCv9’s ◊l1{=’ce} and ◊l1{ai}.

There are two different punctuation marks in the lyrics: ◊i{◊lang["zxx"]{,}} (the comma) and ◊i{◊lang["zxx"]{;}} (the semicolon). There are no periods in the lyrics, though they might have simply been omitted by convention. (They ◊em{are} usually omitted in Japanese lyrics.) The spacing around punctuation is inconsistent, but I’d doubt that there’s anything systematic with that.

Talking about the written words is fine and dandy, but what about the pronunciation? Fortunately, the author links to a ◊link["https://piapro.jp/t/3Gxw"]{Piapro page} where the lyrics are written out in kana.

The letter ◊i{◊lang["zxx"]{c}} appears only once on the first line: ◊i{◊lang["zxx"]{af cm uh n go x ui a}} with the pronunciation given by ◊i{◊ja{あふかあんごくすいあ}}, suggesting that ◊i{◊lang["zxx"]{cm}} is pronounced as ◊i{◊ja{か}}, which sounds more like ◊l3{kʰɤ̝} in the actual song. ◊i{◊lang["zxx"]{m}} also seems to be used as a sort of vowel in the word ◊i{◊lang["zxx"]{lmja}} ◊l3{ɺə̆ja}.

The second line, ◊i{◊lang["zxx"]{lm z se g n vz ja}} ◊l3{ɺemɯ zɯ se gɯ nɤ tsi a} has another example of a consonant letter (◊i{◊lang["zxx"]{v}}) being used for a vowel sound.

◊i{◊lang["zxx"]{x}} appears more frequently and seems to correspond to ◊l2{ks} most of the time. Note that a vowel can intervene between the two consonants, such as in the first line, in which ◊i{◊lang["zxx"]{go x ui a}} is pronounced as ◊l3{ɡo kʰɯ se̝i̯ a}. In some cases (such as in the third stanza), ◊i{◊lang["zxx"]{x}} is pronounced as ◊l2{s} or ◊l2{ɕ}. In other cases, it is pronounced as ◊l2{sk}, as in ◊i{◊lang["zxx"]{voepfxo}} ◊l3{voːɸ̆sɯkio} (no, I’m not making this up).

◊i{◊lang["zxx"]{j}} is either ◊l2{i~j} or ◊l2{dʑ}, and in one place, ◊l2{e}. ◊i{◊lang["zxx"]{z}} is usually ◊l2{t͡s} but occasionally ◊l2{z}.

There are four one-letter words that are vowels: ◊i{◊lang["zxx"]{i}} and ◊i{◊lang["zxx"]{j}} (both pronounced ◊l3{i}), ◊i{◊lang["zxx"]{a}} (◊l3{a}), and ◊i{◊lang["zxx"]{e}} (◊l3{e}). There are 14 one-consonant words: ◊i{◊lang["zxx"]{b}} (◊l3{bɯ}, ◊l3{ba}), ◊i{◊lang["zxx"]{d}} (◊l3{de}), ◊i{◊lang["zxx"]{f}} (◊l3{∅}, ◊l3{ɸa}, ◊l3{ɸi}), ◊i{◊lang["zxx"]{g}} (◊l3{gɯ}) ◊i{◊lang["zxx"]{h}} (◊l3{çi}), ◊i{◊lang["zxx"]{k}} (◊l3{ka}, ◊l3{kʰɯ}), ◊i{◊lang["zxx"]{l}} (◊l3{ɺa}, ◊l3{ɺi}, ◊l3{ɺe}), ◊i{◊lang["zxx"]{m}} (◊l3{me}), ◊i{◊lang["zxx"]{n}} (◊l3{n}, ◊l3{ni}), ◊i{◊lang["zxx"]{r}} (◊l3{ɺe}) ◊i{◊lang["zxx"]{s}} (◊l3{sɯ}), ◊i{◊lang["zxx"]{v}} (◊l3{ʋə}, ◊l3{ve}, ◊l3{vɯ}), ◊i{◊lang["zxx"]{x}} (◊l3{ks}, ◊l3{eks}), and ◊i{◊lang["zxx"]{z}} (◊l3{zɯ}, ◊l3{ze}, ◊l3{t͡sɯ}). I’m surprised that some of the consonants were unused.

Two-letter words that don’t seem to have any vowels fall into one of two classes: either one of the letters secretly represents a vowel, or a vowel is inserted somewhere in the word.

If you weren’t already convinced that the author just spaced up some keyboard smash and made up a pronunciation that seemed to make sense to the untrained listener, then I hope you are now.

◊section{Man, this slaps}

Should I mention that the author of the song drew the picture and made the video themselves?

Since the keyboard smash was the only bad thing about this song, I’ve been thinking of creating a conlang with similar aesthetics (albeit with Cenvos, not the Latin script), spoken in Njôro as Ŋarâþ Crîþ is.

Question: how to evolve the features mentioned earlier? Ŋarâþ Crîþ is a special child in Njôro, having slid by without too much adherence to diachronics. In-world, you could chalk it up to heavy language planning that the speakers just went with, but I’d rather not set up a similar situation again.

In general, kill off most of the morphology and wear down the words to be short. After the latest orthographic reform, massage allophones of some phonemes into being distinct, merging a few of them with other phonemes.

There’s some implied randomness with how the orthography is mapped to the phonology. In Ŋarâþ Crîþ v9, some noun declension paradigms use letter sums for quasi-random vowels, so letter sums are probably going to be important in this language as well.

Note also that in the song, the usual pronunciation for ◊i{◊lang["zxx"]{x}} is ◊l3{eks}, with the ◊l3{e} elided after a vowel. (Or vice versa: ◊l3{ks} is the default, with ◊l3{e} added to the start after a consonant.)
