#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Diary}

A journal for exploring questions about Ŋarâþ Crîþ.

◊(embed-pagetree "index.ptree")

◊section{◊cite{Segments} submissions}

Articles that I’ve submitted to ◊link["https://www.reddit.com/r/conlangs/comments/l859u8/introducing_segments_a_journal_of_constructed/"]{◊cite{Segments}}.

◊items{
  ◊item{◊link["https://www.reddit.com/r/conlangs/comments/pzjycn/segments_a_journal_of_constructed_languages_issue/"]{#03 (Noun Constructions)}: ◊cite/inbook{Nouns in Ŋarâþ Crîþ: A historical perspective}}
  ◊item{◊link["https://old.reddit.com/r/conlangs/comments/sa79my/segments_a_journal_of_constructed_languages_issue/"]{#04 (Lexicon)}: ◊cite/inbook{Translation Challenge (Ŋarâþ Crîþ v9)} (◊link["segments-04-addendum.html"]{appendix})}
}

◊section{Typography of Cenvos}

Scans of handwritten notes about Cenvos. (These are all PDF documents.)

◊items{
  ◊item{◊link["toc/toc-0x00.pdf"]{0x00} ◊render-date[#:fmt "(~a / ~a)"]{2021-10-15}}
}

◊section[#:id "archives"]{Archives of old posts}

These are old posts that I’ve written about Necarasso Cryssesa or Ŋarâþ Crîþ elsewhere. They are now mirrored here in case something happens to the original sources. Some formatting has been applied.

◊(embed-pagetree "archives/index.ptree")

Here are additional sources for old materials:

◊items{
  ◊item{◊link["https://gitlab.com/ncv9/ncsfiles/"]{Necarasso Cryssesa files archive}}
  ◊item{◊link["https://www.reddit.com/r/NecarassoCryssesa/"]{The /r/NecarassoCryssesa subreddit}}
}
