#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{The ECL0 language}
◊define-meta[date]{2023-12-09}

◊b{E}xperimental ◊b{C}on◊b{l}ang ◊b{0} (tentative name).

◊section{Orthography and phonology}

◊table/x[
  #:options (table-options
    #:caption "Alphabet of ECL0 with IPA values. Asterisks indicate letters used only in loanwords."
  )
]{
  Letter & IPA
  ◊ecl0{a} & a
  ◊ecl0{b}* & b
  ◊ecl0{d} & d
  ◊ecl0{e} & e
  ◊ecl0{f}* & f
  ◊ecl0{g} & g
  ◊ecl0{h} & h (+ ʲ → ç)
  ◊ecl0{i} & i
  ◊ecl0{k} & k
  ◊ecl0{l} & r (word-initially) ~ l (elsewhere)
  ◊ecl0{m} & m
  ◊ecl0{n} & n (+ ʲ → ɲ)
  ◊ecl0{o} & o
  ◊ecl0{p}* & p
  ◊ecl0{r} & ɹ (+ ʲ → j)
  ◊ecl0{s} & s (+ ʲ → ʃ)
  ◊ecl0{t} & t
  ◊ecl0{u} & u
  ◊ecl0{v}* & v
  ◊ecl0{x} & ɬ
  ◊ecl0{z} & z (+ ʲ → ʒ)
}

Syllable structure is (C)(C)V(V)(N), where

◊items{
  ◊item{the onset clusters allowed are ◊i{◊ecl0{dl}}, ◊i{◊ecl0{dr}}, ◊i{◊ecl0{gl}}, ◊i{◊ecl0{gr}}, ◊i{◊ecl0{kl}}, ◊i{◊ecl0{kr}}, ◊i{◊ecl0{sk}}, ◊i{◊ecl0{sl}}, ◊i{◊ecl0{sm}}, ◊i{◊ecl0{sn}}, ◊i{◊ecl0{st}}, ◊i{◊ecl0{tl}}, ◊i{◊ecl0{tr}}, ◊i{◊ecl0{zd}}, ◊i{◊ecl0{zg}}, ◊i{◊ecl0{zl}}, ◊i{◊ecl0{zm}}, and ◊i{◊ecl0{zn}}, plus ◊i{◊ecl0{bl}}, ◊i{◊ecl0{br}}, ◊i{◊ecl0{fl}}, ◊i{◊ecl0{fr}}, ◊i{◊ecl0{pl}}, ◊i{◊ecl0{pr}}, ◊i{◊ecl0{vl}}, ◊i{◊ecl0{vr}} in loanwords}
  ◊item{the codas allowed are ◊i{◊ecl0{k}}, ◊i{◊ecl0{l}}, ◊i{◊ecl0{n}}, ◊i{◊ecl0{r}}, ◊i{◊ecl0{s}}, ◊i{◊ecl0{t}}, and ◊i{◊ecl{z}}}
  ◊item{the diphthongs allowed are ◊i{◊ecl0{ai}}, ◊i{◊ecl0{ei}}, ◊i{◊ecl0{oi}}, ◊i{◊ecl0{ui}}, ◊i{◊ecl0{ia}}, ◊i{◊ecl0{ie}}, and ◊i{◊ecl0{iu}}. An ◊i{◊ecl0{i}} as the first vowel in a diphthong palatalizes the preceding consonant. An ◊i{◊ecl0{i}} as the second vowel in a closed syllable palatalizes the coda and is not pronounced: ◊i{◊ecl0{kia}} → ◊l3{kʲa}, ◊i{◊ecl0{kai}} → ◊l3{kai̯}, ◊i{◊ecl0{kain}} → ◊l3{kaɲ}, ◊i{◊ecl0{kair}} → ◊l3{kai̯}.}
  ◊item{Hiatus between two vowels is allowed but rare. It occurs mostly in “made of” adjectives. In the romanization, a sequence of two vowels that would otherwise be ambiguous with a diphthong is written with a diaeresis on the second vowel.}
}

◊section{Nouns}

ECL0 nouns have eight cases as outlined by ◊xref/l["cases" "Table"].

◊table/x[#:options (table-options
    #:caption "The cases of ECL0."
    #:first-col-header? #f
    #:colgroup '(c d)
  )
  #:id "cases"]{
  Name & Use
  Direct & ◊;
      The unmarked form of the noun: S in intransitive clauses, A in accusative clauses, and O in ergative clauses.
  Accusative & ◊;
      O in accusative clauses.
  Ergative & ◊;
      A in ergative clauses.
  Dative & ◊;
      The R argument of a ditransitive verb.
  Possessive & ◊;
      Indicates possession. Not used for composition or description (uses derived adjectives instead) or for kinship relationships (uses a dedicated preposition).
  Partitive & ◊;
      Used to indicate a whole from which a part is derived (e.g. ◊i{many of ◊b{the people}}) or something that is measured (e.g. ◊i{a cup ◊b{of tea}}, ◊i{a group ◊b{of men}}). Also used in place of the accusative when the action is irresultative, O is nonspecific, and with a numeral greater than 1. For verbs of sensation, using the partitive for O indicates intentionality (e.g. ‘look at’), while using the accusative indicates lack thereof (e.g. ‘see’).
  Locative & ◊;
      Indicates the location of something.
  Vocative & ◊;
      Used for addressing something or someone. This form is often identical to the nominative form.
}

Across all paradigms, most nouns are pluralized by prepending the vowel of the initial syllable if the root is consonant-initial or prepending ◊i{◊ecl0{x}} if vowel-initial. A few nouns, however, have suppletive forms.

◊subsection{Declension class I (vowel-final nouns)}

◊table/x[#:options (table-options
    #:caption "Declensions for class I."
  )
  #:id "noun-I"]{
  Case & Ia & Ia′ & Ib & Ic
  Direct & ◊ecl0{-a} & ◊ecl0{-ea} & ◊ecl0{-ia} & ◊ecl0{-u}
  Accusative & ◊ecl0{-e} & ◊ecl0{-ei} & ◊ecl0{-ie} & ◊ecl0{-on}
  Ergative & ◊ecl0{-ak} & ◊ecl0{-eak} & ◊ecl0{-ek} & ◊ecl0{-uk}
  Dative & ◊ecl0{-an} & ◊ecl0{-ean} & ◊ecl0{-in} & ◊ecl0{-un}
  Possessive & ◊ecl0{-o} & ◊ecl0{-eo} & ◊ecl0{-o} & ◊ecl0{-o}
  Partitive & ◊ecl0{-i} & ◊ecl0{-eï} & ◊ecl0{-i} & ◊ecl0{-ui}
  Locative & ◊ecl0{-az} & ◊ecl0{-eaz} & ◊ecl0{-iz} & ◊ecl0{-uz}
  Vocative & ◊ecl0{-∅}* & ◊ecl0{-ea} & ◊ecl0{-e} & ◊ecl0{-u}
}

For class Ia nouns, the vocative form has a null affix if phonotactics permit; otherwise, it has the same affix as the direct case.

◊subsection{Declension class II (non-vowel-final nouns)}

The final consonant of the stem has two variants: one used for the direct and vocative forms and the other used for the rest.

◊table/x[#:options (table-options
    #:caption "Consonant grades in ECL0."
  )
  #:id "consonant-grades"]{
  Label & Dir/Voc & Others
  a & ◊ecl0{n} & ◊ecl0{m}
  b & ◊ecl0{n} & ◊ecl0{n}
  c & ◊ecl0{t} & ◊ecl0{d}
  d & ◊ecl0{s} & ◊ecl0{t}
  e & ◊ecl0{s} & ◊ecl0{st}
  f & ◊ecl0{k} & ◊ecl0{h}
  g & ◊ecl0{k} & ◊ecl0{g}
  h & ◊ecl0{z} & ◊ecl0{r}
  i & ◊ecl0{l} & ◊ecl0{l}
  j & ◊ecl0{∅} & ◊ecl0{h}
  k & ◊ecl0{∅} & ◊ecl0{∅}
}

◊table/x[#:options (table-options
    #:caption "Declensions for class II."
  )
  #:id "noun-II"]{
  Case & Suffix
  Direct & ◊ecl0{-∅}
  Accusative & ◊ecl0{-a}
  Ergative & ◊ecl0{-u}
  Dative & ◊ecl0{-ui}
  Possessive & ◊ecl0{-e}
  Partitive & ◊ecl0{-ai}
  Locative & ◊ecl0{-ez}
  Vocative & ◊ecl0{-∅}
}

◊subsection{Existential forms}

To express that a referent expressed by a noun phrase exists, a suffix is added to a direct or partitive form of a noun, which then acts like a finite verb. Unlike verbs, existential noun forms distinguish between past, present, and future tenses; the indicative and subjunctive moods are available, but the imperative is not.

◊table/x[#:options (table-options
    #:caption "Existential forms."
  )
  #:id "existential-forms"]{
  && ◊colspan[4]{Polarity / Case}
  && ◊colspan[2]{Affirmative} & ◊colspan[2]{Negative}
  Mood & Tense & Dir & Part & Dir & Part
  ◊HEADER
  Indicative & Present & ◊ecl0{-te} & ◊ecl0{-tu} & ◊ecl0{-temai} & ◊ecl0{-tomai}
             & Past & ◊ecl0{-ne} & ◊ecl0{-nu} & ◊ecl0{-nemai} & ◊ecl0{-nomai}
             & Future & ◊ecl0{-sin} & ◊ecl0{-sin} & ◊ecl0{-simai} & ◊ecl0{-simai}
  Subjunctive & Present &
              & Past &
              & Future &
}

Existential affixes from multiple tenses can be stacked:

◊gloss/x[#:lang "art-x-ecl0"]{
  ◊glfree{◊ecl0{tulkatesimai.}}
  ◊gla{tulk-a-te-simai.}
  ◊glb{fruit-%dir-%ex.%pr-%ex.%fut.%neg}
  ◊glfree{The fruit is there but (soon) won’t be.}
}

◊subsection{Personal pronouns}

◊table/x[#:options (table-options
    #:caption "Personal pronouns."
  )
  #:id "personal-pronouns"]{
  Person & ◊colspan[2]{1} & ◊colspan[2]{2} & ◊colspan[2]{3 anim.} & ◊colspan[2]{3 prox.} & ◊colspan[2]{3 dist.}
  Case \ Number & Sg. & Pl. & Sg. & Pl. & Sg. & Pl. & Sg. & Pl. & Sg. & Pl.
  ◊HEADER
  Direct & ◊ecl0{kei} & ◊ecl0{tui} & ◊ecl0{zol} & ◊ecl0{dei} & ◊ecl0{an} & ◊ecl0{xan} & ◊ecl0{gu} & ◊ecl0{?} & ◊ecl0{mu} & ◊ecl0{?}
  Accusative & ◊ecl0{keis} & ◊ecl9{tuis} & ◊ecl0{zo} & ◊ecl0{des} & ◊ecl0{ra} & ◊ecl0{xara} & ◊ecl0{ge} & ◊ecl0{?} & ◊ecl0{me} & ◊ecl0{?}
  Ergative & ◊ecl0{keik} & ◊ecl0{tuik} & ◊ecl0{zok} & ◊ecl0{deik} & ◊ecl0{anak} & ◊ecl0{xanak} & ◊ecl0{gek} & ◊ecl0{?} & ◊ecl0{mak} & ◊ecl0{?}
  Dative & ◊ecl0{kein} & ◊ecl0{tuin} & ◊ecl0{ze} & ◊ecl0{din} & ◊ecl0{zain} & ◊ecl0{xain} & ◊ecl0{gein} & ◊ecl0{?} & ◊ecl0{main} & ◊ecl0{?}
  Possessive & ◊ecl0{kui} & ◊ecl0{tel} & ◊ecl0{zei} & ◊ecl0{dui} & ◊ecl0{zai} & ◊ecl0{tei} & ◊ecl0{gei} & ◊ecl0{?} & ◊ecl0{mei} & ◊ecl0{?}
  Partitive & ◊ecl0{kai} & ◊ecl0{tai} & ◊ecl0{zai} & ◊ecl0{dai} & ◊ecl0{zai} & ◊ecl0{xai} & ◊ecl0{kai} & ◊ecl0{?} & ◊ecl0{nai} & ◊ecl0{?}
  Locative & ◊ecl0{kaiz} & ◊ecl0{tuiz} & ◊ecl0{daiz} & ◊ecl0{daiz} & ◊ecl0{anze} & ◊ecl0{xanze} & ◊ecl0{guze} & ◊ecl0{?} & ◊ecl0{muze} & ◊ecl0{?}
  Vocative & ◊ecl0{kai} & ◊ecl0{tei} & ◊ecl0{zui} & ◊ecl0{dei} & ◊ecl0{zain} & ◊ecl0{xain} & ◊ecl0{goi} & ◊ecl0{?} & ◊ecl0{moi} & ◊ecl0{?}
  Prepositional &
}

The proximal and distal pronouns are also used as demonstrative determiners, even when the head refers to an animate referent.

◊subsection{Indefinite determiners and pronouns}

◊table/x[#:options (table-options
    #:caption "Indefinite determiners."
  )
  #:id "indefinite-pronouns"]{
  Case & All & Some
  Direct & ◊ecl0{tez} & ◊ecl0{slatu}
  Accusative & ◊ecl0{teza} & ◊ecl0{slaton}
  Ergative & ◊ecl0{tezu} & ◊ecl0{slatuk}
  Dative & ◊ecl0{tezui} & ◊ecl0{slatun}
  Possessive & ◊ecl0{teze} & ◊ecl0{slato}
  Partitive & ◊ecl0{tezai} & ◊ecl0{slatui}
  Locative & ◊ecl0{terez} & ◊ecl0{slatuz}
  Vocative & ◊ecl0{teze} & ◊ecl0{slate}
}

◊subsection{Adjectives}

Adjectives are declined for case and number identically to nouns. Some adjectives are transitive and act as a sort of adposition; such adjectives are distinct from true prepositions. The complement of a transitive adjective is adjacent to the adjective and is declined in the partitive case. Unlike verbs, adjectives can be ambitransitive, in which case omission of a complement implies an unspecified referent in its place.

◊section{Verbs}

Verbs are conjugated for person, number, alignment, tense, polarity, and evidentiality. Direct evidentiality is unmarked, while indirect evidentiality is marked with the affix ◊i{◊ecl0{-og-}} after the verb stem. There is a binary tense distinction between nonpast (‘present’) and past. Imperatives require specifying the purported beneficiary of the desired action: A for the speaker, B for the listener, or C for a third party.

◊table/x[#:id "verb-conj" #:options (table-options #:caption ◊@{
  Conjugation of verbs in ECL0.
} #:num-header-cols 4)]{
  &&&& ◊colspan[4]{Polarity / Number}
  &&&& ◊colspan[2]{Affirmative} & ◊colspan[2]{Negative}
  Mood & Tense & Alignment & Person & Singular & Plural & Singular & Plural
  ◊HEADER
  ◊cellspan[5 1]{Indicative} & ◊cellspan[4 1]{Present} & ◊cellspan[3 1]{Accusative} & 1 ◊;
    & ◊ecl0{-az} & ◊ecl0{-at} & ◊ecl0{-azmai} & ◊ecl0{-asmai}
  2 & ◊ecl0{-ak} & ◊ecl0{-at} & ◊ecl0{-akmai} & ◊ecl0{-asmai}
  3 & ◊ecl0{-e} & ◊ecl0{-ei} & ◊ecl0{-emai} & ◊ecl0{-imai}
  Ergative & * & ◊ecl0{-a} & ◊ecl0{-ai} & ◊ecl0{-amai} & ◊ecl0{-amai}
  Past & Ergative & * & ◊ecl{-ar} & ◊ecl{-air} & ◊ecl0{-armai} & ◊ecl0{-armai}
  ◊cellspan[5 1]{Subjunctive} & ◊cellspan[4 1]{Present} & ◊cellspan[3 1]{Accusative} & 1 ◊;
    & ◊ecl0{-uz} & ◊ecl0{-uiz} & ◊ecl0{-uzrai} & ◊ecl0{-uzrei}
  2 & ◊ecl0{-uk} & ◊ecl0{-ui} & ◊ecl0{-ukrai} & ◊ecl0{-urei}
  3 & ◊ecl0{-ui} & ◊ecl0{-ut} & ◊ecl0{-urei} & ◊ecl0{-ukrai}
  Ergative & * & ◊ecl0{-el} & ◊ecl0{-eil} & ◊ecl0{-elai} & ◊ecl0{-elei}
  Past & Ergative & * & ◊ecl{-as} & ◊ecl{-ais} & ◊ecl0{-atrai} & ◊ecl0{-atrei}
  ◊cellspan[3 1]{Imperative} & ◊cellspan[3 1]{Present} & ◊cellspan[3 1]{Accusative} & ◊;
  A & ◊ecl{-ok} & ◊ecl{-ake} & ◊ecl0{-okrai} & ◊ecl0{-akerai}
  B & ◊ecl{-on} & ◊ecl{-eke} & ◊ecl0{-onnai} & ◊ecl0{-ekerai}
  C & ◊ecl{-at} & ◊ecl{-uke} & ◊ecl0{-atrai} & ◊ecl0{-ukerai}
}

The citation form of the verb is the ergative singular present indicative.

◊subsection{Irregular verbs}

◊subsubsection{◊ecl0{en} ‘be’ (and ◊ecl0{oren} ‘become’)}

This verb has no ergative forms. Only the present-tense forms are distinguished for evidentiality. In this case, the indirect-evidentiality forms are formed regularly as if the stem were ◊i{◊ecl0{el-}}.

◊table/x[#:id "verb-conj-en" #:options (table-options #:caption ◊@{
  Conjugation of ◊i{◊ecl0{en}}.
} #:num-header-cols 3)]{
  &&& ◊colspan[4]{Polarity / Number}
  &&& ◊colspan[2]{Affirmative} & ◊colspan[2]{Negative}
  Mood & Tense & Person & Singular & Plural & Singular & Plural
  ◊HEADER
  ◊cellspan[9 1]{Indicative} & ◊cellspan[3 1]{Present} & 1 ◊;
    & ◊ecl0{enze} & ◊ecl0{enzu} & ◊ecl0{maz} & ◊ecl0{maxiz}
  2 & ◊ecl0{enuk} & ◊ecl0{enuis} & ◊ecl0{mak} & ◊ecl0{mais}
  3 & ◊ecl0{en} & ◊ecl0{enna} & ◊ecl0{mai} & ◊ecl0{maiä}
  ◊cellspan[3 1]{Past} & 1 ◊;
    & ◊ecl0{ataz} & ◊ecl0{atar} & ◊ecl0{hudaz} & ◊ecl0{hudat}
  2 & ◊ecl0{atak} & ◊ecl0{atar} & ◊ecl0{hudak} & ◊ecl0{hudat}
  3 & ◊ecl0{at} & ◊ecl0{arta} & ◊ecl0{hut} & ◊ecl0{huda}
  ◊cellspan[3 1]{Future} & 1 ◊;
    & ◊ecl0{enaze} & ◊ecl0{enazei} & ◊ecl0{mazna} & ◊ecl0{maznai}
  2 & ◊ecl0{enuke} & ◊ecl0{enukei} & ◊ecl0{makna} & ◊ecl0{maknai}
  3 & ◊ecl0{tuise} & ◊ecl0{tuiste} & ◊ecl0{maina} & ◊ecl0{mannai}
  ◊cellspan[9 1]{Subjunctive} & ◊cellspan[3 1]{Present} & 1 ◊;
    & ◊ecl0{onzu} & ◊ecl0{onzui} & ◊ecl0{onzurai} & ◊ecl0{onzurei}
  2 & ◊ecl0{onuk} & ◊ecl0{onui} & ◊ecl0{onukrai} & ◊ecl0{onurei}
  3 & ◊ecl0{on} & ◊ecl0{onut} & ◊ecl0{nurei} & ◊ecl0{nukrai}
  ◊cellspan[3 1]{Past} & 1 ◊;
    & ◊ecl0{zedaz} & ◊ecl0{zedar} & ◊ecl0{zeduzrai} & ◊ecl0{zeduzrei}
  2 & ◊ecl0{zedak} & ◊ecl0{zedar} & ◊ecl0{zedukrai} & ◊ecl0{zedurei}
  3 & ◊ecl0{zet} & ◊ecl0{zeda} & ◊ecl0{zedurei} & ◊ecl0{zedukrai}
  ◊cellspan[3 1]{Future} & 1 ◊;
    & ◊ecl0{onazu} & ◊ecl0{onazui} & ◊ecl0{onzuraina} & ◊ecl0{onzureina}
  2 & ◊ecl0{onuke} & ◊ecl0{onukui} & ◊ecl0{onukraina} & ◊ecl0{onureina}
  3 & ◊ecl0{tuiska} & ◊ecl0{tuiskat} & ◊ecl0{nurein} & ◊ecl0{nukrain}
  ◊cellspan[3 1]{Imperative} & ◊cellspan[3 1]{Present} & ◊;
  A & ◊ecl{sidok} & ◊ecl{sidake} & ◊ecl0{sidokrai} & ◊ecl0{sidakerai}
  B & ◊ecl{sidon} & ◊ecl{sideke} & ◊ecl0{sidonnai} & ◊ecl0{sidekerai}
  C & ◊ecl{sidat} & ◊ecl{siduke} & ◊ecl0{sidatrai} & ◊ecl0{sidukerai}
}

◊i{◊ecl0{oren}} ‘become’ is inflected the same way, but with the ◊i{◊ecl0{or-}} prefix.

◊i{◊ecl0{en}} and ◊i{◊ecl0{oren}} are copular verbs, so they take a complement in the direct case. Alternatively, they can take an adjectival complement, which is inflected as an adverb.

◊subsubsection{◊ecl0{ira} ‘have, possess’}

In this and the subsequent cases, the indirect-evidentiality forms are formed regularly.

◊table/x[#:id "verb-conj-ira" #:options (table-options #:caption ◊@{
  Conjugation of ◊i{◊ecl0{ira}}.
} #:num-header-cols 4)]{
  &&&& ◊colspan[4]{Polarity / Number}
  &&&& ◊colspan[2]{Affirmative} & ◊colspan[2]{Negative}
  Mood & Tense & Alignment & Person & Singular & Plural & Singular & Plural
  ◊HEADER
  ◊cellspan[5 1]{Indicative} & ◊cellspan[4 1]{Present} & ◊cellspan[3 1]{Accusative} & 1 ◊;
    & ◊ecl0{ertiz} & ◊ecl0{ertil} & ◊ecl0{ertizmai} & ◊ecl0{ertilmai}
  2 & ◊ecl0{erik} & ◊ecl0{erihe} & ◊ecl0{erikmai} & ◊ecl0{erikmai}
  3 & ◊ecl0{ire} & ◊ecl0{irei} & ◊ecl0{iremai} & ◊ecl0{irimai}
  Ergative & * & ◊ecl0{ira} & ◊ecl0{irai} & ◊ecl0{iramai} & ◊ecl0{iramai}
  Past & Ergative & * & ◊ecl{stia} & ◊ecl{stair} & ◊ecl0{starmai} & ◊ecl0{starmai}
  ◊cellspan[5 1]{Subjunctive} & ◊cellspan[4 1]{Present} & ◊cellspan[3 1]{Accusative} & 1 ◊;
    & ◊ecl0{akuz} & ◊ecl0{akuiz} & ◊ecl0{akuzrai} & ◊ecl0{akuzrei}
  2 & ◊ecl0{anuk} & ◊ecl0{akui} & ◊ecl0{anukrai} & ◊ecl0{akurei}
  3 & ◊ecl0{akui} & ◊ecl0{akut} & ◊ecl0{akurei} & ◊ecl0{anukrai}
  Ergative & * & ◊ecl0{ankel} & ◊ecl0{ankeil} & ◊ecl0{ankelai} & ◊ecl0{ankelei}
  Past & Ergative & * & ◊ecl{enkas} & ◊ecl{enkais} & ◊ecl0{enkatrai} & ◊ecl0{enkatrei}
  ◊cellspan[3 1]{Imperative} & ◊cellspan[3 1]{Present} & ◊cellspan[3 1]{Accusative} & ◊;
  A & ◊ecl{marok} & ◊ecl{marake} & ◊ecl0{marokrai} & ◊ecl0{marakerai}
  B & ◊ecl{maron} & ◊ecl{mareke} & ◊ecl0{maronnai} & ◊ecl0{marekerai}
  C & ◊ecl{marat} & ◊ecl{maruke} & ◊ecl0{maratrai} & ◊ecl0{marukerai}
}

◊subsubsection{◊ecl0{tenka} ‘give’}

◊i{◊ecl0{ektenka}} ‘send’ is conjugated analogously.

◊table/x[#:id "verb-conj-tenka" #:options (table-options #:caption ◊@{
  Conjugation of ◊i{◊ecl0{tenka}}.
} #:num-header-cols 4)]{
  &&&& ◊colspan[4]{Polarity / Number}
  &&&& ◊colspan[2]{Affirmative} & ◊colspan[2]{Negative}
  Mood & Tense & Alignment & Person & Singular & Plural & Singular & Plural
  ◊HEADER
  ◊cellspan[5 1]{Indicative} & ◊cellspan[4 1]{Present} & ◊cellspan[3 1]{Accusative} & 1 ◊;
    & ◊ecl0{tonkaz} & ◊ecl0{tonkat} & ◊ecl0{tonkazmai} & ◊ecl0{tonkasmai}
  2 & ◊ecl0{tonkak} & ◊ecl0{tonkat} & ◊ecl0{tonkakmai} & ◊ecl0{tonkasmai}
  3 & ◊ecl0{tonke} & ◊ecl0{tonkei} & ◊ecl0{tonkemai} & ◊ecl0{tonkimai}
  Ergative & * & ◊ecl0{tenka} & ◊ecl0{tenkai} & ◊ecl0{tenkamai} & ◊ecl0{tenkamai}
  Past & Ergative & * & ◊ecl{telkar} & ◊ecl{telkair} & ◊ecl0{telkarmai} & ◊ecl0{telkarmai}
  ◊cellspan[5 1]{Subjunctive} & ◊cellspan[4 1]{Present} & ◊cellspan[3 1]{Accusative} & 1 ◊;
    & ◊ecl0{tonkuz} & ◊ecl0{tonkuiz} & ◊ecl0{tonkuzrai} & ◊ecl0{tonkuzrei}
  2 & ◊ecl0{tonkuk} & ◊ecl0{tonkui} & ◊ecl0{tonkukrai} & ◊ecl0{tonkurei}
  3 & ◊ecl0{tonkui} & ◊ecl0{tonkut} & ◊ecl0{tonkurei} & ◊ecl0{tonkukrai}
  Ergative & * & ◊ecl0{tenkel} & ◊ecl0{tenkeil} & ◊ecl0{tenkelai} & ◊ecl0{tenkelei}
  Past & Ergative & * & ◊ecl{telkas} & ◊ecl{telkais} & ◊ecl0{telkatrai} & ◊ecl0{telkatrei}
  ◊cellspan[3 1]{Imperative} & ◊cellspan[3 1]{Present} & ◊cellspan[3 1]{Accusative} & ◊;
  A & ◊ecl{etlok} & ◊ecl{tenlake} & ◊ecl0{etorlai} & ◊ecl0{telakerai}
  B & ◊ecl{enkon} & ◊ecl{tenleke} & ◊ecl0{enkonnai} & ◊ecl0{telekerai}
  C & ◊ecl{enkat} & ◊ecl{tenluke} & ◊ecl0{enkatrai} & ◊ecl0{telukerai}
}

◊subsubsection{◊ecl0{kira} ‘do’}

◊table/x[#:id "verb-conj-tenka" #:options (table-options #:caption ◊@{
  Conjugation of ◊i{◊ecl0{kira}}.
} #:num-header-cols 4)]{
  &&&& ◊colspan[4]{Polarity / Number}
  &&&& ◊colspan[2]{Affirmative} & ◊colspan[2]{Negative}
  Mood & Tense & Alignment & Person & Singular & Plural & Singular & Plural
  ◊HEADER
  ◊cellspan[5 1]{Indicative} & ◊cellspan[4 1]{Present} & ◊cellspan[3 1]{Accusative} & 1 ◊;
    & ◊ecl0{kiraz} & ◊ecl0{kirat} & ◊ecl0{kirazmai} & ◊ecl0{kirasmai}
  2 & ◊ecl0{kirak} & ◊ecl0{kirat} & ◊ecl0{kirakmai} & ◊ecl0{kirasmai}
  3 & ◊ecl0{kur} & ◊ecl0{krui} & ◊ecl0{kiremai} & ◊ecl0{kirimai}
  Ergative & * & ◊ecl0{kira} & ◊ecl0{kirai} & ◊ecl0{kiramai} & ◊ecl0{kiramai}
  Past & Ergative & * & ◊ecl{kilar} & ◊ecl{kilair} & ◊ecl0{kilarmai} & ◊ecl0{kilarmai}
  ◊cellspan[5 1]{Subjunctive} & ◊cellspan[4 1]{Present} & ◊cellspan[3 1]{Accusative} & 1 ◊;
    & ◊ecl0{mektuz} & ◊ecl0{mektuiz} & ◊ecl0{mektuzrai} & ◊ecl0{mektuzrei}
  2 & ◊ecl0{mektuk} & ◊ecl0{mektui} & ◊ecl0{mektukrai} & ◊ecl0{mekturei}
  3 & ◊ecl0{mektui} & ◊ecl0{mektut} & ◊ecl0{mekturei} & ◊ecl0{mektukrai}
  Ergative & * & ◊ecl0{mekel} & ◊ecl0{mekeil} & ◊ecl0{mekelai} & ◊ecl0{mekelei}
  Past & Ergative & * & ◊ecl{kirkas} & ◊ecl{kirkais} & ◊ecl0{kirkatrai} & ◊ecl0{kirkatrei}
  ◊cellspan[3 1]{Imperative} & ◊cellspan[3 1]{Present} & ◊cellspan[3 1]{Accusative} & ◊;
  A & ◊ecl{kirok} & ◊ecl{kirake} & ◊ecl0{kirokrai} & ◊ecl0{kirakerai}
  B & ◊ecl{kiron} & ◊ecl{kireke} & ◊ecl0{kironnai} & ◊ecl0{kirekerai}
  C & ◊ecl{kirat} & ◊ecl{kiruke} & ◊ecl0{kiratrai} & ◊ecl0{kirukerai}
}

◊subsection{Relative clauses}

A relative clause starts with a relativizer and uses the same form of the verb as a finite clause would.

◊table/x[#:options (table-options
    #:caption "Relativizers in ECL0."
  )
  #:id "relativizer"]{
  & ◊colspan[3]{Alignment of verb}
  & Accusative & ◊colspan[2]{Ergative}
  & ◊colspan[3]{Case of NP in embedded clause}
  Case of NP in matrix clause & Direct & Direct & Ergative
  ◊HEADER
  Direct & ◊ecl0{ai} & ◊ecl0{ko} & ◊ecl0{so}
  Accusative & ◊ecl0{te} & ◊ecl0{kon} & ◊ecl0{sun}
  Ergative & ◊ecl0{to}
  Dative & ◊ecl0{tai}
  Possessive & ◊ecl0{tain} & ◊ecl0{kon} & ◊ecl0{sen}
  Partitive & ◊ecl0{tai}
  Locative & ◊ecl0{taiz}
  Vocative & ◊ecl0{ai}
}

◊subsection{Content clauses}

A declarative content clause consists of a particle before or after its remainder. There is a special form of the particle used for complements of a preposition.

◊table/x[#:options (table-options
    #:caption "Content clause markers in ECL0."
  )
  #:id "relativizer"]{
  Case & Marker
  ◊HEADER
  Direct & ◊ecl0{kel}
  Accusative & ◊ecl0{ken}
  Ergative & ◊ecl0{ket}
  Dative & ◊ecl0{kegi}
  Possessive & ◊ecl0{kele}
  Partitive & ◊ecl0{kelai}
  Locative & ◊ecl0{kelez}
  Vocative & ◊ecl0{kel}
  Prepositional & ◊ecl0{ka}
}

◊section{Prepositions}

True prepositions (as opposed to transitive adjectives) are uninflected and take a direct-case complement. Complements that are personal pronouns are put in a special form.

◊section{Conjunctions}

◊items{
  ◊item{◊ecl0{zi}: and}
  ◊item{◊ecl0{klait}: when …}
  ◊item{◊ecl0{sure}: while …}
  ◊item{◊ecl0{klaital}: because …}
  ◊item{◊ecl0{ema}: in order to, so that …}
}

◊section{Numerals}

The numeral for 1 has separate adjectival and nominal forms: ◊i{◊ecl0{enu}} for the adjectival and ◊i{◊ecl0{rena}} for the nominal. All other numerals are solely nouns. The noun phrase referring to the thing being quantified is put in the partitive case:

◊gloss/x[#:lang "art-x-ecl0"]{
  ◊glfree{◊ecl0{tana tuini}}
  ◊gla{tan-a tuin-i}
  ◊glb{two-%dir island-%part}
  ◊glfree{two islands}
}

◊section{Derivations and compounding}

◊subsection{Nouns derived from verbs}

◊items{
  ◊item{Agent: class I, ◊ecl{-aza, -aze} on the verb stem (also adj.)}
  ◊item{Patient: class I, ◊ecl{-anta, -ante} on the verb stem (also adj.)}
}

◊subsection{Adjectives derived from nouns}

◊items{
  ◊item{“made of”: class I, ◊ecl0{-ea, -ei} on the direct form}
  ◊item{“full of”: class II, ◊ecl0{-tek, -teha} on the direct form}
}

◊subsection{Adverbs derived from adjectives}

stem + ◊i{◊ecl0{-en}}

◊section{Lexicon}

◊dl{
  ◊dt{◊ecl0{aketa}}◊dd{◊i{vt.} ◊S moves away from, escapes ◊O}◊;
  ◊dt{◊ecl0{ala, ale}}◊dd{◊i{n.} child, offspring}◊;
  ◊dt{◊ecl0{anxa}}◊dd{◊i{vt.} ◊S follows a path ◊O to get to ◊I}◊;
  ◊dt{◊ecl0{arais, araista}}◊dd{◊i{adji.} new}◊;
  ◊dt{◊ecl0{araisten}}◊dd{◊i{adv.} again}◊;
  ◊dt{◊ecl0{balla, balle}}◊dd{◊i{adji.} nonsensical, absurd}◊;
  ◊dt{◊ecl0{ballilka, ballilke}}◊dd{◊i{adji.} an absurd amount of}◊;
  ◊dt{◊ecl0{doma, dome}}◊dd{◊i{n.} meal}◊;
  ◊dt{◊ecl0{dil, dila}}◊dd{◊i{n.} tooth}◊;
  ◊dt{◊ecl0{dlara}}◊dd{◊i{vt.} ◊S chooses ◊O in preference to (non)}◊;
  ◊dt{◊ecl0{dreika}}◊dd{◊i{vt.} ◊S touches ◊O}◊;
  ◊dt{◊ecl0{duga}}◊dd{◊i{vt.} ◊S knocks ◊O out of its original position}◊;
  ◊dt{◊ecl0{duik, duiga}}◊dd{◊i{n.} night}◊;
  ◊dt{◊ecl0{dun, duna}}◊dd{◊i{adja.} below, under}◊;
  ◊dt{◊ecl0{ek}}◊dd{◊i{pp.} away from, out of}◊;
  ◊dt{◊ecl0{ektenka, …}}◊dd{◊i{vt.} ◊S sends ◊|O| to ◊I}◊;
  ◊dt{◊ecl0{eliga}}◊dd{◊i{vt.} ◊S leaves behind ◊O for or at ◊I}◊;
  ◊dt{◊ecl0{en, …}}◊dd{◊i{vc.} ◊S is ◊S2}◊;
  ◊dt{◊ecl0{enu, enon}}◊dd{◊i{adj.} one}◊;
  ◊dt{◊ecl0{enuiz, enuira}}◊dd{◊i{adj.} alone, isolated, sole, only}◊;
  ◊dt{◊ecl0{elka}}◊dd{◊i{vt.} ◊S eats ◊O}◊;
  ◊dt{◊ecl0{ese}}◊dd{◊i{pp.} at (a time)}◊;
  ◊dt{◊ecl0{gada}}◊dd{◊i{vi.} ◊S makes a sudden loud noise}◊;
  ◊dt{◊ecl0{gina}}◊dd{◊i{vt.} ◊S notifies, informs ◊I of ◊|O|}◊;
  ◊dt{◊ecl0{glon, glona}}◊dd{◊i{n.} bone}◊;
  ◊dt{◊ecl0{graku, grakon}}◊dd{◊i{n.} maize, corn}◊;
  ◊dt{◊ecl0{gul}}◊dd{◊i{adv.} in response, back}◊;
  ◊dt{◊ecl0{haha, hahe}}◊dd{◊i{n.} legume, bean}◊;
  ◊dt{◊ecl0{hasli}}◊dd{◊i{adv.} quickly; (as an emphasizer) already, at last, finally}◊;
  ◊dt{◊ecl0{hiata}}◊dd{◊i{vi.} ◊S grows}◊;
  ◊dt{◊ecl0{hienda}}◊dd{◊i{vt.} ◊S grows ◊O for farming}◊;
  ◊dt{◊ecl0{hiendaza, hiendaze}}◊dd{◊i{n.} farmer}◊;
  ◊dt{◊ecl0{ilka, ilke}}◊dd{◊i{adja.} many, more than}◊;
  ◊dt{◊ecl0{ira, …}}◊dd{◊i{vt.} ◊S has, possesses ◊O}◊;
  ◊dt{◊ecl0{kalka}}◊dd{◊i{vt.} ◊S bites or chews ◊O}◊;
  ◊dt{◊ecl0{karu, karon}}◊dd{◊i{n.} flower}◊;
  ◊dt{◊ecl0{katuna, katune}}◊dd{◊i{n.} stranger}◊;
  ◊dt{◊ecl0{kensa, kense}}◊dd{◊i{adja.} friendly, considerate, nice, pleasant (to)}◊;
  ◊dt{◊ecl0{kersa}}◊dd{◊i{vi.} ◊S is located at ◊|loc#|}◊;
  ◊dt{◊ecl0{kien, kiena}}◊dd{◊i{adji.} small}◊;
  ◊dt{◊ecl0{kil}}◊dd{◊i{adv.} want to}◊;
  ◊dt{◊ecl0{kiatra, kiatre}}◊dd{◊i{n.} friend}◊;
  ◊dt{◊ecl0{kira, …}}◊dd{◊i{vt.} ◊S completes a task ◊|O|; ◊S does, carries out ◊|O|; ◊S compels ◊O to be done; ◊S makes ◊O}◊;
  ◊dt{◊ecl0{klanzia, klanzie}}◊dd{◊i{n.} sword}◊;
  ◊dt{◊ecl0{klei, kleiha}}◊dd{◊i{n.} book}◊;
  ◊dt{◊ecl0{krosa, krose}}◊dd{◊i{n.} home, shelter, domicile}◊;
  ◊dt{◊ecl0{kruza}}◊dd{◊i{vt.} ◊S flattens, crushes ◊O}◊;
  ◊dt{◊ecl0{kruzanta}}◊dd{◊i{adji.} flat, level, thin}◊;
  ◊dt{◊ecl0{kuhea, kuhei}}◊dd{◊i{adja.} former; before (in time)}◊;
  ◊dt{◊ecl0{kuil, kuila}}◊dd{◊i{n.} stone, rock}◊;
  ◊dt{◊ecl0{kun, kume}}◊dd{◊i{n.} neck}◊;
  ◊dt{◊ecl0{kusata, kusate}}◊dd{◊i{n.} unpaved road}◊;
  ◊dt{◊ecl0{laine}}◊dd{◊i{adv.} (with subjunctive) destined to, fated to}◊;
  ◊dt{◊ecl0{lanka}}◊dd{◊i{vt.} ◊S moves toward, goes to ◊O}◊;
  ◊dt{◊ecl0{lei, leiha}}◊dd{◊i{n.} dance}◊;
  ◊dt{◊ecl0{leika}}◊dd{◊i{vi.} ◊S dances}◊;
  ◊dt{◊ecl0{lentu, lenton}}◊dd{◊i{adji.} more, additional, extra}◊;
  ◊dt{◊ecl0{liaxa, liaxe}}◊dd{◊i{n.} marketplace, collection of stores}◊;
  ◊dt{◊ecl0{loz, lora}}◊dd{◊i{n.} person}◊;
  ◊dt{◊ecl0{luxa, luxe}}◊dd{◊i{n.} feather}◊;
  ◊dt{◊ecl0{mama, mame}}◊dd{◊i{n.} mother}◊;
  ◊dt{◊ecl0{matla, matle}}◊dd{◊i{n.} plate, dish, tray}◊;
  ◊dt{◊ecl0{meil}}◊dd{◊i{adv.} capable of, able to}◊;
  ◊dt{◊ecl0{mirza}}◊dd{◊i{vt.} ◊S sees, looks at ◊O}◊;
  ◊dt{◊ecl0{mona}}◊dd{◊i{vt.} ◊S says ◊O to ◊I}◊;
  ◊dt{◊ecl0{montoka, montoke}}◊dd{◊i{n.} messenger}◊;
  ◊dt{◊ecl0{montu, monton}}◊dd{◊i{n.} message}◊;
  ◊dt{◊ecl0{munu, munon}}◊dd{◊i{n.} speech; something said by ◊|poss#|}◊;
  ◊dt{◊ecl0{nala, nale}}◊dd{◊i{adji.} feeling comfortable, relieved}◊;
  ◊dt{◊ecl0{nela, nele}}◊dd{◊i{adji.} placed in a horizontal position; supine}◊;
  ◊dt{◊ecl0{net, neda}}◊dd{◊i{n.} name}◊;
  ◊dt{◊ecl0{non}}◊dd{◊i{pp.} than, over, beyond, in preference to}◊;
  ◊dt{◊ecl0{nukka}}◊dd{◊i{vt.} ◊S builds (a stationary structure) ◊O}◊;
  ◊dt{◊ecl0{oin, oima}}◊dd{◊i{n.} mouth}◊;
  ◊dt{◊ecl0{oren, …}}◊dd{◊i{vc.} ◊S becomes ◊S2}◊;
  ◊dt{◊ecl0{rama, rame}}◊dd{◊i{adji.} whole, entire}◊;
  ◊dt{◊ecl0{rena, rene}}◊dd{◊i{n.} one}◊;
  ◊dt{◊ecl0{rukka, rukke}}◊dd{◊i{n.} ruler, leader, king, queen, chief}◊;
  ◊dt{◊ecl0{runu, runon}}◊dd{◊i{n.} hair, fur}◊;
  ◊dt{◊ecl0{runutek, runuteha}}◊dd{◊i{adji.} hairy, furry}◊;
  ◊dt{◊ecl0{sesta, seste}}◊dd{◊i{n.} garden, yard}◊;
  ◊dt{◊ecl0{sintla}}◊dd{◊i{vi.} ◊S returns, goes back ◊|loc#|}◊;
  ◊dt{◊ecl0{skela}}◊dd{◊i{vt.} ◊S makes ◊O orderly or tidy}◊;
  ◊dt{◊ecl0{sken, skena}}◊dd{◊i{n.} brick}◊;
  ◊dt{◊ecl0{skenea, skenei}}◊dd{◊i{adji.} made of brick}◊;
  ◊dt{◊ecl0{sketta, skette}}◊dd{◊i{n.} table, desk}◊;
  ◊dt{◊ecl0{sketu, sketon}}◊dd{◊i{adjt.} using, with, with the help of, from (ingredients)}◊;
  ◊dt{◊ecl0{slatta, slatte}}◊dd{◊i{n.} forest}◊;
  ◊dt{◊ecl0{sliden}}◊dd{◊i{adv.} today}◊;
  ◊dt{◊ecl0{slit, slida}}◊dd{◊i{adji.} today}◊;
  ◊dt{◊ecl0{smaidra}}◊dd{◊i{vt.} ◊S intends or plans for ◊O to happen}◊;
  ◊dt{◊ecl0{snak, snaga}}◊dd{◊i{n.} root vegetable}◊;
  ◊dt{◊ecl0{stai, staiha}}◊dd{◊i{n.} field, plain}◊;
  ◊dt{◊ecl0{steku, stekon}}◊dd{◊i{n.} barricade, wall, fence}◊;
  ◊dt{◊ecl0{stok, stoga}}◊dd{◊i{n.} fox}◊;
  ◊dt{◊ecl0{stura, sture}}◊dd{◊i{n.} vine}◊;
  ◊dt{◊ecl0{suilu, suilon}}◊dd{◊i{n.} river, stream, creek}◊; 
  ◊dt{◊ecl0{suga, suge}}◊dd{◊i{n.} bird}◊;
  ◊dt{◊ecl0{suka}}◊dd{◊i{vt.} ◊S pulls ◊O away from (ek)}◊;
  ◊dt{◊ecl0{suta, sute}}◊dd{◊i{n.} color, hue, shade}◊;
  ◊dt{◊ecl0{sutatek, sutateha}}◊dd{◊i{adji.} colorful, (in the singular modifying a plural NP) various}◊;
  ◊dt{◊ecl0{takia, takie}}◊dd{◊i{n.} log of a cut tree}◊;
  ◊dt{◊ecl0{tai, taiha}}◊dd{◊i{n.} ear}◊;
  ◊dt{◊ecl0{taiza}}◊dd{◊i{vt.} ◊S hears, listens to ◊O}◊;
  ◊dt{◊ecl0{talha, talhe}}◊dd{◊i{n.} room}◊;
  ◊dt{◊ecl0{tana, tane}}◊dd{◊i{n.} two}◊;
  ◊dt{◊ecl0{tanma}}◊dd{◊i{vt.} ◊S sells ◊O to ◊I}◊;
  ◊dt{◊ecl0{teik, teiha}}◊dd{◊i{n.} feast, party, festival, banquet}◊;
  ◊dt{◊ecl0{telsure}}◊dd{◊i{adv.} formerly; in the past}◊;
  ◊dt{◊ecl0{tenka, …}}◊dd{◊i{vt.} ◊S gives ◊|O| to ◊I}◊;
  ◊dt{◊ecl0{tien, tiema}}◊dd{◊i{n.} stock, broth, clear soup}◊;
  ◊dt{◊ecl0{tonra}}◊dd{◊i{vt.} ◊S causes ◊O to begin}◊;
  ◊dt{◊ecl0{tonsa}}◊dd{◊i{vi.} ◊S begins; (of a celestial body) ◊S rises}◊;
  ◊dt{◊ecl0{tuina, tuine}}◊dd{◊i{n.} island}◊;
  ◊dt{◊ecl0{tulka, tulke}}◊dd{◊i{n.} fruit}◊;
  ◊dt{◊ecl0{uilka}}◊dd{◊i{vi.} ◊S attends, partakes in, goes to ◊|loc#|}◊;
  ◊dt{◊ecl0{urda, urde}}◊dd{◊i{n.} sky, space, air}◊;
  ◊dt{◊ecl0{zdol, zdola}}◊dd{◊i{n.} container for cooking, pot, pan}◊;
  ◊dt{◊ecl0{zina, zine}}◊dd{◊i{n.} dog}◊;
  ◊dt{◊ecl0{zinda}}◊dd{◊i{vt.} ◊S needs ◊O}◊;
  ◊dt{◊ecl0{zlamitu, zlamiton}}◊dd{◊i{n.} dense forest}◊;
  ◊dt{◊ecl0{zmena, zmene}}◊dd{◊i{n.} small house}◊;
  ◊dt{◊ecl0{znaha, znahe}}◊dd{◊i{n.} sun}◊;
  ◊dt{◊ecl0{zneva}}◊dd{◊i{vi.} ◊S sleeps}◊;
  ◊dt{◊ecl0{znista}}◊dd{◊i{vt.} ◊S attaches, affixes, adds ◊O to ◊I; ◊S calls ◊I by (a name) ◊O}◊;
  ◊dt{◊ecl0{znoga, znoge}}◊dd{◊i{n.} controlled outdoors fire, campfire}◊;
  ◊dt{◊ecl0{zora, zore}}◊dd{◊i{adja.} absent from}◊;
  ◊dt{◊ecl0{zu}}◊dd{◊i{pp.} toward, to}◊;
}
