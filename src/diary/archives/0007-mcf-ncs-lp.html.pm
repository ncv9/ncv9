#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{[1.6 | WIP] Necarasso Cryssesa!}
◊define-meta[date]{2013-04-20}

◊archive-note["https://www.minecraftforum.net/forums/archive/language-packs/933120-1-6-wip-necarasso-cryssesa"]{Minecraft Forum}

◊b{New update! Almost complete yet not completely complete End Poem Translation!}

I've seen many cheesy language packs, and decided to create one for the 1.6 snapshot based on a conlang I made! This language pack translates names into a constructed language I created. You can learn how to speak the language here.

Currently, I am preparing for the next revision of the language, as well as the upcoming resource pack system.

Installation:

◊details{
  ◊summary{1.5}◊;
  ◊enum{
    ◊item{Go to your minecraft folder, and rename ◊filename{bin/minecraft.jar} to ◊filename{minecraft.zip.}}
    ◊item{Go to the lang folder in the archive and extract ◊filename{languages.txt}.}
    ◊item{Edit the extracted version by adding this line anywhere: “◊kbd{ncs_AA=Necarasso Cryssesa (AA)}” (no quotes; make sure your editor handles Unix-style returns).}
    ◊item{Save, and transfer ◊filename{languages.txt} and ◊filename{ncs_AA.lang} into the lang directory in the archive.}
    ◊item{Delete the ◊filename{META-INF} folder.}
    ◊item{Rename ◊filename{minecraft.zip} back to ◊filename{minecraft.jar}.}
  }

  If you crash, then make sure you deleted ◊filename{META-INF} and edited the ◊filename{languages.txt} using an editor supporting Unix-style enters!
}

◊details{
  ◊summary{13w16a - 13w23a}◊;
  Waaaaaaay simpler.

  ◊enum{
    ◊item{Go to your minecraft folder, then the ◊filename{assets/lang} directory.}
    ◊item{Transfer ◊filename{ncs_AA.lang} there.}
  }
}

◊details{
  ◊summary{13w24a and later (Coming soon)}◊;
  Move the zip file to the resource pack folder.
}

This pack has all of Mojang’s translations removed, but it should still work!

Sorry, I couldn’t make the numbers appear in hexadecimal.

Pictures: ◊i{omitted from this page}

Download: ◊link["https://dl.dropboxusercontent.com/s/eljauunpvy0gcp6/ncs130625.zip?token_hash=AAGEEACcOLH_J-Nf3iVxcqhik7_nEecGg7L74dnsB4grcQ&dl=1"]{ncs.zip}

Edit: Apparently, someone replaced the imgur link in this post with the constituent pictures. Thanks!
