#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Playing MAL}
◊define-meta[date]{2013-06-13}

◊archive-note["https://necarasso-cryssesa.blogspot.com/2013/06/playing-mal.html"]{the Necarasso Cryssesa blog}

Mal. Not Mao, where you can't discuss rules. Mal stands for Make a Language. And it does involve making languages. Here's how to play, because this is not Mao.

ONE Get a list of sentences, preferably complex ones. 50 or so are fine.

TWO Each pair makes a language to translate all of the sentences in the list. Allow about 3 hours. Pencils and paper are allowed, but computers aren't. Copies are made for the teammate.

THREE Compose 52 sample sentences and write them on index cards.

FOUR Now assign a suit and rank to each card.

FIVE Shuffle the deck.

SIX The game is played like Uno.

SEVEN Deal five cards to each player.

EIGHT On each play, a team member must translate the sentence given using the notes in his or her hand. An incorrect translation gets an extra card.

NINE Whenever a 2 is played, pass notes to the right. Whenever a 3 is played, pass notes to the left.

TEN A player must say the word for 1 in his or her own language at attaining one card.

ELEVEN Give a card to a player who breaks the rules.
