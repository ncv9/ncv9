#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{The Creation of an IAL?}
◊define-meta[date]{2013-06-13}

◊archive-note["https://necarasso-cryssesa.blogspot.com/2013/06/the-creation-of-ial.html"]{the Necarasso Cryssesa blog}

A while back, I was against international auxiliary languages for some unexplained reason (presumably increased resentment among ethnic groups or the extinction of natural languages?). But now, my feeling is “Why not?”

The objective which I am trying to fulfill is to create one that is roughly equally difficult for each “group” to learn. What at which Esperanto and other following IALs failed is that it was biased towards speakers of European languages. Should such imbalance exist, the people for which the said language will revolt, and its purpose will not be fulfilled.

So how do we do this?

Some time ago, I sent this classified transmission (the spaces were not in the original):

pbfvwm tdszrlnł ŧđṡżjļñś kghħŭĺŋş qĝḣḧŵḷņλ xẍ yieaou

These are the glyphs representing the phonemes of the proposed language. In IPA, this is
pbɸβwm tdszɹlnɬ cɟçʝjʎɲçˡ kɡxɣɰʟŋxˡ qɢχʁɰ̠ʟ̠ɴχˡ ʡʡ̬ ɨieaou [1]

In other words, the bilabial consonants come first, then the alveolar, then the palatal, then the velar, then the uvular. The normal order within the blocks are plosives, fricatives, the approximant, the lateral approximant, the nasal, and finally the lateral fricative (for the bilabial, two articulations are impossible) [2]. Then come the epiglottals, then finally the vowels. By the way, primary stress is notated by the acute accent on the vowel, just so you know.

Why such a complex phonology? Well, the second through fifth blocks form a nice matrix when placed on an IPA chart, and a simple phoneme set common to every language would severely restrict the number of distinct roots. And there you go, roughly equally difficult for a native speaker of almost any language. (Hmm, maybe not, since some languages have a ridiculous number of phonemes; if you're one of these people, then good for you.) Comments? Criticisms?

[1] Note that any consonant can appear at the beginning or the end of a syllable.

[2] I did not include trill consonants in the phonology because many people tend to have difficulties pronouncing them.