#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{No Internet Access}
◊define-meta[date]{2013-07-18}

◊archive-note["https://necarasso-cryssesa.blogspot.com/2013/07/no-internet-access.html"]{the Necarasso Cryssesa blog}

Yesterday and today, I haven’t had Internet access on my PC. (I’m typing this on my phone.) There’s the transition from Insight to Time Warner Cable, and someone is going to come here tomorrow to help us. While I was off, I worked on some music; I finished the piece entitled ◊cite[#:lang (ŋc-code 3)]{Iss centara eas iss symessa} (◊cite{The beginning of the end}) and started a new one.
