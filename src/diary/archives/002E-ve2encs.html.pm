#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{VE²ENCS vella ydyr!}
◊define-meta[title-language]{art-x-ncs-v2}
◊define-meta[date]{2013-05-29}

◊archive-note["https://necarasso-cryssesa.blogspot.com/2013/05/ve2encs-vella-ydyr.html" #:addendum "The link in the post is dead."]{the Necarasso Cryssesa blog}

◊link["http://derpy.me/ve2encs"]{http://derpy.me/ve2encs}

◊v2{Nelferass re nacmeresar iss vletmata trespo nas iss enva erse myn (0x1), es merseda erse genlema (0x5), es elsse erse aplamysenrotacveryssanallasmarmessafssalia (0x7dd / 2013).}

◊v2{VE3ENCS geve vyncyd es iss merseda erse ce aplamys ce engatyr!}
