#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Notes to myself for working on the ŊCv9 grammar}
◊define-meta[date]{2021-09-20}

◊section{Dictionary notes}

The definition field is for a detailed description of what the word means; the translation field contains zero or more possible ways to translate it into the metalanguage. Generally, strive to fill in the definition field.

The definition is necessary for all verbs and relationals in order to mark their roles.

Translations should not be taken at face value – a ŊCv9 word might be used in a different way than its English translations.

Looking the desired English equivalent up in the dictionary is a passable way to avoid too much influence from English.

◊section{The grammar}

◊subsection{Enumerations}

◊b{Lenition and eclipsis environments:} whenever you add a rule that triggers lenition or eclipsis, mention this in the phonology section

◊b{A tour of auxiliary verbs:} update whenever an auxiliary verb is added

◊b{A tour of relationals:} update whenever an relational is added

◊subsection{Orthography images}

Calligraphy pen settings for glyphs: width = 2mm, thinning = 10, mass = 5, angle = 30°, fixation = 80%, caps = 0.20, tremor = 0, wiggle = 0
