#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Orthography and phonology}

◊xref/l["alphabet"]{Table} shows the romanization of Ŋarâþ Âlþor from Cenvos as well as the IPA values of each letter. Note that in contrast to Ŋarâþ Crîþ, the ◊i{łil} denotes retroflex consonants, not lenition. The letters ◊l1{š ł č ð} are not used in Ŋarâþ Âlþor.

◊table/x[
  #:options (table-options
    #:caption "Alphabet of Ŋarâþ Âlþor with IPA values, along with digraphs. Asterisks indicate letters used only in loanwords."
  )
  #:id "alphabet"
]{
  Letter & IPA
  ◊ngal{c} & kʰ (k̚ as coda)
  ◊ngal{e} & é
  ◊ngal{n} & n
  ◊ngal{ŋ} & ŋ
  ◊ngal{v} & b
  ◊ngal{o} & ó
  ◊ngal{s} & s
  ◊ngal{þ} & t
  ◊ngal{r} & ɻ
  ◊ngal{l} & l
  ◊ngal{m} & m
  ◊ngal{a} & á
  ◊ngal{f} & p
  ◊ngal{g} & ɡ
  ◊ngal{p} & pʰ (p̚ as coda)
  ◊ngal{t} & tʰ (t̚ as coda)
  ◊ngal{î} & ì
  ◊ngal{j} & j
  ◊ngal{i} & í
  ◊ngal{d} & d
  ◊ngal{h} & h (∅ when part of a checked vowel)
  ◊ngal{ħ} & ∅ (used at start of syllables)
  ◊ngal{ê} & è
  ◊ngal{ô} & ò
  ◊ngal{â} & à
  ◊ngal{u} & u
  ◊ngal{w} & k
  ◊ngal{x} & ɓ
  ◊ngal{y} & w
  ◊ngal{z} & ə
  ◊ngal{n·} & ɳ
  ◊ngal{þ·} & ʈ
  ◊ngal{t·} & ʈʰ (ʈ̚ as coda)
  ◊ngal{d·} & ɖ
}

The phonotactics of Ŋarâþ Âlþor is based on syllables, each of which consists of an onset and a rime. An onset is made of two parts:

◊items{
  ◊item{The ◊term{initial}, which is any of ◊ngal{c n ŋ v s þ r l m f g p t d h ħ w x n· þ· t· d·}.}
  ◊item{The ◊term{on-glide}, which is optionally ◊ngal{j} or ◊ngal{y}.}
}

The rime can be one of the following:

◊items{
  ◊item{A single vowel, optionally followed by an off-glide or coda.}
  ◊item{A long vowel – two vowel letters of the same quality – optionally followed by an off-glide.}
  ◊item{One of ◊ngal{e o a i u z} followed by ◊ngal{h}. This is called a ◊term{checked vowel} and is pronounced somewhat shorter than usual.}
  ◊item{A syllabic ◊ngal{m} or ◊ngal{n}.}
}

An off-glide is ◊ngal{i}, ◊ngal{î} or ◊ngal{u}. A coda is any of ◊ngal{c n ŋ m p t n· t·}.

In a given syllable, it is forbidden for the on-glide to be ◊ngal{j} and the vowel or the off-glide to be ◊ngal{i} or ◊ngal{î}, or for the vowel to be ◊ngal{i} or ◊ngal{î} and the off-glide to be ◊ngal{i} or ◊ngal{î}. Similarly, it is forbidden for the on-glide to be ◊ngal{y} and the vowel or the off-glide to be ◊ngal{u}, or for both the vowel and the off-glide to be ◊ngal{u}.

The bridges ◊ngal{-pp-}, ◊ngal{-tt-}, ◊ngal{-t·t·-}, and ◊ngal{-cc-} are pronounced as the geminated ejectives ◊l3{pʼ}, ◊l3{tʼ}, ◊l3{ʈʼ}, and ◊l3{kʼ}.

◊section{Tones}

The vowel qualities ◊l2{e o a i} are capable of holding tone; they are divided into two tone levels: ◊term{high}, written with ◊ngal{e o a i}, and ◊term{low}, written with ◊ngal{ê ô â î}. The vowels ◊l2{u ə}, written as ◊ngal{u z}, are not capable of holding tone. Additionally, a checked vowel is not capable of holding tone.

This means that a syllable can hold anywhere from 0 to 3 tone levels. However, a syllable with at least one vowel and no tones can ‘steal’ a tone level from a neighboring syllable with at least 2 tones. More precisely, such a syllable can take the last tone of the preceding syllable or the first tone of the following syllable, removing the tone from the original syllable. A syllable can steal from the following syllable only if they are part of the same word. If a syllable can steal from either of its adjacent syllables, then it steals from the preceding one. The decision on which syllable to steal a tone from is made for all eligible syllables simultaneously, so that, for instance, ◊ngal{dunâageh} ◊l2{dunaː˩˥ɡĕ} is realized as ◊l3{du˨naːɡĕ˦}.

◊table/x[
  #:options (table-options
    #:caption "Realizations of tone contours in Ŋarâþ Âlþor."
  )
  #:id "tone-realizations"
]{
  Contour (H/L) & Levels (0 – 6)
  ∅ & 3 (2 after HH, 4 after LL)
  H & 4 (5 on a checked syllable, 3 after HH)
  L & 2 (1 on a checked syllable, 3 after LL)
  HH & 4 (34 after H)
  HL & 42
  LH & 254 (24 on ◊l2{o})
  LL & 21
  HHH & 54
  HHL & 53
  HLH & 534
  HLL & 41
  LHH & 35
  LHL & 23
  LLH & 14
  LLL & 01
}

◊section{Reduplication}

Reduplication is indicated in Cenvos with the ◊i{sen}, ◊ngal{&}. Unlike in Ŋarâþ Crîþ, this always reduplicates the initial syllable. However, multi-syllabic words are not often reduplicated outside of Sadun.
