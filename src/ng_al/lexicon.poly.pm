#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Lexicon}

◊section{Derivations}

◊subsection{On nouns}

Reduplicating a noun creates a noun referring to a collection of something as a whole: ◊ngal{þun} ◊trans{person} to ◊ngal{&þun} ◊trans{humankind}.

An adjective indicating that something is made of some material is formed by suffixing ◊ngal{-da}: ◊ngal{van·} ◊trans{cloth} to ◊ngal{van·da} ◊trans{made of cloth}.

◊subsection{On verbs}

Reduplicating a verb indicates a repeated action:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{&xoc feŋ haai dâc fn ħjââ.}}
  ◊gla{&~xoc feŋ haai dâc fn ħjââ.}
  ◊glb{%freq~talk %pr.3%anim.%sg %intrs.%cl listen %neg %attd}
  ◊glfree{He talks and talks and never listens.}
}

A noun referring to the action of the verb is formed by suffixing ◊ngal{-nâ}: ◊ngal{tjâa} ◊trans{fight} to ◊ngal{tjâana} ◊trans{battle, war}.

◊subsection{On adjectives}

Reduplicating an adjective intensifies the meaning: ◊ngal{tup} ◊trans{white} to ◊ngal{&tup} ◊trans{very white}.

An adjective can be negated by prefixing ◊ngal{ħam-}:
