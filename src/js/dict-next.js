let db;
const statements = {};

async function init() {
  const initSqlJs = window.initSqlJs;
  const sqlPromise = initSqlJs({
    // Required to load the wasm binary asynchronously. Of course, you can host it wherever you want
    // You can omit locateFile completely when running in node
    locateFile: file => `/js/sql/${file}`
  });
  const dataPromise = fetch("/dictionary/next.sqlite").then(res => res.arrayBuffer());
  const [SQL, data] = await Promise.all([sqlPromise, dataPromise]);
  db = new SQL.Database(new Uint8Array(data));
  statements.queryFwd = db.prepare(
    `SELECT F.inflection, F.form, E.headword, E.body
      FROM Form F
      JOIN Entry E ON F.eid = E.rowid
      WHERE F.form = :searchTerm
      ORDER BY E.collation_key1, E.collation_key2, E.rowid;`);
  db.create_function('regexp', (pattern, string) => string.match(new RegExp(pattern)) != null);
  statements.queryFwdHw = db.prepare(
    `SELECT E.headword, E.body
      FROM Entry E
      WHERE E.headword MATCH :searchTerm
      ORDER BY E.collation_key1, E.collation_key2, E.rowid;`);
  statements.queryBack = db.prepare(
    `SELECT E.headword, E.body
      FROM Entry E
      WHERE E.translations MATCH :searchTerm
      ORDER BY E.collation_key1, E.collation_key2, E.rowid;`);
  statements.queryBackFull = db.prepare(
    `SELECT E.headword, E.body
      FROM Entry E
      WHERE E.search_body MATCH :searchTerm
      ORDER BY E.collation_key1, E.collation_key2, E.rowid;`);
  statements.queryAll = db.prepare(
    `SELECT E.headword, E.body
      FROM Entry E
      ORDER BY E.collation_key1, E.collation_key2, E.rowid;`);
  let [{ values: [[entryCount]] }] = db.exec("SELECT COUNT(*) FROM Entry;");
  let [{ values: [[formCount, uniqueFormCount]] }] =
    db.exec("SELECT COUNT(*), COUNT(DISTINCT form) FROM Form;");
  document.getElementById("entry-count").innerText =
    `Loaded ${entryCount} entries, ${formCount} forms (${uniqueFormCount} unique)`;
}

window.onload = init;

const LIMIT = 50;

function search() {
  let searchTerm = document.getElementById("search-term").value;
  let fuzzy = document.getElementById("search-fuzzy").checked;
  let outputArea = document.getElementById("search-results");
  outputArea.innerHTML = "";
  if (searchTerm == "") {
    let statement = statements.queryAll;
    searchSimple(statement, null, outputArea);
  } else if (document.getElementById("sm-forward").checked) {
    let statement = statements.queryFwd;
    statement.bind([searchTerm]);
    let count = 0;
    while (statement.step()) {
      let [inflection, form, headword, body] = statement.get();
      let child = resultElement(inflection, form, body);
      outputArea.appendChild(child);
      count += 1;
    }
    outputArea.appendChild(resultCountElement(count));
  } else if (document.getElementById("sm-backward").checked) {
    let statement = statements.queryBack;
    searchSimple(statement, searchTerm, outputArea);
  } else if (document.getElementById("sm-backward-full").checked) {
    let statement = statements.queryBackFull;
    searchSimple(statement, searchTerm, outputArea);
  } else if (document.getElementById("sm-forward-hw").checked) {
    let statement = statements.queryFwdHw;
    searchSimple(statement, searchTerm, outputArea);
  }
}

function searchSimple(statement, searchTerm, outputArea) {
  if (searchTerm !== null)
    statement.bind([searchTerm]);
  let count = 0;
  while (statement.step()) {
    let [headword, body] = statement.get();
    let child = resultElement(null, null, body);
    outputArea.appendChild(child);
    count += 1;
  }
  outputArea.appendChild(resultCountElement(count));
}

function resultElement(inflection, form, body) {
  let child = document.createElement("div");
  child.className = "search-entry";
  // We trust the contents of the SQLite database.
  child.innerHTML = body;
  if (form !== null) {
    let inflectionIndicator = document.createElement("div");
    inflectionIndicator.className = "search-inflection-list";
    inflectionIndicator.append(decodeInflection(inflection));
    inflectionIndicator.append(" (");
    let inflectionIndicatorWord = document.createElement("span");
    inflectionIndicatorWord.lang = "art-x-ncs-v9";
    inflectionIndicatorWord.innerText = form;
    inflectionIndicator.append(inflectionIndicatorWord);
    inflectionIndicator.append(")");
    child.firstChild.after(inflectionIndicator);
  }
  return child;
}

function resultCountElement(count) {
  let resultCount = document.createElement("div");
  resultCount.className = "result-count";
  resultCount.innerText = `${count} result${count == 1 ? "" : "s"}`;
  return resultCount;
}

const CASES = ["nominative", "accusative", "dative", "genitive", "locative", "instrumental", "abessive", "semblative"];
const NUMBERS = ["direct", "dual", "plural", "singulative", "generic"];
const SUBJECTS = [
  "first-person singular",
  "first-person exclusive dual",
  "first-person exclusive plural",
  "first-person generic",
  "first-person inclusive dual",
  "first-person inclusive plural",
  "second-person singular",
  "second-person dual",
  "second-person plural",
  "second-person generic",
  "third-person singular",
  "third-person dual",
  "third-person plural",
  "third-person generic",
];
const OBJECTS = [
  "none",
  "first-person singular",
  "first-person exclusive dual",
  "first-person exclusive plural",
  "first-person generic",
  "first-person inclusive dual",
  "first-person inclusive plural",
  "second-person singular",
  "second-person dual",
  "second-person plural",
  "second-person generic",
  "third-person celestial singular",
  "third-person terrestrial singular",
  "third-person human singular",
  "third-person human dual",
  "third-person human plural",
  "third-person epicene dual",
  "third-person epicene plural",
  "third-person generic",
  "reflexive",
  "reciprocal",
];
const TENSES = ["present", "past"];
const ASPECTS = ["imperfective", "perfective"];
const RCASES = ["nominative", "accusative", "dative", "genitive of nominative", "genitive of accusative", "genitive of dative"];
const GENDERS = ["celestial", "terrestrial", "human"];
const ATTACHMENTS = ["adnominal", "adverbial"];
const MOTIONS = ["static", "toward", "away"];
const POLARITIES = ["affirmative", "negative"];
const TRANSFINITES = [
  "second-person singular imperative",
  "supine",
  "active gerund (finite)",
  "active gerund (adnominal, terrestrial)",
  "active gerund (adnominal, non-terrestrial)",
  "passive gerund (finite)",
  "passive gerund (adnominal)",
];
const NOMVP_CASE_MOODS = [
  "nominative",
  "accusative",
  "dative indicative",
  "dative subjunctive",
  "genitive",
  "locative indicative",
  "locative subjunctive",
  "instrumental indicative",
  "instrumental subjunctive",
  "abessive indicative",
  "abessive subjunctive",
  "semblative indicative",
  "semblative subjunctive",
];

function decodeInflection(bytes) {
  function finverb() {
    let s = SUBJECTS[bytes[1]];
    let om = bytes[2] == 0 ? "" : ` to ${OBJECTS[bytes[2]]}`;
    let tense = TENSES[bytes[3]];
    let aspect = ASPECTS[bytes[4]];
    console.log(bytes);
    return `${s} ${om} ${aspect} ${tense}`;
  }
  let type = bytes[0];
  if (type == 0) {
    return "lemma";
  }
  if (type == 1) {
    let kaku = CASES[bytes[1]];
    let number = NUMBERS[bytes[2]];
    return `${kaku} ${number}`;
  }
  if (type == 2) {
    return finverb();
  }
  if (type == 3) {
    let rcase = RCASES[bytes[1]];
    let hcase = CASES[bytes[2]];
    let hgender = bytes[3] == 0 ? "" : ` ${GENDERS[bytes[3] - 1]}`;
    let hnumber = bytes[4] == 0 ? "" : ` ${NUMBERS[bytes[4] - 1]}`;
    return `${rcase} / ${hcase}${hgender}${hnumber} participle`;
  }
  if (type == 4) {
    return "infinitive";
  }
  if (type == 5) {
    let attachment = ATTACHMENTS[bytes[1]];
    let motion = bytes[2] == 0 ? "" : ` ${MOTIONS[bytes[2] - 1]}`;
    let obj = bytes[3] == 0 ? "" : ` to ${OBJECTS[bytes[3]]}`;
    let ancobj = bytes[4] == 0 ? "" : ` with ancilliary ${OBJECTS[bytes[4] - 1]}`;
    return `${attachment}${motion}${obj}${ancobj}`;
  }
  if (type == 6) {
    let vp = finverb();
    let motion = bytes[5] == 0 ? "" : `${MOTIONS[bytes[5] - 1]} `;
    let polarity = POLARITIES[bytes[6]];
    return `${motion}${vp} ${polarity} finite`;
  }
  if (type == 7) {
    let kaku = CASES[bytes[1]];
    let number = NUMBERS[bytes[2]];
    let motion = bytes[3] == 0 ? "" : ` ${MOTIONS[bytes[3] - 1]}`;
    return `${kaku} ${number}${motion} nominal`;
  }
  if (type == 8) {
    let trf = TRANSFINITES[bytes[1]];
    let om = bytes[2] == 0 ? "" : ` to ${OBJECTS[bytes[2]]}`;
    return `${trf}${om}`;
  }
  if (type == 9) {
    let trf = NOMVP_CASE_MOODS[bytes[1]];
    return `${trf} infinitive`;
  }
  return "wakaran";
}
