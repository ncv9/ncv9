#lang pollen

◊(require pollen/core pollen/setup)

◊define-meta[title]{Lexicon}
◊define-meta[chapter-type part]

This part describes the various aspects of Ŋarâþ Crîþ’s lexicon.

In this section, many words will be given by lemma alone instead of their full headwords, which can be found in the dictionary.

◊(when/splice (equal? (current-poly-target) 'html)
  (embed-pagetree "index.ptree"))
