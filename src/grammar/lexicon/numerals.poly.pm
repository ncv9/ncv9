#lang pollen

◊define-meta[title]{Numerals}

Ŋarâþ Crîþ has two sets of cardinal numerals: the ◊term{long numerals} and the ◊term{short numerals}.

◊table/x[#:options (table-options #:caption "Comparison between long numerals and short numerals." #:colgroup '(c d d) #:placement 'forcehere)]{
  Feature & Long numerals & Short numerals
  Length & Long & Short
  Range (within integers) & 1 – 16 & All integers
  Agreement & Case and sometimes gender & None
  Use of counter words & Not used & Usually required
  Order relative to noun phrase & After the noun phrase, possibly separated & Immediately after the noun phrase
  Orthography & Always spelled out & Spelled out or figures
}

◊section[#:id "numerals-long"]{Long numerals}

The long numerals up to 6 are inflected for case and gender (Tables ◊xref/l["long-numerals-1" #f] to ◊xref/l["long-numerals-6" #f]).

◊table/x[#:options (table-options #:caption "The long numerals for 1.") #:id "long-numerals-1"]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{mina} & ◊nc{minos} & ◊nc{minal}
  Accusative & ◊nc{minan} & ◊nc{minon} & ◊nc{minan}
  Dative & ◊nc{minas} & ◊nc{minoþ} & ◊nc{minels}
  Genitive & ◊nc{minen} & ◊nc{minen} & ◊nc{minjel}
  Locative & ◊nc{monas} & ◊nc{mjonos} & ◊nc{monas}
  Instrumental & ◊nc{minca} & ◊nc{cjamjonos} & ◊nc{minca}
  Abessive & ◊nc{minþa} & ◊nc{þjam·jonos} & ◊nc{minþa}
  Semblative & ◊nc{menit} & ◊nc{manot} & ◊nc{menit}
}

◊table/x[#:options (table-options #:caption "The long numerals for 2.")]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{nefa} & ◊nc{nefor} & ◊nc{nefac}
  Accusative & ◊nc{nefan} & ◊nc{nefon} & ◊nc{nefan}
  Dative & ◊nc{nefas} & ◊nc{nefos} & ◊nc{nefacþ}
  Genitive & ◊nc{nefen} & ◊nc{nefar} & ◊nc{nefen}
  Locative & ◊nc{nofas} & ◊nc{njofor} & ◊nc{nofas}
  Instrumental & ◊nc{nefca} & ◊nc{cjanjofor} & ◊nc{nefca}
  Abessive & ◊nc{nefoþa} & ◊nc{þjanofor} & ◊nc{nefocþa}
  Semblative & ◊nc{nefit} & ◊nc{nefot} & ◊nc{nefit}
}

◊table/x[#:options (table-options #:caption "The long numerals for 3.")]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{prêno} & ◊nc{prêngos} & ◊nc{prêmo}
  Accusative & ◊nc{prênon} & ◊nc{prêngon} & ◊nc{prêmon}
  Dative & ◊nc{prênos} & ◊nc{prênge} & ◊nc{prêmos}
  Genitive & ◊nc{prênen} & ◊nc{prêngel} & ◊nc{prêmen}
  Locative & ◊nc{prônos} & ◊nc{prôndos} & ◊nc{siłas}
  Instrumental & ◊nc{prônca} & ◊nc{cjaprôndos} & ◊nc{ciþpiłas}
  Abessive & ◊nc{prônþa} & ◊nc{þjap·rôndos} & ◊nc{þilp·iłas}
  Semblative & ◊nc{prênit} & ◊nc{prêngot} & ◊nc{prêmit}
}

◊table/x[#:options (table-options #:caption "The long numerals for 4.")]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{resiþ} & ◊nc{sriel} & ◊nc{resiþ}
  Accusative & ◊nc{resin} & ◊nc{srilen} & ◊nc{rešir}
  Dative & ◊nc{reþþas} & ◊nc{sriles} & ◊nc{reþþac}
  Genitive & ◊nc{resten} & ◊nc{risil} & ◊nc{resčor}
  Locative & ◊nc{risiþas} & ◊nc{ristos} & ◊nc{risiþas}
  Instrumental & ◊nc{rosicþa} & ◊nc{cjaristos} & ◊nc{rosicþa}
  Abessive & ◊nc{rosiþa} & ◊nc{þjaristos} & ◊nc{rosiþa}
  Semblative & ◊nc{rediþ} & ◊nc{redot} & ◊nc{rediþ}
}

◊table/x[#:options (table-options #:caption "The long numerals for 5.")]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{glêma} & ◊nc{glêmos} & ◊nc{glêmac}
  Accusative & ◊nc{glêman} & ◊nc{glêmon} & ◊nc{glêmor}
  Dative & ◊nc{glêmas} & ◊nc{glêmoþ} & ◊nc{glêmecþ}
  Genitive & ◊nc{glêmen} & ◊nc{glîmel} & ◊nc{glêmjor}
  Locative & ◊nc{glômas} & ◊nc{glâmos} & ◊nc{glômas}
  Instrumental & ◊nc{glômeca} & ◊nc{cjaglêmos} & ◊nc{glômeca}
  Abessive & ◊nc{glômeþa} & ◊nc{þjag·lêmos} & ◊nc{glômeþa}
  Semblative & ◊nc{glêmit} & ◊nc{glêmot} & ◊nc{glêmit}
}

◊table/x[#:options (table-options #:caption "The long numerals for 6.") #:id "long-numerals-6"]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{cfersîþ} & ◊nc{cfêrson} & ◊nc{cfêrsor}
  Accusative & ◊nc{cfersîn} & ◊nc{cfêrsanon} & ◊nc{cfêrsanor}
  Dative & ◊nc{cferþþâs} & ◊nc{cfêrsos} & ◊nc{cfêrsos}
  Genitive & ◊nc{cfersên} & ◊nc{cfêršel} & ◊nc{cfêršel}
  Locative & ◊nc{cfirsîþas} & ◊nc{cfîrsion} & ◊nc{cfîrsion}
  Instrumental & ◊nc{cforsîcþa} & ◊nc{cfîrsiol} & ◊nc{cfîrsiol}
  Abessive & ◊nc{cforsîþa} & ◊nc{cfîrsiocþ} & ◊nc{cfîrsiocþ}
  Semblative & ◊nc{cfelsiþ} & ◊nc{cfêlsoþ} & ◊nc{cfêlsoþ}
}

The rest of the long numerals (Tables ◊xref/l["long-numerals-7-to-11" #f] and ◊xref/l["long-numerals-12-to-16" #f]) are inflected for case only.

◊table/x[#:options (table-options #:caption "The long numerals from 7 to 11.") #:id "long-numerals-7-to-11"]{
  Case \ # & 7 & 8 & 9 & 10 & 11
  Nominative & ◊nc{plamis} & ◊nc{ŋatir} & ◊nc{nalarþ} & ◊nc{emra} & ◊nc{nalfo}
  Accusative & ◊nc{plamin} & ◊nc{ŋatjan} & ◊nc{nalan} & ◊nc{emran} & ◊nc{nalfon}
  Dative & ◊nc{plamiþa} & ◊nc{ŋatis} & ◊nc{nalþas} & ◊nc{emras} & ◊nc{nalfos}
  Genitive & ◊nc{plamen} & ◊nc{ŋatin} & ◊nc{naler} & ◊nc{emren} & ◊nc{nalfen}
  Locative & ◊nc{plemelt} & ◊nc{ŋotis} & ◊nc{nelarþ} & ◊nc{imras} & ◊nc{nolfos}
  Instrumental & ◊nc{plemilca} & ◊nc{ŋotica} & ◊nc{nolacþ} & ◊nc{omrac} & ◊nc{nolfoca}
  Abessive & ◊nc{plemilþa} & ◊nc{ŋotirþa} & ◊nc{nolerþ} & ◊nc{omreþ} & ◊nc{nofelþa}
  Semblative & ◊nc{plamit} & ◊nc{ŋadir} & ◊nc{nalirþ} & ◊nc{emlit} & ◊nc{nalfit}
}

◊table/x[#:options (table-options #:caption "The long numerals from 12 to 16.") #:id "long-numerals-12-to-16"]{
  Case \ # & 12 & 13 & 14 & 15 & 16
  Nominative & ◊nc{nedras} & ◊nc{tfalja} & ◊nc{grinjo} & ◊nc{grimin} & ◊nc{meþos}
  Accusative & ◊nc{nedran} & ◊nc{tfaljan} & ◊nc{grinjon} & ◊nc{griman} & ◊nc{meþon}
  Dative & ◊nc{nedraþ} & ◊nc{tfaljas} & ◊nc{grinjos} & ◊nc{grimis} & ◊nc{meþasos}
  Genitive & ◊nc{nedren} & ◊nc{tfaljen} & ◊nc{grinjen} & ◊nc{grimir} & ◊nc{meþel}
  Locative & ◊nc{nidraþ} & ◊nc{tfoljas} & ◊nc{gronjos} & ◊nc{grjomis} & ◊nc{moþos}
  Instrumental & ◊nc{nodracþa} & ◊nc{tfoljac} & ◊nc{grjonoc} & ◊nc{gromir} & ◊nc{cjameþos}
  Abessive & ◊nc{notreða} & ◊nc{tfoljeþ} & ◊nc{grjonoþ} & ◊nc{grjomiþ} & ◊nc{þjam·eþos}
  Semblative & ◊nc{nedlis} & ◊nc{tfalit} & ◊nc{grenit} & ◊nc{grimit} & ◊nc{meðot}
}

The lemma form of a long numeral is its attributive form. Because long numerals inflect for case and possibly gender, they can be separated from their heads.

◊; ◊p{A long numeral is ◊term{virtually terrestrial} if its instrumental and abessive forms are prefixed with ◊l0{cja-} and with ◊l0{þja-}. This includes the terrestrial forms of 1 – 5, plus 16.}

The following types of numerals can be derived from long numerals:

◊items{
  ◊item{To derive the pronominal form, prefix the particle ◊l1{a} (as a separate word): ◊l1{prêno} ◊trans{three}; ◊l1{a prêno} ◊trans{three of them}.}
  ◊item{To refer to a number itself rather than the quantity it represents, prepend ◊l0{âħ-} before a vowel or ◊l0{â-} before a consonant to the celestial form, partially leniting the numeral: ◊l1{meþos} ◊trans{sixteen}; ◊l1{âm·eþos} ◊trans{the number ‘16’}}
  ◊item{To create an adverb denoting the number of times something occurs, append ◊l0{-el} to the dative celestial form. Change any final ◊l0{-s} to ◊l0{-r}, unless there is any ◊l0{r} earlier in the numeral: ◊l1{nefas} ◊trans{two (dative)}; ◊l1{nefarel} ◊trans{twice}. Note that ◊l1{plamiþa} is changed to ◊l1{plamiþel}.}
}

◊subsection{Ordinal long numerals}

Ordinal numerals start from zero, such that number 0 refers to the first object, 1 refers to the second, and so on. Negative numerals refer to objects from the end: −1 refers to the last object, −2 to the second-last object, and so on. Ordinal long numerals occur before the noun phrase they modify but need not be adjacent to them.

◊table/x[#:options (table-options #:caption "The ordinal long numerals for 0.") #:id "long-numerals-ordinal-0"]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{elaþ} & ◊nc{elros} & ◊nc{elacþ}
  Accusative & ◊nc{elor} & ◊nc{eljon} & ◊nc{elor}
  Dative & ◊nc{eli} & ◊nc{eljos} & ◊nc{elic}
  Genitive & ◊nc{eliþ} & ◊nc{elþos} & ◊nc{elicþ}
  Locative & ◊nc{elaþa} & ◊nc{elaþos} & ◊nc{elaþac}
  Instrumental & ◊nc{elica} & ◊nc{elicon} & ◊nc{eliħac}
  Abessive & ◊nc{eliþa} & ◊nc{eliþon} & ◊nc{eliþac}
  Semblative & ◊nc{elit} & ◊nc{elot} & ◊nc{elit}
}

◊table/x[#:options (table-options #:caption "The ordinal long numerals for 1.") #:id "long-numerals-ordinal-1"]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{gesaþ} & ◊nc{geþos} & ◊nc{gesacþ}
  Accusative & ◊nc{gesor} & ◊nc{gešon} & ◊nc{gesor}
  Dative & ◊nc{ges} & ◊nc{geos} & ◊nc{ges}
  Genitive & ◊nc{gešiþ} & ◊nc{gešiþ} & ◊nc{gešicþ}
  Locative & ◊nc{gjaþa} & ◊nc{gjaþos} & ◊nc{gjaþac}
  Instrumental & ◊nc{gecþa} & ◊nc{gecþon} & ◊nc{geþþac}
  Abessive & ◊nc{geþa} & ◊nc{geþon} & ◊nc{geþac}
  Semblative & ◊nc{gesit} & ◊nc{gesot} & ◊nc{gesit}
}

◊table/x[#:options (table-options #:caption "The ordinal long numerals for 2.") #:id "long-numerals-ordinal-2"]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{nefasaþ} & ◊nc{nefaþos} & ◊nc{nefasacþ}
  Accusative & ◊nc{nefasor} & ◊nc{nefasor} & ◊nc{nefasor}
  Dative & ◊nc{nefasel} & ◊nc{nefasel} & ◊nc{nefasel}
  Genitive & ◊nc{nefasaþ} & ◊nc{nefašon} & ◊nc{nefasecþ}
  Locative & ◊nc{nefosaþa} & ◊nc{nefosaþos} & ◊nc{nefosaþac}
  Instrumental & ◊nc{nefosacþa} & ◊nc{nefosacþon} & ◊nc{nefosaþþac}
  Abessive & ◊nc{nefoseþa} & ◊nc{nefoseþon} & ◊nc{nefoseþac}
  Semblative & ◊nc{nefagit} & ◊nc{nafegot} & ◊nc{nefagit}
}

◊table/x[#:options (table-options #:caption "The ordinal long numerals for 3.") #:id "long-numerals-ordinal-3"]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{prêsaþ} & ◊nc{prêþon} & ◊nc{prêsacþ}
  Accusative & ◊nc{prêsor} & ◊nc{prêsor} & ◊nc{prêsor}
  Dative & ◊nc{prêsacel} & ◊nc{prêsacel} & ◊nc{prêsacel}
  Genitive & ◊nc{prêsaþas} & ◊nc{prêsaþon} & ◊nc{prêsaþac}
  Locative & ◊nc{prôsaþa} & ◊nc{prôsaþos} & ◊nc{prôsaþac}
  Instrumental & ◊nc{prôsacþa} & ◊nc{prôsacþon} & ◊nc{prôsaþþac}
  Abessive & ◊nc{prôseþa} & ◊nc{prôseþon} & ◊nc{prôseþac}
  Semblative & ◊nc{prêvit} & ◊nc{prêvot} & ◊nc{prêvit}
}

◊table/x[#:options (table-options #:caption "The ordinal long numerals for −1.") #:id "long-numerals-ordinal-minus-1"]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{mârda} & ◊nc{mârdos} & ◊nc{mârdac}
  Accusative & ◊nc{mârdan} & ◊nc{mârdon} & ◊nc{mârdan}
  Dative & ◊nc{mârdas} & ◊nc{mârdor} & ◊nc{mârdas}
  Genitive & ◊nc{mârden} & ◊nc{mârdel} & ◊nc{mârden}
  Locative & ◊nc{môrdes} & ◊nc{môrdos} & ◊nc{prôsaþac}
  Instrumental & ◊nc{môrdeca} & ◊nc{cjamôrdos} & ◊nc{môrdeca}
  Abessive & ◊nc{môrdeca} & ◊nc{þjam·ôrdos} & ◊nc{môrdeþa}
  Semblative & ◊nc{mârdit} & ◊nc{mârdot} & ◊nc{mârdot}
}

◊section[#:id "numerals-short"]{Short numerals}

The short numerals up to 16 are listed in ◊xref/l["short-numerals"]{Table}.

◊table/x[#:options (table-options #:caption ◊@{
  The short numerals up to 16, along with their forms when fused with ◊l1{sraþ} ◊trans{sixteen}.
} #:first-col-header? #f #:colgroup '(fig fig c c)) #:id "short-numerals"]{
  Hex & Dec & Base & After ◊l1{sraþ}
  0 & 0 & ◊nc{ces} & ◊nc{-sraþ}
  1 & 1 & ◊nc{vil} & ◊nc{-sraþfil}
  2 & 2 & ◊nc{sen} & ◊nc{-sraþþen}
  3 & 3 & ◊nc{en} & ◊nc{-sraþien}
  4 & 4 & ◊nc{tar} & ◊nc{-sraþtar}
  5 & 5 & ◊nc{do} & ◊nc{-sraðo}
  6 & 6 & ◊nc{mja} & ◊nc{-sraþmja}
  7 & 7 & ◊nc{len} & ◊nc{-srałen}
  8 & 8 & ◊nc{fe} & ◊nc{-srafþe}
  9 & 9 & ◊nc{ni} & ◊nc{-sraþni}
  A & 10 & ◊nc{re} & ◊nc{-sraþre}
  B & 11 & ◊nc{pin} & ◊nc{-sraþpin}
  C & 12 & ◊nc{va} & ◊nc{-sreþjon}
  D & 13 & ◊nc{ħas} & ◊nc{-srelħas}
  E & 14 & ◊nc{go} & ◊nc{-sracþo}
  F & 15 & ◊nc{łar} & ◊nc{-sratłar}
  10 & 16 & ◊nc{sraþ}
}

Short numerals up to ◊${16^2 = 256} of the form ◊${16 x + y} are roughly formed as ◊l0{◊${x}-sraþ-◊${y}}. ◊${x} is omitted if it equals 1, and ◊${y} is omitted if it equals 0. ◊l0{-sraþ-} fuses with certain values of ◊${y}, yielding the forms in the last column of ◊xref/l["short-numerals"]{Table}.

Short numerals up to ◊${16^4 = 65536} of the form ◊${256 x + y} are formed as ◊l0{◊${x}-flen-◊${y}}, where ◊${x} is omitted if equal to 1 and ◊${y} is omitted if zero.

Numerals beyond 65536 are formed by splitting the digits into groups of four from the least significant digit and using the words for powers of 65536 in ◊xref/l["short-numerals-powers-of-65536"]{Table }. A coefficient of one on the highest power of 65536 in a short numeral is omitted. Any power of 65536 with a coefficient of zero is omitted.

◊table/x[#:options (table-options #:caption ◊@{Short numerals: powers of 65536.} #:first-col-header? #f #:colgroup '(fig c)) #:id "short-numerals-powers-of-65536"]{
  # & Name
  ◊${16^4} & ◊nc{dara}
  ◊${16^8} & ◊nc{seta}
  ◊${16^{12}} & ◊nc{riso}
  ◊${16^{16}} & ◊nc{nanâ}
  ◊${16^{20}} & ◊nc{gelten}
  ◊${16^{24}} & ◊nc{sałar}
  ◊${16^{28}} & ◊nc{lina}
  ◊${16^{32}} & ◊nc{ðose}
}

◊table/x[#:options (table-options #:caption ◊@{Examples of short numerals.} #:first-col-header? #f #:colgroup '(fig fig c)) #:id "short-numerals-examples"]{
  Hex & Dec & Name
  11 & 17 & ◊nc{sraþfil}
  20 & 32 & ◊nc{sensraþ}
  45 & 69 & ◊nc{tarsraðo}
  BD & 189 & ◊nc{pinsrelħas}
  100 & 256 & ◊nc{flen}
  107 & 263 & ◊nc{flenlen}
  300 & 768 & ◊nc{enflen}
  9B30 & 39 728 & ◊nc{nisraþpinflenensraþ}
  1 87A4 & 100 260 & ◊nc{darafesrałenflenresraþtar}
  4 9AC2 & 301 762 & ◊nc{tardaranisraþrevasraþþen}
  1 0000 0500 & 4 294 968 576 & ◊nc{setadoflen}
  31 0001 0000 & 210 453 463 040 & ◊nc{ensraþfilsetavildara}
}

Like long numerals, short numerals are used primarily as determiners. Unlike long numerals, they require classifiers for most nouns.

◊table/x[#:options (table-options #:caption ◊@{Classifiers for short numerals.} #:first-col-header? #f #:colgroup '(c d)) #:id "short-numeral-classifiers"]{
  Classifier & Description
  ◊nc{-laþ} & humans and other sapient species
  ◊nc{-cef} & birds
  ◊nc{-del} & fish
  ◊nc{-cir} & insects and arachnids
  ◊nc{-mis} & medium-sized (approx. 2 kg) to large animals
  ◊nc{-þaf} & small animals (less than 2 kg)
  ◊nc{-nen} & woody plants (such as trees)
  ◊nc{-min} & non-woody plants (such as grass)
  ◊nc{-jen} & fruits and nuts
  ◊nc{-ðis} & flexible flat objects
  ◊nc{-čis} & rigid flat objects
  ◊nc{-gor} & balls and other spherical objects
  ◊nc{-če} & cylindrical or reasonably thick rod-like objects
  ◊nc{-sei} & ring-like objects
  ◊nc{-sin} & branches, roots, arms, and legs; thin rod-like objects
  ◊nc{-mic} & grains or other small particles; small insects
  ◊nc{-fin} & drops of liquids
  ◊nc{-ven} & written works (such as books)
  ◊nc{-cjan} & artistic or intellectual works (other than written works)
  ◊nc{-čar} & rooms, houses, buildings
  ◊nc{-čin} & weapons
  ◊nc{-tes} & connections or links; chains
  ◊nc{-ðe} & events in time
  ◊nc{-þrem} & celestial bodies
}

The following nouns do not have classifiers, but they must use a long numeral if available:

◊items{
  ◊item{Units of measurement}
  ◊item{The nouns ◊l1{sar} ◊trans{thing} or ◊l1{ðên} ◊trans{occurrence}}
}

◊; TODO: allot a paradigm for numeral+classifiers used as nouns

The numeral ◊l1{ces}, as well as any numeral that is or ends in ◊l1{ħas} or ◊l1{sreþas}, triggers a lenition in the classifier.

Adding the particle ◊l1{ceþe} before a short numeral negates it, eclipsing it. Since there are no negative long numerals, a negative short numeral can always be used for unclassified nouns.

To refer to the quantity described by a short numeral itself, a short numeral can be compounded with ◊l1{mener} as the head. The nominative form of a nominal short numeral may be abbreviated using figures followed by a ◊l1{/}, especially in data.

Short numerals other than the ones for −1, 0, 1, 2, and 3 can be used as ordinal numerals by placing them immediately before the noun phrase being modified with no classifier. For conventionalized ordinal numerals, the short numerals are used even for −1, 0, 1, 2, and 3:

◊gloss/x{
  ◊glfree{◊nc{1 coþħas sêna ceriþo *cortai inoravonen 3 envas tecto ascemat łanu.}}
  ◊gla{vil coþħ-as sêna cer-iþo *cort-ai inoravon-en en env-as tecto ascem-at łan-u.}
  ◊glb{one section-%dat.%sg above remain-%rel.%nom,%cel.%dat pipe-%dat.%pl inoravona-%gen.%sg three day-%dat.%sg before return-%inf must-%3gc}
  ◊glfree{According to Section 2, the remaining pipes must be returned before 3 Inoravona.}
}

◊section[#:id "numerals-nonintegral"]{Non-integral numerals}

◊subsection[#:id "numerals-rational"]{Rational numerals}

Some commonly-used fractions have long forms: ½ for each gender (Table ◊xref/l["long-numerals-1-2" #f]), and ⅓, ⅔, and ⅕ without distinctions in gender (Table ◊xref/l["long-numerals-frac" #f]).

◊table/x[#:options (table-options #:caption "The long numerals for ½.") #:id "long-numerals-1-2"]{
  Case \ Gender & Celestial & Terrestrial & Human
  Nominative & ◊nc{mâna} & ◊nc{mânos} & ◊nc{mânac}
  Accusative & ◊nc{mânan} & ◊nc{mânon} & ◊nc{mânor}
  Dative & ◊nc{môras} & ◊nc{môros} & ◊nc{môras}
  Genitive & ◊nc{mân} & ◊nc{mân} & ◊nc{mân}
  Locative & ◊nc{mâsor} & ◊nc{mâsor} & ◊nc{mâsor}
  Instrumental & ◊nc{mâŋa} & ◊nc{cjamâsel} & ◊nc{mâŋac}
  Abessive & ◊nc{mênþa} & ◊nc{þjam·âsel} & ◊nc{mênþac}
  Semblative & ◊nc{mânat} & ◊nc{mânot} & ◊nc{mânat}
}

◊table/x[#:options (table-options #:caption "The long numerals for other fractions.") #:id "long-numerals-frac"]{
  Case \ # & 1⁄3 & 2⁄3 & 1⁄5
  Nominative & ◊nc{sêþa} & ◊nc{nêþa} & ◊nc{acasa}
  Accusative & ◊nc{sêþan} & ◊nc{nêþan} & ◊nc{acasan}
  Dative & ◊nc{sêþas} & ◊nc{nêþas} & ◊nc{acaras}
  Genitive & ◊nc{sêþen} & ◊nc{nêþen} & ◊nc{acasen}
  Locative & ◊nc{sajoþos} & ◊nc{najoþos} & ◊nc{acos}
  Instrumental & ◊nc{soþoca} & ◊nc{neþoca} & ◊nc{acita}
  Abessive & ◊nc{sotoþa} & ◊nc{netoþa} & ◊nc{arica}
  Semblative & ◊nc{saðat} & ◊nc{nelðat} & ◊nc{acadet}
}

Rational numbers with a numerator of 1 are derived from the short numeral of the denominator plus a suffix that depends on case (Table ◊xref/l["frac-suffixes" #f]). When the short numeral is monosyllabic, an extra syllable ◊l0{-râ-} is infixed between the numeral and the suffix. Because of the presence of case agreement, these numerals are also considered long numerals.

◊table/x[#:options (table-options #:caption "Suffixes for unit fractions.") #:id "frac-suffixes"]{
  Case & Suffix
  Nominative & ◊nc{-ten}
  Accusative & ◊nc{-tane}
  Dative & ◊nc{-tens}
  Genitive & ◊nc{-teri}
  Locative & ◊nc{-tarþ}
  Instrumental & ◊nc{-tarco}
  Abessive & ◊nc{-tarþo}
  Semblative & ◊nc{-telca}
}

◊gloss/x{
  ◊glfree{◊nc{edvel mjare varâten.}}
  ◊gla{edv-el mjar-e va-râ-ten.}
  ◊glb{penny-%nom.%sv shilling-%nom.%sg twelve-%fracb-%frac.%nom}
  ◊glfree{One penny is 1⁄12th of a shilling.}
}

Vulgar fractions (i.e. of the form ◊${n/d}) are formed using the unit fraction for ◊${1/d} immediately followed by the numeral for ◊${n}. Since ◊${n} does not receive a classifier, it must be long if there is a long form available for ◊${n}, in which case its case and gender matches that of ◊${1/d}.

◊gloss/x{
  ◊glfree{◊nc{nemiren acasan nefan mênčesta.}}
  ◊gla{nem-iren acas-an nefan mênč-es-ta.}
  ◊glb{apple-%acc.%pl one_fifth-%acc two.%acc.%cel eat-2%sg-%past}
  ◊glfree{You ate two-fifths of an apple.}
}

◊gloss/x{
  ◊glfree{◊nc{ânirâteri plamen}}
  ◊gla{â-ni-râ-teri plamen}
  ◊glb{%nnom-nine-%fracb-%frac.%gen seven.%gen}
  ◊glfree{of the number 7⁄9}
}

◊gloss/x{
  ◊glfree{◊nc{a nisraþniten mjasraðo}}
  ◊gla{a ni-sraþ-ni-ten mja-sraðo}
  ◊glb{%npn nine-sixteen-nine-%frac.%nom six-sixteen.five}
  ◊glfree{101⁄153 of it}
}

A mixed number ◊${m + n/d} is expressed by using the numeral for ◊${m} (the long form if one is available, otherwise the short form), followed by the conjunction ◊l1{i} plus the numeral for ◊${n/d}.

◊gloss/x{
  ◊glfree{◊nc{nafsa mina i sêþa}}
  ◊gla{nafs-a mina i sêþa}
  ◊glb{nafsa-%nom.%sg one.%nom.%cel %mxn one_third-%nom}
  ◊glfree{1 ⅓ of a nafsa}
}

If a mixed number is used pronominally, then only the numeral for ◊${m} assumes its pronominal form:

◊gloss/x{
  ◊glfree{◊nc{a mina i sêþa}}
  ◊gla{a mina i sêþa}
  ◊glb{%npn one.%nom.%cel %mxn one_third-%nom}
  ◊glfree{1 ⅓ of it}
}

Likewise, a mixed number used nominally receives changes only to the numeral for ◊${m}:

◊gloss/x{
  ◊glfree{◊nc{sraþvilm·ener i mjarâten glêma}}
  ◊gla{sraþvil-m·en-er i mja-râ-ten glêma}
  ◊glb{sixteen.one-number-%nom.%sg %mxn six-%fracb-%frac.%nom five.%nom.%cel}
  ◊glfree{the number 17 ⅚}
}

◊subsection[#:id "numerals-inexact"]{Inexact numerals}

Inexact numerals are represented in scientific notation with base 16. The short-form digits of the significand are listed (with an implied decimal point after the first digit), followed by a suffix representing the exponent.

The exponent suffixes are listed in ◊xref/l["inexact-exponents"]{Table }. Odd powers over ◊${16^{6}} are derived from the power below, prepended with ◊l0{-lî-}. For instance, ◊${16^{11}} has the suffix ◊l0{-lîflerico}, from the suffix for ◊${16^{10}}.

Powers below ◊${16^{-4}} are derived from their corresponding reciprocals by prefixing ◊l0{-maga-}. For instance, ◊${16^{-12}} has the suffix ◊l0{-magaacþeno}, from the suffix for ◊${16^{12}}.

◊table/x[#:options (table-options #:caption ◊@{Exponents for use in short numerals.} #:first-col-header? #f #:colgroup '(fig c)) #:id "inexact-exponents"]{
  # & Name
  ◊${16^{24}} & ◊nc{-vanaso}
  ◊${16^{22}} & ◊nc{-retraþo}
  ◊${16^{20}} & ◊nc{-estrôto}
  ◊${16^{18}} & ◊nc{-dasvito}
  ◊${16^{16}} & ◊nc{-êsravo}
  ◊${16^{14}} & ◊nc{-pertapo}
  ◊${16^{12}} & ◊nc{-acþeno}
  ◊${16^{10}} & ◊nc{-flerico}
  ◊${16^{8}} & ◊nc{-nelepro}
  ◊${16^{6}} & ◊nc{-cjamiþo}
  ◊${16^{5}} & ◊nc{-lîþêrnero}
  ◊${16^{4}} & ◊nc{-têrnero}
  ◊${16^{3}} & ◊nc{-ermenroto}
  ◊${16^{2}} & ◊nc{-henroto}
  ◊${16^{1}} & ◊nc{-ermeþo}
  ◊${16^{0}} & ◊nc{-mino}
  ◊${16^{-1}} & ◊nc{-sevo}
  ◊${16^{-2}} & ◊nc{-garpo}
  ◊${16^{-3}} & ◊nc{-seħarpo}
  ◊${16^{-4}} & ◊nc{-têramo}
}

Note that adding or removing trailing zeroes in the significand ◊link["https://en.wikipedia.org/wiki/Significant_figures"]{changes the precision of the numeral}. A leading zero in the significand is not allowed unless it is the only digit.

Examples:

◊items{
  ◊item{◊l1{ensentarenłarmino} ◊trans{◊${3.243F_{16} \times 16^{0}}}}
  ◊item{◊l1{senvilcescenroto} ◊trans{◊${2.10_{16} \times 16^{2}}}}
  ◊item{◊l1{senvilhenroto} ◊trans{◊${2.1_{16} \times 16^{2}}}, which is a different number.}
  ◊item{◊l1{ceþeħarpo} ◊trans{◊${0 \times 16^{-3}}}; i.e. a quantity whose absolute value is less than ◊${8 \times 16^{-4}}.}
  ◊item{◊l1{lenłarfemjalîdasvito} ◊trans{◊${7.F86_{16} \times 16^{19}}}}
}

Inexact numerals are considered short numerals and thus take classifiers.

An inexact numeral with ◊l1{pen} in the significand is an ◊term{indefinite inexact numeral}, which shows only the order of magnitude of something. Unlike an inexact numeral with ◊l1{ces} as the magnitude, an indefinite inexact numeral establishes a lower bound on the quantity.

The inexact exponents are also used as prefixes for units of measure in systematic systems.

While the inexact numerals are the preferred set for expressing inexact quantities, exact numerals are sometimes used for this purpose in poetic language. Thus, we can prefix ◊l1{pen} to an exact numeral such as ◊l1{dara} to get an ◊term{indefinite exact numeral}.

◊subsection[#:id "numerals-complex"]{Complex numerals}

Complex numerals are composed of the real part, followed by ◊l0{-perin-} or ◊l0{-ŋilis-}, then the imaginary part. ◊l0{-perin-} is used when the imaginary part is positive and ◊l0{-ŋilis-} is used when it is negative. Both the real and the imaginary part are expressed as short numerals, and the resulting numeral is short.

If the real part is omitted, then it is assumed to be zero. If the imaginary part is omitted, then it is assumed to be either 1 or −1, depending on its sign. ◊l0{-ŋilis-} has the form ◊l0{-ŋils} word-finally.

The particle ◊l1{ceþe} affects only the real part of the numeral.

If one or both components are exact but not integral, then a rational numeral is used with a common denominator, in which the numerator is a complex numeral.

Alternatively, one or both components may be inexact, in which case the other must either be integral or inexact. If both components are inexact, then they must still be listed in full.

◊gloss/x{
  ◊glfree{◊nc{mjaperindo}}
  ◊gla{mja-perin-do}
  ◊glb{six-%cpx-five}
  ◊glfree{◊${6 + 5i}}
}

◊gloss/x{
  ◊glfree{◊nc{tarŋiliþen}}
  ◊gla{tar-ŋilis-sen}
  ◊glb{four-%cpx.%neg-two}
  ◊glfree{◊${4 - 2i}}
}

◊gloss/x{
  ◊glfree{◊nc{perinen}}
  ◊gla{perin-en}
  ◊glb{%cpx.%neg-three}
  ◊glfree{◊${3i}}
}

◊gloss/x{
  ◊glfree{◊nc{vilŋils}}
  ◊gla{vil-ŋils}
  ◊glb{one-%cpx.%neg}
  ◊glfree{◊${1 - i}}
}

◊gloss/x{
  ◊glfree{◊nc{ceþe vfeperinva}}
  ◊gla{ceþe v\fe-perin-va}
  ◊glb{minus eight-%cpx-twelve}
  ◊glfree{◊${-8 + 12i}}
}

◊gloss/x{
  ◊glfree{◊nc{âsêþa mjaperinsen}}
  ◊gla{â-sêþa mja-perin-sen}
  ◊glb{%nnom-one_third.%nom six-%cpx-two}
  ◊glfree{◊${\frac{1}{3} (6 + 2i) = 2 + \frac{2}{3}i}}
}

◊gloss/x{
  ◊glfree{◊nc{vilperinentarħaþenmino}}
  ◊gla{vil-perin-en-tar-ħas-sen-mino}
  ◊glb{one-%cpx-three-four-thirteen-two-%inexact.16⁰}
  ◊glfree{◊${\underbrace{1}_{\text{exact}} + \underbrace{3.4D2_{16}}_{\text{4 SFs}}i}}
}

◊section{Interrogative quantities}

Ŋarâþ Crîþ has the determiner ◊l1{met} ◊trans{how many?, how much?}. The nominal counterpart is ◊l1{metos, mjotos, medot} (IIIc.m).

◊section{Number agreement}

The morphological number of a noun modified by a numeral agrees with that numeral. All nouns take the generic number for the numeral 0 and may optionally take it for other numerals. In addition, numerals other than 0 are compatible with a different number depending on the clareþ of the noun:

◊items{
  ◊item{Singular nouns take the singular for ◊neg{1} and 1, the dual for ±2, and the plural for all other numerals.}
  ◊item{Collective nouns take the singulative for 1 and the collective for all other numerals.}
  ◊item{Mass nouns always take the direct number when modified by a numeral.}
}

◊section[#:id "numeral-clitics"]{Clitics for numerals}

The distinctness clitic ◊l1{=’ot} indicates that the items being counted are unique. This clitic can be used on all numerals as well as on the determiners ◊l1{mel} and ◊l1{dân} but makes sense only with integral quantities.

The bounding clitic ◊l1{=’ocþaf} specifies the the numeral specifies an upper bound for the number of items (◊trans{no more than}). This clitic can be used on all numerals. It cannot be used on ◊l1{mel}, nor can it be used on ◊l1{dân} (as it would be redundant).

◊section[#:id "finger-counting"]{Finger counting}

The most common method of finger counting in the Ŋarâþ Crîþ-speaking area represents each hexadecimal digit with one hand.

◊table/x[#:options (table-options #:caption ◊@{The gesture for each digit. The fingers from thumb to pinky are denoted T, I, M, R, and P.} #:first-col-header? #f #:colgroup '(fig c c)) #:id "finger-counting-digits"]{
  & ◊colspan[2]{Fingers extended}
  Digit (Hex) & Main & Variant 1
  ◊HEADER
  0 & –
  1 & P
  2 & RP
  3 & MRP
  4 & I
  5 & IP
  6 & IRP
  7 & IMRP
  8 & M
  9 & MP
  A & IMP
  B & IM & IP
  C & T
  D & TI
  E & TIM & IM
  F & TIMRP
}

◊section[#:id "numeric-prefixes"]{Numeric prefixes}

Ŋarâþ Crîþ has a set of prefixes used for derivation (e.g. in order to describe an entity of some number of parts). These are not based on the ordinary cardinal numeral system but rather on the prime factorization of a number.

An ◊term{ordinary prefix phrase} consists of one or more prefixes from ◊xref/l["numeric-prefix-table"]{Table}, such that the prefixes are sorted first by ascending base, then by descending exponent. A power greater than 6 or less than ◊neg{2} is expressed by compounding multiple prefixes of the same base until the desired power is reached. A ◊term{prefix phrase} is either an ordinary prefix phrase or a prefix from table ◊xref/l["numeric-prefix-special"]{Table}.

◊table/x[#:options (table-options #:caption ◊@{Numeric prefixes in Ŋarâþ Crîþ.} #:colgroup '(fig lb c c c c rb c c)) #:id "numeric-prefix-table"]{
  Factor & ◊${x^{1}} & ◊${x^{2}} & ◊${x^{3}} & ◊${x^{4}} & ◊${x^{5}} & ◊${x^{6}} & ◊${x^{-1}} & ◊${x^{-2}}
  2 & ◊nc{la} & ◊nc{le} & ◊nc{li} & ◊nc{lî} & ◊nc{lê} & ◊nc{lô} & ◊nc{lo} & ◊nc{lâ}
  3 & ◊nc{še} & ◊nc{ši} & ◊nc{šê} & ◊nc{šô} & ◊nc{šî} & ◊nc{šâ} & ◊nc{ša} & ◊nc{šo}
  5 & ◊nc{te} & ◊nc{to} & ◊nc{tâ} & ◊nc{tô} & ◊nc{tê} & ◊nc{ti} & ◊nc{ta} & ◊nc{tî}
  7 & ◊nc{fi} & ◊nc{fa} & ◊nc{fo} & ◊nc{fî} & ◊nc{fê} & ◊nc{fâ} & ◊nc{se} & ◊nc{sô}
  11 & ◊nc{łi} & ◊nc{ła} & ◊nc{łê} & ◊nc{ło} & ◊nc{łô} & ◊nc{łî} & ◊nc{łâ} & ◊nc{łe}
  13 & ◊nc{ga} & ◊nc{co} & ◊nc{gi} & ◊nc{gâ} & ◊nc{gô} & ◊nc{gê} & ◊nc{ge} & ◊nc{gî}
  17 & ◊nc{cþa} & ◊nc{cþâ} & ◊nc{cþi} & ◊nc{cþe} & ◊nc{cþê} & ◊nc{cþî} & ◊nc{cþo} & ◊nc{cþô}
  19 & ◊nc{jo} & ◊nc{cjo} & ◊nc{njo} & ◊nc{vjo} & ◊nc{sjo} & ◊nc{rjo} & ◊nc{je} & ◊nc{cje}
  23 & ◊nc{ja} & ◊nc{cja} & ◊nc{nja} & ◊nc{vja} & ◊nc{sja} & ◊nc{rja} & ◊nc{nje} & ◊nc{vje}
  29 & ◊nc{ri} & ◊nc{cri} & ◊nc{vri} & ◊nc{sri} & ◊nc{fri} & ◊nc{gri} & ◊nc{sje} & ◊nc{rje}
  31 & ◊nc{tfa} & ◊nc{tfâ} & ◊nc{tfi} & ◊nc{tfe} & ◊nc{tfê} & ◊nc{tfî} & ◊nc{tfo} & ◊nc{tfô}
  ◊; 139 & ◊nc{pi} & ◊nc{pe} & ◊nc{po} & ◊nc{pa} & ◊nc{vi} & ◊nc{ve} & ◊nc{vo} & ◊nc{vla}
}

◊table/x[#:options (table-options #:caption ◊@{Special numeric prefixes.} #:first-col-header? #f) #:id "numeric-prefix-special"]{
  Prefix & Gloss
  ◊nc{vli-} & one, mono-, uni-
  ◊nc{vlê-} & many, multi-, poly-
  ◊nc{þra-} & few, oligo-
}

If a prefix phrase modifies a noun that is monosyllabic in the nominative case, then an infix ◊l0{-i-} is added between them.

◊section{Numerals in writing}

The general rule for writing numerals is that anything from the short numeral sytem may either be written with digits or spelled out, and that anything else is always spelled out. For instance, ◊l1{navo glêmac} may not be written as **◊l1{navo 5} or even as **◊l1{navo 5âc}, but ◊l1{navo sensraþpinlaþ} may be written as ◊l1{navo 2Blaþ}. Similarly, ◊l1{nefasaþ forþ} cannot be written using figures at all, while ◊l1{len forþ} may be abbreviated as ◊l1{7 forþ}.

The short-numeral parts of rational numerals, which are as a whole considered long numerals, may also be abbreviated: ◊l1{sraþfilm·ener i mjarâten glêma} to ◊l1{11m·ener i 6râten glêma} (but ◊em{not} **◊l1{11m·ener i 6râten 5}).

Since the digits in inexact numerals are read one by one instead of respecting place value, a ◊i{łil} is inserted after the first digit when an inexact numeral is abbreviated: ◊l1{senvilcescenroto} to ◊l1{2·10henroto}.

◊section[#:id "units-of-measure"]{Units of measure}

For dimensions other than time, there are two systems of measure: the ◊term{traditional system} uses all units. The ◊term{systematic system} uses only one of the units, prefixed with inexact exponents; sometimes, the systematic unit is used without any prefixes, using inexact numerals instead.

◊subsection{Date and time}

The year is approximated as having 403 5⁄24 days; that is, the year length varies between 403 and 404 days, with five leap years per 24-year cycle. (In particular, the leap years fall on years 0, 5, 10, 15, and 20 mod 24.) This approximation drifts from the true year by about 1 day per 1234 years.

◊table/x[#:options (table-options #:caption ◊@{Units of time.} #:first-col-header? #f #:placement 'forcehere) #:id "time-measure"]{
  Unit & Conversion & Approximate SI equivalent
  ◊nc{eleþ} & ◊${403 \frac{5}{24}} ◊nc{envo} & 408.61 (Earth) days
  ◊nc{enva} & 48 ◊nc{elvo} & 24.32 hours
  ◊nc{elva} & 48 ◊nc{cenðor} & 30.4 minutes
  ◊nc{cenðos} & 48 ◊nc{rirêns} & 38 seconds
  ◊nc{rirens} & & 0.792 seconds
}

The year is also divided into 13 half-months (◊nc{senło}; sg. ◊nc{senłas}) of 31 or 32 days (approximating half of the lunar orbital period of 62.02 days). In non-leap years, each half-month is conveniently 31 days long. In leap years, the last half-month is extended by one day.

◊table/x[#:options (table-options #:caption ◊@{The names of each half-month.} #:placement 'forcehere #:colgroup '(fig c c)) #:id "time-half-months"]{
  # & Name & Earth equivalent
  0 & ◊nc{serend·rênerþ} & March 21 – April 17
  1 & ◊nc{tovresarta} & April 18 – May 15
  2 & ◊nc{vôrvaron} & May 16 – June 12
  3 & ◊nc{elþesêren} & June 13 – July 10
  4 & ◊nc{šisonm·êva} & July 11 – August 7
  5 & ◊nc{naram·jena} & August 8 – September 5
  6 & ◊nc{ceâþcfiþar} & September 6 – October 3
  7 & ◊nc{eltasnelserþ} & October 4 – October 31
  8 & ◊nc{inoravona} & November 1 – November 28
  9 & ◊nc{firjarcin} & November 29 – December 26
  10 & ◊nc{ercig·ina} & December 27 – January 23
  11 & ◊nc{sifłit·ano} & January 24 – February 20
  12 & ◊nc{ginasferþ} & February 21 – March 20
}

The calendar has a nine-day week. As a result, the day of week advances by two days every 24-year cycle, and the day-of-week pattern cycles every 216 years. The first six days of the week are considered work days, while the last two are rest days. The seventh day, in modern times, is a ‘half-work’ day.

◊table/x[#:options (table-options #:caption ◊@{The names of the days of the week.} #:placement 'forcehere #:colgroup '(fig c c)) #:id "time-days-of-week"]{
  # & Name & Associated element
  0 & ◊nc{pelesto} & darkness, chaos
  1 & ◊nc{venes} & fire
  2 & ◊nc{ħator} & water
  3 & ◊nc{anar} & earth
  4 & ◊nc{nerals} & air
  5 & ◊nc{cþilgesna} & stars
  6 & ◊nc{sivarja} & moon
  7 & ◊nc{elvina} & sun
  8 & ◊nc{felcaþa} & light, order
}

◊subsection{Length}

◊table/x[#:options (table-options #:caption ◊@{Units of length.} #:first-col-header? #f #:placement 'forcehere) #:id "length-measure"]{
  Unit & Conversion & Approximate SI equivalent
  ◊nc{nafsa} & 16 ◊nc{eletin} & 1.90 km
  ◊nc{eleten} & 12 ◊nc{vetin} & 119 m
  ◊nc{veten} & 8 ◊nc{avanto} & 9.92 m
  ◊nc{avanta} (systematic) & 6 ◊nc{rjasir} & 1.24 m
  ◊nc{rjaser} & 24 ◊nc{cento} & 20.7 cm
  ◊nc{centa} & 256 ◊nc{sanin} & 8.6 mm
  ◊nc{sanen} & & 3.4 μm
}

◊subsection[#:id "units-currency"]{Currency}

The traditional currency system, which was used in the Federation of Crîþja, is almost isomorphic to the British pre-decimal currency system. One ◊nc{erłol} is equivalent to 20 ◊nc{mjari}, and one ◊nc{mjare} is equivalent to 12 ◊nc{edva}. Unlike with the £sd system, one ◊nc{edvel} is divided into seven ◊nc{seedva}, although the ◊nc{seedvel} is used primarily as a unit of accounting.

In the full abbreviation, each unit has its own symbol, which is followed by the number of that unit involved. If there is more than one unit involved, then each such string is separated by spaces. Units of which there are zero are omissible only from the left and the right; that is, **◊l1{9{rł2 e3}} is not allowed and should be written as ◊l1{9{rł2 m0 e3}} instead. Likewise, the unabbreviated reading lists the units with their quantities, except that all units of which there are zero are omitted (i.e. ◊l1{erłoc nefa edva prêno’ce}).

In the condensed abbreviation, the ◊i{mjare} and ◊i{edva} amounts are separated with a ◊i{jedva}, with no ◊l1{m} or ◊l1{e}. If there are zero ◊i{mjari}, then the part before the slash is ◊l1{0}; if there are zero ◊i{edva}, then the part after the slash is the ◊i{ŋos}, ◊l1{’}. Note that the condensed abbreviation is not applicable if there are any ◊i{seedva}.

In the abbreviated reading, which also does not admit any ◊i{seedva}, amounts less than one ◊i{mjare} are expressed using a ◊xref["numerals.html" "numeric-prefixes" "Section"]{numeric prefix} plus ◊l1{edva}. Amounts of one ◊i{mjare} or more are expressed as follows:

◊items{
  ◊item{If there are any ◊i{erłer}, then they are declared as they would in the unabbreviated reading. If there are no lower units, then the process ends here.}
  ◊item{The nominal form of the long numeral corresponding to the number of ◊i{mjari} is inserted. If there are no ◊i{mjari}, then ◊l1{inora} ◊trans{emptiness} is used instead. If there are 17, 18, or 19 ◊i{mjari}, then there is no appropriate long numeral and so the corresponding short numeral with the classifier ◊l0{-čis} is used instead. Note that ◊l1{sraþfilčis}, ◊l1{sraþþenčis}, and ◊l1{sraþienčis} are not declined.}
  ◊item{The conjunction ◊l1{i} is used to separate the ◊i{mjare} and ◊i{edva} amounts.}
  ◊item{The short numeral corresponding to the number of ◊i{edva} is inserted.}
}

◊table/x[
  #:options (table-options
    #:caption "Expressions of various amounts of money in the traditional currency system."
    #:colgroup '(c d d d)
    #:long? #t
  ) #:id "units-currency-moar"
]{
  Quantity & Abbreviation & Long & Short
  1⁄7d. & ◊nc{9{s1}} & ◊nc{seedvel mina} & –
  2⁄7d. & ◊nc{9{s2}} & ◊nc{seedva nefa} & –
  3⁄7d. & ◊nc{9{s3}} & ◊nc{seedva prêno} & –
  4⁄7d. & ◊nc{9{s4}} & ◊nc{seedva resiþ} & –
  5⁄7d. & ◊nc{9{s5}} & ◊nc{seedva glêma} & –
  6⁄7d. & ◊nc{9{s6}} & ◊nc{seedva cfersîþ} & –
  1d. & ◊nc{9{e1}} & ◊nc{edvel mina} & ◊nc{vliedva}
  1 1⁄7d. & ◊nc{9{e1 s1}} & ◊nc{edvel mina seedvel mina’ce} & –
  1 2⁄7d. & ◊nc{9{e1 s2}} & ◊nc{edvel mina seedva nefa’ce} & –
  1 6⁄7d. & ◊nc{9{e1 s6}} & ◊nc{edvel mina seedva cfersîþ’ce} & –
  2d. & ◊nc{9{e2}} & ◊nc{edva nefa} & ◊nc{laedva}
  2 4⁄7d. & ◊nc{9{e2 s4}} & ◊nc{edva nefa seedva resiþ’ce} & –
  3d. & ◊nc{9{e3}} & ◊nc{edva prêno} & ◊nc{šeedva}
  4d. & ◊nc{9{e4}} & ◊nc{edva resiþ} & ◊nc{leedva}
  5d. & ◊nc{9{e5}} & ◊nc{edva glêma} & ◊nc{teedva}
  6d. & ◊nc{9{e6}} & ◊nc{edva cfersîþ} & ◊nc{lašeedva}; ◊nc{lomjare}
  7d. & ◊nc{9{e7}} & ◊nc{edva plamis} & ◊nc{fiedva}
  8d. & ◊nc{9{e8}} & ◊nc{edva ŋatir} & ◊nc{liedva}
  9d. & ◊nc{9{e9}} & ◊nc{edva nalarþ} & ◊nc{šiedva}
  10d. & ◊nc{9{eA}} & ◊nc{edva emra} & ◊nc{lateedva}
  11d. & ◊nc{9{eB}} & ◊nc{edva nalfo} & ◊nc{łiedva}
  1s. & ◊nc{9{m1}}; ◊nc{9{1/’}} & ◊nc{mjare mina} & ◊nc{mjare}; ◊nc{âmina i ces}
  1s. 1d. & ◊nc{9{m1 e1}}; ◊nc{9{1/1}} & ◊nc{mjare mina edvel mina’ce} & ◊nc{âmina i vil}
  1s. 2d. & ◊nc{9{m1 e2}}; ◊nc{9{1/2}} & ◊nc{mjare mina edva nefa’ce} & ◊nc{âmina i sen}
  1s. 3d. & ◊nc{9{m1 e3}}; ◊nc{9{1/3}} & ◊nc{mjare mina edva prêno’ce} & ◊nc{âmina i en}
  1s. 4d. & ◊nc{9{m1 e4}}; ◊nc{9{1/4}} & ◊nc{mjare mina edva resiþ’ce} & ◊nc{âmina i tar}; ◊nc{venos}
  1s. 6d. & ◊nc{9{m1 e6}}; ◊nc{9{1/6}} & ◊nc{mjare mina edva cfersîþ’ce} & ◊nc{âmina i mja}
  1s. 9d. & ◊nc{9{m1 e9}}; ◊nc{9{1/9}} & ◊nc{mjare mina edva nalarþ’ce} & ◊nc{âmina i ni}
  2s. & ◊nc{9{m2}}; ◊nc{9{2/’}} & ◊nc{mjarec nefa} & ◊nc{ânefa i ces}
  2s. 6d. & ◊nc{9{m2 e6}}; ◊nc{9{2/6}} & ◊nc{mjarec nefa edva cfersîþ’ce} & ◊nc{ânefa i mja}; ◊nc{cþîf}
  3s. & ◊nc{9{m3}}; ◊nc{9{3/’}} & ◊nc{mjari prêno} & ◊nc{âprêno i ces}
  3s. 6d. & ◊nc{9{m3 e6}}; ◊nc{9{3/6}} & ◊nc{mjari prêno edva cfersîþ’ce} & ◊nc{âprêno i mja}
  4s. & ◊nc{9{m4}}; ◊nc{9{4/’}} & ◊nc{mjari resiþ} & ◊nc{âresiþ i ces}
  4s. 6d. & ◊nc{9{m4 e6}}; ◊nc{9{4/6}} & ◊nc{mjari resiþ edva cfersîþ’ce} & ◊nc{âresiþ i mja}
  5s. & ◊nc{9{m5}}; ◊nc{9{5/’}} & ◊nc{mjari glêma} & ◊nc{âglêma i ces}; ◊nc{catra}
  6s. & ◊nc{9{m6}}; ◊nc{9{6/’}} & ◊nc{mjari cfersîþ} & ◊nc{âcfersîþ i ces}
  10s. & ◊nc{9{mA}}; ◊nc{9{A/’}} & ◊nc{mjari emra} & ◊nc{âħemra i ces}; ◊nc{ercjor}
  11s. 7d. & ◊nc{9{mB e7}}; ◊nc{9{B/7}} & ◊nc{mjari nalfo edva plamis’ce} & ◊nc{ânalfo i len}
  15s. 2d. & ◊nc{9{mF e2}}; ◊nc{9{F/2}} & ◊nc{mjari grimin edva nefa’ce} & ◊nc{âgrimin i sen}
  16s. 11d. & ◊nc{9{m10 eB}}; ◊nc{9{10/B}} & ◊nc{mjari meþos edva nalfo’ce} & ◊nc{âmeþos i pin}
  17s. & ◊nc{9{m11}}; ◊nc{9{11/’}} & ◊nc{mjari sraþfil} & ◊nc{sraþfilčis}
  17s. 5d. & ◊nc{9{m11 e5}}; ◊nc{9{11/5}} & ◊nc{mjari sraþfil edva glêma’ce} & ◊nc{sraþfilčis i do}
  18s. & ◊nc{9{m12}}; ◊nc{9{12/’}} & ◊nc{mjari sraþþen} & ◊nc{sraþþenčis}
  19s. & ◊nc{9{m13}}; ◊nc{9{13/’}} & ◊nc{mjari sraþien} & ◊nc{sraþienčis}
  19s. 11d. & ◊nc{9{m13 e11}}; ◊nc{9{13/B}} & ◊nc{mjari sraþien edva nalfo’ce} & ◊nc{sraþienčis i pin}
  19s. 11 6⁄7d. & ◊nc{9{m13 e11 s6}} & ◊nc{mjari sraþien edva nalfo’ce seedva cfersîþ’ce} & –
  £1. & ◊nc{9{rł1}}; ◊nc{9{rł1 0/’}} & ◊nc{erłol mina} & ◊nc{erłol}
  £1. 0s. 1d. & ◊nc{9{rł1 m0 e1}}; ◊nc{9{rł1 0/1}} & ◊nc{erłol mina edvel mina’ce} & ◊nc{erłol mina i inora i vil}
  £1. 1s. & ◊nc{9{rł1 m1}}; ◊nc{9{rł1 1/’}} & ◊nc{erłol mina mjare mina’ce} & ◊nc{erłol mina i âmina}
  £1. 3s. 8d. & ◊nc{9{rł1 m3 e8}}; ◊nc{9{rł1 3/8}} & ◊nc{erłol mina mjari prêno’ce edva ŋatir’ce} & ◊nc{erłol mina i âpreno i fe}
  £2. & ◊nc{9{rł2}}; ◊nc{9{rł2 0/’}} & ◊nc{erłoc nefa} & –
  £2. 3s. 6d. & ◊nc{9{rł2 m3 e6}}; ◊nc{9{rł2 3/6}} & ◊nc{erłoc nefa mjari prêno’ce edva cfersîþ’ce} & ◊nc{erłoc nefa i âpreno i mja}
  £14. 8s. 2d. & ◊nc{9{rłE m8 e2}}; ◊nc{9{rłE 8/2}} & ◊nc{erłer grinjo mjari ŋatir’ce edva nefa’ce} & ◊nc{erłer grinjo i âŋatir i sen}
  £34. 17s. 1d. & ◊nc{9{rł22 m11 e1}}; ◊nc{9{rł22 11/1}} & ◊nc{erłer sensraþþen mjari sraþfil’ce edvel mina’ce} & ◊nc{erłer sensraþþen i sraþfilčis i vil}
}

Another format for expressing currency, used in accounting, is ◊l1{9{◊var{E} ◊var{mm}◊var{e}◊var{s}}}, in which the units below the ◊i{erłol} are expressed as fixed-width fields. For instance, £127. 19s. 11 6⁄7d. (◊l1{9{rł7F m13 eB s6}} in the common format) can be expressed as ◊l1{9{7F 13B6}}. The right part of an amount expressed in the accounting format is read digit by digit, with the space read as ◊l1{inora}; thus the prior example would be read as ◊l1{lensratłar inora vilenpinmja}.

Rates of currency against currency (such as tax rates) are customarily given as an amount per ◊i{erłol}, giving a resolution of 1/1680 or roughly 0.06%.
