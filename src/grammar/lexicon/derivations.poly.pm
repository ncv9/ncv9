#lang pollen

◊define-meta[title]{Derivations}

This chapter outlines how new words can be made from existing ones in Ŋarâþ Crîþ.

◊section[#:id "compounding"]{Compounding}

Closed compound nouns can be formed by combining a noun or verb with a noun. The ‘head’ of the compound comes second. If the head noun is neither terrestrial nor a form of ◊l1{vês} ◊trans{system}, then it is lenited. Principal forms are inherited from the head noun. The first element of a closed compound, the modifier, is inflected in one of its ◊term{conjunct forms} depending on whether the head starts with a vowel.

Ŋarâþ Crîþ supports two types of open compounds: ◊term{genitive compounds} and ◊term{zero compounds}. Genitive compounds are made of a genitive noun phrase modifying a noun. The genitive noun phrase always has default number, and the genitive and the head noun cannot be separated by hyperbaton.

A zero compound is a head-initial compound where the second word either agrees with the case of the head noun or is uninflected. If the modifier is marked for case, then the two components can be separated as long as their relative order is preserved. Zero compounding is most often used for the names of languages, such as ◊l1{ŋarâþ crîþ} and ◊l1{ŋarâþ @asoren}. This construction is similar to that of ◊xref["../morphology/nouns.html" "names" "Section"]{qualified names}.

◊section[#:id "derivation"]{Derivation}

◊subsection[#:id "verb2noun"]{Verb to noun}

For all of the following derivations, the derived nouns are in the celestial gender unless they refer to human beings.

◊subsubsection[#:id "verb2noun-agent-inanimate"]{The instrument derivation}

The ◊term{instrument derivation} forms a noun that describes an instrument that performs an action. The resulting noun is a second-declension ◊l0{-ir} noun whose:

◊items{
  ◊item{N stem is ◊l0{◊ṡ{N}◊ṫ[0]ł},}
  ◊item{L stem is ◊l0{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋng}s},}
  ◊item{S stem is ◊l0{◊ṡ{N}◊ṫ[0]ł}, and}
  ◊item{locative vowel is ◊l0{e}.}
}

◊subsubsection[#:id "verb2noun-agent-animate"]{The agent derivation}

The ◊term{agent derivation} forms a noun that describes an animate being that performs an action. Ŋarâþ Crîþ has two different derivations: the ◊term{durable agent derivation} is used to derive nouns that refer to someone who usually performs the action over a long span of time. For instance, it could refer to those performing the action as part of their profession. The durable agent noun of a verb is a fourth-declension noun whose:

◊items{
  ◊item{N stem is ◊l0{◊ṡ{I}ið} for most nouns. If ◊nc{◊ṡ{I}} can be matched to ◊l0{◊ṡ{I′}a}, then it is ◊l0{◊ṡ{I′}eð}; if ◊nc{◊ṡ{I}} can be matched to ◊l0{◊ṡ{I′}i}, then it is ◊l0{◊ṡ{I′}îð}.}
  ◊item{L stem is ◊l0{◊ṡ{L}◊ẋ{þ}},}
  ◊item{S stem is ◊l0{◊ṡ{P}îd}, and}
  ◊item{thematic and locative vowel are ◊l0{a}.}
}

The ◊term{transient agent derivation} is used for referents that generally engage in the action over a more limited span of time. The transient agent noun of a verb is a second-declension ◊l0{-ir} noun whose:

◊items{
  ◊item{N stem is ◊l0{◊ṡ{N}◊ṫ[0]d},}
  ◊item{L stem is ◊l0{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋng}d},}
  ◊item{S stem is ◊l0{◊ṡ{N}◊ṫ[0]d}, and}
  ◊item{locative vowel is ◊l0{e}.}
}

Note that the distinction between the durable and transient agent derivations is lexical. That is, someone who becomes a teacher for one month before switching to another career would still have been a ◊l1{sareþ}, not a ◊l1{saradir}.

◊subsubsection[#:id "verb2noun-patient"]{The patient derivation}

The ◊term{patient derivation} forms a noun that describes something to which an action is done. The resulting noun is a second-declension ◊l0{-êr} noun whose:

◊items{
  ◊item{N stem is the I stem of the verb,}
  ◊item{G stem is the N stem of the verb,}
  ◊item{L stem is the L stem of the verb,}
  ◊item{S stem is ◊l0{◊ṡ{N}(ê)r}, and}
  ◊item{locative vowel is ◊l0{i}.}
}

◊subsubsection[#:id "verb2noun-location"]{The location derivation}

The ◊term{location derivation} forms a noun that describes a location at which an action happens. The resulting noun is a first-declension ◊l0{-es} noun whose:

◊items{
  ◊item{N stem is ◊l0{◊ṡ{I}◊ṫ[0]ł},}
  ◊item{L stem is ◊l0{◊ṡ{I}◊ṫ[0]◊ẋ{◊ẋgen}ł},}
  ◊item{S stem is ◊l0{◊ṡ{N}◊ṫ[0]łc}, and}
  ◊item{locative vowel is ◊l0{a}.}
}

◊subsubsection[#:id "verb2noun-quality"]{The quality derivation}

The ◊term{quality derivation} forms a noun that describes the action or quality described the verb. This is a second-declension ◊l0{-erþ} noun whose:

◊items{
  ◊item{N stem is the I stem of the verb,}
  ◊item{L stem is the L stem of the verb,}
  ◊item{S stem is the N stem of the verb, and}
  ◊item{locative vowel is ◊l0{e}.}
}

If the I stem ends with ◊l0{-a-} or ◊l0{-aħ-}, then the noun is a second-declension ◊l0{-arþ} noun instead, with the final ◊l0{-a-} or ◊l0{-aħ-} removed from the noun’s N and S stems.

If the I stem ends with ◊l0{-r-}, then the quality derivation might be an ◊l0{-eþ} noun instead.

◊subsection[#:id "verb2verb"]{Verb to verb}

◊subsubsection[#:id "aspect"]{Other aspects}

The prefix ◊l0{es-} forms an inceptive or inchoative form of a verb (◊trans{start ~ing}), and ◊l0{car◊|sei|-} forms a cessative (◊trans{stop ~ing}): ◊l1{mitrit} ◊trans{run}; ◊l1{esmitrit} ◊trans{start running}; ◊l1{carm·itrit} ◊trans{stop running}. The cessative prefix does not cause any mutation in inflected forms of ◊l1{eþit} or ◊l1{telit}.

The prefix ◊l0{er◊|sei|-} forms a terminative form of a verb (◊trans{finish ~ing}) and is applicable only to lexically telic verbs. In participles, which do not mark for tense, it usually indicates an action that happened in the past.

◊subsection[#:id "noun2noun"]{Noun to noun}

◊items{
  ◊item{Augmentative: ◊l0{ar-}}
  ◊item{Diminuitive: ◊l0{-in}, ◊l0{-(n)tin}, ◊l0{-(n)čin}, or ◊l0{e-}}
}

◊subsection[#:id "noun2verb"]{Noun to verb}

An associated verb can be derived from a noun, usually with a relational prefix such as ◊l0{el-} or ◊l0{caþ-}.

For first-declension nouns, the resulting verb is a vitreous I₁ verb with ◊ṡ{I}, ◊ṡ{N}, ◊ṡ{P}, ◊ṡ{R}, ◊ṡ{Q} = ◊l0{◊ṡ{N}}, ◊ṡ{L} = ◊l0{◊ṡ{L}}, ◊ṫ[0] = ◊l0{i}, ◊ṫ[1] = ◊l0{a}, ◊ṫ[2] = ◊l0{∅}, and ◊ṫ[3] = ◊l0{s}.

For second-declension nouns, …

For third-declension nouns, the resulting verb is a vitreous I₁ verb with ◊ṡ{I}, ◊ṡ{N}, ◊ṡ{R} = ◊l0{◊ṡ{N}e}, ◊ṡ{P}, ◊ṡ{Q} = ◊l0{◊ṡ{N}◊ẋ{n}}, ◊ṡ{L} = ◊l0{◊ṡ{L}◊ẋ{t}e}, ◊ṫ[0] = ◊l0{a}, ◊ṫ[1] = ◊l0{a}, ◊ṫ[2] = ◊l0{∅}, and ◊ṫ[3] = ◊l0{s}.

For fourth-declension nouns, …

For fifth-declension nouns, the resulting verb is a resinous III₂ verb with ◊ṡ{I} = ◊l0{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}}, ◊ṡ{N} = ◊l0{◊ṡ{N}◊ḋ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}}, ◊ṡ{P} = ◊ṡ{Q} = ◊l0{◊ṡ{N}◊ḋ[1]◊ṫ[2]◊ẋ{◊ẋ*mod}}, ◊ṡ{R} = ◊l0{◊ṡ{N}◊ḋ[2]◊ṫ[2]◊ẋ{◊ẋ*mod}}, ◊ṡ{L} = ◊l0{◊ṡ{L}}, and ◊ṫ[0] = ◊l0{a i i a i a a i} « ◊sc{gen.st}. The rolls are evaluated on the base noun without the relational prefix.

For sixth-declension nouns, the resulting verb is a resinous I₁ verb with ◊ṡ{I}, ◊ṡ{N}, ◊ṡ{P}, ◊ṡ{R}, ◊ṡ{Q} = ◊l0{◊ṡ{N}en}, ◊ṡ{L} = ◊l0{◊ṡ{L}◊ṫ[0]s}, ◊ṫ[0] = ◊l0{i}, ◊ṫ[1] = ◊l0{a}, ◊ṫ[2] = ◊l0{∅}, and ◊ṫ[3] = ◊l0{s}.

◊subsection[#:id "calculus"]{Calculus}

New words can also be derived by differentiating or integrating existing terms.

◊table/x[#:id "calculus-affixes" #:options (table-options #:caption "Calculus affixes in Ŋarâþ Crîþ." #:pdf-backend 'tabularray)]{
  Variable & Operation & Infix & Nominal
  ◊cellspan[3 1]{Time} & Derivative (◊sc{ddt}) & ◊nc{mitra} & ◊nc{mitra, motras, mitrit} (Ic)
  Reciprocal (◊sc{ddt.rec}) & ◊nc{genna} & ◊nc{genna, gelnas, gendit} (Ic)
  Integral (◊sc{idt}) & ◊nc{arcja} & ◊nc{arcja, arpes, arcit} (Ic)
  ◊cellspan[2 1]{Space (1D)} & Derivative (◊sc{ddx}) & ◊nc{cþivo} & ◊nc{cþivo, cþelvas, cþivit} (Ic)
  Integral (◊sc{idx}) & ◊nc{jando} & ◊nc{jando, jondas, jandit} (Ic)
  ◊cellspan[2 1]{Space (2D)} & Derivative (◊sc{dda}) & ◊nc{relne} & ◊nc{relen, rjales, cjareles, cjarilo, relit} (VIc)
  Integral (◊sc{ida}) & ◊nc{senna} & ◊nc{senan, sines, cjasines, cjasuno, sengit} (VIc)
  ◊cellspan[2 1]{Space (3D)} & Derivative (◊sc{ddxv}) & ◊nc{marša} & ◊nc{marša, miršas, maršit} (Ic)
  Integral (◊sc{idxv}) & ◊nc{ganto} & ◊nc{ganto, gentas, gandit} (Ic)
  ◊cellspan[2 1]{Population} & Derivative (◊sc{ddp}) & ◊nc{gille} & ◊nc{gille, gelles, gildit} (Ic)
  Integral (◊sc{idp}) & ◊nc{grija} & ◊nc{grija, rijes, riħit} (Ic)
}

Each affix listed in ◊xref/l["calculus-affixes"]{Table} has a reciprocal counterpart. For most affixes, this is derived by inverting the tone of the second-to-last vowel of the affix (◊l0{-relne-} ◊trans{◊sc{dda}} → ◊l0{-rêlne-} ◊trans{◊sc{dda}.◊sc{rec}; ‘reciprocal of the derivative with respect to 2-dimensional space’}), but the reciprocal of ◊l0{-mitra-} is ◊l0{-genna-}. In the case of the nominal forms, this translates to inverting the tone of the last vowel of each stem. However, the I′ stem of the reciprocal of ◊l0{-senna-} is ◊l0{sîn}.

◊subsubsection[#:id "calculus-verb"]{Infix forms}

Different kinds of verbs can be modified with the calculus affixes by infixing the infix forms immediately after the last vowel of the stem.

The affixes can be used on stative verbs, turning its meaning from ◊trans{◊S is high in ◊var{y}} to ◊trans{◊S is high in ◊${dy/dt} &c.}:

◊items{
  ◊item{◊l1{ecljat} ◊trans{◊S is far from ◊I}}
  ◊item{◊l1{e◊mark{mitra}cljat} ◊trans{◊S is moving quickly from ◊I}}
  ◊item{◊l1{e◊mark{arcja}cljat} ◊trans{◊S has a high absement from ◊I}}
  ◊item{◊l1{e◊mark{genna}cljat} ◊trans{◊S is moving slowly from ◊I}}
}

◊; expand use in the future?
Active verbs can take only ◊l0{-mitra-} and ◊l0{-genna-}, indicating the speed at which the action is done.

◊subsubsection[#:id "calculus-noun"]{Nominal forms}

A compound of a noun (in its conjunct form) and the nominal form of a calculus affix can be used. Nouns that can be modified in this way include those that describe measurable quantities or objects: ◊l1{ariga} ◊trans{warmth}; ◊l1{arigamitra} ◊trans{rate of temperature change}; ◊l1{arigamarša} ◊trans{temperature gradient in 3D space}.

In addition, the numeral ◊l1{âmina} and the determiners ◊l1{mel} and ◊l1{dan} can be combined with the affixes in the same way.
