#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{The Ŋarâþ Crîþ v9 grammar}

◊strong{The current grammar is currently undergoing major revisions.}

This is the official grammar of Ŋarâþ Crîþ.

Note to mobile users: The grammar contains a few extra-wide tables that might overflow the right edge of the paper. If you’re bothered by that, then you might want to turn your phone sideways for those.

An ◊link["grammar.pdf"]{experimental PDF version of the grammar} is available. This is made on a best-effort basis, and there’s no guarantee that it won’t look terrible.

◊(embed-pagetree "index.ptree"
    #:ignore (set 'grammar/old/index.html))

◊show-version[]

◊section[#:id "old-versions"]{Old versions}

Previous stabilized versions of Ŋarâþ Crîþ v9.

PDF versions of these grammars are not provided.

◊(embed-pagetree "old/index.ptree" #:max-depth 1)
