#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Version 9c}
◊define-meta[date]{2022-06-14}

This is ◊b{version 9c} of the grammar of Ŋarâþ Crîþ, stabilized on ◊render-date{2022-06-14}.

Note to mobile users: The grammar contains a few extra-wide tables that might overflow the right edge of the paper. If you’re bothered by that, then you might want to turn your phone sideways for those.

There is also ◊link["ncv9c.kaltoklel"]{a snapshot of the dictionary file} available for this version.

◊(embed-pagetree "index.ptree")

