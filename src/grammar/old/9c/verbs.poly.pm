#lang pollen

◊define-meta[title]{Verbs}

◊term{Predicates} can be divided into ◊term{independent verbs} (◊term{iverbs}) and ◊term{relationals}. In this chapter, we look at verbs; ◊xref/c["relationals.html" #:type 'inline]{the next chapter} covers relationals.

◊table/x[#:options (table-options
    #:caption "Comparison between independent verbs and relationals."
    #:colgroup '(c d d))]{
  Feature & Independent verbs & Relationals
  Semantics & Can indicate an action or state & Can indicate a state in relation to another entity
  Valency & 1 – 3 arguments & Usually 2 arguments, but occasionally 3
  Case frame & Nominative, plus possibly accusative and dative arguments depending on the verb & Nominative plus dative by default; the second argument can be accusative to change the meaning to involve motion towards the object or abessive for motion away from it
  Finite form & Finite conjugations & Attached to a scaffolding verb
  Modifying nouns & Participle forms (marked for case of shared noun in both the embedded and the main clause, as well as the gender of the shared noun in the main clause) & Lemma form or modified, depending on bias
  Modifying verbs & Converbal forms & Lemma form or modified, depending on bias
  Nominalized form & Particle + infinitive & ?
}

The adnominal and adverbial forms of a predicate are collectively called ◊term{modifying forms}.

◊section[#:id "valency"]{Valency and case frame}

All predicates have at least a nominative-case argument (the ◊term{subject}). Verbs are classified into five valency classes:

◊items{
  ◊item{◊term{Intransitive verbs} take only a nominative argument, without an accusative or dative argument.}
  ◊item{◊term{Semitransitive verbs} take a nominative and dative argument.}
  ◊item{◊term{Transitive verbs} take a nominative and accusative argument.}
  ◊item{◊term{Ditransitive verbs} take a nominative, accusative, and dative argument.}
  ◊item{◊xref["verbs.html" "aux-tour" "Subsection"]{◊term{Auxiliary verbs}} are not iverbs and thus are not predicates. Instead, they are a class of ◊term{predicate modifier}.}
}

Note that the labels of “nominative”, “accusative”, and “dative” arguments are somewhat arbitrary; the roles of each case depends on the verb in question. However, there are some general tendencies:

◊items{
  ◊item{The agent, if present, is almost always the nominative argument.}
  ◊item{If the action described by a verb is perceived as having a direct effect on the patient, then the verb is more likely to be transitive than semitransitive.}
  ◊item{Conversely, if the action described by a verb is perceived as having an ◊em{indirect} effect on the patient, then the verb is more likely to be semitransitive than transitive.}
  ◊item{In a ditransitive verb with both a theme and a recipient, the theme is more likely to be accusative than dative.}
  ◊item{If there is an experiencer, then it is more likely than not to be in the nominative.}
}

◊section[#:id "combinators"]{Predicate modifiers}

◊term{Predicate modifiers} take as input one or more predicates and output another predicate as a result. They include:

◊items{
  ◊item{Auxiliary verbs: the resulting predicate has the same case frame as the input}
  ◊item{The causative voice affix ◊l0{do-}, which increases valency by one}
  ◊item{Adverbial phrases}
}

◊section[#:id "categories"]{Verb categories}

Predicates can mark the person and number of various arguments. First-person dual or plural arguments marked this way are also distinguished by clusivity. Whenever this is allowed, it might be possible to instead specify a reflexive or reciprocal argument.

Ŋarâþ Crîþ has two tenses: ◊term{present} (more precisely, ◊term{nonpast}) and ◊term{past}. In addition to the present, the present tense covers the future as well as the immediate past.

The present tense is also used as a narrative present: in stories, the past tense is used only for events that had happened before the current point.

Ŋarâþ Crîþ has two aspects: ◊term{imperfective} and ◊term{perfective}. The imperfective aspect is used for ongoing (such as progressive or habitual) actions.

In conjugation, aspects can be labeled as ◊term{direct} or ◊term{indirect}. The perfective aspect is used for completed actions. The ◊term{direct aspect} is imperfective for the present tense and perfective for the past tense; the ◊term{inverse aspect} is the other aspect.

◊section[#:id "inflection"]{Inflection of verbs}

The primary lemma form of a verb is its ◊term{infinitive}, which ends in ◊l0{-at} or ◊l0{-it}. The ◊term{stem} of a verb is the part of the infinitive before such an ending. The ◊term{thematic vowel} is the vowel of the ending.

In some verbs, the last vowel in the stem changes in the present forms, the past forms, or both. A verb might have different vowel affections between present and past forms. This process is called ◊term{vowel affection}.

◊section[#:id "finite"]{Finite forms}

The following tables list the subject affixes used for the finite forms of a verb. If adding one of these affixes results in two adjacent identical vowels (ignoring differences in tone), then ◊l0{ħ} is added between them. Additionally, if a verb ends in ◊l0{j-at}, then the ◊l0{j} is deleted in the third-person plural form.

◊table/x[#:options (table-options #:caption "Thematic vowel derivatives for verbal conjugation." #:first-col-header? #f)]{
  0 & 1 & 2 & 3
  a & a & e & i
  i & e & a & o
}

The thematic vowels 1◊|prime| and 2◊|prime| are low-tone versions of 1 and of 2.

◊table/x[#:options (table-options #:caption ◊@{Conjugation of verbs according to the subject (direct aspect).})]{
  Person \ Number & Singular & Dual & Plural & Generic
      1st excl. & ◊nc{-1} & ◊nc{-1n} & ◊nc{-2cþ} & ◊nc{-1f}
      1st incl. & & ◊nc{-1◊|prime|n} & ◊nc{-2◊|prime|cþ} &
      2nd & ◊nc{-2s} & ◊nc{-2ns} & ◊nc{-2r} & ◊nc{-2f}
      3rd & ◊nc{-2} & ◊nc{-2n} & ◊nc{-3} & ◊nc{-u}
}

A third-person generic subject in this case often corresponds to an impersonal subject.

Next, an object affix can be added. An object affix shows the person and number of an accusative or dative argument; it is never necessary, but it cannot appear redundantly to an explicitly stated argument.

◊table/x[#:options (table-options #:caption "Object affixes.")]{
  Person \ Number & Singular & Dual & Plural & Generic
  1st excl. & ◊nc{pe} & ◊nc{pjo} & ◊nc{po} & ◊nc{ten}
  1st incl. & & ◊nc{pjô} & ◊nc{pô} &
  2nd & ◊nc{ve} & ◊nc{vi} & ◊nc{vo} & ◊nc{ves}
  3rd celestial & ◊nc{le}
  3rd terrestrial & ◊nc{lu}
  3rd human & ◊nc{les} & ◊nc{lis} & ◊nc{los}
  3rd epicene & & ◊nc{li} & ◊nc{lo} & ◊nc{las}
  Reflexive & ◊colspan[4]{◊nc{cin}}
  Reciprocal & ◊colspan[4]{◊nc{riþ}}
}

If a past-tense form is desired, then ◊l0{-þ} (after a vowel or ◊l0{-r}) or ◊l0{-ta} (otherwise) is added after the object affix (if present).

Conjugation in the inverse aspect uses the following endings.

◊table/x[#:options (table-options #:caption ◊@{Conjugation of verbs according to the subject (inverse aspect).})]{
  Person \ Number & Singular & Dual & Plural & Generic
      1st excl. & ◊nc{-1l} & ◊nc{-1nis} & ◊nc{-2cþis} & ◊nc{-1f}
      1st incl. & & ◊nc{-1◊|prime|nis} & ◊nc{-2◊|prime|cis} &
      2nd & ◊nc{-2res} & ◊nc{-2nsis} & ◊nc{-2ris} & ◊nc{-2f}
      3rd & ◊nc{-2l} & ◊nc{-2nis} & ◊nc{-3ris} & ◊nc{-os}
}

In perfective forms, the verb is eclipsed.

◊section[#:id "participle"]{Participles (adnominal forms)}

Participle forms of verbs are used in ◊term{relative clauses}. The ◊term{common argument} of a relative clause is the referent shared between the relative clause and the outer clause. Participles are then distinguished by:

◊items{
  ◊item{the ◊term{relative case} or ◊term{rcase} – the case of the common argument in the relative clause (nominative, accusative, or dative; as well as the genitives of any of these separataely)}
  ◊item{the ◊term{head case} or ◊term{hcase} – the case of the common argument in the outer clause (any case)}
}

and optionally, depending on the genus:

◊items{
  ◊item{the ◊term{head gender} or ◊term{hgender} – the gender of the common argument in the outer clause}
  ◊item{the ◊term{head number} or ◊term{hnumber} – the number of the common argument in the outer clause}
}

In terms of participle forms, verbs are first divided into ◊term{genera}, which describe the broad pattern of inflection, and within each genus into ◊term{species}.

◊term{Type I genera} mark for hgender but not for hnumber. ◊term{Type II genera} mark for hnumber but not for hgender.

◊p[#:id "impersonator"]{Across all genera, vowel affection occurs: if the rcase is nominative, then the present-tense vowel affection is used; otherwise, the past-tense affection is used. Some verbs, however, have ◊term{impersonator stems}, which are used instead of the stems used for finite forms. (Vowel affection has no effect on impersonator stems.)}

A participle in a type I genus with terrestrial hgender triggers a lenition in the noun it modifies if the noun follows the participle immediately.

Because participles agree with the heads of relative clauses, relative clauses can be moved away from their heads.

Participle forms can also take object affixes, but the affixes occur before the root in this case. If an object affix is present, then it describes a nominative, accusative, or dative argument in the embedded clause that is not the common argument. Again, an object affix cannot appear redundantly to an explicitly stated argument.

◊subsection[#:id "genus0"]{Genus 0}

Genus 0 conjugates for rcase, hcase, and hgender.

Each species of genus 0 specifies only the forms in which the rcase is nominative. The rest are derived from these as such:

◊items{
  ◊item{infix ◊l0{-aþ-} between the stem and the ending to get the respective forms with accusative rcase,}
  ◊item{infix ◊l0{-eþ-} to get the respective forms with dative rcase,}
  ◊item{infix ◊l0{-en-} to get the respective forms with genitive-of-nominative rcase,}
  ◊item{infix ◊l0{-anþ-} to get the respective forms with genitive-of-accusative rcase, and}
  ◊item{infix ◊l0{-enþ-} to get the respective forms with genitive-of-dative rcase.}
}

If the stem ends with ◊l0{þ} or ◊l0{ð}, then all ◊l0{þ}s in the above infixes are replaced with ◊l0{t}s. In addition, some affixes have starting vowels that are elided when the rcase is accusative, dative, or genitive-of-nominative; these forms are marked with ◊i{¹}.

◊subsubsection[#:id "species0c"]{Species 0c}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 0c." #:placement 'forcehere)]{
  Hcase \ Hgender & Celestial & Terrestrial & Human
  Nominative & ◊nc{-a} & ◊nc{-os} & ◊nc{-ac}
  Accusative & ◊nc{-an} & ◊nc{-on} & ◊nc{-or}
  Dative & ◊nc{-as} & ◊nc{-asos} & ◊nc{-as}
  Genitive & ◊nc{-en} & ◊nc{-el} & ◊nc{-(j)or}
  Locative & ◊nc{-es} & ◊nc{-orþ} & ◊nc{-es}
  Instrumental & ◊nc{-epa} & ◊nc{-epos} & ◊nc{-epac}
  Abessive & ◊nc{-eši} & ◊nc{-eši} & ◊nc{-ešic}
  Semblative & ◊nc{-et} & ◊nc{-ot} & ◊nc{-est}
}

◊subsubsection[#:id "species0e"]{Species 0e}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 0e." #:placement 'forcehere)]{
  Hcase \ Hgender & Celestial & Terrestrial & Human
  Nominative & ◊nc{-o} & ◊nc{-or} & ◊nc{-oc}
  Accusative & ◊nc{-an} & ◊nc{-on} & ◊nc{-ol}
  Dative & ◊nc{-es} & ◊nc{-os} & ◊nc{-es}
  Genitive & ◊nc{-en} & ◊nc{-el} & ◊nc{-(j)or}
  Locative & ◊nc{-as} & ◊nc{-orþ} & ◊nc{-as}
  Instrumental & ◊nc{-epo} & ◊nc{-epos} & ◊nc{-epoc}
  Abessive & ◊nc{-ełi} & ◊nc{-ełi} & ◊nc{-ełic}
  Semblative & ◊nc{-et} & ◊nc{-ot} & ◊nc{-est}
}

◊subsubsection[#:id "species0n"]{Species 0n}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 0n." #:placement 'forcehere)]{
  Hcase \ Hgender & Celestial & Terrestrial & Human
  Nominative & ◊nc{-e} & ◊nc{-on} & ◊nc{-ec}
  Accusative & ◊nc{-en} & ◊nc{-anon} & ◊nc{-(j)or}
  Dative & ◊nc{-es} & ◊nc{-os} & ◊nc{-es}
  Genitive & ◊nc{-in} & ◊nc{-el} & ◊nc{-ir}
  Locative & ◊nc{-ari} & ◊nc{-ori} & ◊nc{-aris}
  Instrumental & ◊nc{-ela}¹ & ◊nc{-elon}¹ & ◊nc{-elac}¹
  Abessive & ◊nc{-ena}¹ & ◊nc{-enor}¹ & ◊nc{-enac}¹
  Semblative & ◊nc{-it} & ◊nc{-ot} & ◊nc{-ist}
}

◊subsubsection[#:id "species0v"]{Species 0v}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 0v." #:placement 'forcehere)]{
  Hcase \ Hgender & Celestial & Terrestrial & Human
  Nominative & ◊nc{-il} & ◊nc{-el} & ◊nc{-el}
  Accusative & ◊nc{-in} & ◊nc{-en} & ◊nc{-(j)or}
  Dative & ◊nc{-ils} & ◊nc{-els} & ◊nc{-els}
  Genitive & ◊nc{-iłi} & ◊nc{-ełi} & ◊nc{-iłic}
  Locative & ◊nc{-eri} & ◊nc{-erþ} & ◊nc{-eris}
  Instrumental & ◊nc{-if} & ◊nc{-ef} & ◊nc{-ef}
  Abessive & ◊nc{-eta}¹ & ◊nc{-etor}¹ & ◊nc{-etac}¹
  Semblative & ◊nc{-it} & ◊nc{-et} & ◊nc{-ist}
}

◊subsection[#:id "genus1"]{Genus 1}

Genus 1 conjugates for rcase, hcase, and hgender.

Stems of genus 1 verbs must end with a valid coda. The forms with non-genitive rcases are provided explicitly; genitive-rcase forms are derived from their non-genitive-rcase counterparts by infixing ◊l0{-n-} between the stem and the ending.

◊subsubsection[#:id "species1c"]{Species 1c}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 1c." #:placement 'forcehere)]{
  Hcase \ Hgender & Celestial & Terrestrial & Human
  ◊pseudoheader{Nominative rcase}
  Nominative & ◊nc{-a} & ◊nc{-os} & ◊nc{-ac}
  Accusative & ◊nc{-e} & ◊nc{-es} & ◊nc{-ec}
  Dative & ◊nc{-as} & ◊nc{-oþ} & ◊nc{-as}
  Genitive & ◊nc{-er} & ◊nc{-or} & ◊nc{-er}
  Locative & ◊nc{-ai} & ◊nc{-ai} & ◊nc{-ai}
  Instrumental & ◊nc{-el} & ◊nc{-el} & ◊nc{-el}
  Abessive & ◊nc{-ef} & ◊nc{-ef} & ◊nc{-ef}
  Semblative & ◊nc{-et} & ◊nc{-ot} & ◊nc{-est}
  ◊pseudoheader{Accusative rcase}
  Nominative & ◊nc{-ea} & ◊nc{-ios} & ◊nc{-eac}
  Accusative & ◊nc{-ei} & ◊nc{-osi} & ◊nc{-ei}
  Dative & ◊nc{-eas} & ◊nc{-ioþ} & ◊nc{-eas}
  Genitive & ◊nc{-eri} & ◊nc{-ori} & ◊nc{-eri}
  Locative & ◊nc{-ami} & ◊nc{-ami} & ◊nc{-ami}
  Instrumental & ◊nc{-iel} & ◊nc{-iel} & ◊nc{-iel}
  Abessive & ◊nc{-ief} & ◊nc{-ief} & ◊nc{-ief}
  Semblative & ◊nc{-aþet} & ◊nc{-aþot} & ◊nc{-aþest}
  ◊pseudoheader{Dative rcase}
  Nominative & ◊nc{-ora} & ◊nc{-oros} & ◊nc{-orac}
  Accusative & ◊nc{-ero} & ◊nc{-eros} & ◊nc{-ero}
  Dative & ◊nc{-oras} & ◊nc{-orþ} & ◊nc{-oras}
  Genitive & ◊nc{-eþer} & ◊nc{-eþor} & ◊nc{-eþer}
  Locative & ◊nc{-emi} & ◊nc{-emi} & ◊nc{-emi}
  Instrumental & ◊nc{-erel} & ◊nc{-erel} & ◊nc{-erel}
  Abessive & ◊nc{-eref} & ◊nc{-eref} & ◊nc{-eref}
  Semblative & ◊nc{-eþet} & ◊nc{-eþot} & ◊nc{-eþest}
}

◊subsubsection[#:id "species1e"]{Species 1e}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 1e." #:placement 'forcehere)]{
  Hcase \ Hgender & Celestial & Terrestrial & Human
  ◊pseudoheader{Nominative rcase}
  Nominative & ◊nc{-iþ} & ◊nc{-os} & ◊nc{-icþ}
  Accusative & ◊nc{-inþ} & ◊nc{-on} & ◊nc{-irþ}
  Dative & ◊nc{-iþo} & ◊nc{-oþ} & ◊nc{-icþo}
  Genitive & ◊nc{-ir} & ◊nc{-or} & ◊nc{-ir}
  Locative & ◊nc{-ef} & ◊nc{-ef} & ◊nc{-ef}
  Instrumental & ◊nc{-ens} & ◊nc{-ens} & ◊nc{-ens}
  Abessive & ◊nc{-ilt} & ◊nc{-elt} & ◊nc{-elt}
  Semblative & ◊nc{-it} & ◊nc{-ot} & ◊nc{-ist}
  ◊pseudoheader{Accusative rcase}
  Nominative & ◊nc{-aþ} & ◊nc{-avas} & ◊nc{-acþ}
  Accusative & ◊nc{-anþ} & ◊nc{-avan} & ◊nc{-arþ}
  Dative & ◊nc{-aþo} & ◊nc{-avaþ} & ◊nc{-acþo}
  Genitive & ◊nc{-ar} & ◊nc{-avar} & ◊nc{-ar}
  Locative & ◊nc{-af} & ◊nc{-af} & ◊nc{-af}
  Instrumental & ◊nc{-ans} & ◊nc{-ans} & ◊nc{-ans}
  Abessive & ◊nc{-alt} & ◊nc{-alt} & ◊nc{-alt}
  Semblative & ◊nc{-aþit} & ◊nc{-aþat} & ◊nc{-aþist}
  ◊pseudoheader{Dative rcase}
  Nominative & ◊nc{-eþ} & ◊nc{-evas} & ◊nc{-ecþ}
  Accusative & ◊nc{-enþ} & ◊nc{-evan} & ◊nc{-erþ}
  Dative & ◊nc{-eþo} & ◊nc{-evaþ} & ◊nc{-ecþo}
  Genitive & ◊nc{-er} & ◊nc{-evar} & ◊nc{-er}
  Locative & ◊nc{-eþef} & ◊nc{-eþef} & ◊nc{-eþef}
  Instrumental & ◊nc{-eþens} & ◊nc{-eþens} & ◊nc{-eþens}
  Abessive & ◊nc{-iþelt} & ◊nc{-eþelt} & ◊nc{-eþelt}
  Semblative & ◊nc{-eþit} & ◊nc{-eþot} & ◊nc{-eþist}
}

◊subsection[#:id "genus2"]{Genus 2}

Genus 2 conjugates for rcase, hcase, and hnumber.

Each species of genus 2 specifies only the forms in which the rcase is nominative. The rest are derived from these as such:

◊items{
  ◊item{infix ◊l0{-el-} between the stem and the ending to get the respective forms with accusative rcase,}
  ◊item{infix ◊l0{-eł-} to get the respective forms with dative rcase,}
  ◊item{infix ◊l0{-Vn-} to get the respective forms with genitive-of-nominative rcase, where ◊l0{V} is the first vowel of the nominative-rcase ending,}
  ◊item{infix ◊l0{-Vnl-} to get the respective forms with genitive-of-accusative rcase, and}
  ◊item{infix ◊l0{-Vnł-} to get the respective forms with genitive-of-dative rcase.}
}

◊subsubsection[#:id "species2c"]{Species 2c}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 2c." #:placement 'forcehere)]{
  Hcase \ Hnumber & Singular & Dual & Plural & Generic
  Nominative & ◊nc{-a} & ◊nc{-ac} & ◊nc{-o} & ◊nc{-af}
  Accusative & ◊nc{-an} & ◊nc{-or} & ◊nc{-on} & ◊nc{-ef}
  Dative & ◊nc{-as} & ◊nc{-ecþ} & ◊nc{-os} & ◊nc{-of}
  Genitive & ◊nc{-en} & ◊nc{-el} & ◊nc{-in} & ◊nc{-if}
  Locative & ◊nc{-es} & ◊nc{-esta} & ◊nc{-is} & ◊nc{-efo}
  Instrumental & ◊nc{-els} & ◊nc{-elþa} & ◊nc{-ils} & ◊nc{-efos}
  Abessive & ◊nc{-elca} & ◊nc{-elcac} & ◊nc{-ilca} & ◊nc{-elcef}
  Semblative & ◊nc{-it} & ◊nc{-icta} & ◊nc{-et} & ◊nc{-icþ}
}

◊subsubsection[#:id "species2e"]{Species 2e}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 2e." #:placement 'forcehere)]{
  Hcase \ Hnumber & Singular & Dual & Plural & Generic
  Nominative & ◊nc{-e} & ◊nc{-ec} & ◊nc{-o} & ◊nc{-ef}
  Accusative & ◊nc{-en} & ◊nc{-or} & ◊nc{-on} & ◊nc{-efen}
  Dative & ◊nc{-es} & ◊nc{-ecþ} & ◊nc{-os} & ◊nc{-of}
  Genitive & ◊nc{-in} & ◊nc{-il} & ◊nc{-in} & ◊nc{-if}
  Locative & ◊nc{-is} & ◊nc{-ista} & ◊nc{-is} & ◊nc{-ifo}
  Instrumental & ◊nc{-ils} & ◊nc{-ilþa} & ◊nc{-ils} & ◊nc{-ifos}
  Abessive & ◊nc{-ilca} & ◊nc{-ilcac} & ◊nc{-ilca} & ◊nc{-ilcef}
  Semblative & ◊nc{-it} & ◊nc{-icta} & ◊nc{-et} & ◊nc{-icþ}
}

◊subsubsection[#:id "species2n"]{Species 2n}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 2n." #:placement 'forcehere)]{
  Hcase \ Hnumber & Singular & Dual & Plural & Generic
  Nominative & ◊nc{-on} & ◊nc{-or} & ◊nc{-el} & ◊nc{-u}
  Accusative & ◊nc{-an} & ◊nc{-ar} & ◊nc{-en} & ◊nc{-af}
  Dative & ◊nc{-os} & ◊nc{-ocþ} & ◊nc{-erþ} & ◊nc{-as}
  Genitive & ◊nc{-ol} & ◊nc{-acel} & ◊nc{-il} & ◊nc{-ef}
  Locative & ◊nc{-ecþ} & ◊nc{-ecþ} & ◊nc{-acþ} & ◊nc{-ecef}
  Instrumental & ◊nc{-els} & ◊nc{-els} & ◊nc{-ils} & ◊nc{-elsef}
  Abessive & ◊nc{-elca} & ◊nc{-elcac} & ◊nc{-elca} & ◊nc{-elcef}
  Semblative & ◊nc{-ot} & ◊nc{-octos} & ◊nc{-ot} & ◊nc{-ocþ}
}

◊subsubsection[#:id "species2v"]{Species 2v}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 2v." #:placement 'forcehere)]{
  Hcase \ Hnumber & Singular & Dual & Plural & Generic
  Nominative & ◊nc{-as} & ◊nc{-ac} & ◊nc{-o} & ◊nc{-af}
  Accusative & ◊nc{-an} & ◊nc{-or} & ◊nc{-on} & ◊nc{-ef}
  Dative & ◊nc{-aþ} & ◊nc{-acþ} & ◊nc{-oþ} & ◊nc{-of}
  Genitive & ◊nc{-en} & ◊nc{-el} & ◊nc{-in} & ◊nc{-if}
  Locative & ◊nc{-eþ} & ◊nc{-eþ} & ◊nc{-iþ} & ◊nc{-eþef}
  Instrumental & ◊nc{-ens} & ◊nc{-ens} & ◊nc{-ins} & ◊nc{-ensef}
  Abessive & ◊nc{-elcas} & ◊nc{-elcac} & ◊nc{-elco} & ◊nc{-elcef}
  Semblative & ◊nc{-it} & ◊nc{-icta} & ◊nc{-et} & ◊nc{-icþ}
}

◊subsubsection[#:id "species2o"]{Species 2o}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 2o." #:placement 'forcehere)]{
  Hcase \ Hnumber & Singular & Dual & Plural & Generic
  Nominative & ◊nc{-er} & ◊nc{-ec} & ◊nc{-ir} & ◊nc{-ef}
  Accusative & ◊nc{-en} & ◊nc{-or} & ◊nc{-in} & ◊nc{-efen}
  Dative & ◊nc{-erþ} & ◊nc{-ecþ} & ◊nc{-irþ} & ◊nc{-of}
  Genitive & ◊nc{-els} & ◊nc{-els} & ◊nc{-ens} & ◊nc{-if}
  Locative & ◊nc{-ast} & ◊nc{-ast} & ◊nc{-est} & ◊nc{-ast}
  Instrumental & ◊nc{-al} & ◊nc{-al} & ◊nc{-ol} & ◊nc{-al}
  Abessive & ◊nc{-acjal} & ◊nc{-acjal} & ◊nc{-ecjal} & ◊nc{-acjal}
  Semblative & ◊nc{-it} & ◊nc{-icta} & ◊nc{-et} & ◊nc{-icþ}
}

◊subsubsection[#:id "species2s"]{Species 2s}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 2s." #:placement 'forcehere)]{
  Hcase \ Hnumber & Singular & Dual & Plural & Generic
  Nominative & ◊nc{-in} & ◊nc{-ir} & ◊nc{-ir} & ◊nc{-if}
  Accusative & ◊nc{-ine} & ◊nc{-inor} & ◊nc{-eni} & ◊nc{-efin}
  Dative & ◊nc{-ins} & ◊nc{-inecþ} & ◊nc{-insa} & ◊nc{-is}
  Genitive & ◊nc{-el} & ◊nc{-ecel} & ◊nc{-il} & ◊nc{-ef}
  Locative & ◊nc{-os} & ◊nc{-ocþ} & ◊nc{-or} & ◊nc{-ost}
  Instrumental & ◊nc{-ocþe} & ◊nc{-oþec} & ◊nc{-ocþe} & ◊nc{-ocþef}
  Abessive & ◊nc{-orþe} & ◊nc{-orþec} & ◊nc{-orþe} & ◊nc{-orþef}
  Semblative & ◊nc{-it} & ◊nc{-icte} & ◊nc{-et} & ◊nc{-icþ}
}

◊subsection[#:id "genus3"]{Genus 3}

Genus 3 conjugates for rcase, hcase, and hgender.

Each species of genus 3 specifies only the forms in which the rcase is accusative or dative. The rest are derived from these as such:

◊items{
  ◊item{infix ◊l0{-en-} between the stem and the ending of the accusative-rcase form to get the respective forms with nominative rcase,}
  ◊item{infix ◊l0{-il-} between the stem and the ending of the accusative-rcase form to get the respective forms with genitive-of-nominative rcase,}
  ◊item{infix ◊l0{-ag-} between the stem and the ending of the accusative-rcase form to get the respective forms with genitive-of-accusative rcase,}
  ◊item{infix ◊l0{-eg-} between the stem and the ending of the dative-rcase form to get the respective forms with genitive-of-dative rcase,}
}

◊subsubsection[#:id "species3c"]{Species 3c}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 3c." #:placement 'forcehere)]{
  Hcase \ Hnumber & Celestial & Terrestrial & Human
  ◊pseudoheader{Accusative rcase}
  Nominative & ◊nc{-ar} & ◊nc{-on} & ◊nc{-ar}
  Accusative & ◊nc{-an} & ◊nc{-anon} & ◊nc{-an}
  Dative & ◊nc{-arþ} & ◊nc{-os} & ◊nc{-as}
  Genitive & ◊nc{-en} & ◊nc{-el} & ◊nc{-en}
  Locative & ◊nc{-ica} & ◊nc{-icos} & ◊nc{-iac}
  Instrumental & ◊nc{-eli} & ◊nc{-els} & ◊nc{-eli}
  Abessive & ◊nc{-eno} & ◊nc{-enos} & ◊nc{-eno}
  Semblative & ◊nc{-et} & ◊nc{-ot} & ◊nc{-est}
  ◊pseudoheader{Dative rcase}
  Nominative & ◊nc{-or} & ◊nc{-eħon} & ◊nc{-or}
  Accusative & ◊nc{-aħon} & ◊nc{-onon} & ◊nc{-aħon}
  Dative & ◊nc{-orþ} & ◊nc{-eħos} & ◊nc{-orþ}
  Genitive & ◊nc{-in} & ◊nc{-il} & ◊nc{-in}
  Locative & ◊nc{-oca} & ◊nc{-ecos} & ◊nc{-avac}
  Instrumental & ◊nc{-ore} & ◊nc{-ore} & ◊nc{-ore}
  Abessive & ◊nc{-one} & ◊nc{-one} & ◊nc{-one}
  Semblative & ◊nc{-aħet} & ◊nc{-aħot} & ◊nc{-aħest}
}

◊subsection[#:id "genus4"]{Genus 4}

Genus 4 conjugates for rcase, hcase, and hnumber.

The forms with non-genitive rcases are provided explicitly; genitive-rcase forms are derived from their non-genitive-rcase counterparts by prefixing ◊l0{an-}.

◊subsubsection[#:id "species4c"]{Species 4c}

◊table/x[#:options (table-options #:caption "Participle form inflections for species 4c." #:placement 'forcehere)]{
  Hcase \ Hgender & Singular & Dual & Plural & Generic
  ◊pseudoheader{Nominative rcase}
  Nominative & ◊nc{-esa} & ◊nc{-esac} & ◊nc{-eso} & ◊nc{-esa}
  Accusative & ◊nc{-esan} & ◊nc{-esor} & ◊nc{-eson} & ◊nc{-esan}
  Dative & ◊nc{-esas} & ◊nc{-esacþ} & ◊nc{-esos} & ◊nc{-esas}
  Genitive & ◊nc{-esen} & ◊nc{-ešor} & ◊nc{-esin} & ◊nc{-esen}
  Locative & ◊nc{-esal} & ◊nc{-esal} & ◊nc{-esol} & ◊nc{-esal}
  Instrumental & ◊nc{-esaþ} & ◊nc{-esaþ} & ◊nc{-esoþ} & ◊nc{-esaþ}
  Abessive & ◊nc{-esaf} & ◊nc{-esaf} & ◊nc{-esof} & ◊nc{-esaf}
  Semblative & ◊nc{-eset} & ◊nc{-esecþ} & ◊nc{-esit} & ◊nc{-eset}
  ◊pseudoheader{Accusative rcase}
  Nominative & ◊nc{-esta} & ◊nc{-estac} & ◊nc{-esto} & ◊nc{-esta}
  Accusative & ◊nc{-estan} & ◊nc{-estor} & ◊nc{-eston} & ◊nc{-estan}
  Dative & ◊nc{-estas} & ◊nc{-estacþ} & ◊nc{-estos} & ◊nc{-estas}
  Genitive & ◊nc{-esten} & ◊nc{-ečor} & ◊nc{-estin} & ◊nc{-esten}
  Locative & ◊nc{-estal} & ◊nc{-estal} & ◊nc{-estol} & ◊nc{-estal}
  Instrumental & ◊nc{-estaþ} & ◊nc{-estaþ} & ◊nc{-estoþ} & ◊nc{-estaþ}
  Abessive & ◊nc{-estaf} & ◊nc{-estaf} & ◊nc{-estof} & ◊nc{-estaf}
  Semblative & ◊nc{-estet} & ◊nc{-estecþ} & ◊nc{-estit} & ◊nc{-estet}
  ◊pseudoheader{Dative rcase}
  Nominative & ◊nc{-ista} & ◊nc{-istac} & ◊nc{-isto} & ◊nc{-ista}
  Accusative & ◊nc{-istan} & ◊nc{-istor} & ◊nc{-iston} & ◊nc{-istan}
  Dative & ◊nc{-istas} & ◊nc{-istacþ} & ◊nc{-istos} & ◊nc{-istas}
  Genitive & ◊nc{-isten} & ◊nc{-ičor} & ◊nc{-istin} & ◊nc{-isten}
  Locative & ◊nc{-istal} & ◊nc{-istal} & ◊nc{-istol} & ◊nc{-istal}
  Instrumental & ◊nc{-istaþ} & ◊nc{-istaþ} & ◊nc{-istoþ} & ◊nc{-istaþ}
  Abessive & ◊nc{-istaf} & ◊nc{-istaf} & ◊nc{-istof} & ◊nc{-istaf}
  Semblative & ◊nc{-istet} & ◊nc{-istecþ} & ◊nc{-istit} & ◊nc{-istet}
}

NB: the accusative-rcase forms add a ◊l0{-t-} after the ◊l0{s} in the nominative-rcase forms, and the dative-rcase forms change the ◊l0{-e-} of accusative-rcase forms with ◊l0{-i-}.

◊section[#:id "converb"]{Converbs (adverbial forms)}

Converbs are divided into two categories:

◊items{
  ◊item{◊term{Reduced coordination}: the actions referred to by the converbal clause and the main clause are separate.}
  ◊item{◊term{Emergent coordination}: the actions referred to by the converbal clause and the main clause form a larger action as a whole.}
}

In both of these categories, the converbal clause and the main clause might not match in subject, but in that case, it must be made clear that the respective subjects are different.

Often, such a mismatch occurs because a non-subject argument in the converbal clause should become the subject in the main clause. If this is not already clear, then the clitic ◊l1{=’ul} can be used on the argument in the converbal clause to mark that it is also the subject of the main clause.

Sometimes, the subject in the converbal clause should not be the subject of a main clause. If this is not otherwise clear, then the clitic ◊l1{=’els} can be used on the converb to make it so.

In addition, Ŋarâþ Crîþ has ◊em{so}-clauses, which also act adverbially.

◊subsection[#:id "reduced"]{Reduced coordination}

In reduced coordination, the converb is formed through an affix on the verb stem indicating the conjunction in question instead of the usual conjugation:

◊table/x[#:options (table-options #:caption "Affixes on the converb for reduced conjunction, where X is the statement stated by the converbal clause and Y the statement stated by the main clause.")]{
  Operation \ Infinitive ending & ◊nc{-at} & ◊nc{-it}
  X and Y & ◊nc{-ame} & ◊nc{-ime}
  X or Y (inclusive) & ◊nc{-age} & ◊nc{-ige}
  X or Y (exclusive) & ◊nc{-are} & ◊nc{-ire}
  X to some and Y to others & ◊nc{-amec} & ◊nc{-imec}
}

After an onset containing a voiced obstruent, the ◊l0{-age} and ◊l0{-ige} affixes become ◊l0{-ahe} and ◊l0{-ihe}, respectively. After an onset of ◊l0{m}, affixes beginning with ◊l0{-ame-} and ◊l0{-ime-} have these replaced with ◊l0{-ase} and ◊l0{-ise}, respectively.

◊subsection[#:id "emergent"]{Emergent coordination}

In emergent coordination, the final ◊l0{-t} of the infinitive form of the first verb is replaced with ◊l0{-rþ} if the onset of the following word is either null or a plosive alone, or ◊l0{-þ} otherwise. (Note that the operation is necessarily “and”.)

The use of emergent coordination can be seen as a serial verb construction.

◊subsection[#:id "so-clauses"]{◊i{So}-clauses}

◊term{◊i{So}-clauses} are adverbial clauses consisting of a finite verb phrase followed by a conjunction such as ◊l1{so}, called the ◊term{◊i{so}-particle}:

◊items{
  ◊item{◊l1{A so B}: ◊trans{if A, then B}, with emphatic form ◊l1{sono}. Can be combined with the clitic ◊l1{’moc} to mean ◊trans{even if} or ◊trans{even though}. Also used to express correlative comparisons when the main verbs of both A and B are differentiated with respect to the same variable.}
  ◊item{◊l1{A fose B}: ◊trans{because A, B}. In other words, A is the cause and B is the effect.}
  ◊item{◊l1{A dôm B}: ◊trans{B to the extent that A; B so much that A}. That is, A is the result of B.}
}

◊section[#:id "nominalized"]{Nominalized forms}

◊table/x[#:options (table-options #:caption "Nominalizing particles by case.")]{
  Case & Adverbial & Adnominal
  Nominative & ◊nc{o} & –
  Accusative & ◊nc{on} & –
  Dative & ◊nc{os○} & –
  Genitive & – & ◊nc{en}
  Locative & ◊nc{a●} & ◊nc{sa●}
  Instrumental & ◊nc{ac●} & ◊nc{sec●}
  Abessive & ◊nc{aþ●} & ◊nc{seþ●}
  Semblative & ◊nc{it} & ◊nc{eti}
}

Verb phrases can be used as nouns by using a particle plus one or more infinitives. If there is more than one infinitive, then the first one must not be an auxiliary verb, while the rest must be.

◊gloss/x{
  ◊glfree{◊nc{on mêgennančat triłepjô.}}
  ◊gla{on mê<genna>nč-at trił-e-pjô.}
  ◊glb{%inf.%acc eat<%ddt.%rec>-%inf recommend-1%sg-1%du.%incl}
  ◊glfree{I suggest to us to eat more slowly.}
}

◊gloss/x{
  ◊glfree{◊nc{eleþ os šileat mîrm·aru.}}
  ◊gla{el-eþ os šile-at mîrm·ar-u.}
  ◊glb{sun-%nom.%sg %inf.%dat shine-%inf predict-3%gc}
  ◊glfree{It is predicted that the sun will shine.}
}

◊gloss/x{
  ◊glfree{◊nc{a cþerit elnat łanes.}}
  ◊gla{a cþer-it eln-at łan-es.}
  ◊glb{%inf.%loc write-%inf neat-%inf must-2%sg}
  ◊glfree{When you write, you must do it neatly.}
}

There are different particles depending on whether the nominalized verb phrase is being used adverbially or adnominally. Some particles trigger mutations in the verbal infinitive. Such mutations apply to the first two infinitives in the chain.

If a nominalized VP has only one infinitive, then the last word of a noun phrase attached to it may be moved between the particle and the infinitive. In this case, the moved word takes the same mutation that the infinitive would:

◊gloss/x{
  ◊glfree{◊nc{trecai os v·alit’pe nîs faras miłersoþ notecþista.}}
  ◊gla{trec-ai os v·al-it=’pe nîs far-as miłers-oþ not-ecþis-ta.}
  ◊glb{field-%loc.%pl %inf.%dat reside-%inf=%poss.1 through often-%loc.%di pasture-%loc.5pl walk-1%pl.%excl.%inv-%past}
  ◊glfree{During our residence in the country we often walked in the pastures.}
}

When a nominalized VP is an object of a modifying relational, the adverbial forms are used, but any eclipsis that would have happened to the relational does not happen.

A pronominal subject of a nominalized VP is marked using a possessive clitic on the infinitive:

◊gloss/x{
  ◊glfree{◊nc{a ganasor varmenat’pe intaras môvaþas salis menaþ.}}
  ◊gla{a g\an-asor varmen-at=’pe intar-as môv-aþas sal-is men-a-þ.}
  ◊glb{%inf.%loc (%inf\)sky-%dat.%sg observe-%inf=%poss.1 void-%loc.%sg float-%rel.%acc,%dat.%cel thing-%dat.%sg see-1%sg-%past}
  ◊glfree{When I looked into the sky, I saw something floating in the air.}
}

◊section[#:id "irregular"]{Irregular verbs}

So far, we have mentioned several ways in which verbs can seem irregular: namely, (1) vowel affection, (2) participle species, and (3) stem impersonation in participles. Nevertheless, a few verbs are truly irregular.

◊subsection[#:id "irregular-apn"]{APN-irregular verbs}

As the name suggests, ◊term{APN-irregular verbs} specify finite forms according to aspect, person, and number. Object and tense suffixes apply as usual.

◊subsubsection[#:id "eþit"]{◊nc{eþit, verła, vepraþa, …}}

◊table/x[#:options (table-options #:caption ◊@{Conjugation of ◊nc{eþit, verła, vepraþa, …}, listing the irregular finite forms.} #:placement 'forcehere)]{
  Person \ Number & Singular & Dual & Plural & Generic
  ◊pseudoheader{Direct aspect}
  1st excl. & ◊nc{ve} & ◊nc{ven} & ◊nc{vjaþ} & ◊nc{vef}
  1st incl. & & ◊nc{veac} & ◊nc{vea} &
  2nd & ◊nc{ves} & ◊nc{vesen} & ◊nc{vełar} & ◊nc{vełaf}
  3rd & ◊nc{veła} & ◊nc{vełan} & ◊nc{von} & ◊nc{ver}
  ◊pseudoheader{Inverse aspect}
  1st excl. & ◊nc{vel} & ◊nc{vins} & ◊nc{vjaþis} & ◊nc{vesif}
  1st incl. & & ◊nc{veacel} & ◊nc{varel} &
  2nd & ◊nc{verþ} & ◊nc{vinse} & ◊nc{veris} & ◊nc{vełesaf}
  3rd & ◊nc{vełal} & ◊nc{vełans} & ◊nc{veron} & ◊nc{vros}
}

◊subsubsection[#:id "eseþit"]{◊nc{eseþit, esperła, espeþraþa, …}}

The forms of this verb, which is the inchoative form of ◊l1{eþit}, are independent of aspect.

◊table/x[#:options (table-options #:caption ◊@{Conjugation of ◊nc{eseþit, esperła, espeþraþa, …}, listing the irregular finite forms.} #:placement 'forcehere)]{
  Person \ Number & Singular & Dual & Plural & Generic
  1st excl. & ◊nc{efa} & ◊nc{eson} & ◊nc{esoþ} & ◊nc{esef}
  1st incl. & & ◊nc{iste} & ◊nc{ise} &
  2nd & ◊nc{sos} & ◊nc{essen} & ◊nc{esołar} & ◊nc{esaf}
  3rd & ◊nc{esoła} & ◊nc{esołan} & ◊nc{espo} & ◊nc{eris}
}

◊subsubsection[#:id "telit"]{◊nc{telit, cerła, cirłaþa, …}}

◊table/x[#:options (table-options #:caption ◊@{Conjugation of ◊nc{telit, cerła, cirłaþa, …}, listing the irregular finite forms.} #:placement 'forcehere)]{
  Person \ Number & Singular & Dual & Plural & Generic
  ◊pseudoheader{Direct aspect}
  1st excl. & ◊nc{ce} & ◊nc{cen} & ◊nc{čaþ} & ◊nc{cef}
  1st incl. & & ◊nc{cjor} & ◊nc{cea} &
  2nd & ◊nc{ces} & ◊nc{cesen} & ◊nc{cełar} & ◊nc{cełaf}
  3rd & ◊nc{ceła} & ◊nc{cełan} & ◊nc{cþon} & ◊nc{cir}
  ◊pseudoheader{Inverse aspect}
  1st excl. & ◊nc{cel} & ◊nc{cins} & ◊nc{čaþis} & ◊nc{cesif}
  1st incl. & & ◊nc{tacel} & ◊nc{cjarel} &
  2nd & ◊nc{cerþ} & ◊nc{cinse} & ◊nc{ceris} & ◊nc{cełesaf}
  3rd & ◊nc{cełal} & ◊nc{cełans} & ◊nc{ceþen} & ◊nc{cþaros}
}

◊section[#:id "intpc"]{Interactions with predicate modifiers}

◊subsection[#:id "comparatives"]{Comparatives}

Given a verb, the derived verb prefixed with ◊l0{mir◊|sei2|-} (◊l0{ła◊|sei2|-}) means ‘◊S performs the action in question to a greater (lesser) degree than ◊O by a margin of ◊|I|’:

◊gloss/x{
  ◊glfree{◊nc{mirvesralo.}}
  ◊gla{mir-vesr-a-lo.}
  ◊glb{%cmp-strong-1%sg-3%pl}
  ◊glfree{I am stronger than they are.}
}

If the base action mentions a direct or indirect object, then the argument is retained in the clause eclipsed:

◊gloss/x{
  ◊glfree{◊nc{#môra #saþon giviner âreþþas mirm·ečaþ.}}
  ◊gla{#môr-a #saþ-on g\ivin-er â-reþþas mir-m·eč-a-þ.}
  ◊glb{%name-%nom.%sg %name-%acc.%sg %cdarg\mushroom-%acc.%pl %nnom-four.%dat.%cel %cmp.%gt-find-3%sg-%past}
  ◊glfree{#môra found four more mushrooms than #saþo.}
}

◊; #môra caught more salmon than cod ⇒ ???

The verbs ◊l1{mirit} ◊trans{greater than} and ◊l1{łavrit} ◊trans{less than} also exist.

Equal comparisons are formed by using the relational ◊l1{peŋan} whose object is the noun phrase to which another, of the same case, is being compared:

◊gloss/x{
  ◊glfree{◊nc{#môra #saþo peŋan vadanor mečaþ.}}
  ◊gla{#môr-a #saþ-o peŋan vad-anor meč-a-þ.}
  ◊glb{%name-%nom.%sg %name-%nom.%sg %cmp.%eq tuber-%acc.%pl find-3%sg-%past}
  ◊glfree{#môra found as many tubers as #saþo.}
}

Superlatives are formed using the relational ◊l1{îþ}, which takes a genitive object:

◊gloss/x{
  ◊glfree{◊nc{#môra rascen îþ raga.}}
  ◊gla{#môr-a rasc-en îþ rag-a.}
  ◊glb{%name-%nom.%sg family-%gen.%sg %sup tall-3%sg}
  ◊glfree{#môra is the tallest in their family.}
}

◊subsection[#:id "aux-tour"]{Auxiliary verbs}

The verb modified by an auxiliary verb is called its ◊term{target}, which appears in the infinitive form immediately before the auxiliary verb. The particle ◊l1{fel} can stand in place of a target if one is not specified. In addition, the target of an auxiliary verb can be an auxiliary verb itself, with its own target ◊i{ad infinitum}.

A clause using an auxiliary verb may select a ◊term{shifted subject} (◊|Š|) among the nominative, accusative, and dative arguments. The shifted subject may have a special role in the meaning of an auxiliary verb, such as carrying the volition for the performance or nonperformance of the target action.

An auxiliary is ◊term{positive definite} (◊term{negative definite}) if the truth of the action or state described implies that the action or state described by the target is true (false). It is ◊term{indefinite} if neither such implication holds.

◊subsubsection[#:id "aux-tour-modal"]{Modal auxiliaries}

◊term{Modal auxiliaries} can be classified by their ◊term{force} and ◊term{flavor}. Ŋarâþ Crîþ distinguishes the following flavors:

◊items{
  ◊item{◊term{Situational}: according to the situation at hand}
  ◊item{◊term{Dynamic}: according to the abilities of ◊Š}
  ◊item{◊term{Facultative}: according to the knowledge of ◊Š to perform ◊|T|}
  ◊item{◊term{Deontic}: according to some set of norms}
  ◊item{◊term{Conscientious}: according to the conscientious beliefs of ◊Š (◊i{i.e.} whether ◊Š has a conscientious objection against performing or not performing a certain action)}
  ◊item{◊term{Epistemic}: according to beliefs inferred through other information}
}

The modal auxiliaries of Ŋarâþ Crîþ according to this classification is shown in ◊xref/l["modal-auxiliaries"]{Table}.

◊table/x[#:options (table-options #:caption "Modal auxiliaries in Ŋarâþ Crîþ.") #:id "modal-auxiliaries"]{
  Flavor \ Force & Necessity & Non-necessity & Possibility & Impossibility & Probability
  Situational & ◊nc{marðat} & & ◊nc{pentat} & ◊nc{rjotat}
  Dynamic & & & ◊nc{pentat} & ◊nc{geðat}
  Facultative & & & ◊nc{racfit} & ◊nc{cricþit}
  Deontic & ◊nc{łanat} & ◊nc{rečit} & ◊nc{sarait}
  Conscientious & & & ◊nc{neħrit} & ◊nc{velrjotat}
  Epistemic & ◊nc{vrasit} ◊br ◊nc{cenmirat} & & ◊nc{þarat} & & ◊nc{lerjat}
}

There are a handful of modal auxiliary verbs that do not fit neatly into the grid above:

◊items{
  ◊item{◊l1{gevat}: used in questions to make a request}
  ◊item{◊l1{rendat}: desiderative = ◊trans{◊Š desires that ◊T happens.}}
  ◊item{◊l1{fonat}: negation of ◊l1{rendat} = ◊trans{◊Š has no desire for ◊|T| to happen}}
  ◊item{◊l1{verat}: volitive = ◊trans{◊Š intends to ◊|T|}}
}

◊subsubsection[#:id "aux-tour-degree"]{Degree auxiliaries}

◊term{Degree auxiliaries} show the extent to which the target action or state holds:

◊items{
  ◊item{◊l1{šonat} = ◊trans{◊T is done completely}}
  ◊item{◊l1{iširit} = ◊trans{◊T is true at least slightly}}
}

A subset of degree auxiliaries are ◊term{negative auxiliaries}, which are used to negate the target in some way. Negative auxiliaries are necessarily negative definite, while other degree auxiliaries are positive definite.

◊items{
  ◊item{◊l1{pečit} = ◊trans{◊|Š| avoids inadvertently doing ◊|T|} (◊i{i.e.} the volition belongs to ◊|Š|)}
  ◊item{◊l1{tersat} = ◊trans{◊|Š| insists against doing ◊|T|}}
  ◊item{◊l1{garit} = ◊trans{◊Š stops oneself from doing ◊|T|}}
  ◊item{◊l1{vandrit} = ◊trans{◊Š leaves something uninvolved in an action ◊|T|}}
  ◊item{◊l1{selcit} = ◊trans{◊Š knowingly fails to fulfill their responsibility or obligation to do ◊|T|}}
  ◊item{◊l1{anðat} = ◊trans{◊T has not yet occurred}}
}

◊subsubsection[#:id "aux-tour-aspect"]{Aspectual auxiliaries}

Other auxiliary verbs indicate aspect. Most such auxiliaries are positive definite.

◊items{
  ◊item{◊l1{ħarat} = ◊trans{◊T is done again; ◊T is done to a further degree than previously mentioned}}
  ◊item{◊l1{anłit} = ◊trans{◊Š does ◊T in response to an earlier action against them}}
  ◊item{◊l1{nespat} = ◊trans{◊Š does ◊T habitually}}
  ◊item{◊l1{denecit} = ◊trans{◊Š continues to ◊T}}
  ◊item{◊l1{vespat} = ◊trans{◊Š does ◊T for the first time in a long time}}
  ◊item{◊l1{cagesit} = ◊trans{◊Š does ◊T for the first time in such a long time that the speaker does not remember when it last happened}}
}

◊subsubsection{Others}

◊items{
  ◊item{◊l1{vjełat} = ◊trans{◊|Š| has the character of doing ◊|T|}}
  ◊item{◊l1{cefsit} = ◊trans{◊|Š| has difficulty doing ◊|T|}}
}

◊subsection[#:id "voice"]{Voice}

The default voice is the active voice. There is no passive voice in Ŋarâþ Crîþ because the subject may be omitted instead:

◊gloss/x{
  ◊glfree{◊nc{+tjare siljos šepruþ.}}
  ◊gla{+tjar-e silj-os šepr-u-þ.}
  ◊glb{(name)-%acc.%sg afternoon-%loc.%sg execute-3%gc-%past}
  ◊glfree{+tjaris was executed in the afternoon.}
}

◊gloss/x{
  ◊glfree{◊nc{rešitas tfoson elens’po fêtecto gcemuþ.}}
  ◊gla{rešit-as tfos-on el-ens=’po fê-tecto g\cem-u-þ.}
  ◊glb{cloak-%dat.%sg cross_parent-%acc.%sg year-%acc.%sg=%sep 3%gc-before %pfv\give-3%sg-%past}
  ◊glfree{The cloak was given to my (mother/father) a year ago.}
}

◊subsubsection[#:id "causative-voice"]{The causative voice}

The causative voice is formed by prefixing ◊l0{do-}. It adds a ‘cause’ argument to the verb as the subject, changing its valency.

◊items{
  ◊item{In an intransitive or semitransitive verb, the old subject becomes the direct object, making it transitive or ditransitive.}
  ◊item{In a transitive verb, the old subject becomes the direct object, and the old object becomes the indirect object, making it ditransitive.}
  ◊item{In a ditransitive verb, the old subject becomes the direct object, the old object becomes the indirect object, and the old indirect object becomes the object of the relational ◊l1{ro}, making it a ditransitive verb with one additional oblique argument.}
  ◊item{Auxiliary verbs simply delegate to their targets.}
}

The causative prefix can function either as an inflectional affix or a derivational one:

◊gloss/x{
  ◊glfree{◊nc{ša entas gðenic’ve ndoelpanveþ?}}
  ◊gla{ša ent-as gðen-ic=’ve n\do-elp-an-ve-þ?}
  ◊glb{%int that_thing.%cel-%dat.%sg parent-%nom.%du=%poss.2 %pfv\%caus-wear-3%du-2%sg-%past}
  ◊glfree{Did your parents make you wear that?}
}

◊gloss/x{
  ◊glfree{◊nc{le jonas tê vrêman docjašas.}}
  ◊gla{le jon-as tê vrêm-an do-cjaš-as.}
  ◊glb{%imp now-%loc.%di that.%cel book-%acc.%sg %caus-fall-2%sg}
  ◊glfree{Drop that book right now.}
}

When two consecutive causative prefixes are added to a verb, then they take the form ◊l0{reld·o-}:

◊gloss/x{
  ◊glfree{◊nc{efreþ têrmol reld·odranlaþ.}}
  ◊gla{efr-eþ têrm-ol reld·o-dranl-a-þ.}
  ◊glb{commander-%nom.%sg hostage-%dat.%pl %caus.%caus-die-3%sg-%past}
  ◊glfree{The commander ordered the hostages to be killed.}
}

Note that ◊l1{têrmon} is in the dative case. If it were in the accusative case instead, then the meaning would be different:

◊gloss/x{
  ◊glfree{◊nc{efreþ têrmanor reld·odranlaþ.}}
  ◊gla{efr-eþ têrm-ol reld·o-dranl-a-þ.}
  ◊glb{commander-%nom.%sg hostage-%acc.%pl %caus.%caus-die-3%sg-%past}
  ◊glfree{The commander ordered the hostages to kill.}
}

In addition, the causative prefix and a terminative prefix ◊l0{er◊|sei|-} immediately following it fuse into ◊l0{dar◊|sei|-}:

◊gloss/x{
  ◊glfree{◊nc{mêva lê gedesos niðes ndarc·atlepeþ.}}
  ◊gla{mêv-a lê g\edes-os nið-es n\dar-c·atl-e-pe-þ.}
  ◊glb{rain-%nom.%co this.%cel assignment-%dat.%sg early_time-%loc.%di %caus.%pfv\%term-complete-3%sg-1%sg-%past}
  ◊glfree{I finished the assignment early because of the rain.}
}

◊subsubsection[#:id "applicative-voice"]{The applicative voices}

Ŋarâþ Crîþ has various applicative voices, which promote an oblique adjunct (either a noun phrase in a non-core case or an adverbial relational phrase) to the dative case. The former dative argument, if present, is demoted to where the oblique was.

◊table/x[#:options (table-options #:caption "Applicative prefixes in Ŋarâþ Crîþ")]{
  Case or relation & Prefix
  Locative & ◊nc{si-}
  Instrumental & ◊nc{ca-}
  Abessive & ◊nc{þa-}
  Semblative & ◊nc{ħit-}
  ◊nc{ar} & ◊nc{la-}
  ◊nc{jas} & ◊nc{sa-}
  ◊nc{nîs} & ◊nc{na-}
  ◊nc{uc} & ◊nc{ħic-}
  ◊nc{roc}, ◊nc{rille} & ◊nc{tar-}
}

The applicative voices are primarily used to relativize an oblique argument by promoting it into the dative case.

◊gloss/x{
  ◊glfree{◊nc{melco pecacatleþanon om inorolt clačirþ osjas &taran rema þal!}}
  ◊gla{melc-o pe-ca-catl-eþanon om inor-olt clač-irþ os-jas &tar-an rem-a þal!}
  ◊glb{parallel_parent-%nom.%sg 1%sg-%appl.%inst-hit-%rel.%dat,%acc.%ter that.%ter stick-%acc.%sv break-%ser 3%sg.%ter-from woodchip-%acc.%co make-1%sg %assert}
  ◊glfree{I’ll break that stick my (dad/mom) hit me with and turn it into woodchips!}
}

◊gloss/x{
  ◊glfree{◊nc{#mjôras sieristeþo #flirora cadils nelsit ħareþ.}}
  ◊gla{#mjôr-as si-erist-eþo #fliror-a cad-ils nels-it ħar-e-þ.}
  ◊glb{(name)-%loc.%sg %appl.%loc-meet-%rel.%dat,%dat.%cel (name)-%nom.%sg island-%dat.%sg go-%inf do_again-3%sg-%past}
  ◊glfree{#flirora went once more to the island where they met #môra.}
}

They are also used to make an oblique argument a shifted subject of an auxiliary verb or relational:

◊gloss/x[#:id "she-couldnt-stop-them"]{
  ◊glfree{◊nc{artfoþeþa þanelsit pečit rjotilesta.}}
  ◊gla{artfoþ-eþa þa-nels-it peč-it rjot-i-les-ta.}
  ◊glb{city-%abess.%sg %appl.%abess-go-%inf avoid-%inf fail-3%pl-3%sg.%hum-%past}
  ◊glfree{She couldn’t stop them from going to the city without her.}
}

◊gloss/x[#:id "near-the-mouth"]{
  ◊glfree{◊nc{eltin šimens cþîšari fjonos alaþas âŋa cþîfsocon siveła.}}
  ◊gla{elt-ins šim-ens cþîš-ari fjon-os al-aþas âŋa cþîfsoc-on si-veła.}
  ◊glb{river-%gen.%sg end-%acc.%sg near-%rel.%nom,%loc.%cel place-%loc.%sg east-%dat.%sg bending acute_angle-%nom.%sg %appl.%loc-exist.3%sg}
  ◊glfree{Near the mouth of the river, its course turns sharply towards the East.}
}

In ◊xref/p{she-couldnt-stop-them} and ◊xref/p{near-the-mouth}, the dative argument is understood to be the shifted subject by the use of the applicative.

