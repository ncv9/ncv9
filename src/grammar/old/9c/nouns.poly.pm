#lang pollen

◊define-meta[title]{Nouns}

In Ŋarâþ Crîþ, each noun is in one of three noun classes: ◊term{celestial}, ◊term{terrestrial}, and ◊term{human}. The class of a given noun is fixed.

Nouns are also declined for number and case. There are five numbers: ◊term{direct}, ◊term{dual}, ◊term{plural}, ◊term{singulative}, and ◊term{generic}. No noun can decline for all five numbers; instead, each noun is limited to a subset of these according to its ◊term{clareþ}:

◊items{
  ◊item{◊term{Singular} nouns allow the direct (as a singular), the dual, the plural, and the generic.}
  ◊item{◊term{Collective} nouns allow the direct (as a collective), the singulative, and the generic. The following nouns tend to be collective: ◊;
  ◊items{
    ◊item{Objects that tend to be found in groups}
    ◊item{Some plants, including all trees and flowers}
    ◊item{Small animals}
    ◊item{Diminuitive nouns}
  }}
  ◊item{◊term{Mass} nouns allow only the direct and the generic.}
}

Generic number is used to mean “X in general” or “X as a concept”. It is used on noun phrases that do not refer to a specific referent or referents. Contrast the following, for instance:

◊; TODO: examples actually in ŊCv9
◊items{
  ◊item{◊em{Love is a complex emotion.} – mentions love in general, so this would use the generic}
  ◊item{◊em{Alice’s love for Bob grew every day.} – particular instance of love, so this would use the singular}
  ◊item{◊em{Fish eat flowers.} – general truth, so “fish” would be in the generic and “flowers” would be in the generic}
  ◊item{◊em{The fish eat flowers.} – mentions a particular group of fish, so “fish” would be in the plural, but if the sentence indicates a habitual action, “flowers” would be in the generic}
  ◊item{◊em{A fish will usually eat about 200 flowers during its lifespan.} – “fish” is generic, as this sentence is not referring to a particular fish but rather an idealized individual reflecting the average. “Flowers” is plural and “lifespan” is singular because they are definite relative to the subject, even though the subject is generic.}
  ◊item{◊em{The fish are eating flowers.} – both “fish” and “flowers” would be in the plural}
  ◊item{◊em{The sun shines.} – there’s only one sun as far as we can tell, but this is also a general truth, so would “sun” be in the singular or the generic?}
  ◊item{◊em{The sun is shining.} – “sun” would definitely be in the singular here}
  ◊item{◊em{Charlie fishes for trout every week.} – “Charlie” is singular; “trout” and “week” are generic}
  ◊item{◊em{Charlie is fishing in that river.} – uses the singular}
  ◊item{◊em{All ravens are black.} – also a general truth, so “raven” would be in the generic number. A speaker could reasonably say this, even if they have not seen every raven in existence.}
  ◊item{◊em{All of the ravens landed on the fence.} – “all” is used with a partitive meaning, referring to every individual in some contextually relevant set of ravens, and the statement is made from observation, so this sentence would use the singular (as customary with a noun modified by “all” or “any”)}
  ◊item{◊em{I abhor strawberries.} – “strawberries” would be in the generic, as the speaker is claiming to dislike strawberries in general}
  ◊item{◊em{I abhor these strawberries because they’re too sour.} – “strawberries” would be in the plural here, referring to some strawberries in particular}
  ◊item{◊em{I’m looking for the manager, whoever that may be.} – although the speaker does not know who the manager is, they expect that there exists someone who is the manager, so “manager” would be in the singular}
  ◊item{◊em{Think of a word, any word.} – again, the speaker does not know which word the listener chose, but they expect that a particular word will be chosen, so “word” is in the singular}
  ◊item{
    ◊em{Everyone knows someone.} – This sentence has two interpretations in English. In Ŋarâþ Crîþ, these two interpretations would yield different sentences.
    ◊items{
      ◊item{◊em{There exists an individual y such that for all individuals x, x knows y.} – “Someone” (the ◊i{y} here) would be singular. “Everyone” (the ◊i{x}) would also be singular if the statement is based on observation.}
      ◊item{◊em{For all individuals x, there exists an individual y such that x knows y.} – Assuming again that the statement is based on observation, “everyone” and “someone” would both be singular here as well (there is exactly one “someone” for each individual of “everyone”).}
    }
  }
}

In areas other than noun declension, Ŋarâþ Crîþ has the singular, dual, plural, and generic numbers. This results in a bijective mapping for singular nouns. For collective nouns, the collective is mapped to the plural, and the singulative to the singular; for mass nouns, the direct number is mapped to the singular. In all cases, the generic number is mapped to itself.

Ŋarâþ Crîþ has eight cases (◊xref/l["cases" "Table"]).

◊table/x[#:options (table-options
    #:caption "The cases of Ŋarâþ Crîþ."
    #:first-col-header? #f
    #:colgroup '(c d)
  )
  #:id "cases"]{
  Name & Use
  Nominative & ◊;
      The subject of the clause. The citation form of a noun is the nominative singular.
  Accusative & ◊;
      The “direct object” of the clause.
  Dative & ◊;
      The “indirect object” of the clause. Also used as a vocative.
  Genitive & ◊;
      Shows such things as possession, composition, description, or apposition.
  Locative & ◊;
      Indicates the location or time of an object or an action: ◊trans{at X}, ◊trans{on X}, ◊trans{in X}. On a nominalized verb, this case can be translated as ◊trans{when}, ◊trans{where}, or ◊trans{as long as}.
  Instrumental & ◊;
      Indicates the comitative or the instrumental: ◊trans{with X}.
  Abessive & ◊;
      The negation of the instrumental: ◊trans{without X}. In the dual number, ◊trans{with only one X}.
  Semblative & ◊;
      ◊trans{like X in behavior}. On a nominalized verb, ◊trans{such that}, ◊trans{as though}, or ◊trans{to the point that} (although ◊l1{dôm} is used more often for the last sense). Not used for semblance in appearance.
}

The nominative, accusative, dative, and genitive cases are considered ◊term{core cases}. In general, the first three of these are used for arguments to verbs, the genitive case for adnominal adjuncts, and the other four cases for adnominal or adverbial adjuncts.

◊; TODO: examples for each case

◊section[#:id "declensions"]{Declensions}

Noun declensions are divided into several ◊term{paradigms}; the paradigm to use for a given noun is not entirely predictable from the citation form. We follow the native-language convention of numbering them from zero.

Note that even when not explicitly listed, any declension paradigm (that contains content words) can contain words of the human class, since names can in theory be derived from any content noun.

In all regular paradigms, genitive dual, plural, and singulative forms are eclipsed. Indeclinable parts of compound nouns do not have this behavior. Most irregular nouns do, although there are exceptions.

Declined noun forms are built from two or more ◊term{components}, which include ◊term{constants} and ◊term{variables}. Constants stay the same within a given form of a given paradigm, regardless of the noun within that paradigm to be declined. Variables depend on the noun being declined and can be divided into ◊term{stems} and ◊term{themes}. Each declined form of a noun contains exactly one stem, which is considered the essence of a noun. A theme is a variable that is short (almost always one letter long). Themes are limited to a predefined number of options, while stems can be arbitrary (within phonotactic boundaries). A theme might have one or more ◊term{derivatives}, which are derived systematically from the value of the theme. Each regular noun has a set of ◊term{principal parts}, which are inflected forms of the noun that collectively give enough information to determine all other inflected forms.

Most nouns have at least ◊term{N}, ◊term{L}, and ◊term{S} stems. The N stem is used for the nominative, accusative, genitive, and dative cases; the L stem is used for the locative, instrumental, and abessive; and the S stem is used for the semblative. Paradigm 4 additionally has a ◊term{G} stem, while paradigm 10 adds both a G and an ◊term{A} stem and paradigm 13 adds ◊term{I} and ◊term{I◊prime} stems. In contrast, paradigm 12 lacks an L stem. When an L stem exists, it is almost always distinct from the N stem because some forms differ only in the use of an N or an L stem.

Themes in noun paradigms can be classified into ◊term{thematic consonants} and ◊term{thematic vowels}. In this document, thematic consonants are notated using ◊l0{C} (possibly with modifying symbols) and thematic vowels are denoted by digits (with ◊l0{0} being the primary theme and others being derivatives). These symbols should not be confused with numerals.

◊xref["nouns.html" "summary" "Table" #:type 'inline]{The following} is a summary of noun declensions in Ŋarâþ Crîþ. It is not meant to be a complete guide to declensions; see the individual declension tables for that purpose.

◊declension-table[#:abridged? #t #:id "summary" "A summary of Ŋarâþ Crîþ noun declension." values `(
  (
    ,◊@{0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 13, 14: ◊nc{-VC} ◊br 5, 11, 12: ◊nc{-∅}}
    ,◊@{0, 1, 3, 5, 6, 7, 8, 9, 11, 14: ◊nc{-Vc} ◊br 2: ◊nc{-Vcþ} ◊br 4: ◊nc{-eciC} ◊br 10: ◊nc{-oc} ◊br 12: ◊nc{-c}-like ◊br 13: ◊nc{-or}}
    ,◊@{0, 1, 2, 3, 4, 5, 6, 13, 14: ◊nc{-V₁(C)} ◊br 7, 8, 9: ◊nc{-oC′} ◊br 10: ◊nc{-iel} ◊br 11: ◊nc{-Vr} ◊br 12: ◊i{V → V₁}}
    ,◊@{0: ◊nc{-V₂l} ◊br 1, 2, 3, 4, 6, 11, 14: ◊nc{-VC*in} ◊br 5: ◊nc{-in} ◊br 7, 8, 10, 11: ◊nc{-VC*en} ◊br 9: ◊nc{-olt} ◊br 12: ◊nc{-te} ◊br 13: ◊nc{-Vnþ}}
    ,◊@{0, 1, 2, 4, 5, 6, 11: ◊nc{-Vf} ◊br 3: ◊nc{-Vs} ◊br 7, 8, 9, 12: ◊nc{-u} ◊br 10: ◊nc{-ul} ◊br 13: ◊nc{-e} ◊br 14: ◊nc{-eþVf}}
  )
  (
    ,◊@{0, 1, 5, 7, 8, 10, 11, 12, 14: ◊nc{-Vn} ◊br 2: ◊nc{-Vns} ◊br 3, 6: ◊nc{-VCV₃n} ◊br 4: ◊nc{-e} ◊br 9: ◊nc{-anon} ◊br 13: ◊nc{-VnV₂}}
    ,◊@{0, 1: ◊nc{-V₄Cte} ◊br 2: ◊nc{-V₄þe} ◊br 3, 6: ◊nc{-VCV₃c} ◊br 4: ◊nc{-ec} ◊br 5, 11: ◊nc{-Vs} ◊br 7, 8: ◊nc{-econ} ◊br 9: ◊nc{-anoc} ◊br 10: ◊nc{-oc} ◊br 12, 14: ◊nc{-(j)or} ◊br 13: ◊nc{-Vnor}}
    ,◊@{0, 1: ◊nc{-V₂r} ◊br 2: ◊nc{-V₂rþ} ◊br 3, 6: ◊nc{-V₁CV₃n} ◊br 4: ◊nc{-eri} ◊br 5, 11: ◊nc{-Vs} ◊br 7, 8: ◊nc{-eron} ◊br 9: ◊nc{-anor} ◊br 10: ◊nc{-on} ◊br 12: ◊nc{-jan} ◊br 13: ◊nc{-Vr} ◊br 14: ◊nc{-V₁n}}
    ,◊@{0, 1, 5, 11, 14: ◊nc{-Vnþ} ◊br 2: ◊nc{-Vnsir} ◊br 3, 6: ◊nc{-VrV₃nþ} ◊br 4: ◊nc{-eħin} ◊br 7, 8: ◊nc{-olt} ◊br 9: ◊nc{-anþen} ◊br 10: ◊nc{-enþ} ◊br 12: ◊nc{-ten} ◊br 13: ◊nc{-VnV₂n}}
    ,◊@{0, 1, 4, 5, 6, 11: ◊nc{-Vfen} ◊br 2: ◊nc{-Vfens} ◊br 3: ◊nc{-Vns} ◊br 7, 8, 9, 10, 12: ◊nc{-an} ◊br 13: ◊nc{-a} ◊br 14: ◊nc{-enþVf}}
  )
  (
    ,◊@{0, 1, 5, 7, 13: ◊nc{-VC*s} ◊br 2, 14: ◊nc{-VCVs} ◊br 3, 6: ◊nc{-Vls} ◊br 4: ◊nc{-es} ◊br 8: ◊nc{-asor} ◊br 9, 10: ◊nc{-os} ◊br 11: ◊nc{-i} ◊br 12: ◊nc{-er}}
    ,◊@{0: ◊nc{-Vs} ◊br 1: ◊nc{-Vþe} ◊br 2: ◊nc{-VcþVs} ◊br 3, 6: ◊nc{-VlcV₃s} ◊br 4: ◊nc{-ecþo} ◊br 5, 11: ◊nc{-ic} ◊br 7: ◊nc{-eþoc} ◊br 8: ◊nc{-esoc} ◊br 9: ◊nc{-ensoc} ◊br 10: ◊nc{-ac} ◊br 12: ◊nc{-ga-} ◊br 13: ◊nc{-Vŋa} ◊br 14: ◊nc{-Vcþ}}
    ,◊@{0, 1, 2, 3, 6: ◊nc{-V₄C*i} ◊br 4: ◊nc{-erþ} ◊br 5, 11, 12: ◊nc{-ir} ◊br 7: ◊nc{-orþ} ◊br 8, 9: ◊nc{-ol} ◊br 10: ◊nc{-os} ◊br 13: ◊nc{-eri} ◊br 14: ◊nc{-orþas}}
    ,◊@{0, 1, 3, 13: ◊nc{-VC*in} ◊br 2: ◊nc{-VsVnþ} ◊br 4, 6: ◊nc{-Vlsin} ◊br 5: ◊nc{-sin} ◊br 7: ◊nc{-asen} ◊br 8: ◊nc{-asren} ◊br 9, 10: ◊nc{-oren} ◊br 11: ◊nc{-in} ◊br 12: ◊nc{-tes} ◊br 14: ◊nc{-Vþaren}}
    ,◊@{0, 1, 4, 5, 6, 11: ◊nc{-Vfes} ◊br 2: ◊nc{-Vfeþ} ◊br 3: ◊nc{-Vþ} ◊br 7, 8, 9, 10, 12, 13: ◊nc{-as} ◊br 14: ◊nc{-esþVf}}
  )
  (
    ,◊@{0, 1, 2: ◊nc{-V₂n} ◊br 3, 6: ◊nc{-V₄i} ◊br 4: ◊nc{-en} ◊br 5, 11: ◊nc{-a} ◊br 7, 8, 9, 10: ◊nc{-(j)el} ◊br 12: ◊nc{-es} ◊br 13, 14: ◊nc{-il}}
    ,◊@{0, 1, 2: ◊nc{-V₂cC*} ◊br 3, 6: ◊nc{-V₄ci} ◊br 4: ◊nc{-enco} ◊br 5, 11: ◊nc{-ac} ◊br 7, 8, 9, 10: ◊nc{-acel} ◊br 12: ◊nc{-ec} ◊br 13: ◊nc{-Vc} ◊br 14: ◊nc{-ecil}}
    ,◊@{0, 1, 2: ◊nc{-V₃n} ◊br 3, 6: ◊nc{-V₄vi} ◊br 4: ◊nc{-eþ} ◊br 5, 11: ◊nc{-o} ◊br 7, 8, 9: ◊nc{-iel} ◊br 10: ◊nc{-ol} ◊br 12: ◊nc{-eris} ◊br 14: ◊nc{-Vvi}}
    ,◊@{0, 1, 2, 4: ◊nc{-V₂ns} ◊br 3, 6: ◊nc{-V₄ħin} ◊br 5, 11: ◊nc{-en} ◊br 7, 8, 9: ◊nc{-ełan} ◊br 10: ◊nc{-eren} ◊br 12: ◊nc{-tel} ◊br 13: ◊nc{-erin} ◊br 14: ◊nc{-iren}}
    ,◊@{0, 1, 2, 5, 11: ◊nc{-V₃f} ◊br 3, 6: ◊nc{-Vst} ◊br 4: ◊nc{-efin} ◊br 7, 8, 9, 10, 12: ◊nc{-e} ◊br 13: ◊nc{-o} ◊br 14: ◊nc{-elþVf}}
  )
  (
    ,◊@{0, 1, 2, 5, 11, 13: ◊nc{-VCs} ◊br 3, 4, 6: ◊nc{-Vᴸlt} ◊br 7, 8, 9, 10: ◊nc{-os} ◊br 12: ◊nc{-a} ◊br 14: ◊nc{-as}}
    ,◊@{0, 1, 2, 5, 11: ◊nc{-eCsac} ◊br 3, 4, 6: ◊nc{-VᴸltVc} ◊br 7, 8, 9, 10: ◊nc{-osac} ◊br 12: ◊nc{-ac} ◊br 13: ◊nc{-ecþ} ◊br 14: ◊nc{-acþ}}
    ,◊@{0, 1, 2, 5, 11: ◊nc{-V₁Cs} ◊br 3, 4, 6: ◊nc{-ilt} ◊br 7, 8, 9, 10: ◊nc{-or} ◊br 12: ◊nc{-o} ◊br 13: ◊nc{-erþ} ◊br 14: ◊nc{-os}}
    ,◊@{0, 1, 2, 5, 11, 13, 14: ◊nc{-V₂ns} ◊br 3: ◊nc{-inþ} ◊br 4, 6: ◊nc{-Vlten} ◊br 7, 8, 9, 10: ◊nc{-oren} ◊br 12: ◊nc{-en}}
    ,◊@{0, 1, 2, 5, 11, 12, 13: ◊nc{-Vf} ◊br 3, 4, 6: ◊nc{-Vᴸf} ◊br 7, 8, 9, 10, 14: ◊nc{-ef}}
  )
  (
    ,◊@{0, 1, 2, 5, 11, 12: ◊nc{-ecC*a} ◊br 3, 4, 6: ◊nc{-Vᴸlca} ◊br 7, 8, 9, 10, 13: ◊nc{cja--VC} ◊br 14: ◊nc{-eca}}
    ,◊@{0, 5, 11, 12: ◊nc{-ehac} ◊br 1, 2: ◊nc{-ecþac} ◊br 3, 4, 6: ◊nc{-Vᴸlhac} ◊br 7, 8, 9, 10, 13: ◊nc{cja--Vc} ◊br 14: ◊nc{-egacþ}}
    ,◊@{0, 1, 2, 5, 11: ◊nc{-ecta} ◊br 3, 4, 6: ◊nc{-Vᴸlco} ◊br 7, 8, 9, 10, 13: ◊nc{cja--o(C′)} ◊br 12: ◊nc{-ego} ◊br 14: ◊nc{-ecos}}
    ,◊@{0, 5, 11, 12: ◊nc{-egen} ◊br 1, 2: ◊nc{-ecþen} ◊br 3, 4, 6: ◊nc{-Vlcen} ◊br 7, 8, 10 ◊nc{cja--Vren} ◊br 9: ◊nc{cja--olt} ◊br 14: ◊nc{cja--ans} ◊br 14: ◊nc{-egaren}}
    ,◊@{0, 1, 2, 5, 11, 12: ◊nc{-ecC*V₂f} ◊br 3, 4, 6: ◊nc{-VᴸcV₃f} ◊br 7, 8, 9, 10, 13: ◊nc{cja--ef} ◊br 14: ◊nc{-ecaf}}
  )
  (
    ,◊@{0, 1, 2, 5, 11, 12: ◊nc{-eþC*a} ◊br 3, 4, 6: ◊nc{-Vᴸlþa} ◊br 7, 8, 9, 10, 13: ◊nc{þja○--VC} ◊br 14: ◊nc{-eþas}}
    ,◊@{0, 1, 2, 5, 11, 12: ◊nc{-eþC*ac} ◊br 3, 4, 6: ◊nc{-Vᴸlþac} ◊br 7, 8, 9, 10, 13: ◊nc{þja○--Vc} ◊br 14: ◊nc{-eðacþ}}
    ,◊@{0, 1, 2, 5: ◊nc{-V₄þC*a} ◊br 3, 4, 6: ◊nc{-V₄lþa} ◊br 7, 8, 9, 10, 13: ◊nc{þja○--o(C′)} ◊br 11: ◊nc{-esta} ◊br 12: ◊nc{-eðo} ◊br 14: ◊nc{-eþos}}
    ,◊@{0, 5, 11, 12: ◊nc{-eðen} ◊br 1, 2: ◊nc{-eþþen} ◊br 3, 4, 6: ◊nc{-Vlþen} ◊br 7, 8, 10 ◊nc{þja○--Vren} ◊br 9: ◊nc{þja○--olt} ◊br 14: ◊nc{þja○--ans} ◊br 14: ◊nc{-eðaren}}
    ,◊@{0, 1, 2, 5, 11, 12: ◊nc{-eþC*V₂f} ◊br 3, 4, 6: ◊nc{-eþV₃f} ◊br 7, 8, 9, 10, 13: ◊nc{þja○--ef} ◊br 14: ◊nc{-eþaf}}
  )
  (
    ,◊@{0, 1, 2, 3, 4, 5, 6, 11: ◊nc{-it} ◊br 7, 8, 9, 10, 12: ◊nc{-ot} ◊br 13: ◊nc{-it/-ot} ◊br 14: ◊nc{-irþ}}
    ,◊@{0, 3, 6: ◊nc{-ictV} ◊br 1, 2: ◊nc{-istV} ◊br 4, 5, 11: ◊nc{-iCtV} ◊br 7, 8, 9, 10, 12: ◊nc{-octos} ◊br 13: ◊nc{-octos} ◊br 14: ◊nc{-ircþe}}
    ,◊@{0, 1, 2, 3, 4, 5, 6, 11: ◊nc{-et} ◊br 7, 8, 9, 10, 12: ◊nc{-ot} ◊br 13: ◊nc{-it/-ot} ◊br 14: ◊nc{-ir}}
    ,◊@{0, 1, 2, 3, 4, 5, 6, 14: = du. ◊br 7, 8, 9, 10: ◊nc{-eła} ◊br 11, 12, 13: = pl.}
    ,◊@{0, 1, 2, 3, 4, 5, 6, 11: ◊nc{-icþ} ◊br 7, 8, 9, 10, 12: ◊nc{-ocþ} ◊br 13: ◊nc{-icþ/-ocþ} ◊br 14: ◊nc{-if}}
  )
)]

◊subsection[#:id "paradigm0"]{Paradigm 0}

Pattern: ◊l1{-a}, ◊l1{-e}, or ◊l1{-o}

Noun class: celestial

Stems: N = nom. sg. without last vowel; L = loc. sg. without -Vs; S = sembl. sg. without -it

Themes: 0 = last vowel of nom. sg.; 1, 2, 3 according to ◊xref/l["vowel-derivs-0-1-5" "Table"]

◊table/x[#:options (table-options #:caption "Thematic vowel derivatives for paradigms 0, 1, and 5." #:first-col-header? #f #:placement 'forcehere) #:id "vowel-derivs-0-1-5"]{
  0 & 1 & 2 & 3 & 4
  a & o & e & i & a
  o & e & e & i & a
  e & i & i & i & e
}

◊declension-table/split["Declensions for nouns in paradigm 0." nc]{
  ◊; The comments are required. Don't remove them!
  N0 N0c N1 N2l N0f ◊;
  N0n N4te N2r N0nþ N0fen ◊;
  N0s N0s N4i N0rin N0fes ◊;
  N2n N2c N3n N2ns N3f ◊;
  L0s Lesac L1s L2ns L0f ◊;
  Leca Lehac Lecta Legen Lec2f ◊;
  Leþa Leþac L4þa Leðen Leþ2f ◊;
  Sit Sict0 Set Sict0 Sicþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{gašida} ◊trans{shadow}, with locative collective form ◊l1{gašjodas} and semblative collective form ◊l1{gelšidit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  gašida gašidac gašido gašidel gašidaf ◊;
  gašidan gašidate gašider gašidanþ gašidafen ◊;
  gašidas gašidas gašidai gašidarin gašidafes ◊;
  gašiden ŋgašidec ŋgašidin ŋgašidens gašidif ◊;
  gašjodas gašjodesac gašjodos gašjodens gašjodaf ◊;
  gašjodeca gašjodehac gašjodecta gašjodegen gašjodecef ◊;
  gašjodeþa gašjodeþac gašjodaþa gašjodeðen gašjodeþef ◊;
  gelšidit gelšidicta gelšidet gelšidicta gelšidicþ ◊;
}

◊subsection[#:id "paradigm1"]{Paradigm 1}

Pattern: ◊l1{-as} or ◊l1{-es}

Noun class: celestial

Stems: N = nom. di. without last vowel + s; L = loc. di. without -Vþ; S = sembl. di. without -it

Themes: 0 = last vowel of nom. di.; 1, 2, 3, 4 according to ◊xref/l["vowel-derivs-0-1-5" "Table"]

◊declension-table/split["Declensions for nouns in paradigm 1." nc]{
  ◊; The comments are required. Don't remove them!
  N0s N0c N1(s) N0rin N0f ◊;
  N0n N0ste N2r N0nþ N0fen ◊;
  N0þ N0þe N0ri N0sin N0fes ◊;
  N2n N2cþ N3n N2ns N3f ◊;
  L0þ Leþac L1þ L2nþ L0f ◊;
  Lecþa Lecþac Lecta Lecþen Lecþ2f ◊;
  Leþþa Leþþac L0þþa Leþþen Leþþ2f ◊;
  Sit Sist0 Set Sist0 Sicþ ◊;
}

The ◊l1{s} in the nominative plural form is present only in nouns ending with ◊l1{-es}.

◊declension-table/split[◊@{An example for ◊l1{eltes} ◊trans{river}, with locative singular form ◊l1{ilteþ} and semblative singular form ◊l1{eldit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  eltes eltec eltis elterin eltef ◊;
  elten elteste eltir eltenþ eltefen ◊;
  elteþ elteþe elteri eltesin eltefes ◊;
  eltin gelticþ geltin geltins eltif ◊;
  ilteþ ilteþac iltiþ iltinþ iltef ◊;
  iltecþa iltecþac iltecta iltecþen iltecþif ◊;
  ilteþþa ilteþþac ilteþþa ilteþþen ilteþþif ◊;
  eldit eldiste eldet eldiste eldicþ ◊;
}

◊subsection[#:id "paradigm2"]{Paradigm 2}

Pattern: ◊l1{-aþ}, ◊l1{-eþ}, or ◊l1{-oþ}; rarely ◊l1{-iþ}

Noun class: celestial

Stems: N = nom. di. without last vowel + þ; L = loc. di. without -Vþ; S = sembl. di. without -it

Themes: 0 = last vowel of nom. di.; 1, 2, 3, 3◊prime, 4 according to ◊xref/l["vowel-derivs-2" "Table"]

◊table/x[#:options (table-options #:caption "Thematic vowel derivatives for paradigm 2." #:first-col-header? #f #:placement 'forcehere) #:id "vowel-derivs-2"]{
  0 & 1 & 2 & 3 & 3◊prime & 4
  a & o & e & i & i & a
  o & e & e & i & i & e
  e & i & i & o & e & e
  i & i & i & o & e & e
}

◊declension-table/split["Declensions for nouns in paradigm 2." nc]{
  ◊; The comments are required. Don't remove them!
  N0þ N0cþ N1þ N0sin N0f ◊;
  N0ns N4þe N2rþ N0nsir N0fens ◊;
  N0þ0s N0cþ0s N4ri N0s0nþ N0feþ ◊;
  N2n N2cþ N3′n N2ns N3f ◊;
  L0þ Leþac L1þ L2nþ L0f ◊;
  Lecþa Lecþac Lecta Lecþen Lecþ2f ◊;
  Leþþa Leþþac L0þþa Leþþen Leþþ2f ◊;
  Sit Sist0 Set Sist0 Sicþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{šimeþ} ◊trans{end}, with locative singular form ◊l1{šelmeþ} and semblative singular form ◊l1{šimit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  šimeþ šimecþ šimiþ šimesin šimef ◊;
  šimens šimeþe šimirþ šimensir šimefens ◊;
  šimeþes šimecþes šimeri šimesenþ šimefeþ ◊;
  šimin šimicþ šimen šimins šimof ◊;
  šelmeþ šelmeþac šelmiþ šelmenþ šelmef ◊;
  šelmecþa šelmecþac šelmecta šelmecþen šelmecþif ◊;
  šelmeþþa šelmeþþac šelmeþþa šelmeþþen šelmeþþif ◊;
  šimit šimiste šimet šimiste šimicþ ◊;
}

◊subsection[#:id "paradigm3"]{Paradigm 3}

Pattern: ◊l1{-ar}, ◊l1{-er}, or ◊l1{-ir}

Noun class: celestial

Stems: N = nom. di. without last vowel + r; L = loc. di. without -elt; S = sembl. di. without -it

Themes: 0 = last vowel of nom. di.; 1, 1◊prime, 3, 4 according to ◊xref/l["vowel-derivs-3" "Table"]


◊table/x[#:options (table-options #:caption "Thematic vowel derivatives for paradigm 3." #:first-col-header? #f #:placement 'forcehere) #:id "vowel-derivs-3"]{
  0 & 1 & 1◊prime & 3 & 4
  a & o & e & i & a
  e & i & i & e & e
  i & i & i & e & e
}

◊declension-table/split["Declensions for nouns in paradigm 3." nc]{
  ◊; The comments are required. Don't remove them!
  N0r N0c N1′r N0ðin N0s ◊;
  N0r3n N0r3c N1r3n N0r3nþ N0ns ◊;
  N0ls N0lc3s N4ri N0lsin N0þ ◊;
  N4i N4ci N4vi N4ħin N0st ◊;
  Lelt Lelt0c Lilt Linþ Lef ◊;
  Lelca Lelhac Lelco Lelcen Lec3f ◊;
  Lelþa Lelþac L4lþa Lelþen Leþ3f ◊;
  Sit Sict0 Set Sict0 Sicþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{galar} ◊trans{friend}, with locative singular form ◊l1{gelelt} and semblative singular form ◊l1{galit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  galar galac galer galaðin galas ◊;
  galarin galaric galorin galarinþ galans ◊;
  galals galalcis galari galalsin galaþ ◊;
  galai ŋgalaci ŋgalavi ŋalaħin galast ◊;
  gelelt geleltac gelilt gelinþ gelef ◊;
  gelelca gelelhac gelelco gelelcen gelecif ◊;
  gelelþa gelelþac gelalþa gelelþen geleþif ◊;
  galit galicta galet galicta galicþ ◊;
}

◊subsection[#:id "paradigm4"]{Paradigm 4}

Pattern: ◊l1{-in} or ◊l1{-is}

Noun class: celestial

Stems: N = nom. di. without -in or -is; G = gen. di. without -en; L = loc. di. without -Vlt; S = sembl. di. without -it

Themes: C = last consonant of nom. di.; V = last consonant of loc. di. (either ◊l0{e} or ◊l0{i}); C◊prime = ◊l0{d} if C is ◊l0{n}, and ◊l0{c} if C is ◊l0{s}

◊declension-table/split["Declensions for nouns in paradigm 4." nc]{
  ◊; The comments are required. Don't remove them!
  NiC NeciC NjaC NeC′in Nef ◊;
  Ne Nec Neri Neħin Nefen ◊;
  Nes Necþo Nerþ Nerin Nefes ◊;
  Gen Genco Geþ Gens Gefin ◊;
  LVlt LVltic Gilt GVlten LVf ◊;
  LVlca LVlhac LVlco LVlcen LVcef ◊;
  LVlþa LVlþac Lilþa LVlþen Leþef ◊;
  Sit SiCte Set SiCte Sicþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{leþin} ◊trans{bluebird}, with genitive collective form ◊l1{lersen}, locative collective form ◊l1{liselt}, and semblative collective form ◊l1{leðit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  leþin leþecin leþjan leþedin leþef ◊;
  leþe leþec leþeri leþeħin leþefen ◊;
  leþes leþecþo leþerþ leþerin leþefes ◊;
  lersen lersenco lerseþ lersens lersefin ◊;
  liselt liseltic lersilt lerselten lisef ◊;
  liselca liselhac liselco liselcen lisecef ◊;
  liselþa liselþac lisilþa liselþen liseþef ◊;
  leðit leðinte leðet leðinte leðicþ ◊;
}

◊subsection[#:id "paradigm5"]{Paradigm 5}

Pattern: ends in a consonant; last syllable usually has low tone

Noun class: celestial

Stems: N = nom. di. without final consonant cluster; N◊prime = N with tone of its final syllable inverted; L = loc. di. without -Vs; S = sembl. di. without -it

Themes: C = final consonant cluster of nom. di.; C◊prime =

◊items{
  ◊item{◊l0{nd} if C is ◊l0{n}, ◊l0{ns}, or ◊l0{nþ}}
  ◊item{◊l0{rð} if C is ◊l0{rþ}}
  ◊item{◊l0{l} if C is ◊l0{r}}
  ◊item{◊l0{r} if C is ◊l0{s}}
  ◊item{voiced version of C if C is a single consonant (◊l0{p} mapped to ◊l0{v}, but ◊l0{ł h} not changed)}
  ◊item{C otherwise;}
}

5 = take sum of letters in nom. di. modulo 6 and index into ◊l1{e a i a i e}; 0 = last vowel of loc. di.; 1, 2, 4 according to table for paradigm 0

◊declension-table/split["Declensions for nouns in paradigm 5." nc]{
  ◊; The comments are required. Don't remove them!
  NC NC′ec N′C NC′in NC′af ◊;
  NC′5n NC′as NC′as NC′5nþ NC′afen ◊;
  NC′5s NC′ic NC′ir NCsin NC′afes ◊;
  NC′a NC′ac NC′o NC′en NC′ef ◊;
  L0s Lesac L1s L2ns L0f ◊;
  Leca Lehac Lecta Legen Lec2f ◊;
  Leþa Leþac L4þa Leðen Leþ2f ◊;
  Sit SiCt5 Set SiCt5 Sicþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{ŋarâþ} ◊trans{language}, with locative singular form ◊l1{ŋôrþas} and semblative singular form ◊l1{ŋalit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  ŋarâþ ŋarâðec ŋaraþ ŋarâðin ŋarâðaf ◊;
  ŋarâðen ŋarâðas ŋarâðas ŋarâðenþ ŋarâðafen ◊;
  ŋarâðes ŋarâðic ŋarâðir ŋarâþsin ŋarâðafes ◊;
  ŋarâða ŋarâðac ŋarâðo ŋarâðen ŋarâðef ◊;
  ŋôrþas ŋôrþesac ŋôrþos ŋôrþens ŋôrþaf ◊;
  ŋôrþeca ŋôrþehac ŋôrþecta ŋôrþegen ŋôrþecef ◊;
  ŋôrþeþa ŋôrþeþac ŋôrþaþa ŋôrþeðen ŋôrþeþef ◊;
  ŋalit ŋaliþte ŋalet ŋalicte ŋalicþ ◊;
}

◊subsection[#:id "paradigm6"]{Paradigm 6}

Pattern: ◊l1{-al}, ◊l1{-il}, or ◊l1{-ol}

Noun class: celestial

Stems: N = nom. di. without last vowel + l; L = loc. di. without -ilt; S = sembl. di. without -it

Themes: 0 = last vowel of nom. di.; 0◊prime, 1, 3, 4 according to ◊xref/l["vowel-derivs-6" "Table"]

◊table/x[#:options (table-options #:caption "Thematic vowel derivatives for paradigm 6." #:first-col-header? #f #:placement 'forcehere) #:id "vowel-derivs-6"]{
  0 & 0◊prime & 1 & 3 & 4
  a & e & o & i & a
  i & e & i & e & e
  o & o & e & e & e
}

◊declension-table/split["Declensions for nouns in paradigm 6." nc]{
  ◊; The comments are required. Don't remove them!
  N0l N0c N1 N0rin N0′f ◊;
  N0r3n N0r3c N1r3n N0r3nþ N0′fen ◊;
  N0ls N0lc3s N4ri N0lsin N0′fes ◊;
  N4i N4ci N4vi N4ħin N0′st ◊;
  Lilt Lilt0c Lelt Lilten Lif ◊;
  Lilca Lilhac Lilco Lilcen Lic3f ◊;
  Lilþa Lilþac L4lþa Lilþen Leþ3f ◊;
  Sit Sict0′ Set Sict0′ Sicþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{serfil} ◊trans{autumn}, with locative default form ◊l1{šorfilt} and semblative default form ◊l1{selfit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  serfil serfic serfi serfirin serfef ◊;
  serfiren serfirec serfiren serfirenþ serfefen ◊;
  serfils serfilces serferi serfilsin serfefes ◊;
  serfei serfeci serfevi serfeħin serfest ◊;
  šorfilt šorfiltic šorfelt šorfilten šorfif ◊;
  šorfilca šorfilhac šorfilco šorfilcen šorficef ◊;
  šorfilþa šorfilþac šorfelþa šorfilþen šorfeþef ◊;
  selfit selficte selfet selficte selficþ ◊;
}

◊subsection[#:id "paradigm7"]{Paradigm 7}

Pattern: ◊l1{-os}

Noun class: terrestrial

Stems: N = nom. di. without -os; L = loc. di. without -os; S = sembl. di. without -ot

Themes: ◊em{none}

◊declension-table/split["Declensions for nouns in paradigm 7." nc]{
  ◊; The comments are required. Don't remove them!
  Nos Noc Nor Noren Nu ◊;
  Non Necon Neron Nolt Nan ◊;
  Noþ Neþoc Norþ Nasen Nas ◊;
  Nel Nacel Niel Nełan Ne ◊;
  Los Losac Lor Loren Lef ◊;
  cjaLos cjaLoc cjaLor cjaLoren cjaLef ◊;
  þja○Los þja○Loc þja○Lor þja○Loren þja○Lef ◊;
  Sot Soctos Sot Seła Socþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{mortos} ◊trans{hand}, with locative singular form ◊l1{maldos} and semblative singular form ◊l1{moltot}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  mortos mortoc mortor mortoren mortu ◊;
  morton mortecon morteron mortolt mortan ◊;
  mortoþ morteþoc mortorþ mortasen mortas ◊;
  mortel mortacel mortiel mortełan morte ◊;
  maldos maldosac maldor maldoren maldef ◊;
  cjamaldos cjamaldoc cjamaldor cjamaldoren cjamaldef ◊;
  þjam·aldos þjam·aldoc þjam·aldor þjam·aldoren þjam·aldef ◊;
  moltot moltoctos moltot molteła moltocþ ◊;
}

◊subsection[#:id "paradigm8"]{Paradigm 8}

Pattern: ◊l1{-or}

Noun class: terrestrial

Stems: N = nom. di. without -or; L = loc. di. without -os; S = sembl. di. without -ot

Themes: ◊em{none}

◊declension-table/split["Declensions for nouns in paradigm 8." nc]{
  ◊; The comments are required. Don't remove them!
  Nor Noc Nos Noren Nu ◊;
  Non Necon Neron Nolt Nan ◊;
  Nasor Nesoc Nol Nasren Nas ◊;
  Nel Nacel Niel Nełan Ne ◊;
  Los Losac Lor Loren Lef ◊;
  cjaLor cjaLoc cjaLos cjaLoren cjaLef ◊;
  þja○Lor þja○Loc þja○Los þja○Loren þja○Lef ◊;
  Sot Soctos Sot Seła Socþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{tfełor} ◊trans{road, path}, with locative singular form ◊l1{tfołos} and semblative singular form ◊l1{tfełot}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  tfełor tfełoc tfełos tfełoren tfełu ◊;
  tfełon tfełecon tfełeron tfełolt tfełan ◊;
  tfełasor tfełesoc tfełol tfełasren tfełas ◊;
  tfełel tfełacel tfełiel tfełełan tfełe ◊;
  tfołos tfołosac tfołor tfołoren tfołef ◊;
  cjatfołor cjatfołoc cjatfołos cjatfołoren cjatfołef ◊;
  þjatfołor þjatfołoc þjatfołos þjatfołoren þjatfołef ◊;
  tfełot tfełoctos tfełot tfełeła tfełocþ ◊;
}

◊subsection[#:id "paradigm9"]{Paradigm 9}

Pattern: ◊l1{-on}

Noun class: terrestrial

Stems: N = nom. di. without -on; L = loc. di. without -os; S = sembl. di. without -ot

Themes: ◊em{none}

◊declension-table/split["Declensions for nouns in paradigm 9." nc]{
  ◊; The comments are required. Don't remove them!
  Non Noc Nor Nolt Nu ◊;
  Nanon Nanoc Nanor Nanþen Nan ◊;
  Nos Nensoc Nol Noren Nas ◊;
  Nel Nacel Niel Nełan Ne ◊;
  Los Losac Lor Loren Lef ◊;
  cjaLon cjaLoc cjaLor cjaLolt cjaLef ◊;
  þja○Lon þja○Loc þja○Lor þja○Lolt þja○Lef ◊;
  Sot Soctos Sot Seła Socþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{cenčon} ◊trans{road, path}, with locative singular form ◊l1{cinčos} and semblative singular form ◊l1{cenčot}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  cenčon cenčoc cenčor cenčolt cenču ◊;
  cenčanon cenčanoc cenčanor cenčanþen cenčan ◊;
  cenčos cenčensoc cenčol cenčoren cenčas ◊;
  cenčel gcenčacel gcenčiel gcenčełan cenče ◊;
  cinčos cinčosac cinčor cinčoren cinčef ◊;
  cjacinčon cjacinčoc cjacinčor cjacinčolt cjacinčef ◊;
  þjac·inčon þjac·inčoc þjac·inčor þjac·inčolt þjac·inčef ◊;
  cenčot cenčoctos cenčot cenčeła cenčocþ ◊;
}

◊subsection[#:id "paradigm10"]{Paradigm 10}

Pattern: ◊l1{-el}

Noun class: terrestrial

Stems: N = nom. di. without -el; A = acc. di. without -en, G = gen. di. without -(j)el; L = loc. di. without -os; S = sembl. di. without -ot

Themes: ◊em{none}

◊declension-table/split["Declensions for nouns in paradigm 10." nc]{
  ◊; The comments are required. Don't remove them!
  Nel Noc Niel Neren Aul ◊;
  Aen Aoc Aon Aenþ Aan ◊;
  Aos Aac Nos Aoren Aas ◊;
  G(j)el Nacel Gol Geren Ae ◊;
  Los Losac Lor Loren Lef ◊;
  cjaLel cjaLoc cjaLor cjaLeren cjaLef ◊;
  þja○Lel þja○Loc þja○Lor þja○Leren þja○Lef ◊;
  Sot Soctos Sot Seła Socþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{łercþel} ◊trans{firefly}, with accusative collective form ◊l1{łercþen}, genitive collective form ◊l1{łircþjel}, locative collective form ◊l1{łircþos} and semblative collective form ◊l1{łelþot}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  łercþel łercþoc łercþiel łercþeren łercþul ◊;
  łercþen łercþoc łercþon łercþenþ łercþan ◊;
  łercþos łercþac łercþos łercþoren łercþas ◊;
  łircþel lłercþacel lłircþol lłircþeren łercþe ◊;
  łircþos łircþosac łircþor łircþoren łircþef ◊;
  cjałircþel cjałircþoc cjałircþor cjałircþeren cjałircþef ◊;
  þjałircþel þjałircþoc þjałircþor þjałircþeren þjałircþef ◊;
  łelþot łelþoctos łelþot łelþeła łelþocþ ◊;
}

◊subsection[#:id "paradigm11"]{Paradigm 11}

Pattern: ends in a consonant

Noun class: any

Stems: N = nom. di. without final consonant cluster; L = loc. di. without -as; S = sembl. di. without -it

Themes: C = final consonant cluster of nom. di.; C◊prime = (defined as in paradigm 5)

5 = if celestial, then take sum of letters in nom. di. modulo 5 and index into ◊l1{a e i e a}; if human, then take same letter sum modulo 4 and index into ◊l1{e o a i}; always ◊l1{o} if terrestrial

0 = ◊l1{a} if celestial or human; ◊l1{o} if terrestrial

6 = ◊l1{i} if celestial or human; ◊l1{e} if terrestrial

◊declension-table/split["Declensions for nouns in paradigm 11." nc]{
  ◊; The comments are required. Don't remove them!
  NC NC′ec NC′0r NC′6n NC′af ◊;
  NC′5n NC′0s NC′0s NC′5nþ NC′afen ◊;
  NC′i NC′ic NC′ir NC′in NC′afes ◊;
  NCa NCac NCo NCen NCef ◊;
  Las Lesac Los Lens Laf ◊;
  Leca Lehac Lecta Legen Lecef ◊;
  Leþa Leþac Lesta Leðen Leþef ◊;
  Sit SiCt5 Set Set Sicþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{crîþ} ◊trans{forest} (◊i{terrestrial}), with locative singular form ◊l1{crîlþas} and semblative singular form ◊l1{clîþit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  crîþ crîðec crîðor crîðen crîðaf ◊;
  crîðon crîðos crîðos crîðonþ crîðafen ◊;
  crîði crîðic crîðir crîðin crîðafes ◊;
  crîþa gcrîþac gcrîþo gcrîþen crîþef ◊;
  crîlþas crîlþesac crîlþos crîlþens crîlþaf ◊;
  crîlþeca crîlþehac crîlþecta crîlþegen crîlþecef ◊;
  crîlþeþa crîlþeþac crîlþesta crîlþeðen crîlþeþef ◊;
  clîþit clîþiþto clîþet clîþet clîþicþ ◊;
}

◊subsection[#:id "paradigm12"]{Paradigm 12}

Pattern: ends in a simple coda other than ◊nc{c} or ◊nc{t} (possibly empty)

Noun class: any

Stems: N = nom. di. without (j +) final vowel + consonant cluster; S = sembl. di. without -ot

Themes: 0 = (j +) final vowel of nom. di., C = final coda of nom. di.; 0* = 0% if C is ◊nc{n} or ◊nc{f} and 0 otherwise

◊table/x[#:options (table-options #:caption "Thematic vowel derivatives for paradigm 12." #:first-col-header? #f #:placement 'forcehere)]{
  0 & 0% & 1 & 1◊prime & 2
  a & o & o & o & e
  e & jo & i & i & a
  i & jo & ja & a & i
  o, u & o & je & e & u
  â & ô & ô & ô & a
  ê & jô & î & î & e
  î & jô & â & â & o
  ô & ô & ê & ê & i
  ja & jo & jo & e & jâ
  je & jo & i & i & jê
  jo & i & i & i & jô
  jâ & jô & jô & ô & jê
  jê & jô & î & î & jâ
  jô & î & jâ & ê & ô
}

◊table/x[#:options (table-options #:caption "Thematic consonant derivatives for paradigm 12." #:first-col-header? #f #:placement 'forcehere)]{
  C & C◊prime & C*
  ∅ & ħ & c
  s & r & cþ
  r & r & c
  n & nþ & r
  þ & s & cþ
  rþ & rs & cþ
  l & l & c
  f & m & r
}

◊declension-table/split["Declensions for nouns in paradigm 12." nc]{
  ◊; The comments are required. Don't remove them!
  N0C N0*C* N1C N0C′te N0C′u ◊;
  N0C′in N0C′jor N1′C′jan N0C′ten N0C′an ◊;
  N0C′er N0gaC N0C′ir N0C′tes N0C′as ◊;
  N0C′es N0C′ec N0C′eris N0C′tel N0C′e ◊;
  N2C′a N2C′ac N2C′o N2C′en N2C′af ◊;
  N2C′eca N2C′ehac N2C′ego N2C′egen N2C′ecaf ◊;
  N2C′eþa N2C′eþac N2C′eðo N2C′eðen N2C′eþaf ◊;
  Sot Soctos Sot Sot Socþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{ron} ◊trans{figure, form, shape, grammatical case}, with semblative direct form ◊l1{ranot}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  ron ror rjen ronþen ronþu ◊;
  ronþin ronþjor renþjan ronþen ronþan ◊;
  ronþer rogan ronþir ronþes ronþas ◊;
  ronþes ronþec ronþeris ronþel ronþe ◊;
  runþa runþac runþo runþen runþaf ◊;
  runþeca runþehac runþego runþegen runþecaf ◊;
  runþeþa runþeþac runþeðo runþeðen runþeþaf ◊;
  ranot ranoctos ranot ranot ranocþ ◊;
}

◊subsection[#:id "paradigm13"]{Paradigm 13}

Pattern: ◊nc{-en} or ◊nc{-in}

Noun class: celestial

Stems: N = nom. di. without final vowel + ◊nc{-n}; L = loc. di. without ◊nc{-es}; I = instr. di. without ◊nc{cja-} & ◊nc{-es}; I◊prime = instr. pl. or sv. without ◊nc{cja-} & ◊nc{-o}; S = sembl. di. without ◊nc{-it} or ◊nc{-ot}

Themes: 0 = final vowel of nom. di., 5 = ◊nc{i} if letter sum of nom. di. mod 7 is even and ◊nc{o} if it is odd; 5◊prime = whichever vowel was not chosen as 5; 1, 2 according to ◊xref/l["vowel-derivs-13" "Table"]

◊table/x[#:options (table-options #:caption "Thematic vowel derivatives for paradigm 13." #:first-col-header? #f #:placement 'forcehere) #:id "vowel-derivs-13"]{
  0 & 1 & 2
  e & i & a
  i & ie & e
}

◊declension-table/split["Declensions for nouns in paradigm 13." nc]{
  ◊; The comments are required. Don't remove them!
  N0n Nor N1n N0nþ Ne ◊;
  N0n2 N0nor N0r N0n2n Na ◊;
  N0ns N0ŋa Neri N0sin Nas ◊;
  Nil N0c Nevi Nerin No ◊;
  Les Lecþ Lerþ Lins Lef ◊;
  cjaI0s cjaI0c cjaI′o cjaI′ans cjaIef ◊;
  þja○I0s þja○I0c þja○I′o þja○I′ans þja○Ief ◊;
  S5t Sectos S5′t S5′t S5cþ ◊;
}

◊declension-table/split[◊@{An example for ◊l1{relten} ◊trans{mist}, with locative collective form ◊l1{riltes}, instrumental collective form ◊l1{cjariltes}, instrumental singulative form ◊l1{cjareltans}, and semblative collective form ◊l1{reltit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  relten reltor reltin reltenþ relte ◊;
  reltena reltenor relter reltenan relta ◊;
  reltens relteŋa relteri reltesin reltas ◊;
  reltil reltec reltevi relterin relto ◊;
  riltes riltecþ rilterþ riltins riltef ◊;
  cjariltes cjariltec cjarelto cjareltans cjariltef ◊;
  þjariltes þjariltec þjarelto þjareltans þjariltef ◊;
  reltit reltectos reltot reltot relticþ ◊;
}

◊subsection[#:id "paradigm14"]{Paradigm 14}

Pattern: ◊nc{-erþ}, ◊nc{-arþ}

Noun class: celestial

Stems: N = nom. di. without final vowel + ◊nc{-rþ}; L = loc. di. without ◊nc{-as}; S = sembl. di. without ◊nc{-irþ}

Themes: 0 = final vowel of nom. di.; 1 according to ◊xref/l["vowel-derivs-14" "Table"]

◊table/x[#:options (table-options #:caption "Thematic vowel derivatives for paradigm 14." #:first-col-header? #f #:placement 'forcehere) #:id "vowel-derivs-14"]{
  0 & 1
  e & jo
  a & o
}

◊declension-table/split["Declensions for nouns in paradigm 14." nc]{
  ◊; The comments are required. Don't remove them!
  N0rþ N0c Norþ N0ðen Neþ0f ◊;
  N0n N1r Non N0nþ Nenþ0f ◊;
  N0rþas N0cþ Norþas N0þaren Nesþ0f ◊;
  Nil N0cil N0vi Niren Nelþ0f ◊;
  Las Lacþ Los Lens Lef ◊;
  Lecas Legacþ Lecos Legaren Lecaf ◊;
  Leþas Leðacþ Leþos Leðaren Leþaf ◊;
  Sirþ Sircþe Sir Sircþe Sif ◊;
}

◊declension-table/split[◊@{An example for ◊l1{virerþ} ◊trans{lethargy or stupor after waking up}, with locative direct form ◊l1{vjorþas}, and semblative direct form ◊l1{vilirþ}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  virerþ virec virorþ vireðen vireþef ◊;
  viren virjor viron virenþ virenþef ◊;
  virerþas virecþ virorþas vireþaren viresþef ◊;
  viril virecil virevi viriren virelþef ◊;
  vjorþas vjorþacþ vjorþos vjorþens vjorþef ◊;
  vjorþecas vjorþegacþ vjorþecos vjorþegaren vjorþecaf ◊;
  vjorþeþas vjorþeðacþ vjorþeþos vjorþeðaren vjorþeþaf ◊;
  vilirþ vilircþe vilir vilircþe vilif ◊;
}

Because many nouns in Paradigm 14 are derived from verbs, this paradigm assigns defaults to the L and S stems from the N stem. The L stem is derived by changing the last nucleus in the N stem according to ◊xref/l["paradigm14-l-vowels" "Table"]. Depending on the consonants after the last vowel (the ◊term{bridge}), additional letters might be appended:

◊items{
  ◊item{If the bridge ends with ◊l0{-þ} or ◊l0{-ð}, then nothing is added.}
  ◊item{If the bridge ends with ◊l0{-s}, then it is changed to ◊l0{-þ}.}
  ◊item{If the bridge is empty or a valid coda not listed above, then ◊l0{-þ} is added.}
  ◊item{Otherwise, ◊l0{-eþ} is added.}
}

◊table/x[#:options (table-options #:caption "Nucleus changes for deriving the L stem for paradigm 14.") #:id "paradigm14-l-vowels"]{
  N stem vowel & L stem vowel
  ◊v7{a}, ◊v7{e} & ◊v7{o}
  ◊v7{ja}, ◊v7{je}, ◊v7{i} & ◊v7{jo}
  ◊v7{o}, ◊v7{u} & ◊v7{u}
  ◊v7{â} & ◊v7{ô}
  ◊v7{jâ}, ◊v7{î}, ◊v7{ê} & ◊v7{jô}
  ◊v7{ô}, ◊v7{jo}, ◊v7{jô} & ◊v7{ê}
}

The S stem is derived by changing the bridge in the N stem. All instances of ◊l0{r} in the bridge are replaced with ◊l0{l}, except those that follow one of ◊l0{a o â ô} and precede a consonant. In addition, if the bridge is one of ◊l0{t st s} (exactly), then it becomes ◊l0{d}; similarly, if it is one of ◊l0{þ rþ lþ} (exactly), then it becomes ◊l0{ð}.

◊subsection[#:id "paradigm14r"]{Paradigm 14r}

This is a variant of paradigm 14 in which some instances of ◊l0{r} in the suffixes are missing.

Pattern: ◊nc{-eþ}, ◊nc{-aþ}

Noun class: celestial

Stems: N = nom. di. without final vowel + ◊nc{-þ} (usually ending in ◊nc{-r} itself); L = loc. di. without ◊nc{-Vs}; S = sembl. di. without ◊nc{-irþ}

Themes: 0 = final vowel of nom. di.; 1 according to ◊xref/l["vowel-derivs-14" "Table"]

◊declension-table/split["Declensions for nouns in paradigm 14r." nc]{
  ◊; The comments are required. Don't remove them!
  N0þ N0c Noþ N0sen Neþ0f ◊;
  N0n N1þ Non N0nþ Nenþ0f ◊;
  N0þas N0cþ Noþas N0þaren Nesþ0f ◊;
  Nil N0cil N0vi Niren Nelþ0f ◊;
  Las Lacþ Los Lens Lef ◊;
  Lecas Legacþ Lecos Legaren Lecaf ◊;
  Leþas Leðacþ Leþos Leðaren Leþaf ◊;
  Sirþ Sircþe Sir Sircþe Sif ◊;
}

◊declension-table/split[◊@{An example for ◊l1{clareþ} ◊trans{clareþ}, with locative singular form ◊l1{clirþas}, and semblative singular form ◊l1{calirþ}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  clareþ clarec claroþ claresen clareþef ◊;
  claren clarjoþ claron clarenþ clarenþef ◊;
  clareþas clarecþ claroþas clareþaren claresþef ◊;
  claril gclarecil gclarevi gclariren clarelþef ◊;
  clirþas clirþacþ clirþos clirþens clirþef ◊;
  clirþecas clirþegacþ clirþecos clirþegaren clirþecaf ◊;
  clirþeþas clirþeðacþ clirþeþos clirþeðaren clirþeþaf ◊;
  calirþ calircþe calir calircþe calif ◊;
}

◊section[#:id "pronouns"]{Pronouns and determiners}

◊subsection[#:id "pronouns-personal"]{Personal pronouns}

◊subsubsection[#:id "pronouns-personal-basic"]{Basic personal pronouns}

The ◊term{basic personal pronouns} (Tables ◊xref/l["pronouns-1" #f] – ◊xref/l["pronouns-3h" #f]) are defective: they lack any forms for the core cases.

◊table/x[#:options (table-options #:caption "Declensions for first-person pronouns.") #:id "pronouns-1"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Locative & ◊nc{pelas} & ◊nc{pelsac} & ◊nc{pelir} & ◊nc{peris}
  Instrumental & ◊nc{pelca} & ◊nc{pelcac} & ◊nc{pelcar} & ◊nc{pelcef}
  Abessive & ◊nc{pilþa} & ◊nc{pilþac} & ◊nc{pilþar} & ◊nc{pilþef}
  Semblative & ◊nc{pjot} & ◊nc{pjocte} & ◊nc{pjet} & ◊nc{perþ}
}

◊table/x[#:options (table-options #:caption "Declensions for second-person pronouns.") #:id "pronouns-2"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Locative & ◊nc{olas} & ◊nc{olsac} & ◊nc{oler} & ◊nc{ores}
  Instrumental & ◊nc{olca} & ◊nc{olcac} & ◊nc{olcar} & ◊nc{olcef}
  Abessive & ◊nc{eþa} & ◊nc{eþac} & ◊nc{eþar} & ◊nc{aþef}
  Semblative & ◊nc{cet} & ◊nc{cete} & ◊nc{cet} & ◊nc{cefte}
}

◊table/x[#:options (table-options #:caption "Declensions for third-person celestial pronouns.") #:id "pronouns-3c"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Locative & ◊nc{eri} & ◊nc{erjor} & ◊nc{eren} & ◊nc{eref}
  Instrumental & ◊nc{cjas} & ◊nc{cjac} & ◊nc{cjar} & ◊nc{cjaf}
  Abessive & ◊nc{irþa} & ◊nc{irþac} & ◊nc{irþar} & ◊nc{irþef}
  Semblative & ◊nc{atir} & ◊nc{irce} & ◊nc{adit} & ◊nc{aden}
}

◊table/x[#:options (table-options #:caption "Declensions for third-person terrestrial pronouns.") #:id "pronouns-3t"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Locative & ◊nc{ose} & ◊nc{osec} & ◊nc{oros} & ◊nc{oref}
  Instrumental & ◊nc{cjos} & ◊nc{cjoc} & ◊nc{cjor} & ◊nc{cjof}
  Abessive & ◊nc{irþos} & ◊nc{irþoc} & ◊nc{irþor} & ◊nc{irþof}
  Semblative & ◊nc{ator} & ◊nc{ircon} & ◊nc{adit} & ◊nc{aden}
}

◊table/x[#:options (table-options #:caption "Declensions for third-person human pronouns.") #:id "pronouns-3h"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Locative & ◊nc{lase} & ◊nc{lasec} & ◊nc{laser} & ◊nc{lasef}
  Instrumental & ◊nc{lasce} & ◊nc{lascel} & ◊nc{lasci} & ◊nc{lascef}
  Abessive & ◊nc{laþes} & ◊nc{laþecþ} & ◊nc{laþer} & ◊nc{laþef}
  Semblative & ◊nc{lefen} & ◊nc{lefedi} & ◊nc{adit} & ◊nc{aden}
}

The first- and second-person pronouns are treated as if they were in the celestial gender, even though they will often refer to humans.

Basic personal pronouns are in the category ◊var{p} and avoid manifesting as free morphemes. They manifest in the following ways, ordered from most to least preferred:

◊items{
  ◊item{Fused with a coordinating conjunction if found as the left element}
  ◊item{In a relational with an object prefix}
  ◊item{As an object affix on the verb if in the accusative or dative case}
  ◊item{As a ◊xref["nouns.html" "pronouns-personal-possessive" "Sub-subsection"]{possessive postclitic} if in the genitive case, or in the nominative case of a nominalized verb phrase}
  ◊item{Omitted if inferable from the subject affix on a finite verb form}
  ◊item{As an independent form when in a non-core case}
  ◊item{Homophonic with the ◊xref["nouns.html" "pronouns-personal-emphatic" "Sub-subsection"]{emphatic pronouns}}
}

◊subsubsection[#:id "pronouns-personal-possessive"]{Possessive clitics}

Ŋarâþ Crîþ uses clitics to mark a pronominal possessor, listed in ◊xref/l["poss-clitics" "Table"].

◊table/x[#:options (table-options #:caption "Pronominal clitics in Ŋarâþ Crîþ.") #:id "poss-clitics"]{
  Person ◊|'amp| gender & Form
  1st & ◊nc{=’pe}
  2nd & ◊nc{=’ve}
  3rd celestial & ◊nc{=’(a)c}
  3rd terrestrial & ◊nc{=’oc}
  3rd human & ◊nc{=’(o)r}
}

The vowels of the third-person celestial and human possessive clitics are omitted after an open syllable.

The clitic ◊nc{=’ħe} is used to indicate that the possessor is the referent of a prior ◊xref["nouns.html" "quantification" "Subsection"]{◊nc{šino} or ◊nc{nema}}.

The third-person possessive suffixes are also used in the double-marked ◊term{possessive construction}. In such a construction, the possessor takes the same case as the possessee and the clitic ◊l1{=’þ} after a vowel or ◊l1{=’eþ} after a consonant. The possessee takes the appropriate possessive clitic depending on the gender of the possessor. The possessor and possessee are not required to be adjacent to each other or even in a particular order.

In the general case, the possessive construction is used strictly for possession. That is, it does not have other functions of the genitive case such as apposition or composition.

The possessive construction is also used with the noun ◊l1{aliþ} ◊trans{something other than} as the possessee to ‘negate’ the possessor. This usage cannot be substituted with the genitive (although using non-third-person pronominal clitics on ◊l1{aliþ} is permitted).

This construction is used with the pronouns ◊l1{šino} and ◊l1{nema} as the possessor in order to avoid ambiguity with the determiners, which are the genitive singular forms of these pronouns.

The use of the possessive construction is otherwise quite rare.

◊subsubsection[#:id "pronouns-personal-reflexive"]{Reflexive and reciprocal pronouns}

There is only one reflexive pronoun, ◊l1{cenþ}, whose declensions are shown in ◊xref/l["reflexive-declensions"]{Table}.

◊table/x[#:options (table-options #:caption ◊@{Declensions for the reflexive pronoun ◊l1{cenþ}.} #:placement 'forcehere) #:id "reflexive-declensions"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Nominative & ◊nc{cenþ} & ◊nc{cenþ} & ◊nc{cemar} & ◊nc{cemu}
  Accusative & ◊nc{cemen} & ◊nc{cemas} & ◊nc{cemas} & ◊nc{ceman}
  Dative & ◊nc{cemi} & ◊nc{cemic} & ◊nc{cemir} & ◊nc{cemase}
  Genitive & ◊nc{cema} & ◊nc{cemac} & ◊nc{cemo} & ◊nc{ceme}
  Locative & ◊nc{ces} & ◊nc{cesor} & ◊nc{cis} & ◊nc{cesef}
  Instrumental & ◊nc{ceŋa} & ◊nc{ceŋac} & ◊nc{ciŋa} & ◊nc{ceŋaf}
  Abessive & ◊nc{cinþa} & ◊nc{cinþac} & ◊nc{cinþa} & ◊nc{cinþaf}
  Semblative & ◊nc{cemit} & ◊nc{cjorto} & ◊nc{cit} & ◊nc{cemicþ}
}

◊subsubsection[#:id "pronouns-personal-emphatic"]{Emphatic pronouns}

Combining a reflexive pronoun with a possessive clitic creates an ◊term{emphatic pronoun}, which acts roughly like a personal pronoun with an independent form but places focus on the referent.

◊subsubsection[#:id "pronouns-personal-clusive"]{Clusive pronouns}

◊subsection[#:id "pronouns-interrogative"]{Interrogative pronouns and determiners}

The interrogative determiners and pronouns in Ŋarâþ Crîþ are shown in ◊xref/l["interrogative-determiners"]{Table }.

◊table/x[#:options (table-options #:caption "Interrogative determiners and pronouns in Ŋarâþ Crîþ." #:colgroup '(c d)) #:id "interrogative-determiners"]{
  Type & Interrogative
  Determiner & ◊nc{mê◊|sei2|}
  Pronoun & ◊nc{pen, …}
  Pronoun (human) & ◊nc{penna, peþas, mpadit} (0h)
  Pronoun (elective) & ◊nc{meel, maen, mełel, mirłos, meħot} (10t)
  Pronoun (place) & ◊nc{parja, perþas, pjalit} (0c)
  Pronoun (time) & ◊nc{penelva, pełevas, pełevit} (0c.m)
  Pronoun (event) & ?
  Pronoun (idea or speech) & ◊nc{peler, …}
  Pro-verb & ◊nc{nepit, nelpa, nolpaþa}
}

Note that ◊l1{penna} has an intrinsically mutated S stem.

The pronouns ◊l1{pen} and ◊l1{peler} are irregular.

◊table/x[#:options (table-options #:caption ◊@{The declension of the irregular pronoun ◊l1{pen} ◊trans{what}.} #:placement 'forcehere)]{
  Case \ Number & Singular & Dual & Plural & Generic
  Nominative & ◊nc{pen} & ◊nc{pen} & ◊nc{penar} & ◊nc{penaf}
  Accusative & ◊nc{penen} & ◊nc{penas} & ◊nc{penas} & ◊nc{penas}
  Dative & ◊nc{peni} & ◊nc{penic} & ◊nc{penir} & ◊nc{penef}
  Genitive & ◊nc{pena} & ◊nc{vpenac} & ◊nc{vpeno} & ◊nc{penaf}
  Locative & ◊nc{pes} & ◊nc{pesor} & ◊nc{pis} & ◊nc{pesac}
  Instrumental & ◊nc{peŋa} & ◊nc{peŋac} & ◊nc{piŋa} & ◊nc{peŋaf}
  Abessive & ◊nc{pineþ} & ◊nc{pinþac} & ◊nc{pinþa} & ◊nc{pinþaf}
  Semblative & ◊nc{pedit} & ◊nc{pjorto} & ◊nc{pit} & ◊nc{pedecþ}
}

◊table/x[#:options (table-options #:caption ◊@{The declension of the irregular pronoun ◊l1{peler} ◊trans{what (idea, speech)}.} #:placement 'forcehere)]{
  Case \ Number & Singular & Dual & Plural & Generic
  Nominative & ◊nc{peler} & ◊nc{pelec} & ◊nc{penare} & ◊nc{penafel}
  Accusative & ◊nc{penreþ} & ◊nc{penareþ} & ◊nc{penareþ} & ◊nc{penres}
  Dative & ◊nc{penres} & ◊nc{penrecþ} & ◊nc{penares} & ◊nc{peneves}
  Genitive & ◊nc{penril} & ◊nc{vpenric} & ◊nc{vpenal} & ◊nc{penavil}
  Locative & ◊nc{penraþ} & ◊nc{penraþ} & ◊nc{penarþ} & ◊nc{penavaþ}
  Instrumental & ◊nc{penracþa} & ◊nc{penracþa} & ◊nc{penarcþa} & ◊nc{penavacþ}
  Abessive & ◊nc{penraþa} & ◊nc{penraþa} & ◊nc{penarþa} & ◊nc{penavaþa}
  Semblative & ◊nc{pelet} & ◊nc{pelecþ} & ◊nc{pelat} & ◊nc{pelfet}
}

Informally, ◊l1{pen} can be used instead of ◊l1{penna} to refer to persons.

◊subsection[#:id "pronouns-demonstrative"]{Demonstrative pronouns and determiners}

The demonstrative determiners and pronouns in Ŋarâþ Crîþ are shown in ◊xref/l["demonstrative-determiners"]{Table }.

The determiners ◊l1{lê} and ◊l1{tê} trigger eclipsis only if they lie directly before the head of what they modify.

◊table/x[#:options (table-options #:caption "Demonstrative determiners and pronouns in Ŋarâþ Crîþ." #:colgroup '(c d d)) #:id "demonstrative-determiners"]{
  Type & Proximal & Distal
  Determiner (celestial or human) & ◊nc{lê◊|uru|} & ◊nc{tê◊|uru|}
  Determiner (terrestrial) & ◊nc{el} & ◊nc{om}
  Pronoun (celestial) & ◊nc{ela, elras elit} (0c) & ◊nc{enta, ontas, ensit} (0c)
  Pronoun (terrestrial) & ◊nc{elos, elros, elot} (9t) & ◊nc{entos, ontos, ensot} (9t)
  Pronoun (human) & ◊nc{eltan, elnas, enlit} (11h) & ◊nc{eften, iftes, cjariftes, cjarefto, evrit} (13h)
  Pronoun (place) & ◊nc{elgren, …} & ◊nc{engren, …}
  Pronoun (time) & ◊nc{endir, ondelt, endit} (3c.m), ◊nc{ina, jonas, insit} (0c.m) &
  Pronoun (event) & ? & ?
  Pronoun (idea or speech) & ◊nc{felja, foljas, felit} (0c) & ◊nc{fetja, fotas, fedit} (0c)
  Pro-verb & ◊nc{ħelit} &
}

The pronouns ◊nc{elgren} and ◊nc{engren} are irregular.

◊table/x[#:options (table-options #:caption ◊@{The declension of the irregular pronoun ◊l1{elgren} ◊trans{here}.} #:placement 'forcehere)]{
  Case \ Number & Singular & Dual & Plural & Generic
  Nominative & ◊nc{elgren} & ◊nc{elgjor} & ◊nc{elgrin} & ◊nc{elgref}
  Accusative & ◊nc{elgranen} & ◊nc{elgranor} & ◊nc{elgrenin} & ◊nc{elgrenef}
  Dative & ◊nc{elgres} & ◊nc{elgrecþ} & ◊nc{elgras} & ◊nc{elgresef}
  Genitive & ◊nc{elgrer} & ◊nc{elgreric} & ◊nc{elgrir} & ◊nc{elgrerif}
  Locative & ◊nc{eši} & ◊nc{ešic} & ◊nc{ešin} & ◊nc{ešif}
  Instrumental & ◊nc{esar} & ◊nc{esac} & ◊nc{esor} & ◊nc{esaf}
  Abessive & ◊nc{eþa} & ◊nc{eþac} & ◊nc{eþar} & ◊nc{ecþaf}
  Semblative & ◊nc{elgrit} & ◊nc{elgricte} & ◊nc{elgret} & ◊nc{elgricþ}
}

◊table/x[#:options (table-options #:caption ◊@{The declension of the irregular pronoun ◊l1{engren} ◊trans{there}.} #:placement 'forcehere)]{
  Case \ Number & Singular & Dual & Plural & Generic
  Nominative & ◊nc{engren} & ◊nc{engjor} & ◊nc{engrin} & ◊nc{engref}
  Accusative & ◊nc{engranen} & ◊nc{engranor} & ◊nc{engrenin} & ◊nc{engrenef}
  Dative & ◊nc{engres} & ◊nc{engrecþ} & ◊nc{engras} & ◊nc{engresef}
  Genitive & ◊nc{engrer} & ◊nc{engreric} & ◊nc{engrir} & ◊nc{engrerif}
  Locative & ◊nc{eči} & ◊nc{ečic} & ◊nc{ečin} & ◊nc{ečif}
  Instrumental & ◊nc{etar} & ◊nc{etac} & ◊nc{etor} & ◊nc{etaf}
  Abessive & ◊nc{eða} & ◊nc{eðac} & ◊nc{eðar} & ◊nc{egðaf}
  Semblative & ◊nc{engrit} & ◊nc{engricte} & ◊nc{engret} & ◊nc{engricþ}
}

◊subsection[#:id "quantification"]{Quantification}

The pronouns ◊l1{šino, šjonos, šedit} (0c) means ◊trans{all}, and ◊l1{nema, nomas, nemit} (0c) means ◊trans{some} or ◊trans{any}. When qualified with a modifying phrase, their scopes are restricted:

◊gloss/x{
  ◊glfree{◊nc{naven šinof ndranlos.}}
  ◊gla{nav-en šin-of n\dranl-os.}
  ◊glb{human-%gen.%sg all-%nom.%gc %pfv\die-3%gc.%pfv}
  ◊glfree{All humans [will] die.}
}

However, both of these quantifiers can also be used in the genitive singular as determiners, provided that the head of the noun phrase being modified is partially lenited. Furthermore, forms of ◊l1{ðên} are not mutated. That is, the above example may have used ◊l1{šinen navaf} instead.

When a noun phrase containing ◊l1{šino} or ◊l1{nema} is in the generic number, it is considered to cover all or some of the relevant individuals in general. When such a noun phrase is in any other number, it is considered to have a partitive meaning, with the number reflecting the quantity of the whole:

◊gloss/x{
  ◊glfree{◊nc{naven šiner sâna mênčelta.}}
  ◊gla{nav-en šin-er sân-a mênč-el-ta.}
  ◊glb{human-%gen.%sg all-%acc.%pl bear-%nom.%sg eat-3%sg.%pfv-%past}
  ◊glfree{All of the humans were eaten by a bear.}
}

Of course, this example could have used ◊l1{šinen naver} instead.

TODO: figure out interaction between quantifiers and auxiliary verbs

Multiple quantifiers take precedence according to linear order. In other words, the outermost quantifier corresponds to the outermost level of quantification:

◊gloss/x{
  ◊glfree{◊nc{šine nemer racro.}}
  ◊gla{šin-e nem-er racr-o.}
  ◊glb{all-%nom.%pl any-%acc.%pl know-3%pl}
  ◊glfree{All of them know someone out of them. = For all ◊var{x}, there exists ◊var{y} such that ◊var{x} knows ◊var{y}.}
}

◊gloss/x{
  ◊glfree{◊nc{nemer šine racro.}}
  ◊gla{nem-er šin-e racr-o.}
  ◊glb{any-%acc.%pl all-%nom.%pl know-3%pl}
  ◊glfree{There is someone out of them whom all of them know. = There exists ◊var{y} such that for all ◊var{x}, ◊var{x} knows ◊var{y}.}
}

The semantically related noun ◊l1{ruf} modifies a noun somewhere before it in the same clause with the same case. If ◊l1{ruf} does not immediately follow the noun that it affects, that noun undergoes a partial lenition if it does not already have a mutation.

The determiner ◊l1{mel} means ◊trans{much} or ◊trans{many}. It is not inflected, but the corresponding pronoun ◊l1{denfo, danfos, denfit} (0c) is. From the latter is derived ◊l1{&denfo, &danfos, &denfit} (0c) ◊trans{majority}. Similarly, the determiner ◊l1{dân} ◊trans{few, little} corresponds to the pronoun ◊l1{dane, dones, denit} (0c), but the word for ◊trans{minority} is ◊l1{resa, risas, redit} (0c). Additionally, ◊l1{dân} triggers eclipsis in the head noun.

For numerals, see ◊xref/c["numerals.html" #:type 'inline]{Numerals}.

◊section[#:id "coordination"]{Coordination}

Noun phrases are coordinated by attaching a clitic to all except the first coordinand. A noun phrase may be coordinated only with others of the same case.

◊table/x[#:options (table-options #:caption "Coordinating clitics in Ŋarâþ Crîþ.") #:id "coordination-table"]{
  Operation & ◊var{X} = NP & ◊var{X} = 1 & ◊var{X} = 2 & ◊var{X} = 3 & ◊@{Inherits number & gender from}
  ◊var{X} and ◊var{Y} & ◊nc{=’ce} & ◊nc{=’cjo} & ◊nc{=’gjo} & ◊nc{=’cil} & ◊var{X} plus ◊var{Y}
  ◊var{X} or ◊var{Y} & ◊nc{=’te} & ◊nc{=’čo} & ◊nc{=’djo} & ◊nc{=’čil} & ◊var{Y}
  ◊var{X} xor ◊var{Y} & ◊nc{=’re} & ◊nc{=’pre} & ◊nc{=’vre} & ◊nc{=’ril} & ◊var{Y}
  ◊var{X} but not ◊var{Y} & ◊nc{=’ne} & ◊nc{=’njo} & ◊nc{=’mjo} & ◊nc{=’nil} & ◊var{X}
}

When the first coordinand is pronominal, then it is fused into the coordinating clitic, leaving the other coordinands behind.

The gender of a coordinated noun phrase involving the ◊trans{and} operation is the strongest of those of the coordinands. For this purpose, the human gender is stronger than the celestial, which is stronger than the terrestrial gender.

All coordinated noun phrases inherit the person in the same way: the first person takes precedence over the second, which takes precedence over the third.

When there are more than two coordinands, then the respective clitics occur on each element after the first. ◊trans{◊var{X}◊sub{1} xor … xor ◊var{X}◊sub{◊var{n}}} means ◊trans{exactly one of ◊var{X}◊sub{◊var{i}}}; ◊trans{◊var{X}◊sub{1} but not … but not ◊var{X}◊sub{◊var{n}}} means ◊trans{◊var{X}◊sub{1} but not any later ◊var{X}◊sub{◊var{i}}}. All pronominal clitics occur at the end of the coordinated noun phrase. In ‘but not’-coordinated phrases, there can be only one pronominal clitic (namely, the one representing the first item).

◊section[#:id "quotatives"]{Quotatives}

Quotatives are formed by wrapping the quoted material in quotation marks, followed by a particle depending on case and directness, forming a noun phrase.

Direct quotatives are used for verbatim speech. Indirect quotatives indicate some kind of paraphrasing and do not necessarily represent what someone has said. There exists a separate set of switch-reference indirect quotatives, which are used when (1) both the outer and inner clauses have a third-person subject and (2) the subjects do not corefer.

◊table/x[#:options (table-options #:caption "Quotative particles in Ŋarâþ Crîþ.") #:id "quotative-table"]{
  Case & Direct & Indirect & Indirect-SR
  Nominative & ◊nc{ner} & ◊nc{ler} & n/a
  Accusative & ◊nc{ne} & ◊nc{reþ} & ◊nc{rast}
  Dative & ◊nc{nes} & ◊nc{res} & ◊nc{rens}
  Genitive & ◊nc{nel} & ◊nc{ril} & ◊nc{rels}
  Locative & ◊nc{nos} & ◊nc{raþ} & ◊nc{reþþe}
  Instrumental & ◊nc{noca} & ◊nc{racþa} & ◊nc{racþaf}
  Abessive & ◊nc{noþa} & ◊nc{raþa} & ◊nc{raþas}
  Semblative & ◊nc{nit} & ◊nc{ret} & ◊nc{ret}
}

Direct quotatives necessarily inherit the personal and temporal deixis of the one who said its contents. The personal deixis does not shift inside an indirect quotative, but the temporal deixis shifts to that of the outer event:

◊gloss/x{
  ◊glfree{◊nc{#flirora «eltan cþasce» reþ maraþ.}}
  ◊gla{#fliror-a «elt-an cþasc-e» reþ mar-a-þ.}
  ◊glb{(name)-%nom.%sg salmon-%acc.%co cook-3%sg %quot.%acc.%ind say-3%sg-%past}
  ◊glfree{#flirora₁ said that they₁ ◊i{(sg)} were cooking salmon.}
}

◊gloss/x{
  ◊glfree{◊nc{#flirora «eltan cþasce» rast maraþ.}}
  ◊gla{#fliror-a «elt-an cþasc-e» rast mar-a-þ.}
  ◊glb{(name)-%nom.%sg salmon-%acc.%co cook-3%sg %quot.%acc.%ind.%sr say-3%sg-%past}
  ◊glfree{#flirora₁ said that they₂ ◊i{(sg)} were cooking salmon.}
}

◊section[#:id "names"]{Names}

◊subsection[#:id "names-personal"]{Personal names}

Ŋarâþ Crîþ recognizes two parts of personal names: the surname and the given name, in that order. Surnames are marked with either a ◊i{tor}, ◊l1{+} or a ◊i{njor}, ◊l1{+*}.

A given name is marked with a ◊i{carþ}, ◊l1{#}. If a person is known by a single name only, then the name is treated as a given name.

Both the surname and the given name are declined nouns.

A person is addressed or mentioned using the surname, given name, or both, with the surname being more formal than the given name. Nonetheless, the choice of whether to address someone by their surname or by their given name can also depend on other factors such as convenience of pronunciation or distinguishing between multiple people with the same given name or surname.

The use of titles is limited: there is no direct equivalent to ◊i{Mr.} or ◊i{Ms.}. Instead, titles are used merely to describe the role of the person. Notably, (1) they are always nouns, (2) they are never used in the vocative, (3) they are used with the person’s name when the person in question is introduced, and (4) the use of the title alone in later mentions over the name carries no social connotation otherwise. The closest equivalent to ◊i{sir} or ◊i{madam} is ◊l1{cercerin}, meaning ◊trans{stranger}, which is used to address someone whose name is not known.

A title is applied by putting it before the name and suffixing the relevant markers to the title: ◊l1{+} for surnames (regardless of the actual markers on the surname), ◊l1{#} for given names, and ◊l1{+#} if both are present:

◊gloss/x{
  ◊glfree{◊nc{dosaradir+# +astova #ageþne «tfaren inora sarałen arnenden cenventals ndogenħal» reþ maraþ.}}
  ◊gla{dosarad-ir+# +astov-a #ageþn-e «tfar-en inor-a sarał-en arnend-en cenvent-als n\dogenħ-al» reþ mar-a-þ.}
  ◊glb{teacher-%nom.%sg (surname)-%nom.%sg (given)-%nom.%sg money-%gen.%sg void-%nom.%sg school-%acc.%sg music-%gen.%co course-%dat.%co %pfv\%caus-twist-3%sg.%inv %quot.%acc.%ind say-3%sg-%past}
  ◊glfree{+astova #ageþne, a teacher, stated that the lack of money has caused the school to reduce funding for music classes.}
}

Because titles are used with names only to introduce a person, ◊l1{+#} is the most common choice for the postmarker.

◊subsection[#:id "names-place"]{Place names}

The name of any kind of place is marked with an ◊i{es}, ◊l1{@}.

Sometimes, a place name might be preceded by a noun describing the type of place. In this case, the common noun is suffixed with the marker ◊l1{@}: ◊l1{vlêcadir@ @elþana} ◊trans{the Elþana archipelago}.

◊subsection[#:id "names-language"]{Language names}

◊subsection[#:id "names-work"]{Titles of works}
