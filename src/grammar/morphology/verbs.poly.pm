#lang pollen

◊define-meta[title]{Verbs}

◊term{Predicates} can be divided into ◊term{independent verbs} (◊term{iverbs}) and ◊term{relationals}. In this chapter, we look at verbs; ◊xref/c["relationals.html" #:type 'inline]{the next chapter} covers relationals.

◊table/x[#:options (table-options
    #:caption "Comparison between independent verbs and relationals."
    #:colgroup '(c d d))]{
  Feature & Independent verbs & Relationals
  Semantics & Can indicate an action or state & Can indicate a state in relation to another entity
  Valency & 1 – 3 arguments & Usually 2 arguments, but occasionally 3
  Case frame & Nominative, plus possibly accusative and dative arguments depending on the verb & Nominative plus dative by default; the second argument can be accusative to change the meaning to involve motion towards the object or abessive for motion away from it
  Finite form & Finite conjugations & Attached to a scaffolding verb
  Modifying nouns & Participle forms (marked for case of shared noun in both the embedded and the main clause, as well as the gender of the shared noun in the main clause) & Lemma form or modified, depending on bias
  Modifying verbs & Converbal forms & Lemma form or modified, depending on bias
  Nominalized form & Particle + infinitive & ?
}

The adnominal and adverbial forms of a predicate are collectively called ◊term{modifying forms}.

◊section[#:id "valency"]{Valency and case frame}

All predicates have at least a nominative-case argument (the ◊term{subject}). Verbs are classified into five valency classes:

◊items{
  ◊item{◊term{Intransitive verbs} take only a nominative argument, without an accusative or dative argument.}
  ◊item{◊term{Semitransitive verbs} take a nominative and dative argument.}
  ◊item{◊term{Transitive verbs} take a nominative and accusative argument.}
  ◊item{◊term{Ditransitive verbs} take a nominative, accusative, and dative argument.}
  ◊item{◊xref["verbs.html" "aux-tour" "Subsection"]{◊term{Auxiliary verbs}} are not iverbs and thus are not predicates. Instead, they are a class of ◊term{predicate modifier}.}
}

Note that the labels of “nominative”, “accusative”, and “dative” arguments are somewhat arbitrary; the role of each case depends on the verb in question. However, there are some general tendencies:

◊items{
  ◊item{The agent, if present, is almost always the nominative argument.}
  ◊item{If the action described by a verb is perceived as having a direct effect on the patient, then the verb is more likely to be transitive than semitransitive.}
  ◊item{Conversely, if the action described by a verb is perceived as having an ◊em{indirect} effect on the patient, then the verb is more likely to be semitransitive than transitive.}
  ◊item{In a ditransitive verb with both a theme and a recipient, the theme is more likely to be accusative than dative.}
  ◊item{If there is an experiencer, then it is more likely than not to be in the nominative.}
}

◊section[#:id "combinators"]{Predicate modifiers}

◊term{Predicate modifiers} take as input one or more predicates and output another predicate as a result. They include:

◊items{
  ◊item{Auxiliary verbs: the resulting predicate has the same case frame as the input}
  ◊item{The causative voice affix ◊l0{do-}, which increases valency by one}
  ◊item{Adverbial phrases}
}

◊section[#:id "categories"]{Verb categories}

Predicates can mark the person and number of various arguments. First-person dual or plural arguments marked this way are also distinguished by clusivity. Whenever this is allowed, it might be possible to instead specify a reflexive or reciprocal argument.

Ŋarâþ Crîþ has two tenses: ◊term{present} (more precisely, ◊term{nonpast}) and ◊term{past}. In addition to the present, the present tense covers the future as well as the immediate past.

The present tense is also used as a narrative present: in stories, the past tense is used only for events that had happened before the current point.

Ŋarâþ Crîþ has two aspects: ◊term{imperfective} and ◊term{perfective}. The imperfective aspect is used for ongoing (such as progressive or habitual) actions.

In conjugation, aspects can be labeled as ◊term{direct} or ◊term{indirect}. The perfective aspect is used for completed actions. The ◊term{direct aspect} is imperfective for the present tense and perfective for the past tense; the ◊term{inverse aspect} is the other aspect.

◊section[#:id "inflection"]{Inflection of verbs}

A verb has six stems: I (infinitive), N (nonpast), P (past), R (nominative-rcase relative), Q (other relative), and L (locative). In addition, it has one thematic vowel, ◊nc{◊ṫ[0]}, which is either ◊l0{a} or ◊l0{i}.

The primary lemma form of a verb is its ◊term{infinitive}, which is always ◊l0{◊ṡ{I}◊ṫ[0]t}.

If the stem of a verb ends with a vowel that is the same as the initial vowel of the following affix or differs only in tone, then ◊l0{ħ} is inserted as a bridge between them. This epenthesis applies to all verb forms and is implied whenever an abstract form is given.

The consonantal conjunct form of a verb is ◊l0{◊ṡ{I}i}, and the vocalic conjunct form is ◊l0{◊ṡ{I}j}.

◊section[#:id "finite"]{Finite forms}

Verbs can be divided into ◊term{vitreous} and ◊term{resinous} verbs based on the conjugation of their finite forms. Resinous verbs exhibit more fusion in the finite forms than vitreous verbs, and they have a few forms that are absent in vitreous verbs.

A third-person generic subject not specified elsewhere often corresponds to an impersonal subject.

◊subsection[#:id "finite-vitreous"]{Vitreous verbs}

In vitreous verbs, there are two different affixes used depending on the aspect relative to the tense (Tables ◊xref/l["vitreous-direct" #f] & ◊xref/l["vitreous-inverse" #f]).

◊table/x[#:id "vitreous-direct" #:options (table-options #:caption ◊@{
  Conjugation of vitreous verbs according to the subject (direct aspect).
})]{
  Person \ Number & Singular & Dual & Plural & Generic
      1st excl. & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}n} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}cþ} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}◊ṫg{·}}
      1st incl. & & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3 · ◊ẋht}n} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2 · ◊ẋht}cþ} &
      2nd & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}s} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}ns}* & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}r} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}◊ṫg{·}}
      3rd & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}n} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv3}} & ◊nc{-u}
}

◊table/x[#:id "vitreous-inverse" #:options (table-options #:caption ◊@{
  Conjugation of vitreous verbs according to the subject (inverse aspect).
})]{
  Person \ Number & Singular & Dual & Plural & Generic
      1st excl. & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}l} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}nis} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}cþis} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3}◊ṫg{·}}
      1st incl. & & ◊nc{-◊ṫ[0]◊ẋ{◊ẋfr3 · ◊ẋht}nis} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2 · ◊ẋht}cis} &
      2nd & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}res} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}nsis} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}ris} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}◊ṫg{·}}
      3rd & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}l} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv2}nis} & ◊nc{-◊ṫ[0]◊ẋ{◊ẋv3}ris} & ◊nc{-os}
}

Present-tense forms use the N stem and past-tense forms use the P stem. For the past tense, there is an additional tense suffix if the person–number affix has only one vowel: ◊l0{-þ} when the preceding affix ends with a vowel or ◊l0{-r} and ◊l0{-ta} otherwise. (To be precise, the empty coda is changed to ◊l0{-þ}, and ◊l0{-r} is changed to ◊l0{-rþ}.) If the person–number affix contains two or more vowels, then there is no tense affix and ◊nc{◊ṡ{P}◊ẋ{t}} is used in place of ◊nc{◊ṡ{P}}.

Additionally, in perfective forms, the verb is eclipsed.

The second-person dual direct suffix is ◊l0{-◊ṫ[0]◊ẋ{◊ẋv2}ns} if there are no subsequent suffixes but ◊l0{-◊ṫ[0]◊ẋ{◊ẋv2}n} if there are.

The first- and second-person generic forms use phi consonants according to the stem used, possibly with fusion, but before ◊l0{ħ}-epenthesis.

An object affix can be added immediately after the person–number affix (i.e. before the tense affix if it is present) and shows the person and number of an accusative or dative argument. It is never necessary, but it cannot appear redundantly to an explicitly stated argument that is not postposed.

◊table/x[#:id "vitreous-object" #:options (table-options #:caption "Object affixes for vitreous verbs.")]{
  Person \ Number & Singular & Dual & Plural & Generic
  1st excl. & ◊nc{pe} & ◊nc{pjo} & ◊nc{po} & ◊nc{ten}
  1st incl. & & ◊nc{pjô} & ◊nc{pô} &
  2nd & ◊nc{ve} & ◊nc{vi} & ◊nc{vo} & ◊nc{ves}
  3rd celestial & ◊nc{le}
  3rd terrestrial & ◊nc{lu}
  3rd human & ◊nc{les} & ◊nc{lis} & ◊nc{los}
  3rd epicene & & ◊nc{li} & ◊nc{lo} & ◊nc{las}
  Reflexive & ◊colspan[4]{◊nc{cin}}
  Reciprocal & ◊colspan[4]{◊nc{riþ}}
}

◊subsection[#:id "finite-resinous"]{Resinous verbs}

The resinous finite forms are described in Tables ◊xref/l["resinous-ni" #f] – ◊xref/l["resinous-pp" #f].

◊table/x[#:id "resinous-ni" #:options (table-options #:caption ◊@{
  Conjugation of resinous verbs according to the subject (present imperfective).
  ◊sup{1} ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2}} if followed by an object affix.
})]{
  Person \ Number & Singular & Dual & Plural & Generic
      1st excl. & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3 · ◊ẋht}c} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2}þ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}◊ṫg{N}}
      1st incl. & & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}c} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2 · ◊ẋht}þ} &
      2nd & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2}s} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2}vi} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2}r} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2}◊ṫg{N}}
      3rd & ◊nc{◊ṡ{N}◊ẋ{ε}}◊sup{1} & ◊nc{◊ṡ{N}◊ẋ{t}e} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv3}} & ◊nc{◊ṡ{N}u}
}

◊table/x[#:id "resinous-np" #:options (table-options #:caption ◊@{
  Conjugation of resinous verbs according to the subject (present perfective).
  ◊sup{1} When the final bridge of ◊nc{◊ṡ{N}} is empty, then this form is ◊nc{◊|uru|◊ṡ{N}p◊ṫ[0]◊ẋ{◊ẋfr3}}, with the final bridge being stripped from ◊nc{◊ṡ{N}}.
  ◊sup{2} The object affix is prefixed instead, and lenition occurs on the object affix instead of eclipsis.
})]{
  Person \ Number & Singular & Dual & Plural & Generic
      1st excl. & ◊nc{◊|uru|◊ṡ{N}◊ṫ[0]e}◊sup{1} & ◊nc{◊|sei|◊ṡ{N}aŋe} & ◊nc{◊|sei|◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}n} & ◊nc{◊ṡ{P}◊ṫ[0]◊ẋ{◊ẋfr3}◊ṫg{P}}
      1st incl. & & ◊nc{◊|sei|◊ṡ{N}âge} & ◊nc{◊|sei|◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3 · ◊ẋht}r} &
      2nd & ◊nc{◊|uru|◊ṡ{N}ea} & ◊nc{◊|uru|◊ṡ{N}◊ṫ[0]ste} & ◊nc{◊|uru|◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2}ris} & ◊nc{◊ṡ{P}◊ṫ[0]◊ẋ{◊ẋv2}◊ṫg{P}}
      3rd & ◊nc{◊|uru|◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2}}◊sup{2} & ◊nc{◊|uru|◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2}n} & ◊nc{◊|uru|◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv3}} & ◊nc{◊ṡ{N}os}
}

◊table/x[#:id "resinous-pi" #:options (table-options #:caption ◊@{
  Conjugation of resinous verbs according to the subject (past imperfective).
  ◊sup{1} ◊nc{◊ṡ{P}◊ṫ[0]◊ẋ{◊ẋfr3}l} when followed by an object affix.
  ◊sup{2} ◊nc{◊ṡ{P}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋv2}r} when followed by an object affix.
  ◊sup{3} ◊nc{◊ṡ{P}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋv2}n} when followed by an object affix.
})]{
  Person \ Number & Singular & Dual & Plural & Generic
      1st excl. & ◊nc{◊ṡ{P}◊ṫ[0]◊ẋ{◊ẋfr3}lt}◊sup{1} & ◊nc{◊ṡ{P}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋfr3}si} & ◊nc{◊ṡ{P}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋfr3}vi} & ◊nc{◊|sei2|◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}◊ṫg{N}ta}
      1st incl. & & ◊nc{◊ṡ{P}◊ẋ{n}eca} & ◊nc{◊ṡ{P}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋfr3}i} &
      2nd & ◊nc{◊ṡ{P}◊ṫ[0]◊ẋ{◊ẋv2}cþ} & ◊nc{◊ṡ{P}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋv2}c} & ◊nc{◊ṡ{P}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋv2}ł◊ṫ[0]◊ẋ{◊ẋfr3}} & ◊nc{◊|sei2|◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋv2}◊ṫg{N}ta}
      3rd & ◊nc{◊ṡ{P}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋv2}l}◊sup{2} & ◊nc{◊ṡ{P}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋv2}nþ}◊sup{3} & ◊nc{◊ṡ{P}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋv3}r} & ◊nc{◊ṡ{P}oþ}
}

◊table/x[#:id "resinous-pp" #:options (table-options #:caption ◊@{
  Conjugation of resinous verbs according to the subject (past perfective).
  ◊sup{1} ◊nc{◊|uru|◊ṡ{Q}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋfr3 · ◊ẋht}s} when followed by an object affix.
  ◊sup{2} ◊nc{◊|uru|◊ṡ{Q}◊ẋ{t}êłi} when followed by an object affix.
  ◊sup{3} ◊nc{◊|uru|◊ṡ{P}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋv2}l} when followed by an object affix.
})]{
  Person \ Number & Singular & Dual & Plural & Generic
      1st excl. & ◊nc{◊|uru|◊ṡ{P}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋfr3}} & ◊nc{◊|uru|◊ṡ{Q}◊ẋ{t}ôr} & ◊nc{◊|uru|◊ṡ{Q}◊ṫ[0]◊ẋ{◊ẋfr3}st}◊sup{1} & ◊nc{◊|uru|◊ṡ{P}◊ṫ[0]◊ẋ{◊ẋfr3}◊ṫg{P}ta}
      1st incl. & & ◊nc{◊|uru|◊ṡ{Q}◊ẋ{t}or} & ◊nc{◊|uru|◊ṡ{Q}◊ẋ{t}ełt}◊sup{2} &
      2nd & ◊nc{◊|uru|◊ṡ{P}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋv2}s} & ◊nc{◊|uru|◊ṡ{P}◊ẋ{t}eši} & ◊nc{◊|uru|◊ṡ{P}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋv2 · ◊ẋgen}s} & ◊nc{◊|uru|◊ṡ{P}◊ṫ[0]◊ẋ{◊ẋv2}◊ṫg{P}ta}
      3rd & ◊nc{◊|uru|◊ṡ{P}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋv2}} & ◊nc{◊|uru|◊ṡ{P}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋv2}ri} & ◊nc{◊|uru|◊ṡ{P}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋv2}lt}◊sup{3} & ◊nc{◊|uru|◊ṡ{P}oþ}
}

Resinous verbs use object affixes at the end, but the forms used are different from those for vitreous verbs, and there are alternate forms if the object suffix follows ◊l0{-l}.

◊table/x[#:id "resinous-object" #:options (table-options #:caption "Object affixes for resinous verbs.")]{
  Person \ Number & Singular & Dual & Plural & Generic
  1st excl. & ◊nc{pe} & ◊nc{pal} (◊nc{pae}) & ◊nc{gins} & ◊nc{peł} (◊nc{peþ})
  1st incl. & & ◊nc{pâl} (◊nc{paê}) & ◊nc{gîns} &
  2nd & ◊nc{ve} & ◊nc{vel} (◊nc{ven}) & ◊nc{tor} & ◊nc{veł} (◊nc{veþ})
  3rd celestial & ◊nc{li} (◊nc{gi})
  3rd terrestrial & ◊nc{rel}
  3rd human & ◊nc{li} (◊nc{gi}) & ◊nc{lec} (◊nc{geþ}) & ◊nc{ljen} (◊nc{gjan})
  3rd epicene & & ◊nc{lec} (◊nc{geþ}) & ◊nc{lje} (◊nc{gja}) & ◊nc{sâ}
  Reflexive & ◊colspan[4]{◊nc{cin}}
  Reciprocal & ◊colspan[4]{◊nc{riþ} (◊nc{giþ})}
}

If the object affix is prefixed as in the present perfective third-person singular forms, then the first-person plural exclusive and inclusive forms become ◊l0{gin-} and ◊l0{gîn-}. Additionally, if an object affix would otherwise be eclipsed, it is instead lenited.

◊subsubsection[#:id "finite-resinous-transfinite"]{Transfinite forms}

On top of the finite forms, resinous verbs have ◊term{transfinite} forms, which encode additional categories. They may be syntactically finite or otherwise and receive object affixes as usual:

◊items{
  ◊item{The second-person singular imperative: ◊l0{◊ṡ{N}◊ẋ{þ}au}. The headp ◊l1{le} is used even when this form is used.}
  ◊item{The ◊term{supine} form, which is an adverbial or adnominal form indicating the purpose of another action: ◊l0{◊ṡ{N}◊ẋ{n}els} (◊l0{◊ṡ{N}◊ẋ{n}ase} before an object affix). This is equivalent to using the relational ◊l1{roc} with a dative nominalized form.}
  ◊item{The ◊term{active gerundive} forms, which indicate the intended purpose or fate of the subject. The predicative form is ◊l0{◊ṡ{N}◊ẋ{n}elv◊ṫ[0]◊ẋ{◊ẋv2}}, and the attributive form is ◊l0{◊ṡ{N}◊ẋ{n}ełor} when the head NP is terrestrial and ◊l0{◊ṡ{N}◊ẋ{n}elv◊ṫ[0]◊ẋ{◊ẋv2 · ◊ẋgen}n} when it is of any other gender. The attributive form has prefixed object affixes. The terrestrial attributive form can also be used adverbially.}
  ◊item{The ◊term{passive gerundive} forms, which indicate the intended purpose or fate of the direct or indirect object. The predicative form is ◊l0{◊ṡ{N}◊ẋ{þ}elv◊ṫ[0]◊ẋ{◊ẋv2}}, and the attributive form is ◊l0{◊ṡ{N}◊ẋ{þ}elv◊ṫ[0]◊ẋ{◊ẋv2 · ◊ẋgen}n}. The attributive form has prefixed object affixes. The attributive form can also be used adverbially.}
}

The active and passive gerundive forms are often used for future actions.

◊section[#:id "participle"]{Participles (adnominal forms)}

Participle forms of verbs are used in ◊term{relative clauses}. The ◊term{common argument} of a relative clause is the referent shared between the relative clause and the outer clause. Participles are then distinguished by:

◊items{
  ◊item{the ◊term{relative case} or ◊term{rcase} – the case of the common argument in the relative clause (nominative, accusative, or dative; as well as the genitives of any of these separataely)}
  ◊item{the ◊term{head case} or ◊term{hcase} – the case of the common argument in the outer clause (any case)}
}

and optionally, depending on the genus:

◊items{
  ◊item{the ◊term{head gender} or ◊term{hgender} – the gender of the common argument in the outer clause}
  ◊item{the ◊term{head number} or ◊term{hnumber} – the number of the common argument in the outer clause}
}

In general, participles with nominative rcase use the R stem, while participles with other rcases use the Q stem.

In terms of participle forms, verbs are first divided into ◊term{genera}, which describe the broad pattern of inflection, and within each genus into ◊term{species}.

Participles of ◊term{genus I} verbs mark for hgender but not for hnumber. Participles of ◊term{genus II} verbs mark for hnumber but not for hgender. Participles of ◊term{genus III} verbs mark for whether or not the hnumber is singular and mark for hgender only when the hnumber is singular.

A participle of a genus I verb with terrestrial hgender triggers a lenition in the noun it modifies if the noun follows the participle immediately.

Because participles agree with the heads of relative clauses, relative clauses can be moved away from their heads.

Participle forms can also take object affixes, but the affixes occur before the root in this case, and the vitreous set is used regardless of the verb’s material. If an object affix is present, then it describes a nominative, accusative, or dative argument in the embedded clause that is not the common argument. Again, an object affix cannot appear redundantly to an explicitly stated argument, and if an object affix would otherwise be eclipsed, it is instead lenited.

Relative clauses can modify free noun phrases only; they cannot modify bound pronouns (such as subject or object inflections on verbs or possessive clitics). This restriction sometimes requires a personal pronoun to be expressed in a free form as an ◊xref["nouns.html" "pronouns-personal-emphatic" "Sub-subsection"]{emphatic pronoun}.

A semantically vague noun such as ◊l1{sar} or ◊l1{nava} can be used as the head of a relative clause. Alternatively, in informal language, the participle may be modified by a demonstrative determiner for the same purpose.

◊subsection[#:id "genus-i"]{Genus I}

Genus I participles distinguish rcase, hcase, and hgender.

◊subsubsection[#:id "species-i1"]{Species I₁}

Species I₁ specifies only the forms in which the rcase is nominative. The rest are derived from these as such:

◊items{
  ◊item{infix ◊l0{-aþ-} between the stem and the ending to get the respective forms with accusative rcase,}
  ◊item{infix ◊l0{-◊ṫ[1]◊ẋ{◊ẋgen}þ-} to get the respective forms with dative rcase,}
  ◊item{infix ◊l0{-◊ṫ[1]◊ẋ{◊ẋgen}n-} to get the respective forms with genitive-of-nominative rcase,}
  ◊item{infix ◊l0{-anþ-} to get the respective forms with genitive-of-accusative rcase, and}
  ◊item{infix ◊l0{-◊ṫ[1]◊ẋ{◊ẋgen}nþ-} to get the respective forms with genitive-of-dative rcase.}
}

If the stem ends with ◊l0{þ} or ◊l0{ð}, then all ◊l0{þ}s in the above infixes are replaced with ◊l0{t}s.

Each species I₁ verb specifies a celestial ending and a terrestrial ending. The celestial ending is one of ◊l0{-a}, ◊l0{-e}, ◊l0{-o}, ◊l0{-an}, ◊l0{-en}, or ◊l0{-in}. The terrestrial ending can be ◊l0{-os} or ◊l0{-or} if the celestial ending ends in a vowel, and ◊l0{-on} or ◊l0{-or} if it ends in ◊l0{-n}.

For this species, let ◊nc{◊ṫ[1]} be the vowel of the celestial ending, ◊nc{◊ṫ[2]} its (possibly empty) coda, and ◊nc{◊ṫ[3]} the coda of the terrestrial ending.

For nominative to locative hcases, ◊xref["verbs.html" "part-ia-nl-t" "Table"]{the forms for terrestrial hgender} are determined separately from ◊xref["verbs.html" "part-ia-nl-c" "Table"]{the forms for other hgenders}.

◊table/x[#:options (table-options #:caption ◊@{
  Terrestrial-hcase forms by ending type.
} #:placement 'please-here) #:id "part-ia-nl-t"]{
  ◊nc{◊ṫ[2]} & ◊colspan*[2 'merged]{∅} & ◊colspan*[2 'merged]{◊nc{n}}
  ◊nc{◊ṫ[3]} & ◊nc{s} & ◊nc{r} & ◊nc{n} & ◊nc{r}
  ◊HEADER
  Nominative & ◊nc{-os} & ◊nc{-or} & ◊nc{-on} & ◊nc{-or}
  Accusative & ◊nc{-on} & ◊nc{-on} & ◊nc{-anon} & ◊nc{-on}
  Dative & ◊nc{-oþ} & ◊nc{-oþ} & ◊nc{-os} & ◊nc{-os}
  Genitive & ◊nc{-el} & ◊nc{-el} & ◊nc{-el} & ◊nc{-el}
  Locative & ◊nc{-orþ} & ◊nc{-orþ} & ◊nc{-ori} & ◊nc{-ori}
}

◊table/x[#:options (table-options #:caption ◊@{
  Celestial- and human-hcase forms by ending type.
} #:placement 'please-here) #:id "part-ia-nl-c"]{
  ◊nc{◊ṫ[2]} & ◊colspan*[2 'merged]{∅} & ◊colspan*[2 'merged]{◊nc{n}}
  Hgender & ◊ab["Celestial"]{Cel} & ◊ab["Human"]{Hum} & ◊ab["Celestial"]{Cel} & ◊ab["Human"]{Hum}
  ◊HEADER
  Nominative & ◊nc{-◊ṫ[1]} & ◊nc{-◊ṫ[1]c} & ◊nc{-◊ṫ[1]n} & ◊nc{-◊ṫ[1]n}
  Accusative & ◊nc{-◊ṫ[1]n} & ◊nc{-ôr} & ◊nc{-◊ṫ[1]na} & ◊nc{-◊ṫ[1]◊ẋ{a/e}an}
  Dative & ◊nc{-◊ṫ[1]s} & ◊nc{-◊ṫ[1]s} & ◊nc{-◊ṫ[1]ns} & ◊nc{-◊ṫ[1]ns}
  Genitive & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}n} & ◊nc{-jôr} & ◊nc{-il} & ◊nc{-il}
  Locative & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}s} & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}s} & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}s} & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}s}
}

Species I₁ contains three sets of suffixes for the instrumental- and abessive-hcase forms (◊xref/l["part-ia-ia"]{Table}). The set to be used must be memorized for each verb; however, verbs whose R or Q stems contain labial consonants will generally not take the ◊i{α}-class endings, and those whose stems end with ◊l0{þ} or ◊l0{ð} take the ◊i{δ}-class endings if they would not otherwise be in class ◊i{α}.

◊table/x[#:options (table-options #:caption ◊@{
  Instrumental- and abessive-hcase endings.
} #:placement 'please-here) #:id "part-ia-ia"]{
  Case & ◊colspan*[3 'merged]{Instrumental} & ◊colspan*[3 'merged]{Abessive}
  Hgender & ◊ab["Celestial"]{Cel} & ◊ab["Terrestrial"]{Ter} & ◊ab["Human"]{Hum} & ◊ab["Celestial"]{Cel} & ◊ab["Terrestrial"]{Ter} & ◊ab["Human"]{Hum}
  ◊HEADER
  α & ◊nc{-epa} & ◊nc{-epos} & ◊nc{-epac} & ◊nc{-eši} & ◊nc{-eši} & ◊nc{-ešic}
  β & ◊nc{-(e)la} & ◊nc{-(e)lon} & ◊nc{-(e)lac} & ◊nc{-(e)na} & ◊nc{-(e)nor} & ◊nc{-(e)nac}
  γ & ◊nc{-(e)la} & ◊nc{-(e)lon} & ◊nc{-(e)lac} & ◊nc{-(e)ta} & ◊nc{-(e)tor} & ◊nc{-(e)tac}
  δ & ◊nc{-êl} & ◊nc{-êl} & ◊nc{-êl} & ◊nc{-eva} & ◊nc{-evor} & ◊nc{-evac}
}

In rcases other than nominative, the ◊i{β}- and ◊i{γ}-class instrumental suffixes lack the initial ◊l0{e}. That is, the infixes for these rcases become ◊l0{-aþl-}, ◊l0{-◊ṫ[1]◊ẋ{◊ẋgen}þl-}, ◊l0{-◊ṫ[1]◊ẋ{◊ẋgen}nl-}, ◊l0{-anþl-}, and ◊l0{-◊ṫ[1]◊ẋ{◊ẋgen}nþl-}.

The abessive suffixes work similarly, except that the genitive-of-nominative-rcase infix becomes ◊l0{-ann-} or ◊l0{-ant-} instead. (Equivalently, these suffixes work as if they cause the stem plus rcase infix to be fused with their first consonant.)

Finally, the semblative forms have the affixes ◊l0{-eł} for all three genders.

◊subsubsection[#:id "species-i3"]{Species I₃}

Species I₃ specifies the ◊xref["verbs.html" "part-i3" "Table"]{forms for the nominative, accusative, and genitive rcases}. The genitive-rcase forms are derived from their non-genitive-rcase counterparts by fusing the appropriate stem with ◊l0{n}. This species contains two subspecies: ◊i{w} and ◊i{x}.

◊table/x[#:id "part-i3" #:options (table-options #:caption "Participle form inflections for species I₃.")]{
  Subspecies & ◊colspan*[3 'merged]{w} & ◊colspan*[3 'merged]{x}
  Hcase \ Hgender & ◊ab["Celestial"]{Cel} & ◊ab["Terrestrial"]{Ter} & ◊ab["Human"]{Hum} & ◊ab["Celestial"]{Cel} & ◊ab["Terrestrial"]{Ter} & ◊ab["Human"]{Hum}
  ◊HEADER
  ◊pseudoheader{Nominative rcase}
  Nominative & ◊nc{-a} & ◊nc{-os} & ◊nc{-ac} & ◊nc{-iþ} & ◊nc{-os} & ◊nc{-icþ}
  Accusative & ◊nc{-an} & ◊nc{-on} & ◊nc{-ôr} & ◊nc{-inþ} & ◊nc{-on} & ◊nc{-orþ}
  Dative & ◊nc{-as} & ◊nc{-oþ} & ◊nc{-as} & ◊nc{-iþo} & ◊nc{-oþ} & ◊nc{-icþo}
  Genitive & ◊nc{-er} & ◊nc{-or} & ◊nc{-er} & ◊nc{-ir} & ◊nc{-or} & ◊nc{-ir}
  Locative & ◊nc{-ei} & ◊nc{-ei} & ◊nc{-ei} & ◊nc{-ei} & ◊nc{-ei} & ◊nc{-ei}
  Instrumental & ◊colspan*[6 'merged]{See below}
  Abessive & ◊colspan*[6 'merged]{See below}
  Semblative & ◊nc{-et} & ◊nc{-ot} & ◊nc{-est} & ◊nc{-it} & ◊nc{-ot} & ◊nc{-ist}
  ◊pseudoheader{Accusative rcase}
  Nominative & ◊nc{-e} & ◊nc{-ios} & ◊nc{-ec} & ◊nc{-aþ} & ◊nc{-avas} & ◊nc{-acþ}
  Accusative & ◊nc{-en} & ◊nc{-osi} & ◊nc{-en} & ◊nc{-anþ} & ◊nc{-avan} & ◊nc{-arþ}
  Dative & ◊nc{-es} & ◊nc{-ioþ} & ◊nc{-es} & ◊nc{-aþo} & ◊nc{-avaþ} & ◊nc{-acþo}
  Genitive & ◊nc{-eri} & ◊nc{-ori} & ◊nc{-eri} & ◊nc{-ar} & ◊nc{-avar} & ◊nc{-ar}
  Locative & ◊nc{-ami} & ◊nc{-ami} & ◊nc{-ami} & ◊nc{-ami} & ◊nc{-ami} & ◊nc{-ami}
  Instrumental & ◊colspan*[6 'merged]{◊l0{◊ṡ{Q}◊ẋ{t}} + nominative-rcase endings}
  Abessive & ◊colspan*[6 'merged]{◊l0{◊ṡ{Q}◊ẋ{t}} + nominative-rcase endings}
  Semblative & ◊nc{-aþet} & ◊nc{-aþot} & ◊nc{-aþest} & ◊nc{-aþit} & ◊nc{-aþot} & ◊nc{-aþist}
  ◊pseudoheader{Dative rcase}
  Nominative & ◊nc{-ae} & ◊nc{-ajos} & ◊nc{-ace} & ◊nc{-eþ} & ◊nc{-evas} & ◊nc{-ecþ}
  Accusative & ◊nc{-aen} & ◊nc{-ajon} & ◊nc{-acen} & ◊nc{-enþ} & ◊nc{-evan} & ◊nc{-erþ}
  Dative & ◊nc{-aes} & ◊nc{-ajoþ} & ◊nc{-aces} & ◊nc{-eþo} & ◊nc{-evaþ} & ◊nc{-ecþo}
  Genitive & ◊nc{-ari} & ◊nc{-aôr} & ◊nc{-ari} & ◊nc{-er} & ◊nc{-evar} & ◊nc{-er}
  Locative & ◊nc{-emi} & ◊nc{-emi} & ◊nc{-emi} & ◊nc{-emi} & ◊nc{-emi} & ◊nc{-emi}
  Instrumental & ◊colspan*[6 'merged]{◊l0{◊ṡ{Q}◊ẋ{þ}} + nominative-rcase endings}
  Abessive & ◊colspan*[6 'merged]{◊l0{◊ṡ{Q}◊ẋ{þ}} + nominative-rcase endings}
  Semblative & ◊nc{-eþet} & ◊nc{-eþot} & ◊nc{-eþest} & ◊nc{-eþit} & ◊nc{-eþot} & ◊nc{-eþist}
}

Species I₃ uses the same instrumental and abessive affixes as I₁, except that the vowels in the ◊i{β}- and ◊i{γ}-class suffixes are never omitted.

◊subsection[#:id "genus-ii"]{Genus II}

Genus II participles distinguish rcase, hcase, and hnumber.

◊subsubsection[#:id "species-ii4"]{Species II₄}

Species II₄ specifies only the forms in which the rcase is nominative. The rest are derived from these as such:

◊items{
  ◊item{infix ◊l0{-el-} between the stem and the ending to get the respective forms with accusative rcase,}
  ◊item{infix ◊l0{-eł-} to get the respective forms with dative rcase,}
  ◊item{infix ◊l0{-◊ṫ[1]n-} to get the respective forms with genitive-of-nominative rcase, where ◊nc{◊ṫ[1]} is the first vowel of the nominative-rcase ending,}
  ◊item{infix ◊l0{-◊ṫ[1]nl-} to get the respective forms with genitive-of-accusative rcase, and}
  ◊item{infix ◊l0{-◊ṫ[1]nł-} to get the respective forms with genitive-of-dative rcase.}
}

The ending can be one of ◊l0{-a}, ◊l0{-e}, ◊l0{-o}, ◊l0{-as}, ◊l0{-es}, ◊l0{-ar}, ◊l0{-er}, ◊l0{-os} or ◊l0{-or}, or ◊l0{-on}. The last three endings encompass the ◊xref["verbs.html" "part-ii4-t" "Table"]{terrestrial ending set}, and the others encompass the ◊xref["verbs.html" "part-ii4-t" "Table"]{celestial ending set}.

Let ◊nc{◊ṫ[1]} be the vowel of the ending and ◊nc{◊ṫ[2]} its (possibly empty) coda. Then the celestial ending set is as follows:

◊table/x[#:id "part-ii4-c" #:options (table-options #:caption ◊@{
  Participle form inflections for species II₄ (celestial endings).
  ◊sup{1} ◊nc{-o} if ◊nc{◊ṫ[2]} is ◊l0{s}.
})]{
  Hcase \ Hnumber & Singular & Dual & Plural & Generic
  Nominative & ◊nc{-◊ṫ[1]◊ṫ[2]} & ◊nc{-◊ṫ[1]c} & ◊nc{-◊ṫ[1]◊ẋ{◊ẋpl}} & ◊nc{-◊ṫ[1]◊ẋ{◊ẋfr}◊ṫg{·}}
  Accusative & ◊nc{-◊ṫ[1]n} & ◊nc{-ôr} & ◊nc{-on} & ◊nc{-◊ṫ[1]◊ẋ{◊ẋfr}◊ṫg{·}en}
  Dative & ◊nc{-◊ṫ[1]s}◊sup{1} & ◊nc{-◊ẋ{t}◊ṫ[1]s} & ◊nc{-os} & ◊nc{-◊ṫ[1]◊ẋ{◊ẋfr}◊ṫg{·}es}
  Genitive & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}n} & ◊nc{-◊ẋ{t}◊ṫ[1]◊ẋ{◊ẋgen}n} & ◊nc{-in} & ◊nc{-◊ẋ{n}e◊ṫg{·}}
  Locative & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}s} & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}sta} & ◊nc{-is} & ◊nc{-e◊ṫg{·}o}
  Instrumental & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}ls} & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}lþa} & ◊nc{-ils} & ◊nc{-e◊ṫg{·}os}
  Abessive & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}ma} & ◊nc{-◊ṫ[1]◊ẋ{◊ẋgen}nva} & ◊nc{-ima} & ◊nc{-elce◊ṫg{·}}
  Semblative & ◊nc{-it} & ◊nc{-icta} & ◊nc{-et} & ◊nc{-icþ}
}

The terrestrial ending set is as follows:

◊table/x[#:id "part-ii4-t" #:options (table-options #:caption ◊@{
  Participle form inflections for species II₄ (terrestrial endings). In this case, ◊nc{◊ṫ[1]} is always ◊l0{o}.
  ◊sup{1} ◊nc{-es} when ◊nc{◊ṫ[2]} is ◊l0{r}.
  ◊sup{2} ◊nc{-an} when ◊nc{◊ṫ[2]} is ◊l0{n}.
  ◊sup{3} ◊nc{-oþ} when ◊nc{◊ṫ[2]} is ◊l0{s}.
} #:placement 'forcehere)]{
  Hcase \ Hnumber & Singular & Dual & Plural & Generic
  Nominative & ◊nc{-o◊ṫ[2]} & ◊nc{-oc} & ◊nc{-or}◊sup{1} & ◊nc{-u}
  Accusative & ◊nc{-on}◊sup{2} & ◊nc{-ar} & ◊nc{-en} & ◊nc{-a◊ṫg{·}}
  Dative & ◊nc{-os}◊sup{3} & ◊nc{-ocþ} & ◊nc{-erþ} & ◊nc{-as}
  Genitive & ◊nc{-el} & ◊nc{-acel} & ◊nc{-il} & ◊nc{-e◊ṫg{·}}
  Locative & ◊nc{-ecþ} & ◊nc{-ecþ} & ◊nc{-acþ} & ◊nc{-ece◊ṫg{·}}
  Instrumental & ◊nc{-els} & ◊nc{-elþa} & ◊nc{-ils} & ◊nc{-e◊ṫg{·}os}
  Abessive & ◊nc{-ema} & ◊nc{-enva} & ◊nc{-ima} & ◊nc{-elce◊ṫg{·}}
  Semblative & ◊nc{-ot} & ◊nc{-octos} & ◊nc{-ot} & ◊nc{-ocþ}
}

◊subsubsection[#:id "species-ii3"]{Species II₃}

In species II₃, the stem is followed by the rcase infix, which come in two sets: ◊l0{-es- -est- -ist- -ens- -ent- -int-} (◊term{◊i{s}-infixes}) and ◊l0{-ev- -eft- -ift- -env- -enf- -inf-} (◊term{◊i{v}-infixes}).

The rcase infix is then followed by the ◊xref["verbs.html" "part-ii3" "Table"]{hcase–hnumber suffix}. The generic-hnumber forms are identical to the singular-hnumber forms.

◊table/x[#:id "part-ii3" #:options (table-options #:caption ◊@{
  Hcase–hnumber suffixes for species II₃.
} #:placement 'forcehere)]{
  Hcase \ Hnumber & ◊ab["Singular"]{Sg.} / ◊ab["Generic"]{Gc.} & Dual & Plural
  Nominative & ◊nc{-a} & ◊nc{-ac} & ◊nc{-o}
  Accusative & ◊nc{-an} & ◊nc{-or} & ◊nc{-on}
  Dative & ◊nc{-as} & ◊nc{-acþ} & ◊nc{-os}
  Genitive & ◊nc{-en} & ◊nc{-er} & ◊nc{-in}
  Locative & ◊nc{-al} & ◊nc{-al} & ◊nc{-al}
  Instrumental & ◊nc{-aþ} & ◊nc{-aþ} & ◊nc{-oþ}
  Abessive & ◊nc{-af} / ◊nc{-ał} & ◊nc{-af} / ◊nc{-ał} & ◊nc{-ef} / ◊nc{-eł}
  Semblative & ◊nc{-et} & ◊nc{-ecþ} & ◊nc{-it}
}

The abessive-hcase suffixes use ◊l0{f} for the ◊i{s}-infixes and ◊l0{ł} for the ◊i{v}-infixes.

◊subsection[#:id "genus-iii"]{Genus III}

Genus I participles distinguish rcase and hcase. The forms with singular hnumber also distinguish hgender; otherwise, both hnumber and hgender are unspecified.

◊subsubsection[#:id "species-iii1"]{Species III₂}

Species III₂ specifies only ◊xref["verbs.html" "part-iii2" "Table"]{the forms in which the rcase is accusative or dative}. The rest are derived from these as such:

◊items{
  ◊item{Infix ◊l0{-en-} between the stem and the ending of the accusative-rcase form to get the respective forms with nominative rcase. If the ending starts with ◊l0{e}, in which case infix ◊l0{-an-} instead.}
  ◊item{Infix ◊l0{-il-} between the stem and the ending of the accusative-rcase form to get the respective forms with genitive-of-nominative rcase.}
  ◊item{Infix ◊l0{-ag-} between the stem and the ending of the accusative-rcase form to get the respective forms with genitive-of-accusative rcase. However, the infix is ◊l0{-aþ-} for the locative-hcase forms.}
  ◊item{Infix ◊l0{-eg-} between the stem and the ending of the dative-rcase form to get the respective forms with genitive-of-dative rcase.}
}

◊table/x[#:id "part-iii2" #:options (table-options #:caption "Participle form inflections for species III₂." #:placement 'forcehere)]{
  Hnumber & ◊colspan*[3 'merged]{Singular} & Other
  Hcase \ Hgender & Celestial & Terrestrial & Human & —
  ◊HEADER
  ◊pseudoheader{Accusative rcase}
  Nominative & ◊nc{-ar} & ◊nc{-on} & ◊nc{-ar} & ◊nc{-als}
  Accusative & ◊nc{-an} & ◊nc{-anon} & ◊nc{-an} & ◊nc{-alna}
  Dative & ◊nc{-arþ} & ◊nc{-os} & ◊nc{-as} & ◊nc{-arþ}
  Genitive & ◊nc{-en} & ◊nc{-el} & ◊nc{-en} & ◊nc{-il}
  Locative & ◊nc{-eca} & ◊nc{-ecos} & ◊nc{-eac} & ◊nc{-ego}
  Instrumental & ◊nc{-eli} & ◊nc{-els} & ◊nc{-eli} & ◊nc{-ili}
  Abessive & ◊nc{-eno} & ◊nc{-enos} & ◊nc{-eno} & ◊nc{-ene}
  Semblative & ◊nc{-et} & ◊nc{-ot} & ◊nc{-est} & ◊nc{-et}
  ◊pseudoheader{Dative rcase}
  Nominative & ◊nc{-or} & ◊nc{-eħon} & ◊nc{-or} & ◊nc{-els}
  Accusative & ◊nc{-aħon} & ◊nc{-onon} & ◊nc{-aħon} & ◊nc{-ana}
  Dative & ◊nc{-orþ} & ◊nc{-eħos} & ◊nc{-orþ} & ◊nc{-orþ}
  Genitive & ◊nc{-in} & ◊nc{-il} & ◊nc{-in} & ◊nc{-in}
  Locative & ◊nc{-oca} & ◊nc{-ecos} & ◊nc{-avac} & ◊nc{-avo}
  Instrumental & ◊nc{-ore} & ◊nc{-ore} & ◊nc{-ore} & ◊nc{-ori}
  Abessive & ◊nc{-one} & ◊nc{-one} & ◊nc{-one} & ◊nc{-oneþ}
  Semblative & ◊nc{-aħet} & ◊nc{-aħot} & ◊nc{-aħest} & ◊nc{-aħet}
}

In addition, the R stem is used for both nominative and accusative rcases.

◊section[#:id "converb"]{Converbs (adverbial forms)}

Converbs are divided into two categories:

◊items{
  ◊item{◊term{Reduced coordination}: the actions referred to by the converbal clause and the main clause are separate.}
  ◊item{◊term{Emergent coordination}: the actions referred to by the converbal clause and the main clause form a larger action as a whole.}
}

In both of these categories, the converbal clause and the main clause might not match in subject, but in that case, it must be made clear that the respective subjects are different.

Often, such a mismatch occurs because a non-subject argument in the converbal clause should become the subject in the main clause. If this is not already clear, then the clitic ◊l1{=’ul} can be used on the argument in the converbal clause to mark that it is also the subject of the main clause.

Sometimes, the subject in the converbal clause should not be the subject of a main clause. If this is not otherwise clear, then the clitic ◊l1{=’els} can be used on the converb to make it so.

In addition, Ŋarâþ Crîþ has ◊em{so}-clauses, which also act adverbially.

◊subsection[#:id "reduced"]{Reduced coordination}

In reduced coordination, the converb is formed through an affix on the verb stem indicating the conjunction in question instead of the usual conjugation:

◊table/x[#:options (table-options #:caption "Affixes on the converb for reduced conjunction, where X is the statement stated by the converbal clause and Y the statement stated by the main clause.")]{
  Operation \ Infinitive ending & ◊nc{-at} & ◊nc{-it}
  X and Y & ◊nc{-ame} & ◊nc{-ime}
  X or Y (inclusive) & ◊nc{-age} & ◊nc{-ige}
  X or Y (exclusive) & ◊nc{-are} & ◊nc{-ire}
  X (succeeded) but Y & ◊nc{-ameca} & ◊nc{-imeca}
  X (was attempted) but Y & ◊nc{-omeca} & ◊nc{-emeca}
  X to some and Y to others & ◊nc{-amec} & ◊nc{-imec}
  not only X but also Y & ◊nc{-eacþ} & ◊nc{-iacþ}
  X is preferrable (e.g. more virtuous, easier) to Y & ◊nc{-emos} & ◊nc{-argis}
}

After an onset containing a voiced obstruent, the ◊l0{-age} and ◊l0{-ige} affixes become ◊l0{-ahe} and ◊l0{-ihe}, respectively. After an onset of ◊l0{m}, affixes beginning with ◊l0{-◊var{V}me-} (for any vowel ◊var{V}) have these replaced with ◊l0{-◊var{V}se-}.

The “but” suffixes are distinguished by telicity of the first action:

◊gloss/x{
  ◊glfree{◊nc{vecłimeca fenne gcreðasta.}}
  ◊gla{vecł-imeca fenr-ne g\creðas-ta.}
  ◊glb{persuade-but regret-%acc.%di (%pfv\)feel_fainty-1%sg.%pst.%pfv}
  ◊glfree{I persuaded him, but I regretted it.}
}

◊gloss/x{
  ◊glfree{◊nc{vecłemeca mešat terselta.}}
  ◊gla{vecł-emeca meš-at ters-el-ta.}
  ◊glb{persuade-but.%atelic listen-%inf refuse-3%sg.%inv-%pst}
  ◊glfree{I tried to persuade him, but he didn’t listen.}
}

However, if attempt is indicated explicitly for hte first action, the telic suffixes are favored:

◊gloss/x{
  ◊glfree{◊nc{vecłit senlameca mešat terselta.}}
  ◊gla{vecł-it sel-ameca meš-at ters-el-ta.}
  ◊glb{persuade-%inf try_to-but listen-%inf refuse-3%sg.%inv-%pst}
  ◊glfree{I tried to persuade him, but he didn’t listen.}
}

◊subsection[#:id "emergent"]{Emergent coordination}

In emergent coordination, the final ◊l0{-t} of the infinitive form of the first verb is replaced with ◊l0{-rþ} if the onset of the following word is either null or a plosive alone, or ◊l0{-þ} otherwise. (Note that the operation is necessarily “and”.)

For the verb ◊l1{minait} ◊trans{be alone}, the final vowel is changed to ◊l0{e}.

The use of emergent coordination can be seen as a serial verb construction.

◊subsection[#:id "so-clauses"]{◊i{So}-clauses}

◊term{◊i{So}-clauses} are adverbial clauses consisting of a finite verb phrase followed by a conjunction such as ◊l1{so}, called the ◊term{◊i{so}-particle}:

◊items{
  ◊item{◊l1{A so B}: ◊trans{if A, then B}, with emphatic form ◊l1{sono}. Can be combined with the clitic ◊l1{’moc} to mean ◊trans{even if} or ◊trans{even though}. Also used to express correlative comparisons when the main verbs of both A and B are differentiated with respect to the same variable.}
  ◊item{◊l1{A fose B}: ◊trans{because A, B}, with emphatic form ◊l1{foseno}. In other words, A is the cause and B is the effect.}
  ◊item{◊l1{A dôm B}: ◊trans{B to the extent that A; B so much that A}. That is, A is the result of B.}
}

The emphatic forms ◊l1{sono} and ◊l1{foseno} cannot be used in a postposed ◊i{so}-clause. Adjunct postposition cannot be applied to a ◊i{so}-clause.

◊section[#:id "nominalized"]{Nominalized forms}

◊xref/l["nominalized-verbs"]{Table} lists the nominalized forms of a verb by case and mood.

◊table/x[#:id "nominalized-verbs" #:options (table-options #:caption "Nominalized forms by case and mood.")]{
  Case \ Mood & Indicative & Subjunctive
  Nominative & ◊nc{o ◊ṡ{I}◊ṫ[0]t} & –
  Accusative & ◊nc{on ◊ṡ{I}◊ṫ[0]t} & –
  Dative & ◊nc{○◊ṡ{I}ilt} & ◊nc{○◊ṡ{I}i◊ṫg{I}os}
  Genitive & – & ◊nc{en ◊ṡ{I}◊ṫ[0]t}
  Locative & ◊nc{●◊ṡ{L}eve} & ◊nc{a ●◊ṡ{I}◊ṫ[0]t}
  Instrumental & ◊nc{●◊ṡ{L}eveca} & ◊nc{ac ●◊ṡ{I}◊ṫ[0]t}
  Abessive & ◊nc{●◊ṡ{L}eveþa} & ◊nc{aþ ●◊ṡ{I}◊ṫ[0]t}
  Semblative & ◊nc{it ◊ṡ{I}◊ṫ[0]t} & ◊nc{eti ◊ṡ{I}◊ṫ[0]t}
}

In the nominative case, the particle ◊l1{o} is optional but may be retained for clarity.

The indicative mood is used for realis actions, as well as general activities:

◊gloss/x{
  ◊glfree{◊nc{šinaf d·endilt idesa neman racru.}}
  ◊gla{šin-af d·end-ilt i-desa nem-an racr-u.}
  ◊glb{all-%nom.%gc hunt-%inf.%dat %adn-below some-%acc.%sg know-3%gc}
  ◊glfree{Everybody knows (something) about hunting.}
}

In contrast, the subjunctive mood indicates irrealis actions.

Some of the nominalized forms are made of a particle plus the infinitive form, possibly mutated.

◊gloss/x{
  ◊glfree{◊nc{on mêgennančat triłepjô.}}
  ◊gla{on mê<genna>nč-at trił-e-pjô.}
  ◊glb{%inf.%acc eat<%ddt.%rec>-%inf recommend-1%sg-1%du.%incl}
  ◊glfree{I suggest to us to eat more slowly.}
}

◊gloss/x{
  ◊glfree{◊nc{eleþ šileifos mîrm·oru.}}
  ◊gla{el-eþ šile-ifos mîrm·or-u.}
  ◊glb{sun-%nom.%sg shine-%inf.%dat.%subj predict-3%gc}
  ◊glfree{It is predicted that the sun will shine.}
}

◊gloss/x{
  ◊glfree{◊nc{cþereve elnat łanes.}}
  ◊gla{cþer-eve eln-at łan-es.}
  ◊glb{write-%inf.%loc neat-%inf must-2%sg}
  ◊glfree{When you write, you must do it neatly.}
}

In such a nominalized form of an auxiliary verb, the particle also precedes all other verbs in the auxiliary chain. In such a nominalized VP of a non-auxiliary verb, the last word of a noun phrase attached to it may be moved between the particle and the infinitive. In this case, the moved word takes the same mutation that the infinitive would.

When a nominalized VP in the locative, instrumental, or abessive case is an object of a modifying relational, the nominative forms are used.

A pronominal subject of a nominalized VP is marked using a possessive clitic on the infinitive:

◊gloss/x{
  ◊glfree{◊nc{trecai v·alilt’pe nîs faras miłersos nostecþis.}}
  ◊gla{trec-ai v·al-ilt=’pe nîs far-as miłers-os nos-t-ecþis.}
  ◊glb{field-%loc.%pl reside-%inf.%dat=%poss.1 through often-%loc.%di pasture-%loc.%pl walk-%past-1%pl.%excl.%inv}
  ◊glfree{During our residence in the country we often walked in the pastures.}
}

◊gloss/x{
  ◊glfree{◊nc{anoþ varmjoneve’pe intaras môvarþ sarħi menaþ.}}
  ◊gla{an-oþ varmjon-eve=’pe intar-as môv-arþ sarħ-i men-a-þ.}
  ◊glb{sky-%dat.%sg observe-%inf.%loc=%poss.1 void-%loc.%sg float-%rel.%acc,%dat.%cel thing-%dat.%sg see-1%sg-%past}
  ◊glfree{When I looked into the sky, I saw something floating in the air.}
}

A non-pronominal subject of a nominalized VP is usually in the nominative case. Sometimes, however the subject is indicated as the possessor in a double-marked possessive construction in order to put it as the topic or focus in the outer clause:

◊gloss/x{
  ◊glfree{◊nc{narvit on pentat’or racre.}}
  ◊gla{narv-it on pent-at=’or racr-e.}
  ◊glb{sing-%inf %inf.%acc know-%inf=%poss.3.%hu know-1%sg}
  ◊glfree{I know that she can sing.}
}

◊gloss/x{
  ◊glfree{◊nc{cemen’or’eþ narvit on pentat’or racre.}}
  ◊gla{cem-en=’or=’eþ narv-it on pent-at=’or racr-e.}
  ◊glb{self-%acc.%sg=%poss.3.%hu=%possm sing-%inf %inf.%acc know-%inf=%poss.3.%hu know-1%sg}
  ◊glfree{She, I know can sing.}
}

◊gloss/x{
  ◊glfree{◊nc{narvit on pentat’or cemen’or’eþ racre.}}
  ◊gla{narv-it on pent-at=’or cem-en=’or=’eþ racr-e.}
  ◊glb{sing-%inf %inf.%acc know-%inf=%poss.3.%hu self-%acc.%sg=%poss.3.%hu=%possm know-1%sg}
  ◊glfree{I know that ◊strong{she} can sing.}
}

Like participles, nominalized verbs can take object affixes before the verb stem. The vitreous set is used regardless of material. Mutations apply to the start of the word, not to the start of the stem: ◊l1{p·emečilt}, not *◊l1{pem·ečilt}. As usual, an object prefix that would be eclipsed is lenited instead: ◊l1{p·emičeve}, not *◊l1{vpemičeve}. Object affixes occasionally stand in for nominative arguments; in this case, the singular set is always used.

The genitive-case nominalized form is sometimes used to form another type of relative clause, especially when the common argument in the embedded clause is an oblique argument. This use is chiefly informal, with the participle form (perhaps in an applicative voice) being preferred in formal language.

The instrumental indicative nominalized form is often used as a resultative, especially as an adjunct to the verb ◊l1{eþit}:

◊gloss/x{
  ◊glfree{◊nc{šinen m·olon mêntreveca sâna elto gasnelsal.}}
  ◊gla{šin-en m·ol-on mêntr-eveca sân-a elt-o g\asnels-al.}
  ◊glb{all-%gen.%sg nut-%acc.%co eat-%inf.%inst bear-%nom.%sg river-%dat.%sg %pfv\return-3%sg.%inv}
  ◊glfree{Having eaten all of the nuts, the bear returns to the river.}
}

◊gloss/x{
  ◊glfree{◊nc{niðes vlomeveca eþit lłanel viþca.}}
  ◊gla{nið-es vlom-eveca eþ-it l\łan-el viþca.}
  ◊glb{early-%loc.%di revise-%inf.%inst exist-%inf %pfv\must-3%sg.%inv %cond}
  ◊glfree{It would have had to have been changed early.}
}

The abessive case, in contrast, is used to indicate the lack of a certain concurrent action:

◊gloss/x{
  ◊glfree{◊nc{le neffełen voðeveþa cehas.}}
  ◊gla{le neff-ełen voð-eveþa ceh-as.}
  ◊glb{%imp sheep-%acc.%gc buy-%inf.%abess come-2%sg}
  ◊glfree{Come here without buying any sheep.}
}

◊section[#:id "irregular"]{Irregular verbs}

The conjugation of most verbs can be derived from at most seven principal parts. Nevertheless, a few verbs are truly irregular.

◊subsection[#:id "irregular-apn"]{APN-irregular verbs}

As the name suggests, ◊term{APN-irregular verbs} specify finite forms according to aspect (relative to tense), person, and number. All APN-irregular verbs are vitreous; object suffixes apply as usual, while the past suffix is always ◊l0{-þ} or ◊l0{-ta} – that is, ◊nc{◊ṡ{P}◊ẋ{t}} is never used.

◊subsubsection[#:id "eþit"]{◊nc{eþit, ve, velta, verła, verłena, vepraþos, gispeve, …}}

◊table/x[#:options (table-options #:caption ◊@{Conjugation of ◊nc{eþit, ve, velta, verła, verłena, vepraþos, gispeve, …}, listing the irregular finite forms.} #:placement 'forcehere)]{
  Person \ Number & Singular & Dual & Plural & Generic
  ◊pseudoheader{Direct aspect}
  1st excl. & ◊nc{ve} & ◊nc{ven} & ◊nc{vjaþ} & ◊nc{vef}
  1st incl. & & ◊nc{veac} & ◊nc{vea} &
  2nd & ◊nc{ves} & ◊nc{vesen} & ◊nc{vełar} & ◊nc{vełaf}
  3rd & ◊nc{veła} & ◊nc{vełan} & ◊nc{von} & ◊nc{ver}
  ◊pseudoheader{Inverse aspect}
  1st excl. & ◊nc{vel} & ◊nc{vins} & ◊nc{vjaþis} & ◊nc{vesif}
  1st incl. & & ◊nc{veacel} & ◊nc{varel} &
  2nd & ◊nc{verþ} & ◊nc{vinse} & ◊nc{veris} & ◊nc{vełesaf}
  3rd & ◊nc{vełal} & ◊nc{vełans} & ◊nc{veron} & ◊nc{vros}
}

◊subsubsection[#:id "eseþit"]{◊nc{eseþit, efa, efaþ, esperła, esperłena, espeþraþos, gedispeve, …}}

The forms of this verb, which is the inchoative form of ◊l1{eþit}, are independent of aspect.

◊table/x[#:options (table-options #:caption ◊@{Conjugation of ◊nc{eseþit, efa, efaþ, esperła, esperłena, espeþraþos, gedispeve, …}, listing the irregular finite forms.} #:placement 'forcehere)]{
  Person \ Number & Singular & Dual & Plural & Generic
  1st excl. & ◊nc{efa} & ◊nc{eson} & ◊nc{esoþ} & ◊nc{esef}
  1st incl. & & ◊nc{iste} & ◊nc{ise} &
  2nd & ◊nc{sos} & ◊nc{essen} & ◊nc{esołar} & ◊nc{esaf}
  3rd & ◊nc{esoła} & ◊nc{esołan} & ◊nc{espo} & ◊nc{eris}
}

◊subsubsection[#:id "telit"]{◊nc{telit, ce, celta, cerła, cerłena, cirłaþos, dtołeve, …}}

◊table/x[#:options (table-options #:caption ◊@{Conjugation of ◊nc{telit, ce, celta, cerła, cerłena, cirłaþos, dtołeve, …}, listing the irregular finite forms.} #:placement 'forcehere)]{
  Person \ Number & Singular & Dual & Plural & Generic
  ◊pseudoheader{Direct aspect}
  1st excl. & ◊nc{ce} & ◊nc{cen} & ◊nc{čaþ} & ◊nc{cef}
  1st incl. & & ◊nc{cjor} & ◊nc{cea} &
  2nd & ◊nc{ces} & ◊nc{cesen} & ◊nc{cełar} & ◊nc{cełaf}
  3rd & ◊nc{ceła} & ◊nc{cełan} & ◊nc{cþon} & ◊nc{cir}
  ◊pseudoheader{Inverse aspect}
  1st excl. & ◊nc{cel} & ◊nc{cins} & ◊nc{čaþis} & ◊nc{cesif}
  1st incl. & & ◊nc{tacel} & ◊nc{cjarel} &
  2nd & ◊nc{cerþ} & ◊nc{cinse} & ◊nc{ceris} & ◊nc{cełesaf}
  3rd & ◊nc{cełal} & ◊nc{cełans} & ◊nc{ceþen} & ◊nc{cþaros}
}

◊subsubsection[#:id "varit"]{◊nc{varit, tan, oveþ, saltan, salteši, sortaþon, voreve, …}}

◊table/x[#:options (table-options #:caption ◊@{Conjugation of ◊nc{varit, tan, oveþ, saltan, salteši, sortaþon, voreve, …}, listing the irregular finite forms.} #:placement 'forcehere)]{
  Person \ Number & Singular & Dual & Plural & Generic
  ◊pseudoheader{Direct aspect}
  1st excl. & ◊nc{tan} & ◊nc{tar} & ◊nc{tare} & ◊nc{tanfe}
  1st incl. & & ◊nc{târ} & ◊nc{târe} &
  2nd & ◊nc{tas} & ◊nc{tasen} & ◊nc{tai} & ◊nc{tasfe}
  3rd & ◊nc{teha} & ◊nc{tehan} & ◊nc{tejen} & ◊nc{toru}
  ◊pseudoheader{Inverse aspect}
  1st excl. & ◊nc{ove} & ◊nc{ovins} & ◊nc{ovicþ} & ◊nc{oveł}
  1st incl. & & ◊nc{orins} & ◊nc{oricþ} &
  2nd & ◊nc{opres} & ◊nc{ovesis} & ◊nc{overis} & ◊nc{overeł}
  3rd & ◊nc{ovel} & ◊nc{ovens} & ◊nc{ovelt} & ◊nc{voris}
}

◊subsubsection[#:id "nepit"]{◊nc{nepit, nea, nepelta, nelpa, nelpeta, nolpaþos, nolpeve, …}}

◊table/x[#:options (table-options #:caption ◊@{Conjugation of ◊nc{nepit, nea, nepelta, nelpa, nelpeta, nolpaþos, nolpeve, …}, listing the irregular finite forms.} #:placement 'forcehere)]{
  Person \ Number & Singular & Dual & Plural & Generic
  ◊pseudoheader{Direct aspect}
  1st excl. & ◊nc{nea} & ◊nc{nean} & ◊nc{nepacþ} & ◊nc{nêf}
  1st incl. & & ◊nc{nefên} & ◊nc{nepâcþ} &
  2nd & ◊nc{nepas} & ◊nc{nepans} & ◊nc{nepar} & ◊nc{nepał}
  3rd & ◊nc{nae} & ◊nc{nepan} & ◊nc{nepo} & ◊nc{nepu}
  ◊pseudoheader{Inverse aspect}
  1st excl. & ◊nc{nepel} & ◊nc{nenfe} & ◊nc{nencþis} & ◊nc{nêf}
  1st incl. & & ◊nc{nenfôr} & ◊nc{nencþîs} &
  2nd & ◊nc{nefras} & ◊nc{nefrast} & ◊nc{nefris} & ◊nc{nepał}
  3rd & ◊nc{naeł} & ◊nc{naens} & ◊nc{nares} & ◊nc{nepos}
}

◊section[#:id "intpc"]{Interactions with predicate modifiers}

◊subsection[#:id "comparatives"]{Comparatives}

Given a verb, the derived verb prefixed with ◊l0{mir◊|sei2|-} (◊l0{ła◊|sei2|-}) means ‘◊S performs the action in question to a greater (lesser) degree than ◊O by a margin of ◊|I|’:

◊gloss/x{
  ◊glfree{◊nc{mirvesralo.}}
  ◊gla{mir-vesr-a-lo.}
  ◊glb{%cmp-strong-1%sg-3%pl}
  ◊glfree{I am stronger than they are.}
}

If the base action mentions a direct or indirect object, then the argument is retained in the clause eclipsed:

◊gloss/x{
  ◊glfree{◊nc{#môra #saþon giviner âreþþas mirm·ečaþ.}}
  ◊gla{#môr-a #saþ-on g\ivin-er â-reþþas mir-m·eč-a-þ.}
  ◊glb{%name-%nom.%sg %name-%acc.%sg %cdarg\mushroom-%acc.%pl %nnom-four.%dat.%cel %cmp.%gt-find-3%sg-%past}
  ◊glfree{#môra found four more mushrooms than #saþo.}
}

◊; #môra caught more salmon than cod ⇒ ???

The verbs ◊l1{mirit} ◊trans{greater than} and ◊l1{łavrit} ◊trans{less than} also exist.

Equal comparisons are formed by using the relational ◊l1{peŋan} whose object is the noun phrase to which another, of the same case, is being compared:

◊gloss/x{
  ◊glfree{◊nc{#môra #saþo peŋan vadanor mečaþ.}}
  ◊gla{#môr-a #saþ-o peŋan vad-anor meč-a-þ.}
  ◊glb{%name-%nom.%sg %name-%nom.%sg %cmp.%eq tuber-%acc.%pl find-3%sg-%past}
  ◊glfree{#môra found as many tubers as #saþo.}
}

Superlatives are formed using the relational ◊l1{îþ}, which takes a genitive object:

◊gloss/x{
  ◊glfree{◊nc{#môra rascen îþ raga.}}
  ◊gla{#môr-a rasc-en îþ rag-a.}
  ◊glb{%name-%nom.%sg family-%gen.%sg %sup tall-3%sg}
  ◊glfree{#môra is the tallest in their family.}
}

◊subsection[#:id "voice"]{Voice}

The default voice is the active voice. There is no passive voice in Ŋarâþ Crîþ because the subject may be omitted instead:

◊gloss/x{
  ◊glfree{◊nc{+tjare siljes šeproþ.}}
  ◊gla{+tjar-e silj-es šepr-oþ.}
  ◊glb{(name)-%acc.%sg afternoon-%loc.%sg execute-3%gc.%past.%pfv}
  ◊glfree{+tjaris was executed in the afternoon.}
}

◊gloss/x{
  ◊glfree{◊nc{rešitas tfoson elens’po fêtecto gcemuþ.}}
  ◊gla{rešit-as tfos-on el-ens=’po fê-tecto g\cem-u-þ.}
  ◊glb{cloak-%dat.%sg osP-%acc.%sg year-%acc.%sg=%sep 3%gc-before %pfv\give-3%sg-%past}
  ◊glfree{The cloak was given to my (mother/father) a year ago.}
}

◊subsubsection[#:id "causative-voice"]{The causative voice}

The causative voice is formed by prefixing ◊l0{do-}. It adds a ‘cause’ argument to the verb as the subject, changing its valency.

◊items{
  ◊item{In an intransitive or semitransitive verb, the old subject becomes the direct object, making it transitive or ditransitive.}
  ◊item{In a transitive verb, the old subject becomes the direct object, and the old object becomes the indirect object, making it ditransitive.}
  ◊item{In a ditransitive verb, the old subject becomes the direct object, the old object becomes the indirect object, and the old indirect object becomes the object of the relational ◊l1{ro}, making it a ditransitive verb with one additional oblique argument.}
  ◊item{Auxiliary verbs simply delegate to their targets.}
}

The causative prefix can function either as an inflectional affix or a derivational one:

◊gloss/x{
  ◊glfree{◊nc{ša entas gðenic’ve ndoelpanveþ?}}
  ◊gla{ša ent-as gðen-ic=’ve n\do-elp-an-ve-þ?}
  ◊glb{%int that_thing.%cel-%dat.%sg parent-%nom.%du=%poss.2 %pfv\%caus-wear-3%du-2%sg-%past}
  ◊glfree{Did your parents make you wear that?}
}

◊gloss/x{
  ◊glfree{◊nc{le jonas tê vrêman docjašas.}}
  ◊gla{le jon-as tê vrêm-an do-cjaš-as.}
  ◊glb{%imp now-%loc.%di that.%cel book-%acc.%sg %caus-fall-2%sg}
  ◊glfree{Drop that book right now.}
}

When two consecutive causative prefixes are added to a verb, then they take the form ◊l0{reld·o-}:

◊gloss/x{
  ◊glfree{◊nc{efreþ têrmasor reld·odranlêta.}}
  ◊gla{efr-eþ têrm-asor reld·o-dranl-ta.}
  ◊glb{commander-%nom.%sg hostage-%dat.%pl %caus.%caus-die-3%sg.%past.%pfv}
  ◊glfree{The commander ordered the hostages to be killed.}
}

Note that ◊l1{têrmon} is in the dative case. If it were in the accusative case instead, then the meaning would be different:

◊gloss/x{
  ◊glfree{◊nc{efreþ têrmanor reld·odranlêta.}}
  ◊gla{efr-eþ têrm-anor reld·o-dranl-ta.}
  ◊glb{commander-%nom.%sg hostage-%acc.%pl %caus.%caus-die-3%sg.%past.%pfv}
  ◊glfree{The commander ordered the hostages to kill.}
}

In addition, the causative prefix and a terminative prefix ◊l0{er◊|sei|-} immediately following it fuse into ◊l0{dar◊|sei|-}:

◊gloss/x{
  ◊glfree{◊nc{mêva lê gedesos niðes ndarc·atlepeþ.}}
  ◊gla{mêv-a lê g\edes-os nið-es n\dar-c·atl-e-pe-þ.}
  ◊glb{rain-%nom.%co this.%cel assignment-%dat.%sg early_time-%loc.%di %caus.%pfv\%term-complete-3%sg-1%sg-%past}
  ◊glfree{I finished the assignment early because of the rain.}
}

◊subsubsection[#:id "applicative-voice"]{The applicative voices}

Ŋarâþ Crîþ has various applicative voices, which promote an oblique adjunct (either a noun phrase in a non-core case or an adverbial relational phrase) to the dative case. The former dative argument, if present, is demoted to where the oblique was.

◊table/x[#:options (table-options #:caption "Applicative prefixes in Ŋarâþ Crîþ")]{
  Case or relation & Prefix
  Locative & ◊nc{si-}
  Instrumental & ◊nc{ca-}
  Abessive & ◊nc{þa-}
  Semblative & ◊nc{ħit-}
  ◊nc{ar} & ◊nc{la-}
  ◊nc{jas} & ◊nc{sa-}
  ◊nc{nîs} & ◊nc{na-}
  ◊nc{uc} & ◊nc{ħic-}
  ◊nc{roc}, ◊nc{rille} & ◊nc{tar-}
}

The applicative voices are primarily used to relativize an oblique argument by promoting it into the dative case.

◊gloss/x{
  ◊glfree{◊nc{melco pecacatleþanon om inorelt clačirþ osjas &taran rema þal!}}
  ◊gla{melc-o pe-ca-catl-eþanon om inor-elt clač-irþ os-jas &tar-an rem-a þal!}
  ◊glb{ssP-%nom.%sg 1%sg-%appl.%inst-hit-%rel.%dat,%acc.%ter that.%ter stick-%acc.%sv break-%ser 3%sg.%ter-from woodchip-%acc.%co make-1%sg %assert}
  ◊glfree{I’ll break that stick my (dad/mom) hit me with and turn it into woodchips!}
}

◊gloss/x{
  ◊glfree{◊nc{#mjôras sieristeþo #flirora cadils nelsit ħarte.}}
  ◊gla{#mjôr-as si-erist-eþo #fliror-a cad-ils nels-it ħar-te.}
  ◊glb{(name)-%loc.%sg %appl.%loc-meet-%rel.%dat,%dat.%cel (name)-%nom.%sg island-%dat.%sg go-%inf do_again-3%sg.%past.%pfv}
  ◊glfree{#flirora went once more to the island where they met #môra.}
}

They are also used to make an oblique argument a shifted subject of an auxiliary verb or relational:

◊gloss/x[#:id "she-couldnt-stop-them"]{
  ◊glfree{◊nc{artfoteþa þanelsit pečit rjotilesta.}}
  ◊gla{artfoþ-eþa þa-nels-it peč-it rjot-i-les-ta.}
  ◊glb{city-%abess.%sg %appl.%abess-go-%inf avoid-%inf fail-3%pl-3%sg.%hum-%past}
  ◊glfree{She couldn’t stop them from going to the city without her.}
}

◊gloss/x[#:id "near-the-mouth"]{
  ◊glfree{◊nc{eltin šimerin cþîšari fjones alals âŋa cþîfsocon siveła.}}
  ◊gla{elt-in šim-erin cþîš-ecþ fjon-es al-als âŋa cþîfsoc-on si-veła.}
  ◊glb{river-%gen.%sg end-%acc.%sg near-%rel.%nom,%loc.%sg place-%loc.%sg east-%dat.%di bending acute_angle-%nom.%sg %appl.%loc-exist.3%sg}
  ◊glfree{Near the mouth of the river, its course turns sharply towards the East.}
}

In ◊xref/p{she-couldnt-stop-them} and ◊xref/p{near-the-mouth}, the dative argument is understood to be the shifted subject by the use of the applicative.

◊subsection[#:id "aux-tour"]{Auxiliary verbs}

The verb modified by an auxiliary verb is called its ◊term{target}, which appears in the infinitive form immediately before the auxiliary verb. The particle ◊l1{fel} can stand in place of a target if one is not specified. In addition, the target of an auxiliary verb can be an auxiliary verb itself, with its own target ◊i{ad infinitum}.

A clause using an auxiliary verb may select a ◊term{shifted subject} (◊|Š|) among the nominative, accusative, and dative arguments. The shifted subject may have a special role in the meaning of an auxiliary verb, such as carrying the volition for the performance or nonperformance of the target action.

An auxiliary is ◊term{positive definite} (◊term{negative definite}) if the truth of the action or state described implies that the action or state described by the target is true (false). It is ◊term{indefinite} if neither such implication holds.

◊subsubsection[#:id "aux-tour-modal"]{Modal auxiliaries}

◊term{Modal auxiliaries} can be classified by their ◊term{force} and ◊term{flavor}. Ŋarâþ Crîþ distinguishes the following flavors:

◊items{
  ◊item{◊term{Situational}: according to the situation at hand}
  ◊item{◊term{Dynamic}: according to the abilities of ◊Š}
  ◊item{◊term{Facultative}: according to the knowledge of ◊Š to perform ◊|T|}
  ◊item{◊term{Deontic}: according to some set of norms}
  ◊item{◊term{Conscientious}: according to the conscientious beliefs of ◊Š (◊i{i.e.} whether ◊Š has a conscientious objection against performing or not performing a certain action)}
  ◊item{◊term{Epistemic}: according to beliefs inferred through other information}
  ◊item{◊term{Akindynic}: according to concerns for the safety of ◊Š}
}

The modal auxiliaries of Ŋarâþ Crîþ according to this classification is shown in ◊xref/l["modal-auxiliaries"]{Table}.

◊table/x[#:options (table-options #:caption "Modal auxiliaries in Ŋarâþ Crîþ." #:pdf-backend 'tabularray) #:id "modal-auxiliaries"]{
  Flavor \ Force & Necessity & Non-necessity & Possibility & Impossibility & Probability
  Situational & ◊nc{marðat} & & ◊nc{pentat} & ◊nc{rjotat}
  Dynamic & & & ◊nc{pentat} & ◊nc{geðat}
  Facultative & & & ◊nc{racfit} & ◊nc{cricþit}
  Deontic & ◊nc{łanat} & ◊nc{rečit} & ◊nc{sarait}
  Conscientious & & & ◊nc{neħrit} & ◊nc{velrjotat}
  Epistemic & ◊nc{vrasit} ◊br ◊nc{cenmirat} & & ◊nc{þarat} & & ◊nc{lerjat}
  Akindynic & & & ◊nc{esenvit} & ◊nc{toprat}
}

There are a handful of modal auxiliary verbs that do not fit neatly into the grid above:

◊items{
  ◊item{◊l1{gevat}: used in questions to make a request}
  ◊item{◊l1{rendat}: desiderative = ◊trans{◊Š desires that ◊T happens.}}
  ◊item{◊l1{ehiltat}: ◊trans{◊Š feels a desire to ◊T on a physiological level}}
  ◊item{◊l1{fonat}: negation of ◊l1{rendat} = ◊trans{◊Š has no desire for ◊|T| to happen}}
  ◊item{◊l1{verat}: volitive = ◊trans{◊Š intends to ◊|T|}}
  ◊item{◊l1{senlat} = ◊trans{◊Š attempts to ◊|T|}; also ◊trans{◊Š intends to ◊|T|}}
}

◊subsubsection[#:id "aux-tour-degree"]{Degree auxiliaries}

◊term{Degree auxiliaries} show the extent to which the target action or state holds:

◊items{
  ◊item{◊l1{šonat} = ◊trans{◊T is done completely}}
  ◊item{◊l1{iširit} = ◊trans{◊T is true at least slightly}}
  ◊item{◊l1{vasrit} = ◊trans{◊Š does ◊|T| with the desired effect}}
}

A subset of degree auxiliaries are ◊term{negative auxiliaries}, which are used to negate the target in some way. Negative auxiliaries are necessarily negative definite, while other degree auxiliaries are positive definite.

◊items{
  ◊item{◊l1{pečit} = ◊trans{◊|Š| avoids inadvertently doing ◊|T|} (◊i{i.e.} the volition belongs to ◊|Š|)}
  ◊item{◊l1{tersat} = ◊trans{◊|Š| insists against doing ◊|T|}}
  ◊item{◊l1{garit} = ◊trans{◊Š stops oneself from doing ◊|T|}}
  ◊item{◊l1{vandrit} = ◊trans{◊Š leaves something uninvolved in an action ◊|T|}}
  ◊item{◊l1{selcit} = ◊trans{◊Š knowingly fails to fulfill their responsibility or obligation to do ◊|T|}}
  ◊item{◊l1{anðat} = ◊trans{◊T has not yet occurred}}
  ◊item{◊l1{iselgit} = ◊trans{◊Š fails to do ◊|T| due to forgetfulness}}
}

◊subsubsection[#:id "aux-tour-aspect"]{Aspectual auxiliaries}

Other auxiliary verbs indicate aspect. Most such auxiliaries are positive definite.

◊items{
  ◊item{◊l1{ħarat} = ◊trans{◊T is done again; ◊T is done to a further degree than previously mentioned}}
  ◊item{◊l1{nevlat} = ◊trans{◊Š does ◊T repeatedly}}
  ◊item{◊l1{anłit} = ◊trans{◊Š does ◊T in response to an earlier action against them}}
  ◊item{◊l1{nespat} = ◊trans{◊Š does ◊T habitually}}
  ◊item{◊l1{cfortlat} = ◊trans{◊Š begins to ◊T}}
  ◊item{◊l1{denecit} = ◊trans{◊Š continues to ◊T}}
  ◊item{◊l1{vespat} = ◊trans{◊Š does ◊T for the first time in a long time}}
  ◊item{◊l1{cagesit} = ◊trans{◊Š does ◊T for the first time in such a long time that the speaker does not remember when it last happened}}
  ◊item{◊l1{ennelit} = ◊trans{◊Š has done ◊T as a result of actions over a long time}}
}

◊subsubsection[#:id "aux-tour-others"]{Others}

◊items{
  ◊item{◊l1{vjełat} = ◊trans{◊|Š| has the character of doing ◊|T|}}
  ◊item{◊l1{cefsit} = ◊trans{◊|Š| has difficulty doing ◊|T|}}
  ◊item{◊l1{sivrit} = ◊trans{◊|Š| is afraid of doing ◊|T|}}
  ◊item{◊l1{adrait} = ◊trans{◊|Š| is unafraid of doing ◊|T|}}
  ◊item{◊l1{glanpit} = ◊trans{◊T occurs at a monetary cost to ◊|Š|}}
  ◊item{◊l1{vratrat} = ◊trans{◊Š does not eat until they have completed ◊T}}
  ◊item{◊l1{tfahivit} = ◊trans{◊|Š| does ◊|T| in an extremely cruel or wicked manner}}
  ◊item{◊l1{gerat} = ◊trans{truly}}
}

◊subsubsection[#:id "aux-shifted"]{Explicitly specifying the shifted subject}

Although the shifted subject is usually clear from the context, it is sometimes necessary to specify explicitly which argument it is. The ◊term{rotarg particles} can be used immediately before the target of an auxiliary verb in order to assign each of ◊|S|, ◊|O|, and ◊|I| to the nominative, accusative, and dative cases, specifying the nominative-case argument to be ◊|Š|.

◊table/x[#:id "rotarg" #:options (table-options #:caption "The rotarg particles." #:first-col-header? #f)]{
  Form & Gloss & Nominative (= ◊|Š|) & Accusative & Dative
  ◊nc{is} & ◊sc{rot0} & ◊|S| & ◊|O| & ◊|I|
  ◊nc{pa◊sei} & ◊sc{rot1} & ◊|O| & ◊|I| & ◊|S|
  ◊nc{jâ◊uru} & ◊sc{rot2} & ◊|I| & ◊|S| & ◊|O|
}

For instance, ◊l1{cengrit} is glossed as ◊trans{◊S throws ◊O at ◊|I|} – that is, ◊|S| refers to the thrower, ◊|O| refers to the object being thrown, and ◊|I| refers to the target toward which it is thrown. In ◊l1{is cengrit}, the thrower is specified in the nominative case, the object thrown is in the accusative, and the target in the dative; thus, it can be glossed as ◊trans{◊S throws ◊O at ◊|I|} with the constraint that ◊S will be the same as the ◊|Š| for an auxiliary verb.

In ◊l1{pa c·engrit}, the thrower is specified in the dative case, the object thrown is in the nominative, and the target is in the accusative. This phrase can be glossed as ◊trans{◊I throws ◊S at ◊O} and would be used as the target of an auxiliary verb if ◊|Š| should refer to the object being thrown.

Likewise, in ◊l1{jâ gcengrit}, the thrower is specified in the accusative case, the object thrown is in the dative, and the target is in the nominative. This phrase can be glossed as ◊trans{◊O throws ◊I at ◊S} and would be used as the target of an auxiliary verb if ◊|Š| should refer to the target at which something is thrown.

◊section[#:id "modality"]{Modality}

Most verb forms can be used in both realis and irrealis contexts:

◊gloss/x{
  ◊glfree{◊nc{eši mêvan têmit ħar.}}
  ◊gla{eši mêv-an têm-it ħar-∅.}
  ◊glb{here.%loc.%di rain-%acc.%co precipitate-%inf do_again-3%sg}
  ◊glfree{It is raining here again.}
}

◊gloss/x{
  ◊glfree{◊nc{enven sodas mêvan têma so mîþanelên.}}
  ◊gla{env-en sod-as mêv-an têm-a so mîþanel-ên.}
  ◊glb{day-%gen.%sg next-%loc.%sg rain-%acc.%co precipitate-3%sg if play_mîþanela-1%du.%incl}
  ◊glfree{If it rains tomorrow, we’ll play ◊i{mîþanela}.}
}

◊gloss/x{
  ◊glfree{◊nc{vesro avona celcorin ndofonraþ.}}
  ◊gla{vesr-o avon-a celc-orin n\do-fonr-a-þ.}
  ◊glb{strong-%rel.%nom,%nom.%sg wind-%nom.%di building-%acc.%sg %pfv\%caus-collapse-3%sg-%past}
  ◊glfree{The strong wind caused the building to collapse.}
}

◊gloss/x{
  ◊glfree{◊nc{ŋepriłir vesrelta so fonrit vpečaþ viþca.}}
  ◊gla{ŋeprił-ir vesr-el-ta so fonr-it v\peč-a-þ viþca.}
  ◊glb{support-%nom.%di strong-3%sg.%inv-%past if collapse-%inf %pfv\avoid-3%sg-%past %cond}
  ◊glfree{If the supports were stronger, then it would not have collapsed.}
}

The exception to this property are the dative- and semblative-case ◊xref["verbs.html" "nominalized" "Section"]{nominalized forms}, which have separate indicative and subjunctive forms.

The following sections cover modalities that are not covered by ◊xref["verbs.html" "aux-tour" "Subsection"]{auxiliary verbs}.

◊subsection[#:id "modality-imperative"]{Imperative}

Imperative sentences are formed using the headp ◊l1{le}. The verb in the main clause is conjugated as usual, except that the aspect is always imperfective. In addition, resinous verbs have special forms for second-person singular subjects.

◊gloss/x{
  ◊glfree{◊nc{le enven saden cintos reftos etor cehas.}}
  ◊gla{le env-en sad-en cint-os reft-os etor ceh-as.}
  ◊glb{%imp day-%gen.%sg next-%gen.%sg morning-%loc.%sg library-%dat.%sg in_front_of arrive-2%sg}
  ◊glfree{Arrive in front of the library tomorrow morning.}
}

◊gloss/x{
  ◊glfree{◊nc{le cupas jorniłtau.}}
  ◊gla{le cup-as jornił-þau.}
  ◊glb{%imp immediately-%loc.%di stand-%imp.2%sg}
  ◊glfree{Stand up immediately.}
}

When used with a first-person verb, ◊l1{le} marks the hortative. In this case, the subject ending of the main verb is always put in the first-person exclusive, regardless of the actual clusivity of the subject, unless the verb is followed by a tailp.

◊gloss/x{
  ◊glfree{◊nc{le nêrgim·oþena dofenran.}}
  ◊gla{le nêrgim·oþ-ena do-fenr-an.}
  ◊glb{%imp living_room-%acc.%sg %caus-clean-1%du.%excl}
  ◊glfree{Let’s clean the living room.}
}

◊gloss/x{
  ◊glfree{◊nc{le nelsâcþ þal!}}
  ◊gla{le nels-âcþ þal!}
  ◊glb{%imp go-1%pl.%incl %assert}
  ◊glfree{Let’s go!}
}

To command the listener to take a non-subject role of a verb, the verb ◊l1{menat} ◊trans{◊S sees, makes sure of ◊I} is used.

◊gloss/x{
  ◊glfree{◊nc{le felja reflit p·ečiłos menes.}}
  ◊gla{le felj-a refl-it p·eč-iłos men-es.}
  ◊glb{%imp this_idea-%nom.%sg surprise-%inf avoid-%inf.%dat see-2%sg}
  ◊glfree{Don’t be surprised by this idea.}
}

◊subsection[#:id "modality-conditional"]{Conditional}

Conditional sentences are formed using the tailp ◊l1{viþca}:

◊gloss/x{
  ◊glfree{◊nc{šinen ðês lârne cermjôri visêrče viþca.}}
  ◊gla{šin-en ðês lâr-ne cermjôr-i visêrč-e viþca.}
  ◊glb{all-%gen.%sg occurrence-%loc.%sg cat-%acc.%sg dog-%dat.%sg choose-1%sg %cond}
  ◊glfree{I would always choose a dog over a cat.}
}

