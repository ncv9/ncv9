#lang pollen

◊(require pollen/core pollen/setup)

◊define-meta[title]{Morphology}
◊define-meta[chapter-type part]

This part first describes the morphological framework and notation for describing Ŋarâþ Crîþ. It then describes each part of speech.

◊(when/splice (equal? (current-poly-target) 'html)
  (embed-pagetree "index.ptree"))
