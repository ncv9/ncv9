#lang pollen

◊define-meta[title]{Layers 1 and 2w: Cenvos and its romanization}

Rather than starting at layer 0, we start at layers 1 and 2w.

◊term{Cenvos}, the native script of Ŋarâþ Crîþ, is written from right to left. This script can be analyzed on two levels: ◊term{graphemes}, which constitute the abstract level and ◊term{glyphs}, which are the characters being written. For instance, Cenvos has one grapheme romanized as ◊l1{c} that corresponds to two different glyphs: the non-final form ◊cenvos{c~} (denoted as ◊l2w{c}) and the final form ◊cenvos{c} (◊l2w{◊lig{c$}}). As another example, the sequence ◊cenvos{me} (◊l1{me} = ◊l2w{◊lig{me}}) consists of one glyph but two graphemes.

In this grammar, we primarily use the romanization, whose symbols largely map one-to-one with Cenvos graphemes. Cenvos has four kinds of graphemes:

◊items{
  ◊item{◊term{True letters} are graphemes that represent sounds.}
  ◊item{◊term{Markers}, while considered letters, do not represent sounds. Instead, they indicate that the words affected are treated specially. They occur on the level of a word and do not actively participate in morphology.}
  ◊item{◊term{Punctuation} includes the clause-end punctuation ◊l1{.}, ◊l1{;}, ◊l1{?}, and ◊l1{!}; the clitic boundary mark ◊l1{’}; the lenition mark ◊l1{·}; the grouping brackets ◊l1{{}}; and the quotation marks ◊l1{«»}.}
  ◊item{◊term{Digits} can be used to write short numerals.}
}

Of course, there is also the space.

◊(define (row c n r)
  `(@! ,(cenvos c) "&" ,(nc n) "&" ,(nc r)))

◊table/x[
  #:options (table-options
    #:caption "The graphemes of Ŋarâþ Crîþ. (The columns are read from left to right.)"
    #:colgroup '(c c rb c c rb c c c)
    #:first-col-header? #f
  )
]{
  Cen & Name & Rom & Cen & Name & Rom & Cen & Name & Rom
  ◊pseudoheader{True letters}
  ◊(row "c~" "ca" "c") & ◊(row "m" "ma" "m") & ◊(row "h" "ar" "h")
  ◊(row "e" "e" "e") & ◊(row "a" "a" "a") & ◊(row "ħ" "ħo" "ħ")
  ◊(row "n" "na" "n") & ◊(row "f" "fa" "f") & ◊(row "ê" "ên" "ê")
  ◊(row "ŋ~" "ŋa" "ŋ") & ◊(row "g" "ga" "g") & ◊(row "ô" "ôn" "ô")
  ◊(row "v" "va" "v") & ◊(row "p" "pa" "p") & ◊(row "â" "ân" "â")
  ◊(row "o" "o" "o") & ◊(row "t" "ta" "t") & ◊(row "u" "uħo" "u")
  ◊(row "s" "sa" "s") & ◊(row "č" "ča" "č") & ◊(row "w" "cełaŋa" "w")
  ◊(row "þ" "þa" "þ") & ◊(row "î" "în" "î") & ◊(row "x" "avarte" "x")
  ◊(row "š" "ša" "š") & ◊(row "j" "ja" "j") & ◊(row "y" "priþnos" "y")
  ◊(row "r" "ra" "r") & ◊(row "i" "i" "i") & ◊(row "z" "telrigjon" "z")
  ◊(row "l" "la" "l") & ◊(row "d" "da" "d")
  ◊(row "ł" "ła" "ł") & ◊(row "ð" "ða" "ð")
  ◊pseudoheader{Final forms and ligatures (layer 2w)}
  ◊(row "c" "" (lig "c$")) & ◊(row "me" "" (lig "me")) & ◊(row "ww" "" (lig "ww"))
  ◊(row "ŋ" "" (lig "ŋ$")) & ◊(row "mm" "" (lig "mm")) & ◊(row "xx" "" (lig "xx"))
  ◊(row "ee" "" (lig "ee")) & ◊(row "jâ" "" (lig "jâ")) & ◊(row "yy" "" (lig "yy"))
  ◊(row "em" "" (lig "em")) & ◊(row "âj" "" (lig "âj")) & ◊(row "zz" "" (lig "zz"))
  ◊pseudoheader{Markers}
  ◊(row "#" "carþ" "#") & ◊(row "×" "njor" "+*") & ◊(row "*" "nef" "*")
  ◊(row "+" "tor" "+") & ◊(row "@" "es" "@") & ◊(row "&" "sen" "&")
  ◊pseudoheader{Punctuation}
  ◊(row "." "gen" ".") & ◊(row "'" "ŋos" "’") & ◊(row "«" "fos" "«")
  ◊(row ";" "tja" ";") & ◊(row "·" "łil" "·") & ◊(row "»" "þos" "»")
  ◊(row "?" "šac" "?") & ◊(row "{" "rin" "{") & ◊(row "/" "jedva" "/")
  ◊(row "!" "cjar" "!") & ◊(row "}" "cin" "}") & ◊(row "-" "mivaf·ome" "-")
  ◊(row ":" "vas" ":")
}

The letters ◊l1{w}, ◊l1{x}, ◊l1{y}, and ◊l1{z} are ◊term{USR letters}. These are used in foreign languages written in Cenvos to represent phonemes that are not approximated by the phonology of Ŋarâþ Crîþ. Each foreign orthography is free to assign them as it pleases.

Cenvos has two graphemes that change form at the end of the word: ◊l1{c} and ◊l1{ŋ}, as well as several ligatures. We do not distinguish these forms in the romanization.

The marker ◊l1{*} is used for foreign words, such as loanwords and foreign names. ◊l1{#} is used to prefix ◊xref["../morphology/nouns.html" "names-personal" "Subsection"]{given names}. ◊l1{+} is used to prefix surnames passed by native conventions (i.e. from parent to child within the same gender); ◊l1{+*} marks a surname passed using non-native conventions. ◊xref["../morphology/nouns.html" "names-place" "Subsection"]{Place names} are prefixed with ◊l1{@}. ◊l1{#}, ◊l1{+}, ◊l1{+*}, and ◊l1{@} can all be used with ◊l1{*}, in which case ◊l1{*} occurs first. Note that ◊l1{+*} is a single letter of its own and not a ligature.

At the start of a word, ◊l1{&} indicates reduplication of an unspecified prefix of the rest of the word. For instance, ◊l1{&cên} can be pronounced as if it were ◊l1{cêcên} or ◊l1{cêncên}. (◊l1{&} occurs after all other markers in this case.) This usage is not productive in standard Ŋarâþ Crîþ, but it appears in a few words, as well as in some idiosyncratic cases. At the middle or the end of a word, or alone, it indicates ellipsis of part or all of the word, most often to abbreviate or censor a word. Lastly, ◊l1{&{}} is used similarly to the ellipsis in Western punctuation.

Markers can be applied to multi-word strings by surrounding the string with the grouping brackets ◊l1{{}}. In legal language, ◊l1{{}} are also used around phrases to resolve ambiguities.

The sentence punctuation ◊l1{.}, ◊l1{?}, and ◊l1{!} are used as expected. ◊l1{;} is used to separate two independent clause phrases within the same sentence. 

The quotation marks, ◊l1{«»}, are used around quotations, direct or indirect. A ◊l1{.} at the end of a quotation embedded within another sentence is omitted. In legal language, ◊l1{«»} are used in contracts around terms that refer to specific entities or places.

◊l1{:} is used for the following functions:

◊items{
  ◊item{listing separate elements of a symbolic identifier, in which case it is not surrounded by spaces,}
  ◊item{separating the usual end of an independent clause from a postposed adjunct, in which case it is preceded by a space,}
  ◊item{or separating principal parts of a lexical entry, in which case it is surrounded by spaces on both sides.}
}

◊l1{’} is used to separate clitics from the rest of the word to which they are attached. ◊l1{·} indicates lenition; it could be described as a “letter modifier”. It is also used as a decimal point: officially, it is used after the most significant digit of an inexact numeral when written with digits, but it also used unofficially to write non-integers.

◊l1{/}, as its derivation from ◊l1{i} suggests, is used to separate the number of ◊i{mjari} from the number of ◊i{edva} when writing currency amounts.

The morpheme boundary marker, ◊l1{-}, is sometimes used metalinguistically to mark a morpheme boundary, but it is not strictly a part of layer 1.

Spaces are placed in the following places:

◊items{
  ◊item{between orthographic words, but not between a clitic and the word to which it is attached}
  ◊item{after (but not before) ◊l1{.}, ◊l1{;}, ◊l1{?}, and ◊l1{!}}
  ◊item{before ◊l1{«} and after ◊l1{»} (but not on the other sides)}
  ◊item{around ◊l1{&{}}}
}

[TODO: cover mentions of letters within the language, corresponding to v7 p17 “When letters or markers are referred to, … but the effects on other glyphs are not standardized”]

Digits are interchangeable with short-form numerals, but not with long-form numerals. They are also written right-to-left in Cenvos, with the most significant digit first: ◊cenvos{2A3} is 0x2A3 = 675.

◊table/x[
  #:options (table-options
    #:caption "The digits of Ŋarâþ Crîþ. (The columns are read from left to right.)"
    #:first-col-header? #f
  )
]{
  Cen & # & Cen & # & Cen & # & Cen & #
  ◊cenvos{0} & ◊nc{0} & ◊cenvos{1} & ◊nc{1} & ◊cenvos{2} & ◊nc{2} & ◊cenvos{3} & ◊nc{3}
  ◊cenvos{4} & ◊nc{4} & ◊cenvos{5} & ◊nc{5} & ◊cenvos{6} & ◊nc{6} & ◊cenvos{7} & ◊nc{7}
  ◊cenvos{8} & ◊nc{8} & ◊cenvos{9} & ◊nc{9} & ◊cenvos{A} & ◊nc{A} & ◊cenvos{B} & ◊nc{B}
  ◊cenvos{C} & ◊nc{C} & ◊cenvos{D} & ◊nc{D} & ◊cenvos{E} & ◊nc{E} & ◊cenvos{F} & ◊nc{F}
}

◊section[#:id "letternum"]{Letter numbering}

Sometimes, an integer must be assigned to each letter. In this case, the assignment shown in the table below is used. Note that numbers are not assigned fully sequentially. Furthermore, this function is valid only for layer 1 graphemes.

◊(define (row2 c n)
  `(@! ,(nc c) "&"
    (cls "figure" ,(my-number->string n 16)) "&"
    (cls "figure" ,(my-number->string n))))

◊table/x[
  #:options (table-options
    #:caption "Letter numbering in Ŋarâþ Crîþ. (The columns are read from left to right.)"
    #:colgroup '(c c rb c c rb c c c)
    #:first-col-header? #f
  )
]{
  Letter & Hex & Dec & Letter & Hex & Dec & Letter & Hex & Dec
  ◊pseudoheader{True letters}
  ◊(row2 "c" 0) & ◊(row2 "m" 32) & ◊(row2 "h" 17)
  ◊(row2 "e" 1) & ◊(row2 "a" 9) & ◊(row2 "ħ" 18)
  ◊(row2 "n" 2) & ◊(row2 "f" 10) & ◊(row2 "ê" 257)
  ◊(row2 "ŋ" 43) &◊(row2 "g" 11)  & ◊(row2 "ô" 260)
  ◊(row2 "v" 3) & ◊(row2 "p" 12) & ◊(row2 "â" 265)
  ◊(row2 "o" 4) & ◊(row2 "t" 13) & ◊(row2 "u" 19)
  ◊(row2 "s" 5) & ◊(row2 "č" 222) & ◊(row2 "w" -1)
  ◊(row2 "þ" 85) &◊(row2 "î" 14)  & ◊(row2 "x" -2)
  ◊(row2 "š" 94) & ◊(row2 "j" 110) & ◊(row2 "y" -3)
  ◊(row2 "r" 6) & ◊(row2 "i" 15) & ◊(row2 "z" -4)
  ◊(row2 "l" 7) & ◊(row2 "d" 16)
  ◊(row2 "ł" 119) & ◊(row2 "ð" 341)
  ◊pseudoheader{Markers}
  ◊(row2 "#" 20) & ◊(row2 "+*" 22) & ◊(row2 "*" 25)
  ◊(row2 "+" 21) & ◊(row2 "@" 23) & ◊(row2 "&" 26)
}

The ◊term{letter sum} of a word is the sum of all of its letters. This value is used in some of the noun declension paradigms.

It is theorized that letter numbers were assigned in the following manner:

◊items{
  ◊item{The basic true letters inherited from ◊term{Necarasso Cryssesa} (i.e. those corresponding to ◊l1{c e n v o s r l m a f g p t î i d h}) received sequential numbers from zero. The number of ◊l1{m} was changed due to superstitions against the number eight.}
  ◊item{◊l1{ŋ þ š ł č ð} received numbers based on what letter pairs (or triplets in the case of ◊l1{ð}) they were based on.}
  ◊item{◊l1{ê}, ◊l1{ô}, and ◊l1{â} were numbered as 256 + base glyph number.}
  ◊item{The other letters and the markers received sequential numbers after ◊l1{h}, skipping 0x18.}
}

◊section[#:id "collation"]{Collation}

The true letters and the markers are collated in their respective order, except for ◊l1{&}, which is ignored. Lenited letters are treated as their respective base letters, except when two words differ only by the presence or absence of a lenition mark, in which case the lenited variant is collated after the base letter: ◊l1{saga} < ◊l1{sag·a} < ◊l1{sada} < ◊l1{saħa}. Numerals are collated after all letters.

In a directory of personal names, entries are collated on surnames, with given names considered only when surnames are identical. Headings in such a list include the prefix up to and including the first true letter: ◊l1{+merlan #flirora} would be found under ◊l1{+m}.

Ordered items can be labeled using numerals (starting from 0) or letters. In the latter case, only the letters ◊l1{c e n v o s r l m a f g p t î i d h} are used.

◊section[#:id "numquotes"]{Numquotes}

A digit immediately preceding text surrounded by quotation or grouping marks constitutes a ◊term{numquote}. The digit is usually not pronounced in this case. Numquotes are mainly used for secondary purposes that lack any dedicated punctuation.

◊table/x[#:options (table-options
    #:caption "Numquotes in Ŋarâþ Crîþ."
    #:first-col-header? #f
    #:colgroup '(c d)
  )]{
  Numquote & Meaning
  ◊nc{B{}} & Contains parenthetical information: provides supplementary information. The sentence should still be grammatical without the parenthetical content.
  ◊nc{1{}} & Lists an alias of a referent mentioned by name.
  ◊nc{2{}} & Surrounds a key-value list. Used as such: ◊l1{2{3{&{}} 4{&{}} 3{&{}} 4{&{}}}}
  ◊nc{3{}} & Used for listing a key inside ◊l1{2{}}.
  ◊nc{4{}} & Used for listing a value inside ◊l1{2{}}. When not directly inside a ◊l1{2{}} numquote, marks a list: elements are delimited by spaces, and ◊l1{{}} can be used to insert multi-word elements.
  ◊nc{9{}} & Used to contain abbreviated quantities in the traditional currency system.
  ◊nc{*9{}} & Used to contain abbreviated quantities in a currency system other than the traditional one.
}

◊section[#:id "backrefs"]{Backreferences}

Sometimes, repeated sections of a text are notated using ◊term{backreferences}. A ◊term{backreference definition} consists of two ◊i{ŋos}es followed by a string of letters (the ◊term{identifier}) and a space, and then a phrase inside a ◊l1{{}} pair. The text inside the delimiters will be transcluded using a ◊term{backreference} itself, which consists of two ◊i{łil}s followed by the same identifier.
