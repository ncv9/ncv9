#lang pollen

◊define-meta[title]{Information structure}

Ŋarâþ Crîþ marks the topic and the focus using prosody and word order. In particular, the topic comes at the beginning of an independent clause phrase (after any headps). In contrast, the focus usually occurs near its end, immediately before the possibly implicit verb. However, an interrogative NP as a focus may be fronted instead of backed, as may an NP that acts as an answer to a question.

Prosodically, the topic is marked with a lower and less varied pitch range, while the focus is marked with sentence-level stress on its last stressed syllable.

◊section[#:id "clitic-moc"]{The additive clitic ◊l1{=’moc}}

The clitic ◊l1{=’moc} can be translated to the Japanese particle ◊trans{◊ja{～も}} or to the English words ◊trans{also} or ◊trans{even}. It can be applied to many different constituents:

◊items{
  ◊item{noun phrases}
  ◊item{attributive predicate phrases (verb participles, adverbial or adnominal relationals)}
  ◊item{the ◊i{so}-particle ◊l1{so}, changing the meaning to ◊trans{even if}}
}

◊gloss/x{
  ◊glfree{◊nc{ondelt’moc cerit’pe corðen teha.}}
  ◊gla{ond-elt=’moc cer-it=’pe corð-en teha.}
  ◊glb{now-%loc.%di=also live-%inf=%poss.1 certainty-%acc.%di one_of.3%sg.%pres.%ipfv}
  ◊glfree{Even now, I’m sure that I’m alive.}
}

◊gloss/x[#:id "vrelat2"]{
  ◊glfree{◊nc{celmas vrele so’moc le nemirin cengrit garasle.}}
  ◊gla{celm-as vrel-e so=’moc le nem-irin cengr-it gar-as-le.}
  ◊glb{window-%nom.%sg thick-3%sg if=also %imp apple-%acc.%pl throw-%inf refrain-2%sg-%past}
  ◊glfree{Even if the window is thick, don’t throw apples at it.}
}

◊l1{=’moc} is also used to attach significance to its antecedent:

◊gloss/x[#:id "varon1"]{
  ◊glfree{◊nc{sâna gjorłeve varoþ’moc rille merte.}}
  ◊gla{sân-a g\jorł-eve var-oþ=’moc rille metr-te.}
  ◊glb{bear-%nom.%sg shout-%inf.%loc life-%dat.%di=also on_behalf_of run-1%sg.%past.%pfv}
  ◊glfree{When the bear roared, I ran for as much as my life.}
}

When ◊l1{=’moc} is used on the subject of ◊l1{telit} ◊trans{not exist}, then the existence of the subject is not presupposed. [TODO: more]

◊gloss/x{
  ◊glfree{◊nc{nemir mina’moc ceła.}}
  ◊gla{nem-ir mina=’moc ceła.}
  ◊glb{apple-%nom.%sg one.%nom.%cel=also not_exist.3%sg}
  ◊glfree{There are no apples.}
}

When the ◊l1{=’moc} is attached to a noun or pronoun in the generic number other than a third- or sixth-declension noun in the nominative, accusative, dative, or genitive case, that noun takes the direct number instead, and all modifiers to that noun change in number to reflect this. This change, however, does not apply to verbal affixes that agree in number with the noun in question.

◊gloss/x{
  ◊glfree{◊nc{eši nôro tano’moc mervu.}}
  ◊gla{eši nôr-o tan-o=’moc merv-u.}
  ◊glb{here.%loc.%di small-%rel.%nom,%nom.%pl bird-%nom.%di=also large-3%gc}
  ◊glfree{Here, even the small birds are large.}
}

◊section[#:id "exhaustive"]{Marking exhaustivity}

There is no clitic akin to ◊l1{=’moc} for exhaustive constituents. Instead, the sentence is restructured to equate the exhaustant with a noun phrase involving ◊l1{šino}:

◊gloss/x{
    ◊glfree{◊nc{ceriþo arantil circþîve mîr anljar noršidir šinos cenþal.}}
    ◊gla{cer-iþo arant-il circþîv-e mîr anlj-ar noršid-ir šin-os cens-þal.}
    ◊glb{remain-%rel.%nom,%dat.%cel long_time-%gen.%di battle-%dat.%sg after injure-%rel.%acc,%nom.%hum warrior-%nom.%sg all-%dat.%sg equal-3%sg.%past.%ipfv}
    ◊glfree{After the long battle, all who survived was the injured warrior.}
    ◊glfree[#:prefix "Or: "]{After the long battle, only the injured warrior survived.}
}

◊gloss/x{
    ◊glfree{◊nc{ŋaras crîþe monrit vasrit pentos šinen tfełu vrêmas monru.}}
    ◊gla{ŋar-as crîþ-e monr-it vasr-it pent-os šin-en tfeł-u vrêm-as monr-u.}
    ◊glb{language-%dat.%sg forest-%dat.%sg teach-%inf effective-%inf can-%rel.%nom,%nom.%cel all-%gen.%sg method-%nom.%gc grammar-%dat.%sg teach-3%gc}
    ◊glfree{All methods that can effectively teach Ŋarâþ Crîþ teach its grammar.}
    ◊glfree[#:prefix "Or: "]{Only methods that teach the grammar of Ŋarâþ Crîþ can effectively teach it.}
}

Other uses of ◊trans{only} can be translated into Ŋarâþ Crîþ using other constructions:

◊gloss/x{
  ◊glfree{◊nc{lê nemirin m·ênčilt’pe tecto entan ħelilt’ve mârit folca.}}
  ◊gla{lê nem-irin m·ênč-ilt=’pe tecto ent-an ħel-ilt=’ve mâr-it folc-a.}
  ◊glb{this.%cel apple-%acc eat-%inf.%dat.%ind=%poss.1 before that_thing.%cel-%acc.%sg do_this-%inf.%dat.%ind=%poss.2 wait-%inf intend_to-1%sg}
  ◊glfree{I will wait until you eat that apple before I eat this one.}
  ◊glfree[#:prefix "Or: "]{Only when you eat that apple will I eat this one.}
}

For the use of ◊trans{only} to mean ◊trans{sole} or ◊trans{unique}, the numeral ◊l1{mina} ◊trans{one} is used with the distinctness clitic ◊l1{=’ot}. For the use of ◊trans{only} to mean ◊trans{no more than}, the bounding clitic ◊l1{=’ocþaf} is used.
