#lang pollen

◊define-meta[title]{The world}

◊term{Tergia} is a G-type star measuring 0.973 solar masses.

◊term{Njôro} is a habitable planet orbiting Tergia with a semimajor axis of 1.068 AU and an eccentricity of 0.0548. It has a radius of about 6415 km, a mass of 1.01 earth masses, and an axial tilt of 17.8°. The planet has a day length of roughly 24.32 hours, and a year is 403.21 times as long (= 408.61 Earth days). The northward equinox occurs at a mean anomaly of 104.2° relative to the periapsis. Its major satellite has a radius of 1503 km and orbits Njôro 670,200 km away at a period of 62.85 Earth days.

Compared to the Sun and the Earth, Tergia has a lower mass and Njôro orbits farther from it. As a result, Njôro has a lower average temperature than that of Earth, at about 10.1°C. In addition, the eccentricity of ◊|cri|’s orbit significantly contributes to variations in solar insolation; since the perihelion is near the southward solstice, the northern hemisphere experiences less extreme changes in temperature than the southern hemisphere.

The planet has four major continents:

◊items{
  ◊item{◊term{Crîþja} (originally referring to the federation that occupied most of its territory) is on the northwest corner of the map. Nearby are the islands ◊term{Meðac·adir} (to the northeast), ◊term{Vasacel} (to the northwest), and and ◊term{Ŋełpas} (to the west), as well as the ◊term{Elþana Archipelago} between itself and (unnamed-a). Farther to the east of Meðac·adir are the islansd ◊term{(unnamed-b3)} and ◊term{(unnamed-b4)}.}
  ◊item{◊term{(unnamed-a1)} lies almost entirely in the southern hemisphere, southeast of Crîþja.}
  ◊item{◊term{(unnamed-a2)} is to the north of the eastern part of (unnamed-a) and to the west of Crîþja.}
  ◊item{◊term{(unnamed-a3)} lies directly to the south of Crîþja. To its east are the ◊term{(unnamed-b1)} and ◊term{(unnamed-b2)} islands.}
}

The map is not yet finished but a ◊link["https://gitlab.com/Kyarei/ncv9/-/blob/master/map_data/map_master.svg"]{preview} is available.

◊section[#:id "species"]{The species}

As of (year), Njôro contains about 750 million sapient individuals in total.

The dominant sapient species inhabiting Njôro is the ◊term{nava}. For most purposes, ◊i{navo} can be regarded as humans, but their biology has several differences. Most notably, ◊i{navo} lay eggs instead of giving live birth; in addition, the young are fed by mouth instead of receiving milk. As a result, ◊i{navo} lack any nipples or breasts; additionally, their reproductive organs are different from those of humans. ◊i{Navo} also grow horns during the first six years of life but shed them afterwards until they completely disappear by the age of twelve.

◊subsection[#:id "other-species"]{Other species}

◊i{Navo} are not the only sapient species on Njôro.

◊term{◊i{Vasosôr}} (singular: ◊i{vasor}) are anthropomorphic reptiles. They are bipedal and about 170–190 cm tall, with tails about 50 cm long. They mainly reside in or around Aširel and Farlarþ. They number about 1.8 million, and most live nomadic lifestyles.

◊section[#:id "history"]{History}

History, at least within Crîþja, can be divided into the following eras:

◊items{
  ◊item{◊term{Saegelþe} is an apocryphal era that has no definite distance to the other eras but is said to have lasted between 65536 and 16,777,216 years. Between the end of Saegelþe and the start of Evonmiron lies the first ◊term{inter-era period}.}
  ◊item{◊term{Evonmiron} (c. ◊neg{1000} – 0) marks the beginning of Crîþol civilization as we know it.}
  ◊item{◊term{Minem·eða} (0 – 677) starts with the invention of the first version of the ◊term{Cenvos} script, which was used to write ◊term{Necarasso Cryssesa}, though earlier scripts existed.}
  ◊item{◊term{Naresa} (677 – c. 1400) began with the ◊term{Naresa Reformation}, when ◊term{Enacssyr Malfa} (ŊCv9: ◊nc{+enacþir #malfa}), the daughter of the previous monarch ◊term{Enesor} (ŊCv9: ◊nc{#enesor}), forcibly took power from the existing monarchy, making sweeping reforms to the government and society as well as to the language. The resulting regime was eager to silence any opposition, although the heavy-handed rule subsided over the next few centuries.}
  ◊item{◊term{Cenþed·rełes} (c. 1400 – 1800) – exploration into nearby areas}
  ◊item{◊term{(unnamed-e5)} (c. 1800 – 2100) – industrialization and stuff.}
  ◊item{◊term{Asenmeva} (c. 2100 – 2561) – futuristic technology; ended with the ◊term{Venesos Gating Event}, which caused a global civilizational collapse.}
  ◊item{◊term{Senârmortos} (2561 – 3041) – aftermath of the Venesos Gating Event.}
  ◊item{◊term{Nerita} (3041 – 3497) – started with the alliance between ◊term{Asoren} and ◊term{Viriþis}, forming what would later be the ◊term{Federation of Crîþja}.}
  ◊item{◊term{(unnamed-e9)} (3497 – 3612) – dissolution of the federation, with many wars ensuing.}
  ◊item{◊term{(unnamed-e10)} (3612 – ?).}
}
