#lang pollen

◊(require pollen/core pollen/setup)

◊define-meta[title]{Appendix}
◊define-meta[chapter-type part]

◊(when/splice (equal? (current-poly-target) 'html)
  (embed-pagetree "index.ptree"))