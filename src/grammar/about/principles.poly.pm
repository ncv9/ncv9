#lang pollen

◊define-meta[pdf-type article]
◊define-meta[title]{The principles of Ŋarâþ Crîþ}
◊define-meta[book-author]{+merlan #flirora}

This chapter expands on ideas presented in ◊link["https://flirora.xyz/langdocs/reason4v7.html"]{◊cite/inbook{The reason of Ŋarâþ Crîþ’s existence}} and ◊link["https://ncv9.flirora.xyz/diary/0C66-obvious.html"]{◊cite/inbook{The obvious choice is almost always the wrong choice}}.

Ŋarâþ Crîþ v9 has the following goals:

◊enum{
  ◊item{◊b{Aesthetics} according to +merlan #flirora, above all other criteria.}
  ◊item{◊b{Integrability into art} such as visual art, literature, and music.}
  ◊item{◊b{Precision}: the grammar, along with the dictionary, should leave as little doubt as possible on how to use the language.}
  ◊item{◊b{Practicality}: the language must be reasonably feasible to use.}
}

The following, on the other hand, are explicit non-goals for Ŋarâþ Crîþ v9. They are not necessarily to be avoided – in fact, they may be desirable in some quantity, but they will not be pursued either.

◊items{
  ◊item{◊b{Simplicity and orthogonality} were valued in Necarasso Cryssesa but are not in Ŋarâþ Crîþ.}
  ◊item{◊b{Density} in either speech or writing.}
  ◊item{◊b{Naturalism}: although the language is spoken in a fictional world, it is not important for it to be completely naturalistic.}
  ◊item{◊b{Unambiguity}: ambiguity is acceptable as long as it does not detract from practicality too much. In some cases, special constructions may be added to resolve any ambiguities.}
}

◊section[#:id "aesthetics"]{Aesthetics according to +merlan #flirora}

The aesthetics of a language has three parts: ◊term{phonaesthetics}, ◊term{graphaesthetics}, and ◊term{grammaesthetics}.

As phonaesthetics pertains to the sound of a language, graphaesthetics pertains to its writing. Both of these can easily be appreciated, although the latter might often be overlooked.

Grammaesthetics pertains to the grammar of a language. By convention, it also covers the language’s vocabulary. Unlike phonaesthetics and graphaesthetics, which can be evaluated by hearing or seeing the language in question, grammaesthetics requires one to study the language to appreciate. As a result, most people have a weaker sense of grammaesthetics than of the other two aspects.

One’s sense of aesthetics is frequently influenced by the prestige of the languages and dialects that one knows, and this applies to me as well. However, I still use mine to guide the development of Ŋarâþ Crîþ because I do not believe in condemning someone’s aesthetic preferences solely for being based on (let alone matching) societal prejudices.

In my case, the aesthetics of Ŋarâþ Crîþ has been influenced by Japanese, Welsh, and Irish, as well as other languages such as Latin or Finnish. While Spanish, a language I studied in school at the time, was influential in the earliest stages of Necarasso Cryssesa, its mark on the language waned as it evolved. In terms of features themselves, my preferences, not all of which are in Ŋarâþ Crîþ, are as follows:

◊items{
    ◊item{having ◊l3{θ}, ◊l3{ɬ}, ◊l3{ɹ}, and ◊l3{x}, as well as back unrounded vowels}
    ◊item{not having ◊l3{b} or ◊l3{u}. ◊l3{u̜} is preferable to ◊l3{u}.}
    ◊item{no tenuis stops: either aspirated or voiced}
    ◊item{◊l3{ŋ} at onsets but not at codas}
    ◊item{avoiding repeated sounds}
    ◊item{angular strokes in scripts}
    ◊item{mixed logographic and phonetic writing systems}
    ◊item{fusion or agglutination over isolation}
    ◊item{no sex-based grammatical gender; personal pronouns not distinguished by sex or gender}
    ◊item{no negative concord}
    ◊item{no politeness levels (e.g. T–V distinction, honorific forms)}
}

◊section[#:id "quasinaturalism"]{Quasi-naturalism}

The main reason that I avoid making naturalistic languages is the requirement for diachronic conlanging. While I admire those who create naturalistic languages, this part thereof is not enjoyable for me.

In his commentary on his ◊link["https://www.reddit.com/r/conlangs/comments/7de032/mklang_the_way_i_make_languages/"]{◊cite/inbook{mklang}}, Isoraķatheð writes that “it’s possible to make a naturalistic language using mklang, and in any case a[n] ‘almost-but-not-quite-human’ language is something that is largely unexplored in language creation”. While Ŋarâþ Crîþ proper started out as a revision of Necarasso Cryssesa, it has become such a language that resembles a natural language but has several features that no natural language would have. Naturalism in languages can be analyzed into several elements that can be borrowed back into a not-quite-human language.

◊subsection[#:id "complexity"]{Complexity}

When discussing languages, ‘complexity’ can refer to different ideas. The first division is between ◊term{complexity of description} and ◊term{complexity of use}. Complexity of description relates to describing and consequently learning the language. In contrast, complexity of use describes the amount of computation needed to use the language, such as to evaluate the truth condition of a sentence or to form a sentence with a given truth condition. Both of these ideas can apply to the whole language as well as to individual features within it.

Complexity of description can further be divided into ◊term{intricacy} and ◊term{volume}. Intricacy arises from treating the cells of a paradigm in many different ways, while volume arises from treating many of them in the same way. These two quantities can be seen as the amount of code and the amount of data required to describe the language, although this analogy relies on distinguishing code and data. Indeed, different descriptions of the same linguistic feature might have different intricacy–volume balances. For instance, one description might describe the suffixes for a large set of word forms using a complex set of rules, while another might list all of the suffixes themselves.

Given a certain amount of intricacy, there is a lower bound on volume. The theoretical upper bound is too high to be relevant, but once the volume exceeds the threshold for the intricacy, the language starts seeming artificial or incoherent.

The goal of practicality also puts limits on the various types of complexity – most notably, complexity of use. Intricacy, being correlated with complexity of use, is limited as well. Volume mainly affects the acquisition of the language rather than its use, but excessive volume can also lead to excessive complexity of use. Practicality also imposes a lower bound on all of these, since a language that is too simple will not be able to express all ideas.

Necarasso Cryssesa and another conlang of mine, Ḋraħýl Rase, tended toward the lower end of complexity, or rather, left a large amount of complexity undescribed. Later conlangs, such as Lek-Tsaro and Jbl, went in the opposite direction but introduced too much complexity of use.

Version 7 and early version 9 of Ŋarâþ Crîþ recognized the limits on complexity of use but introduced too much volume instead. Project Caladrius and Elaine, which were enacted on ◊render-date/single{2022-06-14}, collectively shifted the complexity of noun declension away from volume toward intricacy.

◊subsection[#:id "flithe"]{The concept of ◊i{fliþe}}

An important concept in Ŋarâþ Crîþ is that of ◊term{◊i{fliþe}}, which translates to ◊trans{gap} or ◊trans{difference}. When pertaining to language, this word can be translated roughly as ◊trans{asymmetry}. More precisely, a property of a linguistic feature (such as words of a certain part of speech, or phonemes, or inflectional paradigms) has ◊i{fliþe} if it applies to some members of that feature but not to other related members that one would expect it to apply to.

For instance, ◊l1{menat} ◊trans{to see} is semitransitive but the related verb ◊l1{crešit} ◊trans{to hear} is transitive. This is an example of ◊i{fliþe} on case frame in verbs. As another example, the basic color terms in Ŋarâþ Crîþ are divided between nouns and verbs, with only two colors being assigned both parts of speech. This is an example of ◊i{fliþe} on part of speech in color terms.

An example in English, namely on case in pronouns, would be the use of oblique personal pronouns in coordinate noun phrases in subject position.

◊i{Fliþe} is related to the concepts of irregularity, non-◊link["https://en.wikipedia.org/wiki/Orthogonality_(programming)"]{orthogonality}, and redundancy of features.

◊subsection[#:id "redundancy"]{Redundancy}

In Necarasso Cryssesa, redundancy was seen as irrational and avoided. Ŋarâþ Crîþ, on the other hand, does not have such an irrational fear of redundancy.

One must distinguish two different kinds of redundancy: ◊term{redundancy of morphemes} and ◊term{redundancy of features}. Redundancy of morphemes involves multiple morphemes in an utterance coding for the same information, of which agreement is one such type. This sense is what most people mean by ◊i{redundancy} when talking about language. Redundancy of morphemes has the role of a sort of “error correction”, making it easier to understand text from a noisy channel.

In comparison, redundancy of features refers to the existence of multiple features whose uses overlap or could overlap. For example, Ŋarâþ Crîþ has adpositions, serial verb constructions, and auxiliary verbs. These overlap in what they could be used for; in other words, a language with only one of these features is possible. Having all three adds a layer of complexity about which construction to use for a given meaning.

◊subsection[#:id "ambiguity"]{Ambiguity}

Natural languages also have ambiguities, which novice conlangers tend to be afraid of. The difficult part of adding ambiguity to conlangs is determining how much and what kinds of ambiguity are manageable. In addition, an ambiguous phrase needs disambiguated equivalents.

◊subsection[#:id "plausibility"]{Plausibility}

Another consideration in adding a feature is how plausible it is. Namely:

◊items{
    ◊item{How often would the feature in question come into use?}
    ◊item{Is there an evolutionary reason for speakers of this language to want this feature?}
    ◊item{How would this feature have evolved?}
    ◊item{How much burden (e.g. loss of density or increased complexity) does this feature incur when using this language?}
}

◊subsection[#:id "supernatural"]{Can there be a super-naturalistic conlang?}

Sometimes, I have strived to make Ŋarâþ Crîþ a ‘super-naturalistic’ language – one that is more complex than a naturalistic language. Although it is certainly not yet at that point, the question of whether such a language is possible is intriguing. The closest one that comes to my mind is ◊link["http://ithkuil.net/"]{Ithkuil}, although its unfamiliarity might bias it toward seeming complicated.

◊section[#:id "sins"]{What Ŋarâþ Crîþ will never have}

Ŋarâþ Crîþ will never have the following features:

◊enum{
  ◊item{◊b{Productive reduplication}: This goes directly against the goal of aesthetics.}
  ◊item{◊b{An emphatic morpheme}: One does not simply emphasize a word using a suffix. Try moving it near the end of the sentence.}
  ◊item{◊b{A (unified) negative morpheme}: Ŋarâþ Crîþ has various negative auxiliary and suppletive verbs. Nominal negation (i.e. ◊trans{other than}) is expressed with the noun ◊l1{aliþ}.}
  ◊item{◊b{A word for ◊trans{very}}: Either take it out or expand your vocabulary.}
  ◊item{◊b{A T–V distinction}: Don’t even ask for it.}
}
