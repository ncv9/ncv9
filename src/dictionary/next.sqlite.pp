#lang racket

(require pollen/setup)
(require "../pollen/lexicon.rkt")
(provide doc metas)

(define dict
  (parameterize ([current-poly-target 'html])
    (dynamic-require "ncv9.kaltoklel" 'dict)))
(define doc (dictionary-render/sqlite dict))
(define metas (hash))
