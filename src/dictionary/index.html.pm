#lang pollen

◊(require "../pollen/lexicon.rkt" "ncv9.kaltoklel" racket/serialize)

◊define-meta[title]{The Ŋarâþ Crîþ v9 dictionary}

Here’s a page with all of the words in the Ŋarâþ Crîþ v9 dictionary. You can save this page in case you're going to be offline for a while.

An ◊link["next.html"]{experimental interactive version of the dictionary} is now available.

◊(dictionary-render dict)
