#lang racket

(require racket/serialize xml pollen base64 pollen/template "common.rkt")

(provide
 (contract-out
  [relations? contract?]
  [struct example (
                   (phrase string?)
                   (literal (or/c string? #f))
                   (translation xexpr/c)
                   (explanation (or/c xexpr/c #f))
                   )]
  [struct def-listing ((definition (or/c xexpr/c #f))
                       (translations (listof string?))
                       (examples (listof example?)))]
  [struct dictionary-entry ((label string?)
                            (headword string?)
                            (etymology (or/c xexpr/c #f))
                            (def-listings (listof def-listing?))
                            (notes (listof xexpr/c))
                            (relations relations?))]
  [def-translations (->* () (#:examples (listof example?)) #:rest (listof string?) def-listing?)]
  [def-detailed (->* (xexpr/c) (#:examples (listof example?)) #:rest (listof string?) def-listing?)]
  [dictionary-entry# (->* (string? string? (or/c xexpr/c #f) (listof string?))
                          (#:etymology (or/c xexpr/c #f) #:notes (listof xexpr/c) #:examples (listof example?) #:relations relations?)
                          dictionary-entry?)]
  [dictionary-entry#n (->* (string? string? (listof def-listing?))
                           (#:etymology (or/c xexpr/c #f) #:notes (listof xexpr/c) #:relations relations?)
                           dictionary-entry?)]
  [example-render (-> example? xexpr/c)]
  [def-listing-render (-> def-listing? xexpr/c)]
  [dictionary-entry-render (-> dictionary-entry? xexpr/c)]
  [refword (-> string? xexpr/c)]
  [refw (-> string? xexpr/c)]
  [refword/l (->* (string?) (string?) xexpr/c)]
  [struct dictionary ((entries (listof dictionary-entry?)))]
  [dictionary-render (-> dictionary? xexpr/c)]
  [dictionary-render/sqlite (-> dictionary? bytes?)]
  )
 )

(define relations? (hash/c symbol? (listof string?) #:immutable #t))
(define NO-RELS (hash))

(define (word-label->element-id label)
  (string-append
   "zet-"
   (bytes->string/utf-8
    (base64-encode
     (string->bytes/utf-8 label)
     #:endcodes 'url
     #:pad? #t))))

(define/match (relation-type->description rt plural?)
  [('synonym #t) "Synonyms"]
  [('synonym #f) "Synonym"]
  [('antonym #t) "Antonyms"]
  [('antonym #f) "Antonym"]
  [('holonym #t) "Holonyms"]
  [('holonym #f) "Holonym"]
  [('meronym #t) "Meronyms"]
  [('meronym #f) "Meronym"]
  [('hypernym #t) "Hypernyms"]
  [('hypernym #f) "Hypernym"]
  [('hyponym #t) "Hyponyms"]
  [('hyponym #f) "Hyponym"]
  [('confusable _) "Do not confuse"]
  [(x _) (format "<~v>" x)])

(define/match (singleton-list? l)
  [((list a)) #t]
  [(_) #f])

(serializable-struct def-listing
                     (definition
                       translations
                       examples))

(serializable-struct example
                     (phrase
                      literal
                      translation
                      explanation
                      ))

(serializable-struct dictionary-entry
                     (label
                      headword
                      etymology
                      def-listings
                      notes
                      relations
                      ))

(define (def-translations #:examples [examples '()] . trans) (def-listing #f trans examples))
(define (def-detailed def #:examples [examples '()] . trans) (def-listing def trans examples))

(define (dictionary-entry#
         label
         headword
         definition
         translations
         #:etymology [etymology #f]
         #:notes [notes '()]
         #:examples [examples '()]
         #:relations [relations NO-RELS])
  (dictionary-entry label
                    headword
                    etymology
                    (list (def-listing definition translations examples))
                    notes
                    relations))

(define (dictionary-entry#n
         label
         headword
         def-listings
         #:etymology [etymology #f]
         #:notes [notes '()]
         #:relations [relations NO-RELS])
  (dictionary-entry label
                    headword
                    etymology
                    def-listings
                    notes
                    relations))

(define (lang langcode text)
  `(span ((lang ,langcode)) ,text))
(define (nc text) (lang "art-x-ncs-v9" text))

(define (refword label) `(a ((href ,(string-append "#" (word-label->element-id label)))
                             (lang "art-x-ncs-v9"))
                            ,(refw label)))
(define (refw label) (string->symbol (format "%%ARK ~a%%" label)))
(define (refword/l label [form label]) `(a ((href ,(string-append "#" (word-label->element-id label)))
                                            (lang "art-x-ncs-v9"))
                                           ,form))

(define (relations-render relations)
  `(@ . ,(hash-map
          relations
          (lambda (type labels)
            `(p ,(relation-type->description type (not (singleton-list? labels)))
                ": "
                ,@(intersperse (map refword labels) "; ")
                )))))

(define/match (def-listing-render listing)
  [((def-listing definition translations examples))
   `(li
     ,(when/splice definition
                   `(span ((class "definition")) ,definition))
     ,(when/splice (and definition (cons? translations)) " → ")
     ,(when/splice (cons? translations)
                   `(span ((class "translations"))
                          ,(string-join translations ", ")))
     ,(when/splice (cons? examples)
                   `(ul ((class "examples"))
                        ,@(map example-render examples)))
     )])

(define (example-render example)
  `(li ((class "example"))
       ,(nc (example-phrase example))
       ,(when/splice (example-literal example)
                     " ("
                     '(em "lit.")
                     " "
                     (example-literal example)
                     ")")
       " "
       ,(example-translation example)
       ,(when/splice (example-explanation example)
                     ". "
                     (example-explanation example))
       ))

(define (dictionary-entry-render entry)
  (apply @ `(
             ; The heading is no longer needed; it is added by f9i.
             ; (h2 ,(nc hw))
             (p (span ((class "pos")) %%POS%%)
                ,(when/splice (dictionary-entry-etymology entry)
                              `(span ((class "etym")) nbsp "< " ,(dictionary-entry-etymology entry))))
             (ol . ,(map def-listing-render (dictionary-entry-def-listings entry)))
             ,(for/splice ([notes (dictionary-entry-notes entry)])
                `(p ((class "notes")) ,notes))
             ,(relations-render (dictionary-entry-relations entry))
             ; Inflection tables are also generated by f9i.
             ;  ,(when/splice (cons? decl-tables)
             ;                `(div ((class "declension-tables"))
             ;                      ,(collapsible-box "Show inflections"
             ;                                        (map declension-table-render decl-tables))))
             )))

(serializable-struct dictionary (entries))

(define (dictionary-render dictionary)
  (define f9t-file-path (make-temporary-file))
  (define f9t-file-port (open-output-file f9t-file-path #:mode 'binary #:exists 'truncate))
  (for ([entry (dictionary-entries dictionary)])
    (fprintf f9t-file-port
             "~a: ~a\n"
             (dictionary-entry-label entry)
             (dictionary-entry-headword entry))
    (displayln (->html (dictionary-entry-render entry)) f9t-file-port))
  (close-output-port f9t-file-port)
  (define html-file-path (make-temporary-file))
  (define exit-code
    (system*/exit-code (find-executable-path "f9i") "file"
                       f9t-file-path
                       "--output-html" html-file-path))
  (when (not (zero? exit-code))
    (error 'dictionary-render "f9i subprocess returned ~a" exit-code))
  (define html-file-port (open-input-file html-file-path #:mode 'binary))
  (define output (port->string html-file-port))
  (close-input-port html-file-port)
  `(unsafe-raw-html ,output))

(define (dictionary-render/sqlite dictionary)
  (define f9t-file-path (make-temporary-file))
  (define f9t-file-port (open-output-file f9t-file-path #:mode 'binary #:exists 'truncate))
  (for ([entry (dictionary-entries dictionary)])
    (fprintf f9t-file-port
             "~a: ~a\n"
             (dictionary-entry-label entry)
             (dictionary-entry-headword entry))
    (displayln (->html (dictionary-entry-render entry)) f9t-file-port))
  (close-output-port f9t-file-port)
  (define sqlite-file-path (make-temporary-file))
  (define exit-code
    (system*/exit-code (find-executable-path "f9i") "file"
                       f9t-file-path
                       "--output-sqlite" sqlite-file-path))
  (when (not (zero? exit-code))
    (error 'dictionary-render "f9i subprocess returned ~a" exit-code))
  (define sqlite-file-port (open-input-file sqlite-file-path #:mode 'binary))
  (define output (port->bytes sqlite-file-port))
  (close-input-port sqlite-file-port)
  output)
