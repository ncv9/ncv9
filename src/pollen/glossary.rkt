#lang racket

(require pollen/core pollen/setup pollen/unstable/typography txexpr "langspec.rkt" "xref.rkt")

(provide glossary)

(struct gentry (term chapter-url anchor marker definition translations) #:transparent)

(define (stringify-headword/html hw)
  (cond [(string? hw) hw]
        [(txexpr? hw)
         (string-append* (map stringify-headword/html (get-elements hw)))]))

(define/match (stringify-headword/tex hw)
  [((? string?)) hw]
  [(`(@ . ,hws))
   (string-append* (map stringify-headword/tex hws))]
  [(`(cmd ,cmd . ,hws))
   (string-append* (map stringify-headword/tex hws))])

(define (stringify-headword es)
  (case (current-poly-target)
    [(html) (stringify-headword/html es)]
    [(pdf) (stringify-headword/tex es)]))

(define (xref/g chapter-url anchor marker term)
  (cond [anchor (xref chapter-url anchor marker term)]
        [else (xref/c chapter-url term)]))

(define/match (render-gentry/html e)
  [((gentry term chapter-url anchor marker definition translations))
   `(@ (dt ,(xref/g chapter-url anchor marker term))
       (dd (div ((class "definition-main")) ,@definition)
           ,(when/splice
             (cons? translations)
             `(div ((class "definition-translations"))
                   (b "Translations: ")
                   (dl ((class "definition-translations-list"))
                       ,(for/splice ([tl translations])
                          `(dt ,(langcode->name (first tl)))
                          `(dd (i ((lang ,(first tl))) ,(second tl)))))))))])

(define (render-gentries/html es)
  `(dl ,(for/splice ([e es]) (render-gentry/html e))))

(define/match (render-gentry/tex e)
  [((gentry term chapter-url anchor marker definition translations))
   `(@. (cmd/bare/no-space item) "[" (group ,(xref/g chapter-url anchor marker term)) "] "
        ,@definition
        ,(when/splice
          (cons? translations)
          "\n"
          `(cmd textit "Translations: ")
          (for/splice ([tl translations]
                       [i (in-naturals)])
            (when/splice (> i 0) ", ")
            (langcode->name (first tl))
            ": "
            (lang (first tl) (second tl)))))])

(define (render-gentries/tex es)
  `(env ((cmd "description"))
        (nnl "[style=nextline]")
        ,(for/splice ([e es]) (render-gentry/tex e))))

(define (render-gentries es)
  (case (current-poly-target)
    [(html) (render-gentries/html es)]
    [(pdf) (render-gentries/tex es)]))

(define/match (tl-tag? tag)
  [(`(in-ncs ,tl)) `(,(ŋc-code 9) ,tl)]
  [(`(in ,lang ,tl)) `(,lang ,tl)]
  [(_) #f])

(define (parse-entry entry)
  (match entry
    [(list* 'entry term chapter-url anchor marker the-rest)
     (define-values (tls non-tls)
       (partition tl-tag? the-rest))
     (define tls/parsed
       (sort (map tl-tag? tls)
             <
             #:key (lambda (e) (langorder (first e)))))
     (gentry term chapter-url anchor marker non-tls tls/parsed)]))

(define (glossary . contents)
  (let* ([contents (filter (negate whitespace?) contents)]
         [contents (map parse-entry contents)]
         [contents (sort contents string-ci<? #:key (compose1 stringify-headword gentry-term))])
    (render-gentries contents)))
