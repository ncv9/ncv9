#lang pollen

◊define-meta[title]{Introduction}

Suppose that ◊${\mathcal{K}} is the set of all possible lexical entries in a language, ◊${C} is a set of category labels, and ◊${L} is the set of all phonotactically valid words in the language. Then suppose ◊${f : \mathcal{K} \times C \rightarrow L} is the function that inflects words in the language.

We wish to not only be able to inflect a given lexical entry in a given category but also to perform the reverse task: given an inflected word form, find all lexical entries that it could be derived from, along with the category of the inflected form. That is, we wish to devise an algorithm that computes the preimage ◊${f^\leftarrow(\{s\})} for any ◊${s \in L}.

If we are concerned only with finding matches in a fixed dictionary ◊${K \subseteq \mathcal{K}}, then we can store a mapping for all ◊${k \in K} and ◊${c \in C} from ◊${f(k, c)} to ◊${(k, c)}. This method is conceptually simple and requires no structural knowledge of ◊${f}, but it takes ◊${O(|K| \cdot |C|)} entries and requires ◊${C} to be finite, making it space-intensive for highly inflecting languages.

Because Ŋarâþ Crîþ v9’s inflection rules have been complex, this brute-force method has been used for f9i. During the development of sp9 for Project Shiva, however, +merlan #flirora proposed investigating alternative approaches that would not require storing all inflected forms.

◊section[#:id "common-properties"]{The compositionality of linguistic inflection}

Many inflection paradigms are not perfectly fusional. That is, we can express ◊${C} as a Cartesian product of other sets ◊${C_0 \times C_1 \times \cdots \times C_{r - 1}} and define a sequence of functions ◊${f_i : L_i \times C_i \rightarrow L_{i + 1}}, where ◊${L_0 = \mathcal{K}} and ◊${L_r = L}. Then ◊${f} involves applying each ◊${f_i} in succession, passing each category label:

◊align-math{
    f(k, (c_0, c_1, \ldots, c_{r-1})) &= f_{r-1}(\ldots f_1(f_0(k, c_0), c_1), \ldots, c_{r-1})
}

This structure allows us to compute ◊${f^\leftarrow(\{s\})} by inverting each step of the inflection process in reverse order:

◊align-math{
    S_n &= \{(s, \emptyset)\} \\
    S_{i - 1} &= \bigcup_{(s, c) \in S_i} \{(s', (c', c)) \mid (s', c') \in f_{i-1}^\leftarrow(\{s\})\} \\
    f^\leftarrow(\{s\}) &= S_0
}

Additionally, many parts of speech can be categorized into multiple inflectional classes. In mathematical terms, ◊${\mathcal{K}} can be partitioned into sets ◊${P^0, P^1, \ldots, P^{\pi - 1}}, each of which has a function ◊${f^{j.} : P^j \times C \rightarrow L = f\vert_{P^j \times C}} that is ‘simpler’ to implement than ◊${f} itself. Hence, ◊${f^\leftarrow(\{s\})} can be computed by attempting to match against each of the subsets ◊${P^j}:

◊align-math{
    f^\leftarrow(\{s\}) &= \bigcup_{j = 0}^{\pi - 1} (f^{j.})^\leftarrow(\{s\})
}

Alternatively, we can partition the set ◊${C} into sets ◊${C^0, C^1, \ldots C^{\gamma - 1}}, with corresponding functions ◊${f^{.k} : \mathcal{K} \times C^k \rightarrow L = f\vert_{\mathcal{K} \times C^k}}, such that

◊align-math{
    f^\leftarrow(\{s\}) &= \bigcup_{k = 0}^{\gamma - 1} (f^{.k})^\leftarrow(\{s\})
}

Importantly, each of these decompositions produce a set of tuples ◊${(\mathcal{K}', C', L', f')} which can be analyzed as a subproblem of the original problem and treated in the same way.

As an example, consider a language that has five classes for nominal inflections, which can be categorized into two broad groups, X and Y. This language also has cases in three groups:

◊items{
    ◊item{Group A cases are the most commonly used cases and have distinct declensions per class.}
    ◊item{Group B cases are less commonly used than group A cases and are declined differently between X classes and Y classes, but the declensions are the same within each group of classes.}
    ◊item{Group C cases are the least commonly used and are declined in the same way across all nouns. Additionally, while group A and B cases are coexponential with number, group C cases are monoexponential in that nouns in group C cases have a case ending followed by a number ending.}
}

Then we could decompose the problem ◊${(\mathcal{K}, C, L, f)} first by partitioning ◊${C} into ◊${C^A \cup C^B \cup C^C}. Subsequently, ◊${(\mathcal{K}, C^A, L, f^{.A})} is decomposed by partitioning ◊${\mathcal{K}} into ◊${P^0 \cup \ldots \cup P^4}, while ◊${(\mathcal{K}, C^B, L, f^{.B})} is decomposed by partitioning the same set as ◊${P^X \cup P^Y}.

In contrast, ◊${(\mathcal{K}, C^C, L, f^{.C})} is decomposed by composition into ◊${(\mathcal{K}, C^C_0, L_1, f_0^{.C})} (adding the case affix) and ◊${(L_1, C^C_1, L, f_1^{.C})} (adding the number affix). We now have the following simpler problems:

◊gather-math{
    (P^0, C^A, L, f^{0.A}) \\
    (P^1, C^A, L, f^{1.A}) \\
    (P^2, C^A, L, f^{2.A}) \\
    (P^3, C^A, L, f^{3.A}) \\
    (P^4, C^A, L, f^{4.A}) \\
    (P^X, C^B, L, f^{X.B}) \\
    (P^Y, C^B, L, f^{Y.B}) \\
    (\mathcal{K}, C^C_0, L_1, f_0^{.C}) \\
    (L_1, C^C_1, L, f_1^{.C})
}

This compositionality was the reason that we did not choose to define the set of possible categories ◊${C(k)} as dependent on the lexical entry. If we did so, then we could partition ◊${\mathcal{K}} into equivalence classes on ◊${C(k)} to yield subproblems that have a fixed set of category labels.
