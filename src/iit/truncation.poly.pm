#lang pollen

◊define-meta[title]{Truncation}

In many cases, inflection involves the addition of prefixes or suffixes to a stem. It follows that inverting this process would involve the removal of these affixes. However, concatenation often results in phonological changes to one or both of the stem and the affix. For example, ◊trans{I meet you} in Ŋarâþ Crîþ would be derived as ◊i{◊nc{cenv-a-ve}}, but the co-occurrence of ◊i{◊nc{v}} in the resulting word is considered undesirable (◊i{◊nc{oginiþe cfarðerþ}}) and is resolved by replacing the first ◊i{◊nc{v}} with an ◊i{◊nc{n}}. Thus, the resulting word is not **◊i{◊nc{cenvave}} but rather ◊i{◊nc{cennave}}.

Let ◊${\Sigma} be an alphabet and ◊${f: \Sigma^* \times \Sigma^* \rightarrow \Sigma^* \times \Sigma^*} be a function describing concatenation rules transforming pairs of strings ◊${(a, b)}. Generally, ◊${f} can be seen as the composition of some number of individual ‘rules’ ◊${f_0, f_1, \ldots, f_{n - 1} : \Sigma^* \times \Sigma^* \rightarrow \Sigma^* \times \Sigma^*}.

Then let ◊${a \stackrel{f}{\sim} b} be the concatenation of ◊${a} and ◊${b} under ◊${f}, or ◊term{◊${f}-concatenation}, defined as

◊align-math{a \stackrel{f}{\sim} b = a' \cdot b' \text{ where } (a', b') = f(a, b)}

Given ◊${b, w \in \Sigma^*}, we wish to find all ◊${a \in \Sigma^*} such that ◊${a \stackrel{f}{\sim} b = w}. If we can compute the preimage of a set over ◊${f}, then this is simply

◊align-math{
    A = \bigcup_{i = 0}^{|w|} \{ \alpha \mid (\alpha', \beta') \in f^\leftarrow(\{(w[0..i], w[i..|w|])\}) \land \beta' = b  \}
}

That is, we do the following:

◊items{
    ◊item{Split ◊${w} into ◊${(\alpha, \beta)} in all possible ways.}
    ◊item{Find all ◊${(\alpha', \beta')} such that ◊${f(\alpha', \beta') = (\alpha, \beta)}.}
    ◊item{Collect all such ◊${\alpha'} where ◊${\beta' = b}.}
}

An analogous problem and its solution can be stated for removing prefixes.

This algorithm is quite general, and certain properties of ◊${f} admit simpler algorithms. (The solution is trivial if ◊${f} is the identity function.)

If for all ◊${a, b} and ◊${(a', b') = f(a, b)}, the strings ◊${b} and ◊${b'} have the same length, then ◊${f} is considered to be ◊term{right-isometric}. In this case, we do not have to try all combinations of ◊${(\alpha, \beta)} but rather only the one in which ◊${|\beta| = |b|}.

More generally, let

◊align-math{
    D_f(b) = \{ |b'| \mid a \in \Sigma^*, (a', b') = f(a, b) \}
}

be the set of possible lengths that ◊${f} could transform ◊${b} to become. Then we only have to try combinations of ◊${(\alpha, \beta)} where ◊${|\beta| \in D_f(b)}.

A stronger property that right isometry is ◊term{right invariance}, which requires ◊${b = b'}; in other words, ◊${f} is not allowed to change the second string. If ◊${f} is right-invariant, then we can return an empty set if ◊${b} is not a suffix of ◊${w}.

Additionally, concatenation rules in human languages rarely change segments far away from the junction. For ◊${a, b \in \Sigma^*} and ◊${(a', b') = f(a, b)}, let ◊${s} be the longest common suffix of ◊${b} and ◊${b'}, leaving ◊${r} and ◊${r'} before it. Then the ◊term{forward dextral radius of influence} of ◊${(a, b)} with respect to ◊${f} is ◊${R_d(f; a, b) = |r|} and the ◊term{backward dextral radius of influence} is ◊${R_d'(f; a, b) = |r'|}. We can also define these radii for the function itself:

◊align-math{
    R_d(f) &= \sup_{a, b \in \Sigma^*} R_d(f; a, b) \\
    R_d'(f) &= \sup_{a, b \in \Sigma^*} R_d'(f; a, b)
}

Note that for right-isometric ◊${f}, ◊${R_d(f) = R_d'(f)}.

Given knowledge of these values, suppose that for a given ◊${(\alpha, \beta)}, we compare ◊${b} and ◊${\beta}, aligning them at their ends. Then if ◊${b} and ◊${\beta} differ in the characters at indices ◊${|b| - i} and ◊${|\beta| - i}, respectively, and if ◊${|b| - i < R_d(f)} or ◊${|\beta| - i < R_d'(f)}, then we can conclude that ◊${f^\leftarrow(\alpha, \beta)} does not contain any pairs ◊${(\alpha', \beta')} such that ◊${\beta' = b} and can thus discard the pair ◊${(\alpha, \beta)}.

Unfortunately, Ŋarâþ Crîþ v9e’s concatenation rules have a dextral radius of influence of 5, while many of its affixes are shorter than 5 assemblage units long. For that reason, radius-of-influence simplifications are unlikely to be useful for Ŋarâþ Crîþ.

◊section[#:id "example-concat-rules"]{Examples of concatenation rules}

The identity function is right-invariant and has a dextral radius of influence of 0.

Let ◊${m} and ◊${n} be nonnegative integers and ◊${g \in \Sigma^m \times \Sigma^n \rightarrow \Sigma^* \times \Sigma^*}. Then denote by ◊${\text{peephole}_{m, n}(g)} the function ◊${f} such that

◊align-math{
    f(a, b) &= \begin{cases}
        (a[..|a|-m] \cdot a', b' \cdot b[n..]) \os{\(|a| \ge m, |b| \ge n, (a', b') = g(a[|a|-m..], b[..n])\)} \\
        (a, b) \os{\(|a| < m\) or \(|b| < n\)} \\
    \end{cases}
}

In effect, ◊${\text{peephole}_{m, n}(g)} is a function that affects only the ◊${m + n} characters around the juncture.

Then the following statements are true:

◊align-math{
    D_f(b) &= \{ l + (|b| - n) \mid l \in D_g(b[..n]) \} \text{ if \(|b| \ge n\)} \\
    R_d(f) &= n \\
    R_d'(f) &= \max_{a, b} |b'| \text{ where \((a', b') = g(a, b)\)}
}

In particular, ◊${f} is right-isometric if ◊${g} is.

Often, we wish to replace one substring with another if a juncture occurs anywhere within the substring. For instance, the rule replacing ‘cat’ with ‘dog’ can be expressed as the composition of two functions ◊${\text{peephole}_{2, 1}(g_2) \circ \text{peephole}_{1, 2}(g_1)} where

◊align-math{
    g_1(a, b) &= \begin{cases}
        (\text{d}, \text{og}) \os{\(a = \text{c}, b = \text{at}\)} \\
        (a, b) \osamgen \\
    \end{cases} \\
    g_2(a, b) &= \begin{cases}
        (\text{do}, \text{g}) \os{\(a = \text{ca}, b = \text{t}\)} \\
        (a, b) \osamgen \\
    \end{cases}
}

Suppose that we have a function ◊${h \in \Sigma^k \rightarrow \Sigma^k}. If ◊${f = \text{subst}_k(h)} is such a substitution function, then it can be expressed as a composition of peephole functions ◊${f_{n - 1} \circ \cdots \circ f_1} where

◊align-math{
    f_i &= \text{peephole}_{i, k - i}(g_i) \\
    g_i(a, b) &= (s'[..i], s'[i..]) \text{ where \(s' = h(a \cdot b)\)}
}

◊${\text{subst}_k(h)} is right-isometric and has a dextral radius of influence of ◊${k - 1}.

We have assumed that the function ◊${h} preserves the length of the substring. We can generalize ◊${\text{subst}} to account for functions that change the length of the input, but we must be careful about where the new juncture is placed.

◊section[#:id "generalizations"]{Generalizations}

In practice, we often want to check a word ◊${w} against multiple suffixes from a fixed set ◊${B \subseteq \Sigma^*}. This problem can be solved similarly to the single-suffix case by matching any of ◊${(\alpha', \beta') \in f^\leftarrow(\ldots)} where ◊${\beta \in B}. This problem might be simplifiable depending on the properties of ◊${f}. If ◊${f} is right-invariant, for instance, then it is possible to use a trie containing the reversed elements of ◊${B}.

Often, we do not want to find all elements of ◊${A} but rather its intersection with a ‘dictionary set’ ◊${K_b}.

◊subsection[#:id "regular-systems"]{Concatenation on regular systems}

The notion of truncation can be generalized to be over any pair of formal languages. In this case, the concatenation rules function may have a different codomain from its domain. Usually, we are interested in languages of a regular system ◊${\Lambda} in which the end state of the first string is equal to the start state of the second string. We define an ◊term{extended concatenative system} ◊${\Phi} over ◊${\Lambda} as a pair of functions ◊${(\kappa, \phi)} where:

◊items{
    ◊item{◊${\kappa : Q \times Q \times Q \rightarrow Q} is a function that takes in three states ◊${(p, q, r)} and outputs a new middle state ◊${q'}, and}
    ◊item{◊${\phi : \bigcup_{p, q, r \in Q} \left[ \{(p, q, r)\} \rightarrow \Lambda(p, q) \times \Lambda(q, r) \rightarrow \Lambda(p, \kappa(p, q, r)) \times \Lambda(\kappa(p, q, r), r) \right]} returns for every triple of states ◊${(p, q, r)} a concatenation rule function for two strings, possibly changing the middle state according to ◊${\kappa}.}
}

For ◊${a \in \Lambda(p, q)} and ◊${b \in \Lambda(q, r)}, the extended concatenation ◊${a \underset{p, q, r}{\stackrel{\Phi}{\sim}} b = a \stackrel{\phi(p, q, r)}{\sim} b} is an element of ◊${\Lambda(p, r)}. If no ambiguity would arise, we omit the state names from the operator and simply write ◊${a \stackrel{\Phi}{\sim} b}.

◊subsection[#:id "n-ary"]{◊${N}-ary concatenation}

We have looked at binary concatenation; concatenation of more than two operands is often assumed to be left associative. In other words, concatenating ◊${(a, b, c)} concatenates ◊${a} and ◊${b} first, then the result of that to ◊${c}.

Another possibility for concatenating multiple morphemes is to apply the juncture rules after all of the concatenations. This means that in this example, any juncture rules applied between ◊${a} and ◊${b} would have access to the contents of ◊${c}. This is more complex than repeated binary concatenation but has the advantage of being able to use word-global information (such as stress or syllable position within a word).

The concept of ◊${N}-ary concatenation itself does not specify the order in which juncture rules are applied to each juncture. For instance, if the juncture rules consist of three subprocesses A, B, and C, then given a word with three junctures labeled 1, 2, and 3 from start to end, then the rules could trigger in any of the following orders (among other possibilities):

◊items{
    ◊item{Apply the subprocesses in sequence to each juncture from start to end: A1, B1, C1, A2, B2, C2, A3, B3, C3}
    ◊item{Apply the subprocesses in sequence to each juncture ◊em{from end to start}: A3, B3, C3, A2, B2, C2, A1, B1, C1}
    ◊item{Apply each subprocess to all junctures from start to end: A1, A2, A3, B1, B2, B3, C1, C2, C3}
    ◊item{Apply subprocesses A and C from start to end but B from end to start: A1, A2, A3, B3, B2, B1, C1, C2, C3}
    ◊item{Apply subprocess A to all junctures, then B and C to each juncture in sequence: A1, A2, A3, B1, C1, B2, C2, B3, C3}
}

In exchange for this flexibility, ◊${N}-ary concatenation has the disadvantage that truncation requires searching a larger space for possible juncture placements.

◊section[#:id "case-study"]{Case study: Ŋarâþ Crîþ v9e}

According to the Ŋarâþ Crîþ v9e grammar, concatenation consists of the following processes applied across the juncture:

◊enum{
    ◊item{Any new instances of ◊l0{j} before ◊l0{i}, ◊l0{î}, or ◊l0{u} are elided.}
    ◊item{Deduplication rules are applied.}
    ◊item{Newly formed bridges are canonicalized and repaired.}
}

We assume that we are working with assemblages in a regular system. Therefore, while a substitution function can be regarded as a composition of multiple functions ◊${f_{n - 1} \circ \cdots \circ f_1}, most of these functions will have no effect on concatenation at a given juncture state.

The first process, ◊i{glide elision}, is a right-invariant substitution function based on

◊align-math{
    h(((\mu, g, o), (\nu, o, n))) &= \begin{cases}
        ((\epsilon, g, o), (\nu, o, n)) \os{\(\nu \in \{\text{i}, \text{î}, \text{u}\}\)} \\
        ((\mu, g, o), (\nu, o, n)) \osamgen \\
    \end{cases}
}

whose preimage is straightforward to compute.

The ◊i{deduplication} rules, which resolve instances of ◊i{oginiþe cfarðerþ}, work as follows:

◊enum{
    ◊item{The onset ◊l0{f} or ◊l0{tf} followed by a non-hatted vowel then ◊l0{f} or ◊l0{p·} is replaced with ◊l0{t}.}
    ◊item{The onset ◊l0{þ} or ◊l0{cþ} followed by a non-hatted vowel then ◊l0{þ} or ◊l0{t·} is replaced with ◊l0{t}. In addition, a preceding ◊l0{þ} or ◊l0{cþ} coda is replaced with ◊l0{s}, and a preceding ◊l0{rþ} coda is replaced with ◊l0{r}.}
    ◊item{◊l0{h} followed by a non-hatted vowel then ◊l0{h} or ◊l0{c·} is replaced with ◊l0{p}.}
    ◊item{◊l0{v} followed by a non-hatted vowel then ◊l0{v} or ◊l0{m·} is replaced with ◊l0{n}.}
    ◊item{◊l0{ð} followed by a non-hatted vowel then ◊l0{ð} or ◊l0{d·} is replaced with ◊l0{ŋ}.}
    ◊item{◊l0{ħ} followed by a non-hatted vowel then ◊l0{ħ} or ◊l0{g·} is replaced with ◊l0{g}.}
}

In f9i, these rules are implemented twice: once for the case when the vowel in question is followed by a nonterminal coda (thus capturing the following initial), and once for the case when it is followed by a terminal coda (in which case only rules #1 and #2 are applicable). In both cases, the preceding coda is captured if available.

This process is more involved than glide elision, but its preimage is not too difficult to compute, and the process is right-isometric.

The assemblage form makes these changes difficult: the first consonant following a vowel might belong to the coda of the same syllable or to the initial of the following syllable, and it may be part of a complex coda or initial. It also complicates situations in which changing a letter requires the word to be syllabified differently; for this problem, v9e simply chooses to apply bridge resolution after deduplication, although this has the disadvantage of failing to remove some cases of ◊i{oginiþe cfarðerþ}.

Ŋarâþ Crîþ v9e’s deduplication rules are quite crude, prompting ad-hoc workarounds to be made in specific instances of inflection. There are plans in Project Shiva to expand the range of ◊i{oginiþe cfarðerþ} and thus the scope of deduplication, as well as a desire to avoid changing the initial consonant of a word. An even more challenging problem is propagation: if deduplication changes a consonant such that a new instance of ◊i{oginiþe cfarðerþ} arises, then additional invocations of deduplication might be required to resolve it.

The final step of concatenation is ◊i{bridge resolution}, which modifies awkward coda–initial pairs to more convenient ones. This process is also used for canonicalizing these pairs according to the maximal onset principle.

Ŋarâþ Crîþ v9e’s version of this process is complicated by the fact that although v7 allowed ◊i{◊nc{ŋ}} as a coda, v9 does not. When ◊i{◊nc{ŋ}} appeared as a coda, it was changed into ◊i{◊nc{r}}, modifying the preceding vowel. ◊i{◊nc{-aŋ}}, ◊i{◊nc{-oŋ}}, and ◊i{◊nc{-uŋ}} were changed to ◊i{◊nc{-or}}, and ◊i{◊nc{-eŋ}} and ◊i{◊nc{-iŋ}} were changed to ◊i{◊nc{-jor}}. Reflecting this change, bridge resolution first outputs either a true coda or a pseudo-coda of ◊i{◊nc{ŋ}}, subsequently resolving the latter case by applying this change to the vowel. That is, a final of ◊i{◊nc{-or}} might have arisen from one of ◊i{◊nc{-or}}, ◊i{◊nc{-aŋ}}, ◊i{◊nc{-oŋ}}, or ◊i{◊nc{-uŋ}}. Likewise, a final of ◊i{◊nc{-jor}} might have arisen from one of ◊i{◊nc{-jor}}, ◊i{◊nc{-jaŋ}}, ◊i{◊nc{-jeŋ}}, ◊i{◊nc{-joŋ}}, ◊i{◊nc{-eŋ}}, or ◊i{◊nc{-iŋ}}.

Since the number of possible bridges is relatively small, tabulation can be used to implement the preimage of the first step.

Because bridge resolution is right-isometric, Ŋarâþ Crîþ v9e’s concatenation rules as a whole are as well.
