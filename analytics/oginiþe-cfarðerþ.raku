use v6;

constant @CONSONANTS = <c n ŋ v s þ š r l ł m f g p t č d ð h ħ>;
constant @VOWELS = <a e i o u â ê î ô>;
constant $CFARÐERÞ = / (@CONSONANTS) (r?) (@VOWELS) (r?) $0 /;
constant @CFTYPES = <cvc vcvc cvcv crvc cvrc>;

sub MAIN(Str $fname) {
    my $oc-freqs = BagHash.new;
    for $fname.IO.lines -> $line {
    for $line.match($CFARÐERÞ, :overlap) -> $match {
        my $c = ~$match[0];
        my $v = ~$match[2];
        my $r1 = ~$match[1];
        my $r2 = ~$match[3];
        next if $r1 && $r2;
        if $r1 {
            $oc-freqs{$c ~ 'crvc'}++;
        } elsif $r2 {
            $oc-freqs{$c ~ 'cvrc'}++;
        } else {
            my $vcvc = $match.prematch.ends-with($v);
            my $cvcv = $match.postmatch.starts-with($v);
            $oc-freqs{$c ~ 'cvc'}++;
            $oc-freqs{$c ~ 'vcvc'}++ if $vcvc;
            $oc-freqs{$c ~ 'cvcv'}++ if $cvcv;
        }
    }
    }
    say "iaj zav; xbsio la, n v km l s zioa";
    print "  ";
    for @CFTYPES -> $t {
        printf "%8s", $t;
    }
    say "";
    for @CONSONANTS -> $c {
        print $c, " ";
        for @CFTYPES -> $t {
            my $count = $oc-freqs{$c ~ $t};
            printf "%8s", $count || "-";
        }
        say "";
    }
#   say "Initial onsets:";
#   show-top $init-onset-freqs;
#   say "\nFinal codas:";
#   show-top $final-coda-freqs;
#   say "\nBridges:";
#   show-top $bridge-freqs;
}
