use v6;

constant @VOWELS = <a e i o u â ê î ô>;

sub show-top(Baggy $b) {
  for $b.pairs.sort(-*.value) -> $p {
    say "{$p.key} {$p.value}";
  }
}

sub MAIN(Str $fname) {
  my @lines = $fname.IO.lines;
  my $bridge-freqs = BagHash.new;
  my $init-onset-freqs = BagHash.new;
  my $final-coda-freqs = BagHash.new;
  for @lines[1..*] -> $line {
    if $line ~~ /^ '#' \s* (\w+) \s* ':' / {
      my $word = ~$0;
      my @PARTS = $word.split(@VOWELS);
      $init-onset-freqs{@PARTS[0]}++;
      $final-coda-freqs{@PARTS[*-1]}++;
      for @PARTS[1 ..^ *-1] -> $bridge {
        $bridge-freqs{$bridge}++;
      }
    }
  }
  say "Initial onsets:";
  show-top $init-onset-freqs;
  say "\nFinal codas:";
  show-top $final-coda-freqs;
  say "\nBridges:";
  show-top $bridge-freqs;
}
